//  Copyright (C) 2009 Tim Blechmann
//
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)
//[spsc_queue_example
#include <boost/thread/thread.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <iostream>
#include <time.h>

#include <boost/atomic.hpp>

boost::lockfree::spsc_queue<long, boost::lockfree::capacity<1024> > spsc_queue;
const int iterations = 1000;

#define CPP_CHRONO
//#define C_TIME



std::chrono::time_point<std::chrono::system_clock> time0, time1;

long get_time()
{
#ifdef CPP_CHRONO
  time1 = std::chrono::system_clock::now();
  return std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
#endif
#ifdef C_TIME
  struct timespec ts;
  timespec_get(&ts, TIME_UTC);
  return long(ts.tv_sec) * long(1000*1000*1000) + long(ts.tv_nsec);
#endif
}


void producer(void)
{
  long duration_send;
  for (int i = 0; i != iterations; ++i) {
    duration_send = get_time();
    while (!spsc_queue.push(duration_send))
      ;
  }
}

boost::atomic<bool> done (false);

void consumer(void)
{
  long sum_delta = 0;
  long max_delta = 0;
  long min_delta = std::numeric_limits<long>::max();
  //
  long duration_send, duration_recv, delta;
  while (!done) {
    while (spsc_queue.pop(duration_send)) {
      duration_recv = get_time();
      delta = duration_recv - duration_send;
      //
      if (delta > max_delta)
        max_delta = delta;
      if (delta < min_delta)
        min_delta = delta;
      sum_delta += delta;
    }
  }

  while (spsc_queue.pop(duration_send)) {
    time1 = std::chrono::system_clock::now();
    duration_recv = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
    delta = duration_recv - duration_send;
    //
    if (delta > max_delta)
      max_delta = delta;
    if (delta < min_delta)
      min_delta = delta;
    sum_delta += delta;
  }
  //
  std::cout << "max_delta=" << max_delta << " min_delta=" << min_delta << " avg_delta=" << (sum_delta / iterations) << "\n";
}

int main(int argc, char* argv[])
{

  boost::thread producer_thread(producer);
  boost::thread consumer_thread(consumer);

  producer_thread.join();
  done = true;
  consumer_thread.join();
}
