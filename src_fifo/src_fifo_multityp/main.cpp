#include <iostream>
#include <thread>
#include <chrono>
#include <vector>

/*
  Your task is to design the data structure of a shared memory
  FIFO enabling fast communication between two threads: a producer
  and a consumer. This data structure must ensure that the two
  threads communicate correctly, while at the same time being
  non-blocking--so mutexes are not allowed!
*/
#include "fifo.hpp"

/*
  messages.hpp contains definitions of two message types that
  we would like our FIFO to support, but you can craft other 
  message type definitions as well: the FIFO should support any
  number of different C++ types, though not necessarily any C++
  type. If your FIFO implementation imposes any requirement on 
  the message type classes it supports, please document it.
*/
#include "messages.hpp"


//#define RUNTIME_DEBUG






int main()
{
  Fifo<Book_Trade, Book_Delta> f;
  using Ttime = std::chrono::time_point<std::chrono::system_clock>;

  size_t n_iter = 100;
  std::vector<Ttime> ListTimeSent(n_iter), ListTimeRecv(n_iter);

  size_t pos_recv=0;
  size_t n_trade_recv=0;
  size_t n_delta_recv=0;
  auto receive_and_time=[&](auto && arg) -> void {
    ListTimeRecv[pos_recv++] = std::chrono::system_clock::now();
    using T = std::decay_t<decltype(arg)>;
    if constexpr (std::is_same_v<T, Book_Trade>) {
#ifdef RUNTIME_DEBUG
      std::cout << "Receive Trade\n";
#endif
      n_trade_recv++;
    }
    if constexpr (std::is_same_v<T, Book_Delta>) {
#ifdef RUNTIME_DEBUG
      std::cout << "Receive Delta\n";
#endif
      n_delta_recv++;
    }
  };


  Book_Trade etrade;
  Book_Delta edelta;

  f.done = false;
  size_t n_trade_send=0;
  size_t n_delta_send=0;
  auto Producer=[&]() -> void {
    for (size_t i=0; i<n_iter; i++) {
      ListTimeSent[i] = std::chrono::system_clock::now();
      if (std::rand() % 2) {
#ifdef RUNTIME_DEBUG
        std::cout << "Sending Trade\n";
#endif
        f.push(etrade);
        n_trade_send++;
      } else {
#ifdef RUNTIME_DEBUG
        std::cout << "Sending Delta\n";
#endif
        f.push(edelta);
        n_delta_send++;
      }
    }
    f.done = true;
  };

  auto Consumer=[&]() -> void {
    f.process(receive_and_time);
  };

  std::cerr << "main, step 5\n";
  std::vector<std::thread> ListThr;
  ListThr.push_back(std::thread(Producer));
  ListThr.push_back(std::thread(Consumer));
  std::cerr << "main, step 6\n";

  ListThr[0].join();
  ListThr[1].join();

  std::cerr << "main, step 7\n";

  // Now reading and processing the timings

  size_t sum_delta = 0;
  size_t max_delta = 0;
  size_t min_delta = std::numeric_limits<int>::max();
  for (size_t i=0; i<n_iter; i++) {
    auto time1 = ListTimeSent[i];
    auto time2 = ListTimeRecv[i];
    size_t edelta = std::chrono::duration_cast<std::chrono::nanoseconds>(time2 - time1).count();
    //    std::cerr << "i=" << i << " edelta=" << edelta << "\n";
    if (edelta > max_delta)
      max_delta = edelta;
    if (edelta < min_delta)
      min_delta = edelta;
    sum_delta += edelta;
  }
  std::cout << "max_delta=" << max_delta << " min_delta=" << min_delta << " avg_delta=" << (sum_delta / n_iter) << "\n";
  std::cerr << "SEND (trade / delta)=" << n_trade_send << " / " << n_delta_send << "\n";
  std::cerr << "RECV (trade / delta)=" << n_trade_recv << " / " << n_delta_recv << "\n";
}
