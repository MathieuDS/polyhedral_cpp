#ifndef INCLUDE_FIFO_HPP
#define INCLUDE_FIFO_HPP

#include <variant>

#define USE_ANTHONY_WILLIAMS
//#define USE_BOOST
//#define USE_LADAN_SHARIT


#ifdef USE_ANTHONY_WILLIAMS
#include "fifo_anthony_williams.hpp"
#endif

#ifdef USE_BOOST
#include "fifo_boost.hpp"
#endif

#ifdef USE_LADAN_SHARIT
// That one is actually buggy.
#include "fifo_LadanMozes_Shavit.hpp"
#endif





template <typename... Msg_types>
class Fifo {
private:
  using Tvar = std::variant<Msg_types...>;
  lock_free_queue<Tvar> q;

public:
  std::atomic<bool> done;
  template<typename T>
  void push(T t)
  {
    Tvar t_var = t;
    q.push(t_var);
  }

  template<typename F>
  void process(F f)
  {
    while (true) {
      std::optional<Tvar> eval = q.pop();
      if (eval) {
        std::visit(f, *eval);
      }
      if (done)
        break;
    }
    // Exiting, now finishing the queue
    while(true) {
      std::optional<Tvar> eval = q.pop();
      if (eval) {
        std::visit(f, *eval);
      } else {
        break;
      }
    }
  }

};



#endif
