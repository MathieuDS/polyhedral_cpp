#ifndef INCLUDE_ANTHONY_WILLIAMS_HPP
#define INCLUDE_ANTHONY_WILLIAMS_HPP

// It is based on the code of listing 7.14 of Anthony Williams
// "C++ Concurrency in Action" (2nd Edition) Page 236.


#include <memory>
#include <atomic>
#include <optional>
#include <iostream>


template<typename T>
class lock_free_queue
{
private:
  struct node
  {
    std::shared_ptr<T> data;
    node* next;
    node(): next(nullptr)
    {
    }
  };
  std::atomic<node*> head;
  std::atomic<node*> tail;
  node* pop_head()
  {
    node* const old_head=head.load();
    if (old_head == tail.load()) {
      return nullptr;
    }
    head.store(old_head->next);
    return old_head;
  }
public:
  lock_free_queue(): head(new node), tail(head.load())
  {
    std::cerr << "is_lock_free=" << atomic_is_lock_free(&head) << "\n";
  }
  lock_free_queue(const lock_free_queue& other)=delete;
  lock_free_queue& operator=(const lock_free_queue& other)=delete;
  ~lock_free_queue()
  {
    while (node* const old_head=head.load()) {
      head.store(old_head->next);
      delete old_head;
    }
  }
  std::optional<T> pop()
  {
    node* old_head=pop_head();
    if (!old_head) {
      return std::optional<T>();
    }
    std::optional<T> const res(*(old_head->data));
    delete old_head;
    return res;
  }
  void push(T new_value)
  {
    std::shared_ptr<T> new_data(std::make_shared<T>(new_value));
    node* p = new node;
    node* const old_tail = tail.load();
    old_tail->data.swap(new_data);
    old_tail->next=p;
    tail.store(p);
  }
};


#endif
