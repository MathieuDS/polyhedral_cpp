# SharedMemoryCode
Implementation of shared memory code for passing data from one thread to another thread.
Two types are needed to be exchanged, that is Book_Trade and Book_Delta. But the design
should be able to handle any kind of structure.


Design choice:
---The use of multiple types is handled with std::variant that allow to consider different
types into one.
---For the input ("push" function) this is completely neutral. On the other hand for the reading
side, we have to expose some this implementation to the user with a "std::variant<Book_Trade, Book_Delta>"
showing up.
---For the Lock-Free FIFO data structures, 3 different implementations were considered:
   ---The code from Anthony Williams "C++ Concurrency in Action" (2nd Edition) Page 236.
      lock-freeness: It is atomic as this can be decided by functions.
   ---The boost code which implements Michael & Scott.
   ---Another code from Ladan-Mozes & Sharit. Unfortunately, some bugs were encountered in this
      implementation and so it was removed from consideration.
---The shared memory system used is a simple one: Two threads share memory and exchange via this
   shared memory. The boost-interprocess library was considered, but this turned out to be too complicated
   to use.
---Timings (latency) were implemented using std::chrono with a computation of timing during runtime and
   a computation at the end.


Allowed data types:
---The data considered needs to be copyable. Adequate data types to be supoported are Plain Old Data (POD)
   Also adequate are std::array


Other remarks:
---The "#pragma once" was removed since it is non-standard and replaced by standard include guards.
---The "Side / Event_type / Printable" were outcommented since they were not used and the "NA" statement
   created a compilation error.


