#ifndef INCLUDE_FIFO_BOOST_HPP
#define INCLUDE_FIFO_BOOST_HPP

// encapsulation of the boost code. It implements
// Michael, M. M. and Scott, M. L.,
// "simple, fast and practical non-blocking and blocking concurrent queue algorithms"

#include <boost/lockfree/queue.hpp>
#include <optional>


template<typename T>
struct lock_free_queue {
private:
  boost::lockfree::queue<T> q;
public:
  lock_free_queue() : q(1000)
  {
  }

  void push(T t)
  {
    (void)q.push(t);
  }

  std::optional<T> pop()
  {
    T elt;
    bool test = q.pop(elt);
    if (test)
      return std::optional<T>(elt);
    return std::optional<T>();
  }


};






#endif
