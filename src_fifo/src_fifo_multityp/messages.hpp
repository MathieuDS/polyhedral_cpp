#ifndef INCLUDE_MESSAGES_HPP
#define INCLUDE_MESSAGES_HPP

#include <array>

/*
enum Side : char {
  B = 'B', // Buy side
  S = 'S', // Sell side
  H = 'H', // "Hidden" Side
  NA = ' ' // N/A
};
*/

/*
enum Event_type : char {
  ADD = 'A',
  CANCEL = 'X',
  EXECUTE = 'E',
  NON_PRINTABLE_EXECUTE = 't',
  NA = '\000'
};

enum Printable : char {
  N = 'N',
  Y = 'Y'
};
*/


struct Book_Trade {
  char message_type;
  char side;
  char event_type;
  char venue;
  uint32_t sym;
  std::array<char, 8> ticker;
  uint64_t timestamp;
  int32_t bid;
  int32_t ask;
  char printable;
  int32_t price;
  uint32_t shares;
};

struct Book_Delta {
  char message_type;
  char side;
  char event_type;
  char venue;
  uint32_t sym;
  std::array<char, 8> ticker;
  uint64_t timestamp;
  int32_t bid;
  int32_t ask;
  int32_t price;
  int32_t delta_shares;
};


#endif
