#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/container/scoped_allocator.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <array>
#include <chrono>
#include <iostream>
#include <unistd.h>
#include <boost/interprocess/containers/vector.hpp>



namespace bip = boost::interprocess;
using Mem = bip::managed_mapped_file;

template <typename T = void>
using Alloc = boost::container::scoped_allocator_adaptor<bip::allocator<T, Mem::segment_manager> >;

template <typename T>
using Vector = boost::container::vector<T, Alloc<T> >;

int main()
{
  const size_t cache_line_size = 64;
  const size_t n_line = 700; // This leaves 16 bytes
  const size_t e_prod = cache_line_size * n_line;
  std::cout << "e_prod=" << e_prod << "\n";
  // create segment and corresponding allocator
  bip::managed_shared_memory segment(bip::open_or_create,"MySharedMemory", 65536);
  std::cout << "After creation of segment\n";
  std::cout << "allowed free_memory=" << segment.get_free_memory() << "\n";

  using MyType = Vector<char>;
  MyType* ptr = segment.find_or_construct<MyType>("SharedVect")(segment.get_segment_manager());


  //
  // Getting the pointer and doing conversion
  // Finding the pointer that is mapped to a cache line
  char* ptr_char_pre = ptr->data();
  std::uintptr_t ptr_addr = reinterpret_cast<std::uintptr_t>(ptr_char_pre);
  std::uintptr_t ptr_addr_res = ptr_addr % 64;
  std::uintptr_t ptr_addr_q = ptr_addr / 64;
  std::cout << "ptr_addr=" << ptr_addr << " ptr_addr_res=" << ptr_addr_res << " ptr_addr_q=" << ptr_addr_q << "\n";
  std::uintptr_t shift_ptr = 64 - ptr_addr_res;
  char* ptr_char = ptr_char_pre + shift_ptr;



  /*

  //
  // Initializing the field
  char echar ='\0';
  size_t e_prod_red = cache_line_size * (n_line-1);
  for (size_t i=0; i<e_prod_red; i++)
    memcpy(ptr_char, &echar, sizeof(char));
  std::cout << "After initialization to 0\n";


  // Reading the current time
  // Hopefully you do not work at midnight
  std::chrono::time_point<std::chrono::system_clock> time0;


  //
  // Reading the data
  std::array<char,64> V;
  int pos = 0;
  while(true) {
    if (uint8_t(V[64*pos + 63]) == 1) {
      long* ptr_long = (long*) ptr_char;
      std::chrono::time_point<std::chrono::system_clock> time1 = std::chrono::system_clock::now();
      long duration_recv = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
      long duration_send = *ptr_long;
      long delta_duration = duration_recv - duration_send;
      std::cerr << "delta_duration=" << delta_duration << "\n";

      ptr_char += 64;
      pos++;
    }
  }

  */

}
