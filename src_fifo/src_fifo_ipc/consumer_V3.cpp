#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/container/scoped_allocator.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <array>
#include <chrono>
#include <iostream>
#include <unistd.h>
#include <boost/interprocess/containers/vector.hpp>



namespace bip = boost::interprocess;

int main()
{
  const size_t cache_line_size = 64;
  const size_t n_line = 700; // This leaves 16 bytes
  const size_t e_prod = cache_line_size * n_line;
  std::cout << "e_prod=" << e_prod << "\n";
  // create segment and corresponding allocator
  bip::shared_memory_object shm(bip::open_or_create,"MySharedMemory", bip::read_write);
  std::cout << "After creation of shm\n";

  shm.truncate(65537);
  std::cout << "After truncate\n";

  bip::mapped_region region(shm, bip::read_write);
  std::cout << "After region\n";

  void* ptr=region.get_address();
  std::cout << "After ptr\n";
  std::cout << "|region|=" << region.get_size() << " ptr=" << ptr << "\n";
  std::memset(ptr, 0, region.get_size());


  //
  // Getting the pointer and doing conversion
  // Finding the pointer that is mapped to a cache line
  char* ptr_char_pre = (char*)ptr;
  std::uintptr_t ptr_addr = reinterpret_cast<std::uintptr_t>(ptr_char_pre);
  std::uintptr_t ptr_addr_res = ptr_addr % 64;
  std::uintptr_t ptr_addr_q = ptr_addr / 64;
  std::cout << "ptr_addr=" << ptr_addr << " ptr_addr_res=" << ptr_addr_res << " ptr_addr_q=" << ptr_addr_q << "\n";
  std::uintptr_t shift_ptr = 64 - ptr_addr_res;
  char* ptr_char = ptr_char_pre + shift_ptr;




  // Reading the current time
  // Hopefully you do not work at midnight
  std::chrono::time_point<std::chrono::system_clock> time0;

  //#define PRINT_STATEMENT
  //
  // Reading the data
  //  uint8_t* ptr_uint8 = (uint8_t*) ptr_char;
  int pos = 0;
#ifdef PRINT_STATEMENT
  long duration_send0 = 0;
  long duration_recv0 = 0;
#endif
  char* ptr_read = ptr_char;
  long sum_delta_duration = 0;
  long min_delta_duration = std::numeric_limits<long>::max();
  long max_delta_duration = 0;
  while(true) {
    if (((uint8_t*)ptr_read)[63] == 1) {
      long* ptr_long = (long*) ptr_read;
      std::chrono::time_point<std::chrono::system_clock> time1 = std::chrono::system_clock::now();
      long duration_recv = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
      long duration_send = *ptr_long;
      long delta_duration = duration_recv - duration_send;
      sum_delta_duration += delta_duration;
      if (delta_duration < min_delta_duration)
        min_delta_duration = delta_duration;
      if (delta_duration > max_delta_duration)
        max_delta_duration = delta_duration;
      ptr_read += 64;
      pos++;
#ifdef PRINT_STATEMENT
      std::cerr << "pos=" << pos << " delta_duration=" << delta_duration << " duration_send=" << duration_send << " duration_recv=" << duration_recv << "\n";
      if (duration_send0 == 0) {
        duration_send0 = duration_send;
        duration_recv0 = duration_recv;
      }
#endif
    }
    if (pos == 100)
      break;
  }
  double avg_delta_duration = double(sum_delta_duration) / pos;
  std::cout << "avg_delta_duration=" << avg_delta_duration << " min_delta=duration=" << min_delta_duration << " max_delta_duration=" << max_delta_duration << "\n";

}
