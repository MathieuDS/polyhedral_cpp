#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/container/scoped_allocator.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <array>
#include <chrono>
#include <iostream>
#include <unistd.h>
#include <boost/interprocess/containers/vector.hpp>
#include <thread>






namespace bip = boost::interprocess;

int main()
{
  const size_t cache_line_size = 64;
  const size_t n_line = 500; // This leaves 16 bytes
  const size_t e_prod = cache_line_size * n_line;
  std::cout << "e_prod=" << e_prod << "\n";
  // create segment and corresponding allocator
  bip::shared_memory_object shm(bip::open_only,"MySharedMemory", bip::read_write);
  std::cout << "After creation of shm\n";

  bip::mapped_region region(shm, bip::read_write);
  std::cout << "After region\n";

  void* ptr=region.get_address();
  std::cout << "After ptr\n";
  std::cout << "|region|=" << region.get_size() << " ptr=" << ptr << "\n";


  //
  // Getting the pointer and doing conversion
  // Finding the pointer that is mapped to a cache line
  char* ptr_char_pre = (char*)ptr;
  std::uintptr_t ptr_addr = reinterpret_cast<std::uintptr_t>(ptr_char_pre);
  std::uintptr_t ptr_addr_res = ptr_addr % 64;
  std::uintptr_t ptr_addr_q = ptr_addr / 64;
  std::cout << "ptr_addr=" << ptr_addr << " ptr_addr_res=" << ptr_addr_res << " ptr_addr_q=" << ptr_addr_q << "\n";
  std::uintptr_t shift_ptr = 64 - ptr_addr_res;
  char* ptr_char = ptr_char_pre + shift_ptr;



  // Reading the current time
  // Hopefully you do not work at midnight
  std::chrono::time_point<std::chrono::system_clock> time0;

  //#define PRINT_STATEMENT
  //
  // Writing the information to the shared memory
  size_t n_iter = n_line; // 4000;
  char* ptr_write = ptr_char;
  for (size_t i_iter=0; i_iter<n_iter; i_iter++) {
    long* ptr_write_long = (long*)ptr_write;
    uint8_t* ptr_write_i = (uint8_t*)ptr_write;
    std::chrono::time_point<std::chrono::system_clock> time1 = std::chrono::system_clock::now();
    long duration_send = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
#ifdef PRINT_STATEMENT
    std::cout << "i_iter=" << i_iter << " duration_send=" << duration_send << "\n";
#endif
    ptr_write_long[0] = duration_send;
    ptr_write_i[63] = 1; // Transmitting the cache_line
    //
    ptr_write += 64;
    //    std::chrono::seconds dura(2);
    //    std::this_thread::sleep_for( dura );
    //    usleep(1000000);
  }


}
