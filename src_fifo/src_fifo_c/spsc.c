#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include <limits.h>

#define QUEUE_SIZE 64

// Let's go the easy way and keep a gap between head and tail when full.
static volatile int queue_head = 1;
static long queue[QUEUE_SIZE];
static volatile int queue_tail = 0;

static void *producer(void *data_ptr);
static void *consumer(void *data_ptr);

const int iterations = 1000;



long get_time()
{
  struct timespec ts;
  timespec_get(&ts, TIME_UTC);
  long val1 = ts.tv_sec;
  long val2 = 1000*1000*1000;
  long val3 = ts.tv_nsec;
  long ret = val1 * val2 + val3;
  return ret;
}



int main()
{
  int* val;
  pthread_t threads[2];
  pthread_create(&threads[0], NULL, producer, val);
  pthread_create(&threads[1], NULL, consumer, val);

  for(int i = 0; i < 2; i++)
    pthread_join(threads[i], NULL);

  return 0;
}

static inline int advance(volatile int *idx)
{
  int old, new;
  do {
    old = *idx;
    new = (old + 1) % QUEUE_SIZE;
  } while(!__sync_bool_compare_and_swap(idx, old, new));
  return old;
}

static void* producer(void* val)
{
  for(int i = 0; i < iterations; i++) {
    while((queue_head + 1) % QUEUE_SIZE == queue_tail) {
    }
    queue[queue_head] = get_time();
    advance(&queue_head);

  }

  return NULL;
}

static void* consumer(void* val)
{
  long sum_delta = 0;
  long max_delta = 0;
  long min_delta = LONG_MAX;
  int idx;
  int nbcorr = 0;
  for(int i = 0; i < iterations; i++) {
    while(queue_tail == queue_head) {
    }

    long send_time = queue[idx];
    long recv_time = get_time();
    idx = advance(&queue_tail);
    long delta = recv_time - send_time;
    //
    if (delta < 100000) {
      if (delta > max_delta)
        max_delta = delta;
      if (delta < min_delta)
        min_delta = delta;
      sum_delta += delta;
      nbcorr++;
    }
  }
  long avg_delta = sum_delta / nbcorr;
  printf("max_delta=%ld   min_delta=%ld   avg_delta=%ld\n", max_delta, min_delta, avg_delta);
  return NULL;
}
