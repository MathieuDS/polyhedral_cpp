#include <unistd.h>
#include <iostream>

#include <boost/archive/tmpdir.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/split_free.hpp>




#include <boost/lockfree/spsc_queue.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/string.hpp>




struct pod_exch {
  int val_i;
  double val_d;
};


namespace boost { namespace serialization {

    template<class Archive>
    inline void serialize(Archive & ar,
                          pod_exch & x,
                          const unsigned int version)
    {
      ar & make_nvp("val_i", x.val_i);
      ar & make_nvp("val_d", x.val_d);
    }

} }



namespace bip = boost::interprocess;
namespace shm
{
  using char_alloc = bip::allocator<char, bip::managed_shared_memory::segment_manager>;
  using shared_string = bip::basic_string<char, std::char_traits<char>, char_alloc >;
  using ring_buffer = boost::lockfree::spsc_queue<shared_string,boost::lockfree::capacity<1024>>;
}





int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "The program is used as\n";
    std::cerr << "prod_cons_pod [prod|cons]\n";
    exit(1);
  }
  std::string opt = argv[1];
  if (opt != "prod" && opt != "cons") {
    std::cerr << "Wrong chosen option\n";
    std::cerr << "opt=" << opt << " but it should be prod or cons\n";
    exit(1);
  }

  // create segment and corresponding allocator


  auto get_segment=[&]() -> bip::managed_shared_memory {
    if (opt == "prod")
      return bip::managed_shared_memory(bip::create_only, "MySharedMemory_test", 65536);
    else
      return bip::managed_shared_memory(bip::open_only, "MySharedMemory_test");
  };

  bip::managed_shared_memory segment = get_segment();
  shm::char_alloc char_alloc(segment.get_segment_manager());

  shm::ring_buffer *queue = segment.find_or_construct<shm::ring_buffer>("queue")();

  if (opt == "prod") {
    for (int i=0; i<10; i++) {
      int val_i = 2*i + i*i;
      double val_d = double(i) / (1.0 + double(i));
      pod_exch pe{val_i, val_d};
      std::cerr << "producer i=" << i << " pe=" << pe.val_i << " / " << pe.val_d << "\n";
      // serialization
      std::ostringstream archive_stream;
      //      boost::archive::text_oarchive archive(archive_stream);
      boost::archive::text_oarchive archive(archive_stream, boost::archive::archive_flags::no_header);
      //      boost::archive::binary_oarchive archive(archive_stream, boost::archive::archive_flags::no_header);
      //boost::archive::binary_oarchive archive(archive_stream);
      archive << pe;
      shm::shared_string str_ss(archive_stream.str(), char_alloc);
      queue->push(str_ss);
    }
  }

  if (opt == "cons") {
    for (int i=0; i<10; i++) {
      shm::shared_string v(char_alloc);
      if (queue->pop(v)) {
        // deserialization
        pod_exch pe;
        std::stringstream iss(v.data());
        //        boost::archive::text_iarchive ia(iss);
        boost::archive::text_iarchive ia(iss, boost::archive::archive_flags::no_header);
        //        boost::archive::binary_iarchive ia(iss, boost::archive::archive_flags::no_header);
        //boost::archive::binary_iarchive ia(iss);
        ia >> pe;
        //
        std::cerr << "Received i=" << i << " pe=" << pe.val_i << " / " << pe.val_d << "\n";
      }

    }
  }
}
