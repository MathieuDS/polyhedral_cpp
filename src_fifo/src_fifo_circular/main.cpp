#include <iostream>
#include <thread>
#include <chrono>
#include <vector>


//#include "circularfifo_memory_relaxed_aquire_release.hpp"
#include "circularfifo_memory_sequential_consistent.hpp"



int main()
{
  std::chrono::time_point<std::chrono::system_clock> time0;
  using T = long;
  //  memory_relaxed_aquire_release::CircularFifo<long, 100> fifo;
  memory_sequential_consistent::CircularFifo<long, 10> fifo;
  int n_iter=10;
  T sum_delta = 0;
  T max_delta = 0;
  T min_delta = std::numeric_limits<T>::max();
  auto Consumer=[&]() -> void {
    int i_iter = 0;
    while(true) {
      T duration_send;
      bool test = fifo.pop(duration_send);
      if (test) {
        std::chrono::time_point<std::chrono::system_clock> time1 = std::chrono::system_clock::now();
        T duration_recv = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
        T edelta = duration_recv - duration_send;
        //
        if (edelta > max_delta)
          max_delta = edelta;
        if (edelta < min_delta)
          min_delta = edelta;
        sum_delta += edelta;
        //
        i_iter++;
      }
      if (i_iter == n_iter)
        break;
    }
  };
  auto Producer=[&]() -> void {
    for (size_t i=0; i<n_iter; i++) {
      std::chrono::time_point<std::chrono::system_clock> time1 = std::chrono::system_clock::now();
      T duration_send = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
      fifo.push(duration_send);
    }
  };
  std::vector<std::thread> ListThr;
  ListThr.push_back(std::thread(Producer));
  ListThr.push_back(std::thread(Consumer));
  ListThr[0].join();
  ListThr[1].join();

  std::cout << "max_delta=" << max_delta << " min_delta=" << min_delta << " avg_delta=" << (sum_delta / n_iter) << "\n";
}
