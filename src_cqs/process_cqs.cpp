#include <iostream>
#include <vector>




int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "process_cqs [FileIn]\n";
    exit(1);
  }
  std::string eFile = argv[1];

  std::FILE* fp;

  fp =  std::fopen(eFile.data(), "r+");
  size_t len = 1000;
  std::vector<uint8_t> ReadBuffer(len);
  uint8_t* ptr = ReadBuffer.data();

  // X86 is little endian.
  size_t ret = std::fread(ptr, sizeof(uint8_t), len, fp);
  if (ret != len) {
    std::cerr << "Error doing the reading of the file\n";
    exit(1);
  }
  std::cerr << "ret=" << ret << "\n";

  uint8_t version = ptr[0];
  std::cerr << "version=" << int(version) << "\n";
  ptr++;

  uint16_t* ptr_blk = (uint16_t*)ptr;
  uint16_t blk_size = *ptr_blk;
  std::cerr << "blk_size=" << blk_size << "\n";
  ptr += 2;

  char data_feed_indicator = *((char*)ptr);
  uint8_t data_feed_indicator_i = *ptr;
  std::cerr << "data_feed_indicator=" << data_feed_indicator << " data_feed_indicator_i=" << int(data_feed_indicator_i) << "\n";
  ptr++;

  uint32_t blk_seq_number = *((uint32_t*)ptr);
  std::cerr << "blk_seq_number=" << int(blk_seq_number) << "\n";
  ptr += 4;

  uint8_t msg_in_block = *ptr;
  std::cerr << "msg_in_block=" << int(msg_in_block) << "\n";
  ptr++;

}
