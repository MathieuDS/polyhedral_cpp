Cube:=ArchimedeanPolyhedra("Cube");

Cube11:=GoldbergConstruction(Cube,1,1);
Cube20:=GoldbergConstruction(Cube,2,0);


Dodec:=ArchimedeanPolyhedra("Dodecahedron");

Dodec11:=GoldbergConstruction(Dodec,1,1);

ExportGraphToPolyhedralCpp("GC10_Cube", PlanGraphToGRAPE(Cube));
ExportGraphToPolyhedralCpp("GC11_Cube", PlanGraphToGRAPE(Cube11));
ExportGraphToPolyhedralCpp("GC20_Cube", PlanGraphToGRAPE(Cube20));
ExportGraphToPolyhedralCpp("GC11_Dodecahedron", PlanGraphToGRAPE(Dodec11));

PlanGraphToCaGe("GC11_cube.grape", Cube11);

PlanGraphToCaGe("GC11_dodecahedron.grape", Dodec11);

GRA:=PlanGraphToGRAPE(Cube11);
nbV:=GRA.order;
LDist:=[];
for i in [1..nbV]
do
  eDist:=Distance(GRA, 1, i);
  Add(LDist, eDist);
od;

#ListEmbed:=S_Embedding(PlanGraphToGRAPE(Dodec11), 3);
