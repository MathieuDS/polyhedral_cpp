n:=4;
k:=2;
TheGramMat:=[
[2 ,  -1,  0,  0],
[-1, 3/2, -1,  0],
[0 ,  -1,  2, -1],
[0 ,   0, -1,  2]];

ProcEnum:=ProcEnum_SublatticeEnumeration();
TheReply:=RankinAllComputations(ProcEnum, k, TheGramMat);
TheCone:=TheReply.GetPerfectCone();
TheDet:=TheReply.TheDet;

ListOrb:=DualDescriptionStandard(TheCone.ListSymmVect, TheCone.GRPperm);
nbSpace:=Length(TheCone.ListSymmVect);
ListDirMat:=[];
for eOrb in ListOrb
do
  eVect:=__FindFacetInequality(TheCone.ListSymmVect, eOrb);
  eMat:=VectorToSymmetricMatrix(eVect, n);
  for i in [1..n]
  do
    eMat[i][i]:=2*eMat[i][i];
  od;
  ListIncd:=Filtered([1..nbSpace], x->Trace(TheCone.ListSymmMat[x]*eMat)=0);
  if ListIncd<>eOrb then
    Print("We have an error here\n");
    Print(NullMat(5));
  fi;
  Add(ListDirMat, eMat);
od;
FileSave:="ExampleA4_adj_ConfigEnum";
MaxDet:=2*TheDet;
Rankin_WriteToC_EnumConfig(FileSave, TheGramMat, k, MaxDet);

for iDir in [1..Length(ListDirMat)]
do
  FileSave:=Concatenation("ExampleA4_adj_FlippingOper", String(iDir));
  Rankin_WriteToC_Flipping(FileSave, TheGramMat, k, ListDirMat[iDir]);
od;
