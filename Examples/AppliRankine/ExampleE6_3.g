k:=3;
TheGramMat:=ClassicalSporadicLattices("E6");
n:=Length(TheGramMat);

ProcEnum:=ProcEnum_SublatticeEnumeration();
TheReply:=RankinAllComputations(ProcEnum, k, TheGramMat);
TheCone:=TheReply.GetPerfectCone();
TheDet:=TheReply.TheDet;

ListOrb:=DualDescriptionStandard(TheCone.ListSymmVect, TheCone.GRPperm);
nbSpace:=Length(TheCone.ListSymmVect);
ListDirMat:=[];
for eOrb in ListOrb
do
  eVect:=__FindFacetInequality(TheCone.ListSymmVect, eOrb);
  eMat:=VectorToSymmetricMatrix(eVect, n);
  for i in [1..n]
  do
    eMat[i][i]:=2*eMat[i][i];
  od;
  ListIncd:=Filtered([1..nbSpace], x->Trace(TheCone.ListSymmMat[x]*eMat)=0);
  if ListIncd<>eOrb then
    Print("We have an error here\n");
    Print(NullMat(5));
  fi;
  Add(ListDirMat, eMat);
od;


FileSave:="ExampleAn_ConfigEnum";
MaxDet:=2*TheDet;
Rankin_WriteToC_EnumConfig(FileSave, TheGramMat, k, MaxDet);

FileSave:="ExampleAn_FlippingOper";
Rankin_WriteToC_Flipping(FileSave, TheGramMat, k, ListDirMat[1]);
