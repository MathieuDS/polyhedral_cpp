#include "CODE_LCDcode.h"
int main(int argc, char *argv[])
{
  try {
    if (argc != 8) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "JointLCDcodeBound n k d_primal d_dual [outfile] [FullSyst]\n";
      std::cerr << "\n";
      std::cerr << "n          : The dimension of the whole space\n";
      std::cerr << "k          : The dimension of the code subspace\n";
      std::cerr << "d_primal   : The minimum distance of primal code\n";
      std::cerr << "d_dual     : The minimum distance of dual code\n";
      std::cerr << "method     : cdd or glpk\n";
      std::cerr << "[outfile]  : optionary argument for writing GAP readable output\n";
      std::cerr << "[FullSyst] : option for printing the full system out 0 /1\n";
      std::cerr << "It returns if those parameters are feasible for a LCD code\n";
      return -1;
    }
    //
    std::cerr << "Reading input\n";
    //
    int n;
    sscanf(argv[1], "%d", &n);
    //
    int k;
    sscanf(argv[2], "%d", &k);
    //
    int d_primal;
    sscanf(argv[3], "%d", &d_primal);
    //
    int d_dual;
    sscanf(argv[4], "%d", &d_dual);
    //
    std::string eMethod=argv[5];
    ChoiceLCD eChoice=GetFullChoice();
    bool FoundMeth=false;
    if (eMethod == "glpk") {
      FoundMeth=true;
      eChoice.GLPKtestFeasibility=true;
      eChoice.FullLinSpaceComputation=false;
    }
    if (eMethod == "cdd") {
      FoundMeth=true;
      eChoice.GLPKtestFeasibility=false;
      eChoice.FullLinSpaceComputation=true;
    }
    if (FoundMeth == false) {
      std::cerr << "Error in method. Allowed: cdd and glpk\n";
      throw TerminalException{1};
    }
    //
    std::string eFileOUT;
    bool DoOutput;
    eFileOUT=argv[6];
    DoOutput=true;
    //
    bool FullSystem;
    int eVal;
    sscanf(argv[7], "%d", &eVal);
    if (eVal == 0) {
      FullSystem=false;
    }
    else {
      FullSystem=true;
    }
    //
    TestFeasibilityLCDformulation<mpq_class>(n, k, d_primal, d_dual, eFileOUT, DoOutput, FullSystem, eChoice);
    std::cerr << "Completion of the program\n";
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
