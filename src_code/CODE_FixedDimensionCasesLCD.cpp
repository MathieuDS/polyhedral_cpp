#include "CODE_LCDcode.h"
int main(int argc, char *argv[])
{
  try {
    if (argc != 3) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "FixedDimensionCases [k] [outfile]\n";
      std::cerr << "\n";
      std::cerr << "k          : The dimension of the code subspace\n";
      std::cerr << "outfile    : The output file for the obtained orbits\n";
      return -1;
    }
    //
    std::cerr << "Reading input\n";
    //
    int k;
    (void)sscanf(argv[1], "%d", &k);
    //
    //  FixedKdata eFix=ComputeParityInformationLCD(k);
    //  FixedKdata eFixRed=ReduceByIsomorphismFixedDimCasesLCD(eFix);
    FixedKdata eFixRed=EnumerateListOrbitLCDenum(k);
    //
    std::ofstream os(argv[2]);
    int nbParity=eFixRed.ListParity.size();
    int len=MyPow<int>(2,k);
    os << " " << nbParity << " " << len << "\n";
    for (int iParity=0; iParity<nbParity; iParity++) {
      ulong eVal=eFixRed.ListParity[iParity];
      Face eFace=UnsignedLongToFace(len, eVal);
      for (int i=0; i<len; i++)
	os << " " << eFace[i];
      os << "\n";
    }
    std::cerr << "Completion of the program\n";
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
