#ifndef MULTIVAR_POLYNOMIAL_INCLUDE
#define MULTIVAR_POLYNOMIAL_INCLUDE



struct PolynomialSymbol {
  int Nvar;
  int Ndeg;
  std::vector<long> MultiToSingle;
  MyMatrix<int> SingleToMulti;
};




PolynomialSymbol GetPolynomialSymbol(int const& Nvar, int const& Ndeg, bool const& IsHomo)
{
  std::cerr << "Nvar=" << Nvar << " Ndeg=" << Ndeg << "\n";
  std::vector<int> eVect(Nvar, 0);
  auto fUpdate=[&]() -> int {
    for (int iVar=0; iVar<Nvar; iVar++)
      if (eVect[iVar] < Ndeg) {
	eVect[iVar]++;
	for (int jVar=0; jVar<iVar; jVar++)
	  eVect[jVar]=0;
	return 0;
      }
    return -1;
  };
  std::vector<std::vector<int> > ListPossibilities;
  while(true) {
    int eSum=0;
    for (int iVar=0; iVar<Nvar; iVar++)
      eSum += eVect[iVar];
    if (IsHomo) {
      if (eSum == Ndeg)
	ListPossibilities.push_back(eVect);
    }
    else {
      if (eSum <= Ndeg)
	ListPossibilities.push_back(eVect);
    }
    int test=fUpdate();
    if (test == -1)
      break;
  }
  int nbPoss=ListPossibilities.size();
  std::cerr << "nbPoss=" << nbPoss << " Nvar=" << Nvar << "\n";
  MyMatrix<int> SingleToMulti(nbPoss, Nvar);
  for (int iPoss=0; iPoss<nbPoss; iPoss++)
    for (int iVar=0; iVar<Nvar; iVar++)
      SingleToMulti(iPoss,iVar)=ListPossibilities[iPoss][iVar];
  long TotalNbCase=1;
  for (int iVar=0; iVar<Nvar; iVar++)
    TotalNbCase *= long(Ndeg+1);
  std::cerr << "TotalNbCase=" << TotalNbCase << "\n";
  auto GetIndex=[&](std::vector<int> const& eInput) -> long {
    long eIdx=0;
    long eProd=1;
    for (int iVar=0; iVar<Nvar; iVar++) {
      eIdx += eInput[iVar]*eProd;
      eProd *= long(Ndeg+1);
    }
    return eIdx;
  };
  std::vector<long> MultiToSingle(TotalNbCase,-1);
  for (int iPoss=0; iPoss<nbPoss; iPoss++) {
    long eIdx=GetIndex(ListPossibilities[iPoss]);
    MultiToSingle[eIdx]=iPoss;
  }
  return {Nvar, Ndeg, MultiToSingle, SingleToMulti};
}

long GetIndex(std::vector<int> const& eVect, PolynomialSymbol const& eSymb)
{
  long eIdxBig=0;
  long eProd=1;
  /*  std::cerr << "V=";
  for (int iVar=0; iVar<eSymb.Nvar; iVar++)
    std::cerr << " " << eVect[iVar];
    std::cerr << "\n";*/
  for (int iVar=0; iVar<eSymb.Nvar; iVar++) {
    eIdxBig += eVect[iVar]*eProd;
    eProd *= long(eSymb.Ndeg+1);
  }
  long eIdx=eSymb.MultiToSingle[eIdxBig];
  return eIdx;
}


template<typename T>
std::vector<T> ProductPolynomial(std::vector<T> const& P1, std::vector<T> const& P2, PolynomialSymbol const& eSymb)
{
  int nbMonomial=eSymb.SingleToMulti.rows();
  std::vector<T> eProd(nbMonomial,0);
  for (int i2=0; i2<nbMonomial; i2++)
    if (P2[i2] != 0)
      for (int i1=0; i1<nbMonomial; i1++)
	if (P1[i1] != 0) {
	  std::vector<int> SumDeg(eSymb.Nvar,0);
	  for (int iVar=0; iVar<eSymb.Nvar; iVar++)
	    SumDeg[iVar]=eSymb.SingleToMulti(i1,iVar) + eSymb.SingleToMulti(i2,iVar);
	  long eIdx=GetIndex(SumDeg, eSymb);
	  assert(eIdx >= 0);
	  eProd[eIdx] += P1[i1]*P2[i2];
	}
  return eProd;
}


template<typename T>
struct SingleEntPoly {
  std::vector<int> ListDeg;
  T eVal;
};


template<typename T>
std::vector<T> GetExpressionPoly(std::vector<SingleEntPoly<T> > const& ListSingleEntPoly, PolynomialSymbol const& eSymb)
{
  int nbMonomial=eSymb.SingleToMulti.rows();
  std::vector<T> RetVal(nbMonomial,0);
  //  std::cerr << "nbMonomial=" << nbMonomial << "\n";
  for (auto& eSingEnt : ListSingleEntPoly) {
    long eIdx=GetIndex(eSingEnt.ListDeg, eSymb);
    //    std::cerr << "eIdx=" << eIdx << "\n";
    RetVal[eIdx] += eSingEnt.eVal;
  }
  return RetVal;
}

template<typename T>
void PrintPolynomial(std::ostream & os, std::vector<T> const& ListVal, PolynomialSymbol const& eSymb)
{
  int nbMonomial=eSymb.SingleToMulti.rows();
  bool IsFirst=true;
  int Nvar=eSymb.Nvar;
  for (int iMon=0; iMon<nbMonomial; iMon++) {
    T eVal=ListVal[iMon];
    if (eVal != 0) {
      if (!IsFirst)
	os << " +";
      IsFirst=false;
      os << eVal << " [";
      for (int iVar=0; iVar<Nvar; iVar++) {
	if (iVar > 0)
	  os << ", ";
	os << eSymb.SingleToMulti(iMon,iVar);
      }
      os << "]";
    }
  }
  os << "\n";
}






#endif
