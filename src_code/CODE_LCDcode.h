#ifndef LCD_CODE_INCLUDE
#define LCD_CODE_INCLUDE

#include "Basic_string.h"
#include "MAT_functions.h"
#include "COMB_Combinatorics.h"
#include "GRP_GroupFct.h"
#include "POLY_LinearProgramming.h"
#include "COMB_Stor.h"
#include "MultivarPolynomial.h"



template<typename T>
std::vector<T> GetTotalPolynomialCoefficient(std::vector<int> const& ListDeg, PolynomialSymbol const& eSymb, PolynomialSymbol const& eSymbHomo)
{
  auto GetPoly=[&](std::vector<T> const& LVal) -> std::vector<T> {
    std::vector<SingleEntPoly<T> > LEnt;
    for (int iVar=0; iVar<4; iVar++) {
      std::vector<int> ListDegMon(4,0);
      ListDegMon[iVar]=1;
      SingleEntPoly<T> eEnt{ListDegMon, LVal[iVar]};
      LEnt.push_back(eEnt);
    }
    //    std::cerr << "Before call to GetExpressionPoly\n";
    std::vector<T> eVect=GetExpressionPoly<T>(LEnt, eSymb);
    //    std::cerr << "After call to GetExpressionPoly\n";
    return eVect;
  };
  std::vector<T> pp1=GetPoly({1, 1, 1, 1});
  std::vector<T> pp2=GetPoly({1, 1,-1,-1});
  std::vector<T> pp3=GetPoly({1,-1, 1,-1});
  std::vector<T> pp4=GetPoly({1,-1,-1, 1});
  std::vector<std::vector<T> > Lpp{pp1,pp2,pp3,pp4};
  //
  SingleEntPoly<T> eEnt{{0,0,0,0},1};
  std::vector<T> ThePow=GetExpressionPoly<T>({eEnt}, eSymb);
  for (int i=0; i<4; i++) {
    for (int iExp=0; iExp<ListDeg[i]; iExp++) {
      ThePow=ProductPolynomial(ThePow, Lpp[i], eSymb);
      //      std::cerr << "Now ThePow=";
      //      PrintPolynomial(std::cerr, ThePow, eSymb);
    }
  }
  int nbCoeff=eSymbHomo.SingleToMulti.rows();
  std::vector<T> ListVal(nbCoeff);
  for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
    std::vector<int> expo(4);
    //    std::cerr << "expo=";
    for (int i=0; i<4; i++) {
      expo[i]=eSymbHomo.SingleToMulti(iCoeff,i);
      //      std::cerr << " " << expo[i];
    }
    //    std::cerr << "\n";
    long eIdx=GetIndex(expo, eSymb);
    ListVal[iCoeff]=ThePow[eIdx];
  }
  return ListVal;
}

template<typename T>
struct LpInformationGeneralized {
  int TotalDim;
  std::vector<MyVector<T> > ListEqua;
  std::vector<std::string> ListDescEqua;
  std::vector<MyVector<T> > ListIneq;
  std::vector<std::string> ListDescIneq;
  std::vector<int> TypeVar; // 0: cst term, 1: quadruplet, 2: Ai 3: Bi
  std::vector<std::vector<int> > ListVarInfo;
};



template<typename T, typename Tint>
T KrawtchukPolynomial(Tint const& n, Tint const& i, Tint const& x)
{
  T TheSum;
  for (Tint j=0; j<=i; j++) {
    T eVal1=MyPow<T>(-1,j);
    T eVal2=BinomialCoefficient<T,Tint>(x, j);
    T eVal3=BinomialCoefficient<T,Tint>(n-x, i-j);
    TheSum += eVal1*eVal2*eVal3;
  }
  return TheSum;
}


struct ChoiceLCD {
  bool IdentificationAi;
  bool IdentificationA0;
  bool IdentificationB0;
  bool IdentificationBi;
  bool DimensionAcode;
  bool DoAntisymEqua;
  bool DoEquaGleason;
  bool DoEquaTrivialIntersection;
  bool DoEquaOrthogonality;
  bool DoEquaMinimalDistance;
  bool DoEquaLevelConstraintA;
  bool DoEquaLevelConstraintB;
  bool DoIneqSumAB;
  bool DoIneqA;
  bool DoIneqB;
  bool DoIneqNonNegativity;
  bool ConvexBodyConstraint;
  bool FullLinSpaceComputation;
  bool GLPKtestFeasibility;
};


ChoiceLCD GetFullChoice()
{
  ChoiceLCD eChoice;
  eChoice.IdentificationAi=true;
  eChoice.IdentificationA0=true;
  eChoice.IdentificationB0=true;
  eChoice.IdentificationBi=true;
  eChoice.DimensionAcode=true;
  eChoice.DoAntisymEqua=true;
  eChoice.DoEquaGleason=true;
  eChoice.DoEquaTrivialIntersection=true;
  eChoice.DoEquaOrthogonality=true;
  eChoice.DoEquaMinimalDistance=true;
  eChoice.DoEquaLevelConstraintA=true;
  eChoice.DoEquaLevelConstraintB=true;
  eChoice.DoIneqSumAB=true;
  eChoice.DoIneqA=true;
  eChoice.DoIneqB=true;
  eChoice.DoIneqNonNegativity=true;
  eChoice.FullLinSpaceComputation=true;
  eChoice.ConvexBodyConstraint=true;
  eChoice.GLPKtestFeasibility=false;
  return eChoice;
}


// LCD[n,k] = max {d s.t. there exist a feasible (n,k,d) code}
// LCK[n,d] = max {d s.t. there exist a feasible (n,k,d) code}
// From the document we know that following exist
// (3,2,2), (4,2,2), (5,2,2), (6,2,3), (7,4,2)
//
// The joint LCD enumerator
// we build the theory all together
// with all the inequalities
// 
template<typename T>
LpInformationGeneralized<T> ComputeJointLCDformulation(int const& n, int const& k, int const& d_primal, int const& d_dual, ChoiceLCD const& eChoice)
{
  std::cerr << "Before call to GetListCoefficientsLCDjoint\n";
  PolynomialSymbol eSymb=GetPolynomialSymbol(4, n, false);
  PolynomialSymbol eSymbHomo=GetPolynomialSymbol(4, n, true);
  int nbCoeff=eSymbHomo.SingleToMulti.rows();
  std::vector<MyVector<T> > ListEqua;
  std::vector<std::string> ListDescEqua;
  std::vector<MyVector<T> > ListIneq;
  std::vector<std::string> ListDescIneq;
  std::string eStr;
  //
  // Ordering of the coefficients:
  // ---First the constant term
  // ---Then the Coefficients of the joint enumerator
  // ---Then the coefficients of A_i for the weight enumerator of C
  // ---Then the coefficients of B_i for the weight enumerator of C^{\perp}.
  //
  int TotalDim=1 + nbCoeff + 2*(n+1);
  //
  // Writing down the description of the variables
  //
  std::vector<int> TypeVar(TotalDim);
  std::vector<std::vector<int> > ListVarInfo(TotalDim);
  TypeVar[0]=0;
  ListVarInfo[0]={};
  for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
    TypeVar[1+iCoeff]=1;
    int iC=eSymbHomo.SingleToMulti(iCoeff,0);
    int jC=eSymbHomo.SingleToMulti(iCoeff,1);
    int kC=eSymbHomo.SingleToMulti(iCoeff,2);
    int lC=eSymbHomo.SingleToMulti(iCoeff,3);
    ListVarInfo[1+iCoeff]={iC, jC, kC, lC};
  }
  for (int iDist=0; iDist<=n; iDist++) {
    TypeVar[1 + nbCoeff + iDist]=2;
    ListVarInfo[1 + nbCoeff + iDist]={iDist};
  }
  for (int iDist=0; iDist<=n; iDist++) {
    TypeVar[1 + nbCoeff + 1 + n + iDist]=3;
    ListVarInfo[1 + nbCoeff + 1 + n + iDist]={iDist};
  }
  //
  // i(u,v) = nr of 00
  // j(u,v) = nr of 01
  // k(u,v) = nr of 10
  // l(u,v) = nr of 11
  //
  // u is in C, v is in in C^perp
  //
  // If l=1 mod 2 then M(i,j,k,l)=0
  // since otherwise scalar product is not 0
  // and hypothesis is that A=C, B=C^{perp}
  //
  if (eChoice.DoEquaOrthogonality) {
    std::cerr << "Before computation of equation on ortgogonality\n";
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      int res=eSymbHomo.SingleToMulti(iCoeff,3) % 2;
      if (res == 1) {
	MyVector<T> eEqua=ZeroVector<T>(TotalDim);
	eStr="Odd case";
	eEqua(1+iCoeff)=1;
	ListEqua.push_back(eEqua);
	ListDescEqua.push_back(eStr);
      }
    }
  }
  //
  // The inequalities M(i,j,k,l) >= 0
  //
  if (eChoice.DoIneqNonNegativity) {
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      MyVector<T> eIneq=ZeroVector<T>(TotalDim);
      eIneq(1+iCoeff)=1;
      ListIneq.push_back(eIneq);
      ListDescIneq.push_back("Non-Negativity of M");
    }
  }
  //
  // First the equality J(C,C^perp) (a,b,c,d) = J(C,C^perp) (a,b,c,-d)
  // 
  std::cerr << "Before computation of equa/ineq, step 2\n";
  std::vector<MyMatrix<T> > ListMatSymmetry;
  if (eChoice.DoAntisymEqua) {
    MyMatrix<T> eMat1=ZeroMatrix<T>(nbCoeff, nbCoeff);
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      int iC=eSymbHomo.SingleToMulti(iCoeff,0);
      int jC=eSymbHomo.SingleToMulti(iCoeff,1);
      int kC=eSymbHomo.SingleToMulti(iCoeff,2);
      int lC=eSymbHomo.SingleToMulti(iCoeff,3);
      T eVal=MyPow<T>(1, iC)*MyPow<T>(1, jC)*MyPow<T>(1, kC)*MyPow<T>(-1, lC);
      eMat1(iCoeff, iCoeff)=eVal;
    }
    ListMatSymmetry.push_back(eMat1);
  }
  /*
  // Second the equality J(C,C^perp) (a,b,c,d) = J(C,C^perp)(-a,-b-c-d) if n is even
  if (n % 2 == 0) {
    MyMatrix<T> eMat2=ZeroMatrix<T>(nbCoeff, nbCoeff);
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      int i=eSymbHomo.SingleToMulti(iCoeff,0);
      int j=eSymbHomo.SingleToMulti(iCoeff,1);
      int k=eSymbHomo.SingleToMulti(iCoeff,2);
      int l=eSymbHomo.SingleToMulti(iCoeff,3);
      T eVal=MyPow<T>(-1, i)*MyPow<T>(-1, j)*MyPow<T>(-1, k)*MyPow<T>(-1, l);
      eMat2(iCoeff, iCoeff)=eVal;
    }
    ListMatSymmetry.push_back(eMat2);
    }*/
  // Third the equality J(C,C^perp)= (1/2^n) J(C,C^perp) (a+b+c+d, ...., 
  if (eChoice.DoEquaGleason) {
    MyMatrix<T> eMat3=ZeroMatrix<T>(nbCoeff, nbCoeff);
    T ePow=MyPow<T>(2,n);
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      std::vector<int> ListDeg(4);
      for (int i=0; i<4; i++)
	ListDeg[i]=eSymbHomo.SingleToMulti(iCoeff,i);
      //      std::cerr << "GetTotalPolynomialCoeff iCoeff=" << iCoeff << "/" << nbCoeff << "\n";
      std::vector<T> ListVal=GetTotalPolynomialCoefficient<T>(ListDeg, eSymb, eSymbHomo);
      for (int jCoeff=0; jCoeff<nbCoeff; jCoeff++)
	eMat3(iCoeff, jCoeff)=ListVal[jCoeff]/ePow;
    }
    ListMatSymmetry.push_back(eMat3);
  }
  // Now building full set of equalities
  int nbMat=ListMatSymmetry.size();
  for (int iMat=0; iMat<nbMat; iMat++) {
    std::cerr << "Adding Matrix symmetry iMat=" << iMat << " / " << nbMat << "\n";
    MyMatrix<T> eMat=ListMatSymmetry[iMat];
    MyMatrix<T> eMatBig(TotalDim,TotalDim);
    for (int i=0; i<TotalDim; i++)
      eMatBig(i,i)=1;
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++)
      for (int jCoeff=0; jCoeff<nbCoeff; jCoeff++)
	eMatBig(1+iCoeff,1+jCoeff)=eMat(iCoeff, jCoeff);
    for (int i=0; i<TotalDim; i++) {
      MyVector<T> eEqua=ZeroVector<T>(TotalDim);
      for (int j=0; j<TotalDim; j++)
	eEqua(j)=eMatBig(j,i);
      eEqua(i) -= 1;
      ListEqua.push_back(eEqua);
      eStr="Matrix equa iMat=" + IntToString(iMat) + " i=" + IntToString(i);
      ListDescEqua.push_back(eStr);
    }
  }
  //
  // Identification of A_0=1
  //
  if (eChoice.IdentificationA0) {
    std::cerr << "Identification of A_0=1\n";
    MyVector<T> eEqua=ZeroVector<T>(TotalDim);
    eEqua(0)=1;
    eEqua(1+nbCoeff)=-1;
    ListEqua.push_back(eEqua);
    ListDescEqua.push_back("A0=1");
  }
  //
  // Identification of B_0=1
  //
  if (eChoice.IdentificationB0) {
    MyVector<T> eEqua=ZeroVector<T>(TotalDim);
    eEqua(0)=1;
    eEqua(1 + nbCoeff + 1 + n)=-1;
    ListEqua.push_back(eEqua);
    ListDescEqua.push_back("B0=1");
  }
  //
  // Now building the weight enumerators
  // every vector u with eD entries 1 correspond to a pair (u,0)
  // i = n - eD     : 00
  // j = 0          : 01
  // k = eD         : 10
  // l = 0          : 11
  // A_i = M(n - eD, 0, eD, 0);
  //
  if (eChoice.IdentificationAi) {
    std::cerr << "Identifications of A_i coefficients\n";
    for (int iDist=0; iDist<=n; iDist++) {
      //      std::cerr << "iDist=" << iDist << "\n";
      MyVector<T> eEqua=ZeroVector<T>(TotalDim);
      std::vector<int> eCoeff{n - iDist, 0, iDist, 0};
      long eIdx=GetIndex(eCoeff, eSymbHomo);
      //      std::cerr << "eIdx=" << eIdx << "\n";
      eEqua(1+eIdx)=1;
      eEqua(1+nbCoeff + iDist)=-1;
      ListEqua.push_back(eEqua);
      ListDescEqua.push_back("Identification A" + IntToString(iDist));
    }
  }
  //
  // Now same story for B_i
  // every vector v with eD entries 1 correspond to a pair (0,v)
  // i = n - eD   : 00
  // j = eD       : 01
  // k = 0        : 10
  // l = 0        : 11
  // B_i = M(n - eD, eD, 0, 0)
  if (eChoice.IdentificationBi) {
    std::cerr << "Identifications of B_i coefficients\n";
    for (int iDist=0; iDist<=n; iDist++) {
      MyVector<T> eEqua=ZeroVector<T>(TotalDim);
      std::vector<int> eCoeff{n - iDist, iDist, 0, 0};
      long eIdx=GetIndex(eCoeff, eSymbHomo);
      eEqua(1+eIdx)=1;
      eEqua(1+nbCoeff + 1 + n + iDist)=-1;
      ListEqua.push_back(eEqua);
      ListDescEqua.push_back("Identification B" + IntToString(iDist));
    }
  }
  //
  // We have C inter C^{perp} = 0
  // henceforth we should have M(i,0,0,l) = 0 for i<=n-1
  //
  if (eChoice.DoEquaTrivialIntersection) {
    //    std::cerr << "Before computation of equation coming from trivial intersection\n";
    for (int i=0; i<=n-1; i++) {
      std::vector<int> eCoeff{i,0,0,n-i};
      long eIdx=GetIndex(eCoeff, eSymbHomo);
      MyVector<T> eEqua=ZeroVector<T>(TotalDim);
      eEqua(1+eIdx)=1;
      ListEqua.push_back(eEqua);
      ListDescEqua.push_back("Trivial intersection, Index i=" + IntToString(i));
    }
  }
  //
  // Minimal distance is d.
  // Therefore if we have an entry M(i,j,k,l), it is zero
  // if the number nb of 1 in u satisfies 1 < nb < d
  //
  if (eChoice.DoEquaMinimalDistance) {
    //    std::cerr << "Before computation of equation on minimal distance\n";
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      int iC=eSymbHomo.SingleToMulti(iCoeff,0);
      int jC=eSymbHomo.SingleToMulti(iCoeff,1);
      int kC=eSymbHomo.SingleToMulti(iCoeff,2);
      int lC=eSymbHomo.SingleToMulti(iCoeff,3);
      int nb0_primal=iC + jC;
      int nb1_primal=kC + lC;
      if (nb0_primal + nb1_primal != n) {
	std::cerr << "Error in the summing\n";
	throw TerminalException{1};
      }
      if (nb1_primal > 0 && nb1_primal < d_primal) {
	MyVector<T> eEqua=ZeroVector<T>(TotalDim);
	eEqua(1+iCoeff)=1;
	ListEqua.push_back(eEqua);
	ListDescEqua.push_back("Minimal distance d (primal) : A" + IntToString(nb1_primal) + " = 0");
      }
      int nb0_dual=iC + kC;
      int nb1_dual=jC + lC;
      if (nb0_dual + nb1_dual != n) {
	std::cerr << "Error in the summing\n";
	throw TerminalException{1};
      }
      if (nb1_dual > 0 && nb1_dual < d_dual) {
	MyVector<T> eEqua=ZeroVector<T>(TotalDim);
	eEqua(1+iCoeff)=1;
	ListEqua.push_back(eEqua);
	ListDescEqua.push_back("Minimal distance d (dual) : B" + IntToString(nb1_dual) + " = 0");
      }
    }
  }
  //
  // Constraints on the level
  // 
  // Product constraints:
  // Number of pairs (u,v) with ||u|| = iA and v anything should A_i 2^(n-k)
  // 
  // i(u,v) = nr of 00
  // j(u,v) = nr of 01
  // k(u,v) = nr of 10
  // l(u,v) = nr of 11
  //
  if (eChoice.DoEquaLevelConstraintA) {
    //    std::cerr << "Equation of constraint on the level A\n";
    int dimCperp=n-k;
    for (int iA=0; iA<=n; iA++) {
      MyVector<T> eEqua=ZeroVector<T>(TotalDim);
      eEqua(1+nbCoeff + iA)=-MyPow<T>(2,dimCperp);
      for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
	int iC=eSymbHomo.SingleToMulti(iCoeff,0);
	int jC=eSymbHomo.SingleToMulti(iCoeff,1);
	int kC=eSymbHomo.SingleToMulti(iCoeff,2);
	int lC=eSymbHomo.SingleToMulti(iCoeff,3);
	int nb0_u=iC + jC;
	int nb1_u=kC + lC;
	if (nb0_u == n-iA && nb1_u == iA)
	  eEqua(1+iCoeff)=1;
      }
      ListEqua.push_back(eEqua);
      ListDescEqua.push_back("Level constraint on A" + IntToString(iA));
    }
  }
  //
  // Constraints on the level
  // 
  // Product constraints:
  // Number of pairs (u,v) with ||v|| = iB and v anything should B_i 2^(k)
  // 
  // i(u,v) = nr of 00
  // j(u,v) = nr of 01
  // k(u,v) = nr of 10
  // l(u,v) = nr of 11
  //
  if (eChoice.DoEquaLevelConstraintB) {
    //    std::cerr << "Equation of constraint on the level B\n";
    for (int iB=0; iB<=n; iB++) {
      MyVector<T> eEqua=ZeroVector<T>(TotalDim);
      eEqua(1+nbCoeff + 1 + n + iB)=-MyPow<T>(2,k);
      for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
	int iC=eSymbHomo.SingleToMulti(iCoeff,0);
	int jC=eSymbHomo.SingleToMulti(iCoeff,1);
	int kC=eSymbHomo.SingleToMulti(iCoeff,2);
	int lC=eSymbHomo.SingleToMulti(iCoeff,3);
	int nb0_v=iC + kC;
	int nb1_v=jC + lC;
	if (nb0_v == n-iB && nb1_v == iB)
	  eEqua(1+iCoeff)=1;
      }
      ListEqua.push_back(eEqua);
      ListDescEqua.push_back("Level constraint on Bi");
    }
  }
  //
  // Inequality A_i >= 0
  //
  if (eChoice.DoIneqA) {
    for (int iA=0; iA<=n; iA++) {
      MyVector<T> eIneq=ZeroVector<T>(TotalDim);
      eIneq(1+nbCoeff + iA)=1;
      ListIneq.push_back(eIneq);
      ListDescIneq.push_back("Inequality A" + IntToString(iA) + " >= 0");
    }
  }
  //
  // Inequality B_i >= 0
  //
  if (eChoice.DoIneqB) {
    for (int iB=0; iB<=n; iB++) {
      MyVector<T> eIneq=ZeroVector<T>(TotalDim);
      eIneq(1 + nbCoeff + 1 + n + iB)=1;
      ListIneq.push_back(eIneq);
      ListDescIneq.push_back("Inequality B" + IntToString(iB) + " >= 0");
    }
  }
  //
  // Constraints A_i + B_i <= (n i)
  //
  if (eChoice.DoIneqSumAB) {
    std::cerr << "Writing down equation of type 2: Constraints on A_i + B_i\n";
    for (int iDist=1; iDist<=n; iDist++) {
      long eIdx1=1 + nbCoeff + iDist;
      long eIdx2=1 + nbCoeff + (1 + n) + iDist;
      MyVector<T> eIneq=ZeroVector<T>(TotalDim);
      T eBinom=BinomialCoefficient<T,int>(n,iDist);
      std::cerr << "C(" << n << "," << iDist << ") = " << eBinom << "\n";
      eIneq(0)=eBinom;
      eIneq(eIdx1)=-1;
      eIneq(eIdx2)=-1;
      ListIneq.push_back(eIneq);
      ListDescIneq.push_back("Binomial constraint : A" + IntToString(iDist) + " + B" + IntToString(iDist) + " <= (n," + IntToString(iDist) + ")");
    }
  }
  //
  // TotalNumber of entry in the system
  // We have |C| = 2^k = sum_k M(n-k,0,k,0)
  //
  // i(u,v) = nr of 00
  // j(u,v) = nr of 01
  // k(u,v) = nr of 10
  // l(u,v) = nr of 11
  //
  if (eChoice.DimensionAcode) {
    //    std::cerr << "Before computation of equation of A code\n";
    MyVector<T> eEqua=ZeroVector<T>(TotalDim);
    eEqua(0)=MyPow<T>(2, k);
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      int iC=eSymbHomo.SingleToMulti(iCoeff,0);
      int jC=eSymbHomo.SingleToMulti(iCoeff,1);
      int kC=eSymbHomo.SingleToMulti(iCoeff,2);
      int lC=eSymbHomo.SingleToMulti(iCoeff,3);
      if (iC < 0 || kC < 0) {
	std::cerr << "Inconsistency in code\n";
	throw TerminalException{1};
      }
      if (jC == 0 && lC == 0)
	eEqua(1+iCoeff)=-1;
    }
    ListEqua.push_back(eEqua);
    ListDescEqua.push_back("Total number of element is 2^k");
  }
  //
  // Convex body constraint
  // 
  if (eChoice.ConvexBodyConstraint) {
    MyVector<T> eIneq=ZeroVector<T>(TotalDim);
    eIneq(0)=1;
    ListIneq.push_back(eIneq);
    ListDescIneq.push_back("convex body constraint");
  }
  //
  bool PrintDeg=true;
  if (PrintDeg) {
    for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
      int iC=eSymbHomo.SingleToMulti(iCoeff,0);
      int jC=eSymbHomo.SingleToMulti(iCoeff,1);
      int kC=eSymbHomo.SingleToMulti(iCoeff,2);
      int lC=eSymbHomo.SingleToMulti(iCoeff,3);
      std::cerr << "iC=" << iCoeff << " [" << iC << "," << jC << "," << kC << "," << lC << "]\n";
    }
  }
  return {TotalDim, ListEqua, ListDescEqua, ListIneq, ListDescIneq, TypeVar, ListVarInfo};
}



template<typename T>
MyVector<T> ComputeAndCheckRepresentativeVector(MyMatrix<int> const& eCode, MyMatrix<int> const& eCodePerp)
{
  int n=eCode.cols();
  PolynomialSymbol eSymb=GetPolynomialSymbol(4, n, false);
  PolynomialSymbol eSymbHomo=GetPolynomialSymbol(4, n, true);
  int nbCoeff=eSymbHomo.SingleToMulti.rows();
  std::vector<MyVector<T> > ListEqua;
  std::vector<MyVector<T> > ListIneq;
  int TotalDim=1+nbCoeff + 2*(n+1);
  MyVector<T> eVectTotal(TotalDim);
  eVectTotal(0)=1;
  //
  int dimC=eCode.rows();
  int dimCperp=eCodePerp.rows();
  if (dimC + dimCperp != n) {
    std::cerr << "dimC=" << dimC << "\n";
    std::cerr << "dimCperp=" << dimCperp << "\n";
    std::cerr << "n=" << n << "\n";
    std::cerr << "Error in the code construction\n";
    throw TerminalException{1};
  }
  MyMatrix<int> eMatC=BuildSet(dimC, 2);
  MyMatrix<int> eMatCperp=BuildSet(dimCperp, 2);
  int nbC=eMatC.rows();
  int nbCperp=eMatCperp.rows();
  MyVector<int> eVectC(n);
  MyVector<int> eVectCperp(n);
  for (int iC=0; iC<nbC; iC++) {
    // Computing vector
    for (int i=0; i<n; i++) {
      int eSum=0;
      for (int iCol=0; iCol<dimC; iCol++)
	eSum += eCode(iCol,i)*eMatC(iC,iCol);
      eVectC(i)=eSum % 2;
    }
    for (int iCperp=0; iCperp<nbCperp; iCperp++) {
      // Computing vector
      for (int i=0; i<n; i++) {
	int eSum=0;
	for (int iCol=0; iCol<dimCperp; iCol++)
	  eSum += eCodePerp(iCol,i)*eMatCperp(iCperp,iCol);
	eVectCperp(i)=eSum % 2;
      }
      // i(u,v) = nr of 00
      // j(u,v) = nr of 01
      // k(u,v) = nr of 10
      // l(u,v) = nr of 11
      int iS=0;
      int jS=0;
      int kS=0;
      int lS=0;
      for (int i=0; i<n; i++) {
	if (eVectC(i) == 0 && eVectCperp(i) == 0)
	  iS++;
	if (eVectC(i) == 0 && eVectCperp(i) == 1)
	  jS++;
	if (eVectC(i) == 1 && eVectCperp(i) == 0)
	  kS++;
	if (eVectC(i) == 1 && eVectCperp(i) == 1)
	  lS++;
      }
      std::vector<int> eCoeff{iS, jS, kS, lS};
      long eIdx=GetIndex(eCoeff, eSymbHomo);
      eVectTotal(1+eIdx)++;
    }
  }
  //
  int MinD_primal=n+1;
  for (int iC=0; iC<nbC; iC++) {
    // Computing vector
    int nb1_primal=0;
    for (int i=0; i<n; i++) {
      int eSum=0;
      for (int iCol=0; iCol<dimC; iCol++)
	eSum += eCode(iCol,i)*eMatC(iC,iCol);
      int res=eSum % 2;
      if (res == 1)
	nb1_primal++;
    }
    if (nb1_primal != 0)
      if (nb1_primal < MinD_primal)
	MinD_primal=nb1_primal;
    eVectTotal(1+nbCoeff + nb1_primal)++;
  }
  //
  int MinD_dual=n+1;
  for (int iCperp=0; iCperp<nbCperp; iCperp++) {
    // Computing vector
    int nb1_dual=0;
    for (int i=0; i<n; i++) {
      int eSum=0;
      for (int iCol=0; iCol<dimCperp; iCol++)
	eSum += eCodePerp(iCol,i)*eMatCperp(iCperp,iCol);
      int res=eSum % 2;
      if (res == 1)
	nb1_dual++;
    }
    if (nb1_dual != 0)
      if (nb1_dual < MinD_dual)
	MinD_dual=nb1_dual;
    eVectTotal(1 + nbCoeff + 1 + n + nb1_dual)++;
  }
  //
  std::cerr << "Print the value of the vector\n";
  MyVector<T> eVectTransform=ZeroVector<T>(nbCoeff);
  for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
    int iC=eSymbHomo.SingleToMulti(iCoeff,0);
    int jC=eSymbHomo.SingleToMulti(iCoeff,1);
    int kC=eSymbHomo.SingleToMulti(iCoeff,2);
    int lC=eSymbHomo.SingleToMulti(iCoeff,3);
    T eVal=eVectTotal(1+iCoeff);
    std::cerr << "iC=" << iCoeff << " [" << iC << "," << jC << "," << kC << "," << lC << "]   val=" << eVal << "\n";
    if (eVal != 0) {
      std::vector<int> ListDeg(4);
      for (int i=0; i<4; i++)
	ListDeg[i]=eSymbHomo.SingleToMulti(iCoeff,i);
      std::vector<T> ListVal=GetTotalPolynomialCoefficient<T>(ListDeg, eSymb, eSymbHomo);
      /*
      for (int jCoeff=0; jCoeff<nbCoeff; jCoeff++) {
	int i2=eSymbHomo.SingleToMulti(jCoeff,0);
	int j2=eSymbHomo.SingleToMulti(jCoeff,1);
	int k2=eSymbHomo.SingleToMulti(jCoeff,2);
	int l2=eSymbHomo.SingleToMulti(jCoeff,3);
	std::cerr << "  jC=" << jCoeff << " [" << i2 << "," << j2 << "," << k2 << "," << l2 << "]   val=" << ListVal[jCoeff] << "\n";
	}*/
      for (int jCoeff=0; jCoeff<nbCoeff; jCoeff++)
	eVectTransform(jCoeff) += eVal*ListVal[jCoeff];
    }
  }
  /*  for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
    std::cerr << "iC=" << iCoeff << " val1=" << eVectTotal(1+iCoeff) << " val2=" << eVectTransform(iCoeff) << "\n";
    }*/
  for (int i=0; i<=n; i++) {
    std::cerr << "A[" << i << "] = " << eVectTotal(1+nbCoeff + i) << "\n";
  }
  for (int i=0; i<=n; i++) {
    std::cerr << "B[" << i << "] = " << eVectTotal(1+nbCoeff + 1 + n + i) << "\n";
  }
  //
  int d_primal=MinD_primal;
  int d_dual=MinD_dual;
  int k=dimC;
  std::cerr << "k=" << k << "\n";
  ChoiceLCD eChoice=GetFullChoice();
  LpInformationGeneralized<T> LpInfo=ComputeJointLCDformulation<T>(n, k, d_primal, d_dual, eChoice);
  std::cerr << "LpInfo.TotalDim=" << LpInfo.TotalDim << "\n";
  std::cerr << "Now check equalities\n";
  int nbEqua=LpInfo.ListEqua.size();
  std::cerr << "|equa|=" << LpInfo.ListEqua.size() << "\n";
  std::cerr << "|DescEqua|=" << LpInfo.ListDescEqua.size() << "\n";
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    std::cerr << "iEqua=" << iEqua << " / " << nbEqua << "\n";
    std::cerr << "symbol=" <<LpInfo.ListDescEqua[iEqua] << "\n";
    T eSum=0;
    //    std::cerr << "   siz=" << LpInfo.ListEqua[iEqua].size() << "\n";
    //    std::cerr << "TotalDim=" << TotalDim << "\n";
    for (int i=0; i<TotalDim; i++)
      eSum += eVectTotal(i)*LpInfo.ListEqua[iEqua](i);
    if (eSum != 0) {
      std::cerr << "Inconsistency with equality system\n";
      std::cerr << " eSum=" << eSum << "\n";
      std::cerr << "Symbol=" << LpInfo.ListDescEqua[iEqua] << "\n";
      std::cerr << "0: eVectTotal(0)=" << eVectTotal(0) << " eEqua(0)=" << LpInfo.ListEqua[iEqua](0) << "\n";
      for (int iCoeff=0; iCoeff<nbCoeff; iCoeff++) {
	int iC=eSymbHomo.SingleToMulti(iCoeff,0);
	int jC=eSymbHomo.SingleToMulti(iCoeff,1);
	int kC=eSymbHomo.SingleToMulti(iCoeff,2);
	int lC=eSymbHomo.SingleToMulti(iCoeff,3);
	std::cerr << "iCoeff=" << iCoeff << " [" << iC << "," << jC << "," << kC << "," << lC << "] valEq=" << LpInfo.ListEqua[iEqua](1+iCoeff) << " valVar=" << eVectTotal(1+iCoeff) << "\n";
      }
      throw TerminalException{1};
    }
  }
  std::cerr << "Now check inequalities\n";
  int nbIneq=LpInfo.ListIneq.size();
  for (int iIneq=0; iIneq<nbIneq; iIneq++) {
    T eSum=0;
    for (int i=0; i<TotalDim; i++)
      eSum += eVectTotal(i)*LpInfo.ListIneq[iIneq](i);
    if (eSum < 0) {
      std::cerr << "Inconsistency with inequality system\n";
      std::cerr << "Symbol=" << LpInfo.ListDescIneq[iIneq] << "\n";
      throw TerminalException{1};
    }
  }
  std::cerr << "No inconsistency has been found\n";
  return eVectTotal;
}



template<typename T>
void TestFeasibilityLCDformulation(int const& n, int const& k, int const& d_primal, int const& d_dual, std::string const& eFileOUT, bool const& DoOutput, bool const& FullSystem, ChoiceLCD const& eChoice)
{
  //
  // First computing the Joint LCD information
  //
  LpInformationGeneralized<T> LpInfo=ComputeJointLCDformulation<T>(n, k, d_primal, d_dual, eChoice);
  LpInfo.ListDescEqua.clear();
  LpInfo.ListDescIneq.clear();
  int nbEqua=LpInfo.ListEqua.size();
  int nbIneq=LpInfo.ListIneq.size();
  int TotalDim=LpInfo.TotalDim;
  std::cerr << "TotalDim=" << TotalDim << "\n";
  std::cerr << "nbEqua=" << nbEqua << "\n";
  std::cerr << "nbIneq=" << nbIneq << "\n";
  int dim=LpInfo.ListEqua[0].size();
  //
  MyMatrix<T> ListEqua(nbEqua, dim);
  for (int iEqua=0; iEqua<nbEqua; iEqua++)
    for (int i=0; i<dim; i++)
      ListEqua(iEqua,i)=LpInfo.ListEqua[iEqua](i);
  LpInfo.ListEqua.clear();
  //
  MyMatrix<T> ListIneq(nbIneq, dim);
  for (int iIneq=0; iIneq<nbIneq; iIneq++)
    for (int i=0; i<dim; i++)
      ListIneq(iIneq,i)=LpInfo.ListIneq[iIneq](i);
  LpInfo.ListIneq.clear();
  //
  // The variables that are being used.
  //
  SelectionRowCol<T> eSelect;
  MyMatrix<T> FAC;
  MyMatrix<T> FACuniq;
  MyMatrix<T> LinSpace;
  bool result=true;
  bool OneMethodUsed=false;
  bool HaveDim=false;
  int TheDim = -1;
  //
  // Now operating full linspace computation
  //
  std::cerr << "TestFeasibilityLCDformulation, step 1\n";
  if (eChoice.FullLinSpaceComputation) {
    OneMethodUsed=true;
    //
    eSelect=TMat_SelectRowCol(ListEqua);
    if (!FullSystem)
      ListEqua.resize(0,0);
    std::cerr << "We have eSelect rank=" << eSelect.TheRank << "\n";
    int dimNSP=eSelect.NSP.rows();
    std::cerr << "dimNSP=" << dimNSP << "\n";
    //
    FAC=ListIneq * eSelect.NSP.transpose();
    if (!FullSystem)
      eSelect.NSP.resize(0,0);
    std::cerr << "We have FAC nbRow=" << FAC.rows() << " nbCol=" << FAC.cols() << "\n";
    //    WriteMatrix(std::cerr, FAC);
    FACuniq=SortUnicizeMatrix(FAC);
    if (!FullSystem)
      FAC.resize(0,0);
    std::cerr << "We have FACuniq nbRow=" << FACuniq.rows() << " nbCol=" << FACuniq.cols() << "\n";
    WriteMatrix(std::cerr, FACuniq);

    LinSpace=LinearDeterminedByInequalities(FACuniq);
    if (!FullSystem)
      FACuniq.resize(0,0);
    TheDim=LinSpace.rows();
    HaveDim=true;
    std::cerr << "We have LinSpace dim=" << TheDim << "\n";
    result=TheDim>0;
  }
  std::cerr << "TestFeasibilityLCDformulation, step 2\n";
  if (eChoice.GLPKtestFeasibility) {
    OneMethodUsed=true;
    //
    MyVector<T> ToBeMinimized(dim-1);
    for (int i=0; i<dim-1; i++) {
      //      int a=rand() % 10;
      //      int b=rand() % 10;
      //   T eVal=a - b;
      T eVal=1;
      ToBeMinimized(i)=eVal;
    }
    GLPKoption eGLPKoption;
    eGLPKoption.UseDouble=false;
    eGLPKoption.UseExact=false;
    eGLPKoption.UseXcheck=true;
    LpSolutionSimple<T> eLpSolSimpl=GLPK_LinearProgramming_Kernel_Dense_PROC(ListEqua, ListIneq, ToBeMinimized, eGLPKoption);
    if (eLpSolSimpl.PrimalDefined) {
      result=true;
    }
    else {
      result=false;
    }
  }
  std::cerr << "TestFeasibilityLCDformulation, step 3\n";
  if (!OneMethodUsed) {
    std::cerr << "You should at least select one method for the computation\n";
    throw TerminalException{1};
  }
  std::cerr << "TestFeasibilityLCDformulation, step 4\n";
  //
  // Now printing the data information
  //
  std::cerr << "[n,k,d_primal,d_dual]=[" << n << "," << k << "," << d_primal << "," << d_dual << "] test=" << result << "\n";
  std::cerr << "DoOutput=" << DoOutput << "\n";
  if (DoOutput) {
    std::cerr << "eFileOUT=" << eFileOUT << "\n";
    std::ofstream OUTfs(eFileOUT);
    OUTfs << "return rec(";
    OUTfs << "n:=" << n;
    OUTfs << ", k:=" << k;
    OUTfs << ", d_primal:=" << d_primal;
    OUTfs << ", d_dual:=" << d_dual;
    if (HaveDim) {
      OUTfs << ", TheDim:=" << TheDim;
    }
    OUTfs << ", result:=" << GAP_logical(result);
    if (FullSystem) {
      OUTfs << ",\n ListEqua:=";
      WriteMatrixGAP(OUTfs, ListEqua);
      OUTfs << ",\n ListIneq:=";
      WriteMatrixGAP(OUTfs, ListIneq);
      OUTfs << ",\n NSP:=";
      WriteMatrixGAP(OUTfs, eSelect.NSP);
      OUTfs << ",\n FAC:=";
      WriteMatrixGAP(OUTfs, FAC);
      OUTfs << ",\n FACuniq:=";
      WriteMatrixGAP(OUTfs, FACuniq);
      OUTfs << ",\n LinSpace:=";
      WriteMatrixGAP(OUTfs, LinSpace);
      //
      MyMatrix<T> LinSpaceFull=LinSpace * eSelect.NSP;
      OUTfs << ",\n LinSpaceFull:=";
      WriteMatrixGAP(OUTfs, LinSpaceFull);
      //
      OUTfs << ",\n TypeVar:=";
      WriteStdVectorGAP(OUTfs, LpInfo.TypeVar);
      OUTfs << ",\n ListVarInfo:=";
      WriteStdVectorStdVectorGAP(OUTfs, LpInfo.ListVarInfo);
    }
    OUTfs << ");\n";
  }
  std::cerr << "TestFeasibilityLCDformulation, step 6\n";
}


struct FixedKdata {
  int k;
  MyMatrix<int> ListVect;
  std::vector<ulong> ListParity;
};


bool IsCodeMatrixLCD(MyMatrix<int> const& CodeMat)
{
  int nbWord=CodeMat.rows();
  int dim=CodeMat.cols();
  //  std::cerr << "nbWord=" << nbWord << " dim=" << dim << "\n";
  auto ScalProd=[&](int const& iWord, int const& jWord) -> int {
    int eScal=0;
    for (int i=0; i<dim; i++)
      eScal += CodeMat(iWord,i) * CodeMat(jWord,i);
    int RetVal = eScal % 2;
    //    std::cerr << "iWord=" << iWord << " jWord=" << jWord << " RetVal=" << RetVal << "\n";
    return RetVal;
  };
  auto IsInKernel=[&](int const& iWord) -> bool {
    for (int jWord=0; jWord<nbWord; jWord++)
      if (ScalProd(iWord, jWord) != 0)
	return false;
    return true;
  };
  int KernelSize=0;
  for (int iWord=0; iWord<nbWord; iWord++) {
    if (IsInKernel(iWord))
      KernelSize++;
    if (KernelSize > 1)
      return false;
  }
  //  std::cerr << "Code matrix (if all ok, it is LCD)\n";
  //  WriteMatrix(std::cerr, CodeMat);
  return true;
}

//
// Dimension k is fixed.
// We have k basis vectors
// v1, v2, ...., vk
// The vectors are 0/1 and so they are expressed as 
// v1 has value 1 on a set S1 0 otherwise and so on.
// Therefore the code is determined only by the pairwise
// intersections S1 \cup S2 \cup ..... \cup Sk
// with also the complement being considered.
// 
// This makes a total of 2^k different choices.
// Only the parity of the choices matter for the LCD ness.
// 
FixedKdata ComputeParityInformationLCD(int const& k)
{
  MyMatrix<int> ListVect=BuildSet(k, 2);
  std::vector<ulong> ListParity;
  int len=ListVect.rows();
  std::cerr << "k=" << k << " len=" << len << "\n";
  int nbWord=len;  // len is not strictly the same as nbWord. They merely coincide.
  BlockIteration01 BlIter(len);
  auto GetBasicCodeMatrix=[&](Face const& LVal) -> MyMatrix<int> {
    int TheSum=0;
    for (int i=0; i<len; i++)
      TheSum += LVal[i];
    MyMatrix<int> CodeMat(len, TheSum);
    int pos=0;
    for (int i=0; i<len; i++)
      if (LVal[i] == 1) {
	std::vector<int> eVectPoss(k);
	for (int iCol=0; iCol<k; iCol++)
	  eVectPoss[iCol]=ListVect(i,iCol);
	for (int iWord=0; iWord<nbWord; iWord++) {
	  int eSum=0;
	  for (int iCol=0; iCol<k; iCol++)
	    eSum += eVectPoss[iCol]*ListVect(iWord,iCol);
	  int res=eSum % 2;
	  CodeMat(iWord,pos)=res;
	}
	pos++;
      }
    return CodeMat;
  };
  auto IsCorrectParityValues=[&](Face const& LVal) -> bool {
    MyMatrix<int> CodeMat=GetBasicCodeMatrix(LVal);
    return IsCodeMatrixLCD(CodeMat);
  };
  int iter=0;
  while(true) {
    Face LVal=BlIter.GetFace();
    bool test=IsCorrectParityValues(LVal);
    if (test) {
      ulong eVal=FaceToUnsignedLong(LVal);
      ListParity.push_back(eVal);
    }
    int val=BlIter.IncrementShow();
    int res=iter % 100000;
    if (res == 0)
      std::cerr << " iter=" << iter << "\n";
    iter++;
    if (val == -1)
      break;
  }
  int siz=ListParity.size();
  std::cerr << "|ListParity|=" << siz << "\n";
  return {k, ListVect, ListParity};
}

std::vector<MyMatrix<int> > GetGeneratorsGLkZ2(int const& k)
{
  MyMatrix<int> gen1;
  gen1.setZero(k,k);
  for (int i=0; i<k; i++)
    gen1(i,i)=1;
  gen1(0,1)=1;
  MyMatrix<int> gen2;
  gen2.setZero(k,k);
  for (int i=0; i<k; i++) {
    int iNext=NextIdx(k,i);
    gen2(iNext,i)=1;
  }
  return {gen1, gen2};
}



// As above we have a configuration of vectors
// And now we change basis.
// we write
// wi = ai1 v1 + ai2 v2 + .... + aik vk
//
// The LCD codeness is unchanged by the change of basis.
// Actually, this acts on the set of columns of the matrix.
//   together with the lines.
//
// If i corresponds to (i1, i2, ..., ik)
// Then LVal[i]=s means that there are s coordinates r such that
// vj[r] = 1 if ij[r]=1
//       = 0 if ij[r]=0
// Now the w and here is how it works.
// wj[r] = 1 if uj[r]=1
//       = 0 if uj[r]=0
// wj
FixedKdata ReduceByIsomorphismFixedDimCasesLCD(FixedKdata const& eFix)
{
  int k=eFix.k;
  int len=eFix.ListVect.rows();
  int nbParity=eFix.ListParity.size();
  std::vector<MyMatrix<int> > ListGen=GetGeneratorsGLkZ2(k);
  auto IsEqual=[](std::vector<int> const& eVect, MyMatrix<int> const& eMat, int const& iWord) -> bool {
    int dim=eMat.cols();
    for (int i=0; i<dim; i++)
      if (eMat(iWord,i) != eVect[i])
	return false;
    return true;
  };
  auto GetPositionListVect=[&IsEqual](std::vector<int> const& eVect, MyMatrix<int> const& eMat) -> int {
    int nbVect=eMat.rows();
    for (int i=0; i<nbVect; i++)
      if (IsEqual(eVect, eMat, i))
	return i;
    std::cerr << "Failed to find\n";
    throw TerminalException{1};
  };
  //
  // Iterating over the generators
  //
  permlib::Permutation ePermS=SortingPerm(eFix.ListParity);
  permlib::Permutation ePermSinv=~ePermS;
  std::vector<permlib::Permutation> ListPermColumn;
  std::vector<permlib::Permutation> ListPermParity;
  for (auto & eMat : ListGen) {
    std::vector<permlib::dom_int> eListColumn(len);
    //    std::cerr << "eMat\n";
    //    WriteMatrix(std::cerr, eMat);
    for (int i=0; i<len; i++) {
      std::vector<int> eVect(k);
      for (int iCol=0; iCol<k; iCol++) {
	int eSum=0;
	for (int iRow=0; iRow<k; iRow++)
	  eSum += eFix.ListVect(i,iRow) * eMat(iRow, iCol);
	int res=eSum % 2;
	eVect[iCol]=res;
      }
      /*
      std::cerr << "eVect=";
      for (int i=0; i<k; i++)
	std::cerr << " " << eVect[i];
	std::cerr << "\n";*/
      //
      int pos=GetPositionListVect(eVect, eFix.ListVect);
      eListColumn[i]=pos;
      //      std::cerr << " i=" << i << " pos=" << pos << "\n";
    }
    permlib::Permutation ePermColumn(eListColumn);
    ListPermColumn.push_back(ePermColumn);
    std::cerr << "We have first permutation\n";
    //
    std::vector<ulong> ListImgFace(nbParity);
    for (int iParity=0; iParity<nbParity; iParity++) {
      Face eVect(len);
      Face eFaceIn=UnsignedLongToFace(len, eFix.ListParity[iParity]);
      //      std::cerr << "eVal=" << eFix.ListParity[iParity] << "\n";
      for (int i=0; i<len; i++) {
	int j=ePermColumn.at(i);
	eVect[j]=eFaceIn[i];
      }
      ulong eVal=FaceToUnsignedLong(eVect);
      ListImgFace[iParity]=eVal;
    }
    permlib::Permutation ePermB=SortingPerm(ListImgFace);
    permlib::Permutation ePermParity=ePermB*ePermSinv;
    ListPermParity.push_back(ePermParity);
    std::cerr << "We have second permutation\n";
  }
  TheGroupFormat GRP=GetPermutationGroup(nbParity, ListPermParity);
  Face eList(nbParity);
  for (int i=0; i<nbParity; i++)
    eList[i]=1;
  std::vector<Face> ListOrb=DecomposeOrbitPoint(GRP, eList);
  int nbOrb=ListOrb.size();
  std::vector<ulong> ListParityNew(nbOrb);
  for (int iOrb=0; iOrb<nbOrb; iOrb++) {
    Face eOrb=ListOrb[iOrb];
    int MinVal=eOrb.find_first();
    ListParityNew[iOrb]=eFix.ListParity[MinVal];
  }
  std::cerr << "nbOrb=" << nbOrb << "\n";
  return {k, eFix.ListVect, ListParityNew};
}



// Next version, tries to be as memory efficient as possible
FixedKdata EnumerateListOrbitLCDenum(int const& k)
{
  MyMatrix<int> ListVect=BuildSet(k, 2);
  int len=ListVect.rows();
  int nbWord=len;  // len is not strictly the same as nbWord. They merely coincide.
  std::vector<MyMatrix<int> > ListGen=GetGeneratorsGLkZ2(k);
  auto IsEqual=[](std::vector<int> const& eVect, MyMatrix<int> const& eMat, int const& iWord) -> bool {
    int dim=eMat.cols();
    for (int i=0; i<dim; i++)
      if (eMat(iWord,i) != eVect[i])
	return false;
    return true;
  };
  auto GetPositionListVect=[&IsEqual](std::vector<int> const& eVect, MyMatrix<int> const& eMat) -> int {
    int nbVect=eMat.rows();
    for (int i=0; i<nbVect; i++)
      if (IsEqual(eVect, eMat, i))
	return i;
    std::cerr << "Failed to find\n";
    throw TerminalException{1};
  };
  //
  // Building the permutations over the columns
  //
  std::size_t ePow=1;
  for (int i=0; i<len; i++)
    ePow *=2;
  std::vector<permlib::Permutation> ListPermColumn;
  for (auto & eMat : ListGen) {
    std::vector<permlib::dom_int> eListColumn(len);
    //    std::cerr << "eMat\n";
    //    WriteMatrix(std::cerr, eMat);
    for (int i=0; i<len; i++) {
      std::vector<int> eVect(k);
      for (int iCol=0; iCol<k; iCol++) {
	int eSum=0;
	for (int iRow=0; iRow<k; iRow++)
	  eSum += ListVect(i,iRow) * eMat(iRow, iCol);
	int res=eSum % 2;
	eVect[iCol]=res;
      }
      /*
      std::cerr << "eVect=";
      for (int i=0; i<k; i++)
	std::cerr << " " << eVect[i];
	std::cerr << "\n";*/
      //
      int pos=GetPositionListVect(eVect, ListVect);
      eListColumn[i]=pos;
      //      std::cerr << " i=" << i << " pos=" << pos << "\n";
    }
    permlib::Permutation ePermColumn(eListColumn);
    ListPermColumn.push_back(ePermColumn);
  }
  //
  // The coding theoretic functionalities
  //
  auto GetBasicCodeMatrix=[&](Face const& LVal) -> MyMatrix<int> {
    int TheSum=0;
    for (int i=0; i<len; i++)
      TheSum += LVal[i];
    MyMatrix<int> CodeMat(len, TheSum);
    int pos=0;
    for (int i=0; i<len; i++)
      if (LVal[i] == 1) {
	std::vector<int> eVectPoss(k);
	for (int iCol=0; iCol<k; iCol++)
	  eVectPoss[iCol]=ListVect(i,iCol);
	for (int iWord=0; iWord<nbWord; iWord++) {
	  int eSum=0;
	  for (int iCol=0; iCol<k; iCol++)
	    eSum += eVectPoss[iCol]*ListVect(iWord,iCol);
	  int res=eSum % 2;
	  CodeMat(iWord,pos)=res;
	}
	pos++;
      }
    return CodeMat;
  };
  auto IsCorrectParityValues=[&](Face const& LVal) -> bool {
    MyMatrix<int> CodeMat=GetBasicCodeMatrix(LVal);
    return IsCodeMatrixLCD(CodeMat);
  };
  //
  // Computing total number of entries
  // Then iterating over possible sizes.
  //

  std::vector<ulong> ListParity;
  for (uint siz=0; siz<=uint(len); siz++) {
    //
    // Computing the binomial mappings
    //
    ulong eBin=BinomialCoefficient<ulong,int>(len,int(siz));
    std::cerr << "---------------------------------------------------------\n";
    std::cerr << "len=" << len << " siz=" << siz << "\n";
    std::cerr << "eBin=" << eBin << " ePow=" << ePow << "\n";
    std::vector<ulong> BinomialToIndex(eBin);
    std::vector<ulong> IndexToBinomial(ePow);
    BlockIteration01 BlIter(len);
    ulong posTot=0;
    ulong posBin=0;
    while(true) {
      Face LVal=BlIter.GetFace();
      //      ulong eVal=FaceToUnsignedLong(LVal);
      if (LVal.count() == siz) {
	//	std::cerr << "posBin=" << posBin << " posTot=" << posTot << "\n";
	BinomialToIndex[posBin]=posTot;
	IndexToBinomial[posTot]=posBin;
	posBin++;
      }
      posTot++;
      int val=BlIter.IncrementShow();
      if (val == -1)
	break;
    }
    std::cerr << "BinomialToIndex / IndexToBinomial have been built\n";
    //
    // The function that compute adjacent elements.
    //
    std::function<std::vector<ulong>(ulong const&)> GetAdjacent=[&](ulong const& posBinI) -> std::vector<ulong> {
      std::vector<ulong> ListRet;
      ulong posTotB=BinomialToIndex[posBinI];
      for (auto& eGen : ListPermColumn) {
	Face eVect(len);
	Face eFaceIn=UnsignedLongToFace(len, posTotB);
	//      std::cerr << "eVal=" << eFix.ListParity[iParity] << "\n";
	for (int i=0; i<len; i++) {
	  int j=eGen.at(i);
	  eVect[j]=eFaceIn[i];
	}
	ulong NewPosTot=FaceToUnsignedLong(eVect);
	ulong NewPosBin=IndexToBinomial[NewPosTot];
	ListRet.push_back(NewPosBin);
      }
      return ListRet;
    };
    //
    // The call to the generic function
    //
    std::cerr << "Before call to ListRepresentativeConnectedComponent\n";
    std::vector<ulong> ListRepr=ListRepresentativeConnectedComponent<ulong,EncapsDynamicBitset,DoubleList<ulong>>(eBin, GetAdjacent);
    std::cerr << "After mighty call\n";
    std::cerr << "|ListRepr|=" << ListRepr.size() << "\n";
    //
    // Now inserting the right orbits
    //
    for (auto &eValBin : ListRepr) {
      ulong eValPos=BinomialToIndex[eValBin];
      Face eFaceIn=UnsignedLongToFace(len, eValPos);
      //      std::cerr << "eVal=" << eValPos << " eFaceIn=";
      //      WriteFaceGAP(std::cerr, eFaceIn);
      //      std::cerr << "\n";
      bool test=IsCorrectParityValues(eFaceIn);
      if (test)
	ListParity.push_back(eValPos);
    }
    std::cerr << "|ListParity|=" << ListParity.size() << "\n";
  }
  std::cerr << "Final |ListParity|=" << ListParity.size() << "\n";
  return {k, ListVect, ListParity};
}




#endif
