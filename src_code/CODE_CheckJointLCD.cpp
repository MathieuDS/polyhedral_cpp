#include "CODE_LCDcode.h"
int main(int argc, char *argv[])
{
  try {
    std::vector<std::vector<int> > TheReturn;
    VectVectInt TheOutput;
    if (argc != 3) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "CheckJointLCD [CMAT] [CPERP]\n";
      std::cerr << "\n";
      std::cerr << "CMAT : generating matrix of C\n";
      std::cerr << "CPERP: generating matrix of Cperp\n";
      return -1;
    }
    std::cerr << "Reading C\n";
    std::ifstream is1(argv[1]);
    MyMatrix<int> eCode=ReadMatrix<int>(is1);
    //
    std::cerr << "Reading Cperp\n";
    std::ifstream is2(argv[2]);
    MyMatrix<int> eCodePerp=ReadMatrix<int>(is2);
    //
    MyVector<mpq_class> eVectTotal=ComputeAndCheckRepresentativeVector<mpq_class>(eCode, eCodePerp);
    int len=eVectTotal.size();
    for (int i=0; i<len; i++)
      std::cerr << "i=" << i << " val=" << eVectTotal(i) << "\n";
    //
    std::cerr << "Completion of the program\n";
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
