#include "Metric.h"

int main(int argc, char *argv[])
{
  try {
    if (argc != 2) {
      fprintf(stderr, "Number of argument is = %d\n", argc);
      fprintf(stderr, "This program is used as\n");
      fprintf(stderr, "TestMetricAlgo [inputMat]\n");
      return -1;
    }
    // reading the matrix
    std::ifstream INmat(argv[1]);
    MyMatrix<double> Coord=ReadMatrix<double>(INmat);
    std::cerr << "Read Coord rows/cols = " << Coord.rows() << " / " << Coord.cols() << "\n";
    //
    TestAdd1(Coord);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
