#include "Metric.h"

int main(int argc, char *argv[])
{
  try {
    if (argc != 3) {
      fprintf(stderr, "Number of argument is = %d\n", argc);
      fprintf(stderr, "This program is used as\n");
      fprintf(stderr, "GeneralProgEmbedding [inputEmbedCase] [outResult]\n");
      return -1;
    }
    // reading the matrix
    std::ifstream IN(argv[1]);
    MyMatrix<double> CoordMat=ReadMatrix<double>(IN);
    MyVector<double> ListDist=ReadVector<double>(IN);
    MyVector<double> ListDistError=ReadVector<double>(IN);
    double Zmeas, ZerrorInput;
    IN >> Zmeas;
    IN >> ZerrorInput;
    ResultGeoloc_absolute resGeoloc=Geoloc_AbsoluteComputation(CoordMat, ListDist, ListDistError, Zmeas, ZerrorInput);
    std::ofstream OUT(argv[2]);
    OUT << resGeoloc.sphCoord.r;
    WriteVector<double>(OUT,resGeoloc.sphCoord.x);
    OUT << resGeoloc.RawRadius;
    WriteVector<double>(OUT,resGeoloc.BestFit);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
