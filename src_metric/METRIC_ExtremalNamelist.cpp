#include "Metric.h"

int main(int argc, char *argv[])
{
  try {
    FullNamelist eFull=NAMELIST_GetExtremalAbsolute();
    if (argc != 2) {
      fprintf(stderr, "Number of argument is = %d\n", argc);
      fprintf(stderr, "This program is used as\n");
      fprintf(stderr, "MetrixExtremalNamelist [namelistFile]\n");
      NAMELIST_WriteNamelistFile(std::cerr, eFull);
      return -1;
    }
    std::string eFileName(argv[1]);
    NAMELIST_ReadNamelistFile(eFileName, eFull);
    EnumerationExtremalPointsAbsolute(eFull);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
