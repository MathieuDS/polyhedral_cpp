#include "Metric.h"

int main(int argc, char *argv[])
{
  try {
    FullNamelist eFull=NAMELIST_GetEmbeddingGeneralized();
    if (argc != 2) {
      fprintf(stderr, "Number of argument is = %d\n", argc);
      fprintf(stderr, "This program is used as\n");
      fprintf(stderr, "MetricAlgoNamelist [inputMat]\n");
      NAMELIST_WriteNamelistFile(std::cerr, eFull);
      return -1;
    }
    std::string eFileName(argv[1]);
    NAMELIST_ReadNamelistFile(eFileName, eFull);
    TestAdd1Generalized(eFull);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
