#ifndef INCLUDE_CODE_METRIC_GLPK
#define INCLUDE_CODE_METRIC_GLPK

#include "Metric.h"
#include "POLY_LinearProgramming_GLPK.h"

MyMatrix<double> FindSmallestMetricPerturbation(MyMatrix<double> const& DistMat)
{
  int n=DistMat.rows();
  int nbDist=n*(n-1)/2;
  MyMatrix<int> MatIdx(n,n);
  int idx=0;
  for (int i=0; i<n; i++) {
    for (int j=i+1; j<n; j++) {
      MatIdx(i,j)=idx;
      MatIdx(j,i)=idx;
      idx++;
    }
  }
  using T2=Eigen::Triplet<double>;
  std::vector<double> ListVal;
  std::vector<T2> ListTriplet;
  idx=0;
  for (int i=0; i<n; i++)
    for (int j=i+1; j<n; j++) {
      for (int k=0; k<n; k++)
	if (i != k && j != k) {
	  double Hijk=DistMat(i,j) - DistMat(i,k) - DistMat(k,j);
	  ListVal.push_back(Hijk);
	  int PosIJ=MatIdx(i,j);
	  int PosIK=MatIdx(i,k);
	  int PosKJ=MatIdx(j,k);
	  T2 eTr(idx, PosIJ, -double(1));
	  T2 fTr(idx, PosIK, double(1));
	  T2 gTr(idx, PosKJ, double(1));
	  ListTriplet.push_back(eTr);
	  ListTriplet.push_back(fTr);
	  ListTriplet.push_back(gTr);
	  idx++;
	}
    }
  for (int iEnt=0; iEnt<nbDist; iEnt++) {
    ListVal.push_back(double(0));
    T2 eTr1(idx,iEnt,double(1));
    T2 fTr1(idx,iEnt+nbDist,double(1));
    ListTriplet.push_back(eTr1);
    ListTriplet.push_back(fTr1);
    idx++;
    ListVal.push_back(double(0));
    T2 eTr2(idx,iEnt,-double(1));
    T2 fTr2(idx,iEnt+nbDist,double(1));
    ListTriplet.push_back(eTr2);
    ListTriplet.push_back(fTr2);
    idx++;
  }
  int nbIneq=idx;
  int dimProb=2*nbDist;
  MySparseMatrix<double> Aspmat(nbIneq, dimProb);
  std::cerr << "We have Aspmat\n";
  Aspmat.setFromTriplets(ListTriplet.begin(), ListTriplet.end());
  std::cerr << "Aspmat insert ListTriplets\n";
  MyVector<double> ListAconst=VectorFromStdVector(ListVal);
  std::cerr << "We have ListAconst\n";
  //
  int nbEqua=0;
  MySparseMatrix<double> Bspmat(nbEqua, dimProb);
  MyVector<double> ListBconst(nbEqua);
  std::cerr << "We have ListBconst\n";
  //
  MyVector<double> ToBeMinimized(dimProb);
  for (int i=0; i<nbDist; i++)
    ToBeMinimized(i)=0;
  for (int i=0; i<nbDist; i++)
    ToBeMinimized(nbDist+i)=1;
  LpSolutionSimple<double> eSol = GLPK_LinearProgramming_Kernel_Sparse_LIBRARY(Aspmat, ListAconst, Bspmat, ListBconst, ToBeMinimized);
  std::cerr << "After GLPK_LinearProgramming_Kernel_LIBRARY\n";
  MyMatrix<double> RetDistMat = DistMat;
  idx=0;
  for (int i=0; i<n; i++)
    for (int j=i+1; j<n; j++) {
      double eVal=eSol.DirectSolution(idx);
      std::cerr << "i=" << i << " j=" << j << " eVal=" << eVal << "\n";
      RetDistMat(i,j) += eVal;
      RetDistMat(j,i) += eVal;
      idx++;
    }
  std::cerr << "Just before return GLPK_LinearProgramming_Kernel_LIBRARY\n";
  return RetDistMat;
}



MyMatrix<double> FindMetricPerturbation_General(MyMatrix<double> const& DistMat)
{
  bool test;
  MyMatrix<double> RetMat=FindMetricPerturbation_Combinatorial_Kernel(DistMat, test);
  if (test)
    return RetMat;
  else
    return FindSmallestMetricPerturbation(DistMat);
}




#endif
