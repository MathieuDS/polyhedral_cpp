#include "Metric.h"
#include "Metric_GLPK.h"

int main(int argc, char *argv[])
{
  try {
    if (argc != 4) {
      fprintf(stderr, "Number of argument is = %d\n", argc);
      fprintf(stderr, "This program is used as\n");
      fprintf(stderr, "METRIC_Geoloc_Relative_Iterate [CanonicPos] [inputEmbedCase] [outResult]\n");
      return -1;
    }
    std::ifstream INpos(argv[1]);
    MyMatrix<double> CoordCan=ReadMatrix<double>(INpos);
    MyMatrix<double> DistMatCan=ComputeDistanceMatrix(CoordCan);
    // reading the matrix
    std::ifstream IN(argv[2]);
    std::ofstream OUT(argv[3]);
    int nbEnt=0;
    bool IsFirst=true;
    MyMatrix<double> PreviousCoordinate;
    int dim=3;
    std::vector<int> TopolInfoPrevious;
    std::vector<int> SumChangeDeterminant;
    bool HasPrevious=false;
    while(1) {
      int status;
      IN >> status;
      nbEnt++;
#ifdef DEBUG
      std::cerr << "status=" << status << " nbEnt=" << nbEnt << "\n";
#endif
      if (status == 0)
	break;
      std::cerr << "-------------------------------------------------------------------\n";
      MyMatrix<double> DistMat_meas=ReadMatrix<double>(IN);
      MyMatrix<double> DistMat_error=ReadMatrix<double>(IN);
      MyVector<double> Zposition=ReadVector<double>(IN);
      MyVector<double> Zerror=ReadVector<double>(IN);
      std::cerr << "Zposition=";
      WriteVector(std::cerr, Zposition);
      std::cerr << "Zerror=";
      WriteVector(std::cerr, Zerror);
      std::cerr << "DistMatCan=\n";
      WriteMatrix(std::cerr, DistMatCan);
      std::cerr << "DistMat_meas=\n";
      WriteMatrix(std::cerr, DistMat_meas);
      //MyMatrix<double> DistMat_meas_post1=FindSmallestMetricPerturbation(DistMat_meas);
      std::cerr << "Eigenvalues of input distance matrix\n";
      PrintEigenvaluesDistanceMatrix(std::cerr, DistMat_meas);
      int OptionDistCorrection=1;
      MyMatrix<double> DistMat_meas_post1;
      if (OptionDistCorrection == 1) {
	DistMat_meas_post1 = FindMetricPerturbation_Combinatorial(DistMat_meas);
      }
      if (OptionDistCorrection == 2) {
	DistMat_meas_post1 = FindSmallestMetricPerturbation(DistMat_meas);
      }
      
      
      MyMatrix<double> DiffMat = DistMat_meas_post1 - DistMat_meas;
      std::cerr << "DiffMat=\n";
      WriteMatrix(std::cerr, DiffMat);
      RecOptionInitial RecOptIni;
      RecOptIni.UsePreviousPosition=true;
      RecOptIni.UseEigenvalueGuess=false;
      ResultGeoloc_relative resGeoloc=Geoloc_RelativeComputation(DistMat_meas_post1, DistMat_error, Zposition, Zerror, IsFirst, PreviousCoordinate, RecOptIni);
      auto GetCoord_and_UpdateStatus=[&](MyMatrix<double> const& CoordCompute) -> MyMatrix<double> {
#ifdef DEBUG
	std::cerr << "PASS GetCoord_and_UpdateStatus, step 1\n";
#endif
	if (IsFirst) {
	  IsFirst=false;
	  PreviousCoordinate=CoordCompute;
#ifdef DEBUG
	  std::cerr << "PASS GetCoord_and_UpdateStatus, step 2\n";
	  std::cerr << "PASS |PreviousCoordinate|=" << StringSizeMatrix(PreviousCoordinate) << " 1\n";
#endif
	  return CoordCompute;
	}
	else {
#ifdef DEBUG
	  std::cerr << "PASS GetCoord_and_UpdateStatus, step 3\n";
	  std::cerr << "PASS |PreviousCoordinate|=" << StringSizeMatrix(PreviousCoordinate) << " 2\n";
#endif
	  MyMatrix<double> CoordRet=FindEmbeddingEquivalence_General(PreviousCoordinate, CoordCompute, dim-1);
#ifdef DEBUG
	  std::cerr << "PASS |CoordRet|=" << StringSizeMatrix(CoordRet) << " 1\n";
	  std::cerr << "PASS GetCoord_and_UpdateStatus, step 4\n";
#endif
	  PreviousCoordinate=CoordRet;
#ifdef DEBUG
	  std::cerr << "PASS GetCoord_and_UpdateStatus, step 5\n";
#endif
	  return CoordRet;
	}
      };
      std::cerr << "nbEnt=" << nbEnt << "\n";
      std::cerr << "resGeoloc.BestFit=\n";
      WriteMatrix(std::cerr, resGeoloc.BestFit);
      MyMatrix<double> Coord=GetCoord_and_UpdateStatus(resGeoloc.BestFit);
      //
      // Now printing data interpretation
      //
      std::cerr << "Coord interpretation\n";
      std::vector<int> TopolInfo=GetTopologicalInformation(Coord, 2);
      if (HasPrevious) {
	int siz=TopolInfoPrevious.size();
	if (SumChangeDeterminant.size() == 0)
	  SumChangeDeterminant.resize(siz,0);
	for (int i=0; i<siz; i++)
	  SumChangeDeterminant[i] += abs(TopolInfo[i] - TopolInfoPrevious[i]);
	std::cerr << "TopolInfo        =";
	WriteStdVector(std::cerr, TopolInfo);
	std::cerr << "TopolInfoPrevious=";
	WriteStdVector(std::cerr, TopolInfoPrevious);
	std::cerr << "SumChangeDeterminant=";
	WriteStdVector(std::cerr, SumChangeDeterminant);
      }
      HasPrevious=true;
      TopolInfoPrevious=TopolInfo;
      std::cerr << "TopolInfo=";
      WriteStdVector(std::cerr, TopolInfo);
      MyMatrix<double> DistMatCoord=ComputeDistanceMatrix(Coord);
      MatrixDist MDistA=ComputeNorms(DistMatCoord, DistMat_meas);
      std::cerr << "MDistA = " << MDistA << "\n";
      MatrixDist MDistB=ComputeNorms(DistMatCan, DistMat_meas);
      std::cerr << "MDistB = " << MDistB << "\n";
	
	
      int iRefPoint=0;
      SetReferencePoint(Coord, iRefPoint, dim-1);
      std::cerr << "Coord=\n";
      WriteMatrix(std::cerr, Coord);
      //      PrintAllDeterminants(std::cerr, Coord, 2);
#ifdef DEBUG      
      std::cerr << "Before writing\n";
#endif
      OUT << "------------ SolutionFound ------------\n";
      
      WriteMatrix<double>(OUT,resGeoloc.BestFit);
#ifdef DEBUG      
      std::cerr << "After writing\n";
#endif
      IsFirst=false;
    }
    std::cerr << "SumChangeDeterminant=";
    WriteStdVector(std::cerr, SumChangeDeterminant);
    
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
