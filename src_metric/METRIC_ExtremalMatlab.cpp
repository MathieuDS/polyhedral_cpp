#include "Metric.h"

int main(int argc, char *argv[])
{
  try {
    if (argc != 3) {
      fprintf(stderr, "Number of argument is = %d\n", argc);
      fprintf(stderr, "This program is used as\n");
      fprintf(stderr, "MetricExtremalMatlab [inputEmbedCase] [outListExtr]\n");
      return -1;
    }
    // reading the matrix
    std::ifstream is(argv[1]);
    int nbPoint;
    is >> nbPoint;
    std::vector<SingleMeas> ListSingleMeas(nbPoint);
    for (int iPoint=0; iPoint<nbPoint; iPoint++) {
      int nature;
      double error;
      double meas;
      is >> nature;
      is >> error;
      is >> meas;
      MyVector<double> xPos=ReadVector<double>(is);
      ListSingleMeas[iPoint]={nature, error, meas, xPos};
    }
    std::vector<MyVector<double>> ListExtremal=EnumerateExtremalPossiblePoints(ListSingleMeas);
    MyMatrix<double> MatExtremal=MatrixFromVectorFamily(ListExtremal);
    std::ofstream os(argv[2]);
    WriteMatrixMatlab(os, MatExtremal);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
