#ifndef INCLUDE_CODE_METRIC
#define INCLUDE_CODE_METRIC

#include "MAT_Matrix.h"
#include "COMB_Combinatorics_elem.h"
#include "Namelist.h"

/*
  The metric matrix entries satisfy Aij = ||vi - vj||^2
  We fix v0 = 0
  We want the Gram matrix formed by b_{ij} = v_{i+1} . v_{j+1}
  This gives us A_{i+1,j+1} = b_{i,i} + b_{j,j} - 2*b_{i,j}
                  --
  First step of the construction is to find the diagonal
  coefficients of the Gram matrix.
  b_{i,i} = v_{i+1} . v_{i+1}
          = || v_{i+1} ||^2
          = || v_{i+1} - v_0 ||^2
          = A_{i+1,0}
                  --
  Second step uses the computation of the diagonal coefficients
  in order to find non-diagonal coefficients:
  b_{i,j} = { b_{i,i} + b_{j,j} - A_{i+1,j+1} }/2
*/
template<typename T>
MyMatrix<T> MetricMatrixToGramMatrix(MyMatrix<T> const& DistMat)
{
  int nbPoint=DistMat.rows();
  MyMatrix<double> DistMatSqr(nbPoint,nbPoint);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      double eVal=DistMat(i,j);
      DistMatSqr(i,j)=eVal*eVal;
    }
  int n=nbPoint - 1;
  MyMatrix<T> GramMat(n,n);
  for (int i=0; i<n; i++)
    GramMat(i,i)=DistMatSqr(i+1,0);
  for (int i=0; i<n; i++) 
    for (int j=0; j<n; j++)
      if (i != j) {
	T bii=GramMat(i,i);
	T bjj=GramMat(j,j);
	T aiP1jP1=DistMatSqr(i+1,j+1);
	T bij=(bii + bjj - aiP1jP1)/2;
	GramMat(i,j)=bij;
      }
  return GramMat;
}


/*
  Suppose that the matrix of distances DistMat is exact
  Then let's consider the corresponding Gram matrix eG.
  The rank of eG is then equal to the dimension of the space in which
  it is embedded, i.e. dim (and equal to 3 in most cases)
                       --
  Since the rank is d and A is a symmetric matrix, it has just
  d non-zero eigenvalues. An embedding can then be recovered from
  the eigenvectors of those eigenvalues
                       --
  Suppose now that DistMat is perturbed. Then the Gram matrix is perturbed
  as well. However, if the perturbation is sufficiently small, then the eigenvalues
  should have the following structure:
  ---d eigenvalues which are relatively large
  ---n - d eigenvalues that are clustered around 0 and can be negative.
                       --  
  Therefore, if one selects the dim largest eigenvalues and take their eigenvectors
  then one recovers an approximate embedding
*/
MyMatrix<double> EigenvalueGuess(MyMatrix<double> const& DistMat, int const& dim)
{
# ifdef DEBUG  
  int nbRow=DistMat.rows();
  int nbCol=DistMat.cols();
  std::cerr << "nbRow=" << nbRow << " nbCol=" << nbCol << "\n";
# endif
  int nbPoint=DistMat.rows();
  MyMatrix<double> eG=MetricMatrixToGramMatrix(DistMat);
  Eigen::SelfAdjointEigenSolver<MyMatrix<double>> eig(eG);
  MyVector<double> ListEig=eig.eigenvalues();
  MyMatrix<double> ListVect=eig.eigenvectors();
  MyMatrix<double> Coord(nbPoint,dim);
# ifdef DEBUG  
  for (int i=0; i<nbPoint-1; i++)
    std::cerr << "i=" << i << " eig=" << ListEig(i) << "\n";
# endif
  for (int i=0; i<dim; i++)
    Coord(0,i)=0;
  for (int i=0; i<dim; i++) {
    int pos=nbPoint-1-dim+i;
    double eVal=ListEig(pos);
    if (eVal < 0)
      eVal=0;
# ifdef DEBUG  
    std::cerr << "i=" << i << " pos=" << pos << " eig=" << eVal << " eVal=" << eVal << "\n";
# endif
    for (int iPt=0; iPt<nbPoint-1; iPt++) {
      double eX=ListVect(iPt,pos);
      Coord(iPt+1,i)=sqrt(eVal)*eX;
    }
  }
  return Coord;
}


/*
  For DEBUGGING purposes.
  This function allows to compute topological information about a configuration of points.
  ---Coord is of size (nbPoint,dim) so for example 5 points in dimension 3.
  ---soughtDim is the sought dim for computation. Value 3 is for 3 dimensional.
     value 2 is for taking only x and y.
  In output we have
  ---a std::vector of signs that specifies the relative positions of the simplices.
  It is useful for the configuration of 5 points on the plane.
  With soughDim=2 we get 10 signs and this allows to detect really well the inversions
  of configuration
*/
std::vector<int> GetTopologicalInformation(MyMatrix<double> const& Coord, int const& soughtDim)
{
  int nbPoint=Coord.rows();
  int iterSize=soughtDim + 1;
  std::vector<int> eList=BinomialStdvect_First(iterSize);
  std::vector<int> TopolInfo;
  while(true) {
    MyMatrix<double> TestMat(iterSize, iterSize);
    for (int i=0; i<iterSize; i++) {
      TestMat(i,0)=1;
      int eIdx=eList[i];
      for (int j=0; j<soughtDim; j++)
	TestMat(i,j+1)=Coord(eIdx,j);
    }
    double det=TestMat.determinant();
    int eSign=0;
    if (det > 0)
      eSign=1;
    if (det < 0)
      eSign=-1;
    TopolInfo.push_back(eSign);
    bool test=BinomialStdvect_Increment(nbPoint, iterSize, eList);
    if (!test)
      break;
  }
  return TopolInfo;
}



void PrintEigenvaluesDistanceMatrix(std::ostream & os, MyMatrix<double> const& DistMat)
{
  int nbPoint=DistMat.rows();
  MyMatrix<double> GramMat=MetricMatrixToGramMatrix(DistMat);
  Eigen::SelfAdjointEigenSolver<MyMatrix<double>> eig(GramMat);
  MyVector<double> ListEig=eig.eigenvalues();
  for (int i=0; i<nbPoint-1; i++) {
    double eVal=ListEig(i);
    os << "i=" << i << " eig=" << eVal << "\n";
  }
}




/*
  Suppose that we have dim points in dimension dim (so for example 3 points in dimension 3)
  This is stored in the variable Coord
  Suppose that we know the distances between points P and the points of Coord (vectDist)
  Then we can compute the possible positions of the point P to those points
  The possible number of solutions is
  --- 0 (for example vectDist is identically 0)
  --- 1 (P is placed at the center of the 3 points)
  --- 2 (most frequent case)
  However, one important point should be noted:
  The numbr of solutions returned is always 2.
  If the number of solutions is 2 then it is the 2 solutions
  If the number of solutions is 1 then it is the single solution duplicated
  If the number of solutions is 0 then the solution is the center of the dim points considered
*/
std::vector<MyVector<double>> trilateration(MyMatrix<double> const& Coord, MyVector<double> const& vectDist)
{
  int dim=Coord.cols();
# ifdef DEBUG
  int nbFixedPoint=Coord.rows();
  if (nbFixedPoint != dim) {
    std::cerr << "nbFixedPoint=" << nbFixedPoint << " dim=" << dim << "\n";
    std::cerr << "We do trilateration only for nbFixedPoint = dim\n";
    throw TerminalException{1};
  }
# endif
  int nbRow=Coord.rows();
  std::cerr << "nbRow=" << nbRow << " dim=" << dim << "\n";
  for (int iRow=0; iRow<nbRow; iRow++)
    for (int iCol=0; iCol<dim; iCol++) {
      std::cerr << "iRow=" << iRow << " iCol=" << iCol << "\n";
      std::cerr << "    val=" << Coord(iRow, iCol) << "\n";
    }
  int dimSyst=dim-1;
  MyMatrix<double> eG=ZeroMatrix<double>(dimSyst,dimSyst);
  for (int i=0; i<dimSyst; i++)
    for (int j=0; j<dimSyst; j++) {
      double sum=0;
      for (int iCol=0; iCol<dim; iCol++) {
	double delta1=Coord(i+1,iCol) - Coord(0,iCol);
	double delta2=Coord(j+1,iCol) - Coord(0,iCol);
	sum += delta1*delta2;
      }
      eG(i,j) = 2*sum;
      }
  MyVector<double> b(dimSyst);
  for (int i=0; i<dimSyst; i++) {
    double d1=vectDist(i+1);
    double d2=vectDist(0);
    double sum=-d1*d1 + d2*d2;
    for (int iCol=0; iCol<dim; iCol++) {
      double delta=Coord(i+1,iCol) - Coord(0,iCol);
      sum += delta*delta;
    }
    b(i)=sum;
  }
  Eigen::FullPivLU<MyMatrix<double>> solver;
  solver.compute(eG);
  MyVector<double> eSol=solver.solve(b);
  MyVector<double> x(dim);
  for (int i=0; i<dim; i++) {
    double sum=Coord(0,i);
    for (int j=0; j<dimSyst; j++) {
      double val=Coord(j+1,i) - Coord(0,i);
      sum += val*eSol(j);
    }
    x(i)=sum;
  }
  MyVector<double> ListXdistSqr(dim);
  for (int iPt=0; iPt<dim; iPt++) {
    double sum=0;
    for (int iCol=0; iCol<dim; iCol++) {
      double val=x(iCol) - Coord(iPt,iCol);
      sum += val*val;
    }
    ListXdistSqr(iPt)=sum;
  }
  MyVector<double> ListDeltaDist(dim);
  for (int iPt=0; iPt<dim; iPt++) {
    double val=vectDist(iPt);
    double sum=val*val - ListXdistSqr(iPt);
    ListDeltaDist(iPt)=sum;
  }
  double sumDelta=0;
  for (int iPt=0; iPt<dim; iPt++)
    sumDelta += ListDeltaDist(iPt);
  sumDelta /= double(dim);
  double sumTotalError=0;
  for (int iPt=0; iPt<dim; iPt++) {
    double val=sumDelta - ListDeltaDist(iPt);
    sumTotalError += T_abs(val);
  }
  MyMatrix<double> Mspace(dim, dimSyst);
  for (int i=0; i<dim; i++)
    for (int iVect=0; iVect<dimSyst; iVect++)
      Mspace(i,iVect)=Coord(iVect+1,i) - Coord(0,i);
  MyVector<double> eOrth = OrthogonalHyperplane(Mspace);
  double sum=0;
  for (int i=0; i<dim; i++) {
    double val=eOrth(i);
    sum += val*val;
  }
  double tol=0.0001;
  if (sum < tol) {
    MyVector<double> eVect1=ZeroVector<double>(dim);
    MyVector<double> eVect2=ZeroVector<double>(dim);
    return {eVect1, eVect2};
  }
  eOrth /= sqrt(sum);
  double alpha;
  if (sumDelta < 0)
    alpha=0;
  else
    alpha = sqrt(sumDelta);
  MyVector<double> Coord1 = x + alpha*eOrth;
  MyVector<double> Coord2 = x - alpha*eOrth;
#if defined DEBUG
  auto SumError=[&](MyVector<double> const& CoordOut) -> double {
    double sumError=0;
    for (int iPt=0; iPt<dim; iPt++) {
      double sum=0;
      for (int i=0; i<dim; i++) {
	double delta=CoordOut(i) - Coord(iPt,i);
	sum += delta*delta;
      }
      double disc=vectDist[iPt] - sqrt(sum);
      sumError += T_abs(disc);
    }
    return sumError;
  };
  std::cerr << "trilateration, step 10\n";
  std::cerr << "Coord1 sumError=" << SumError(Coord1) << "\n";
  std::cerr << "Coord2 sumError=" << SumError(Coord2) << "\n";
  std::cerr << "Exiting trilateration\n";
#endif
  return {Coord1, Coord2};
}


/*
  This code uses a matrix of distances MatDist
  and builds a possible embedding.
  --
  It is distinct from the eigenvalue embedding in that we consider only dim+1 points
  in dimension dim.
  --
  We have the same limitation as for trilateration. The matrix may not be realizable in
  which case the embedding matrix is erroneous, or approximate depending on viewpoint
*/
MyMatrix<double> CoordinatesFromSimplicialMatrix(MyMatrix<double> const& MatDist)
{
  int nbVert=MatDist.rows();
  int dim=nbVert-1;
  if (nbVert == 2) {
    double a=MatDist(0,1);
    MyMatrix<double> A=ZeroMatrix<double>(2,1);
    A(1,0)=a;
    return A;
  }
  MyMatrix<double> MatDistRed(nbVert-1,nbVert-1);
  for (int i=0; i<nbVert-1; i++)
    for (int j=0; j<nbVert-1; j++)
      MatDistRed(i,j)=MatDist(i,j);
  MyMatrix<double> CoordRed=CoordinatesFromSimplicialMatrix(MatDistRed);
  MyMatrix<double> Coord=ZeroMatrix<double>(nbVert-1,dim);
  for (int i=0; i<nbVert-1; i++)
    for (int j=0; j<dim-1; j++)
      Coord(i,j)=CoordRed(i,j);
  MyVector<double> vectDist(nbVert-1);
  for (int i=0; i<nbVert-1; i++)
    vectDist(i)=MatDist(nbVert-1,i);
  /*
  std::cerr << "Coord=\n";
  WriteMatrix(std::cerr, Coord);
  std::cerr << "vectDist=\n";
  WriteVector(std::cerr, vectDist);*/
  std::vector<MyVector<double>> ListVect=trilateration(Coord, vectDist);
  MyMatrix<double> CoordRet=ZeroMatrix<double>(nbVert,dim);
  for (int i=0; i<nbVert-1; i++)
    CoordRet.row(i) = Coord.row(i);
  for (int j=0; j<dim; j++)
    CoordRet(nbVert-1,j)=ListVect[0](j);
  return CoordRet;
}

/*
  Form an elementary rotation matrix.
  the matrix is of dimension dim.
  Rotation is for coordinates i and j.
*/
MyMatrix<double> ElementaryOrthogonalMatrix(int const& dim, int const&i, int const&j, double const& ang)
{
  MyMatrix<double> Omat=IdentityMat<double>(dim);
  Omat(i,i)=cos(ang);
  Omat(i,j)=sin(ang);
  Omat(j,i)=-sin(ang);
  Omat(j,j)=cos(ang);
  return Omat;
}

/*
  The classical Euler angles in dimension 3.
*/
MyMatrix<double> EulerAngleOrthogonalMatrix3(double const& ang1, double const& ang2, double const& ang3)
{
  MyMatrix<double> O1=ElementaryOrthogonalMatrix(3,1,2,ang1);
  MyMatrix<double> O2=ElementaryOrthogonalMatrix(3,0,2,ang2);
  MyMatrix<double> O3=ElementaryOrthogonalMatrix(3,0,1,ang3);
  return O1*O2*O3;
}



/*
  This is a general rotation in dimension n.
  The target is essentially the last coordinate n-1 because we seek
  to diminish the error in the Z-coordinate
  eMult is the sign needed for matrix operations of determinant -1.
*/
MyMatrix<double> EulerAngleOrthogonalMatrix_vect(double const& eMult, MyVector<double> const& V)
{
  int dim=V.rows();
  int n=dim+1;
  MyMatrix<double> Omat=IdentityMat<double>(n);
  Omat(n-1,n-1)=eMult;
  for (int i=0; i<dim; i++)
    Omat *= ElementaryOrthogonalMatrix(n,i,n-1,V(i));
  return Omat;
}



/*
  This is the general function for reducing the error in the distance matrix.
  We look at the distances that can be identified as too high.
  Input:
  ---Distance matrix
  ---test value (but as a reference, actually output)
  Output:
  ---Corrected distance matrix
  Basics: A correct distance matrix must satisfy d(a,c) <= d(a,b) + d(b,c)
  If we find ourseves in the situation where
  d(a,c) > d(a,b) + d(b,c) then we know that we have a problem.
  It can be
  --- d(a,b) and/or d(b,c) too low.
  --- d(a,c) too high (most likely)
  Given n points there are n(n-1)/2 * (n-2) different triangle inequalities.
  We iterate over all of them and record each violation. A violation leads to
  some inequality indicated as too low and some as too high.
  :
  Then if some inequality is indicated as too high AND never indicated as too low
  then we decrease it.
  :
  test is set to true if those distance decreases led to a feasible distance matrix.
 */
MyMatrix<double> FindMetricPerturbation_Combinatorial_Kernel(MyMatrix<double> const& DistMat, bool &test)
{
  int n=DistMat.rows();
  double epsilon = 0.0001;
  int nbDist=n*(n-1)/2;
  MyMatrix<int> MatIndex(n,n);
  int idx=0;
  for (int i=0; i<n; i++)
    for (int j=i+1; j<n; j++) {
      MatIndex(i,j)=idx;
      MatIndex(j,i)=idx;
      idx++;
    }
  /*
  std::cerr << "DistMat=\n";
  WriteMatrix(std::cerr, DistMat);*/
  MyMatrix<double> RetDistMat = DistMat;
  int iter=0;
  while(1) {
    iter++;
    //    std::cerr << "Passing here iter=" << iter << "\n";
    std::vector<int> ListIncrease(nbDist,0), ListDecrease(nbDist,0);
    std::vector<double> MaxMagIncrease(nbDist,0), MaxMagDecrease(nbDist,0);
    for (int i=0; i<n; i++)
      for (int j=i+1; j<n; j++)
	for (int k=0; k<n; k++)
	  if (i != k && j != k) {
	    //	    std::cerr << "i=" << i << " j=" << j << " k=" << k << "\n";
	    double deltaDist=RetDistMat(i,k) + RetDistMat(j,k) - RetDistMat(i,j);
	    if (deltaDist < -epsilon) {
	      int PosIJ=MatIndex(i,j);
	      int PosIK=MatIndex(i,k);
	      int PosJK=MatIndex(j,k);
	      ListDecrease[PosIJ]++;
	      ListIncrease[PosIK]++;
	      ListIncrease[PosJK]++;
	      double eCorr=-deltaDist;
	      //	      std::cerr << "eCorr=" << eCorr << "\n";
	      MaxMagDecrease[PosIJ] = std::max(MaxMagDecrease[PosIJ], eCorr);
	      MaxMagIncrease[PosIK] = std::max(MaxMagIncrease[PosIK], eCorr);
	      MaxMagIncrease[PosJK] = std::max(MaxMagIncrease[PosJK], eCorr);
	    }
	  }
    idx=0;
    int nbOper=0;
    for (int i=0; i<n; i++)
      for (int j=i+1; j<n; j++) {
	//	std::cerr << "i=" << i << " j=" << j << " Inc=" << ListIncrease[idx] << " Dec=" << ListDecrease[idx] << " |Inc|=" << MaxMagIncrease[idx] << " |Dec|=" << MaxMagDecrease[idx] << "\n";
	if (ListIncrease[idx] == 0 && ListDecrease[idx] > 0) {
	  RetDistMat(i,j) -= MaxMagDecrease[idx];
	  nbOper++;
	}
	/*
	if (ListIncrease[idx] > 0 && ListDecrease[idx] == 0) {
	  RetDistMat(i,j) -= MaxMagIncrease[idx];
	  nbOper++;
	  }*/
	idx++;
      }
    /*
    std::cerr << "Now RetDistMat=\n";
    WriteMatrix(std::cerr, RetDistMat);*/
    if (nbOper == 0) {
      if (VectorSum(ListIncrease) > 0 && VectorSum(ListDecrease) > 0) {
	test=false;
      }
      else {
	test=true;
      }
      break;
    }
  }
  return RetDistMat;
}


MyMatrix<double> FindMetricPerturbation_Combinatorial(MyMatrix<double> const& DistMat)
{
  bool test;
  return FindMetricPerturbation_Combinatorial_Kernel(DistMat, test);
}






/*
  Given a set of N points in dimension dim of the form v_0, ...., v_{N-1}
  Form the matrix DistMat with
  DistMat_{i,j} = || v_i - v_j ||
*/
MyMatrix<double> ComputeDistanceMatrix(MyMatrix<double> const& Coord)
{
  int nbPoint=Coord.rows();
  int dim=Coord.cols();
  MyMatrix<double> DistMat(nbPoint, nbPoint);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      double sum=0;
      for (int iCol=0; iCol<dim; iCol++) {
	double delta=Coord(i,iCol) - Coord(j,iCol);
	sum += delta*delta;
      }
      DistMat(i,j)=sqrt(sum);
    }
  return DistMat;
}





/*
  Given two matrices DM1 and DM2 form the distance
  sum_{i,j} || DM1_{i,j} - DM2_{i,j} ||^2
*/
double TotalDistance_DistMat(MyMatrix<double> const&DM1, MyMatrix<double> const&DM2)
{
  int nbPoint=DM1.rows();
  double sum=0;
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      double delta=DM1(i,j) - DM2(i,j);
      sum += delta*delta;
    }
  return sum;
}

/*
  return value of pi
*/
double GetPI()
{
  return double(4)*atan(double(1));
}



/*
  This code does z position adjustment.
  INPUT:
  ---CoordFound for the coordinate of the matrix
  ---zPosition for the altitude.
  :
  The code does rotation on all cordinates in order to have
  the z coordinate of CoordFound fitting as good as possible
  the zPosition in input.
 */
MyMatrix<double> PositionAdjustment(MyMatrix<double> const& CoordFound, MyVector<double> const& zPosition)
{
  int nbPoint=CoordFound.rows();
  int dim=CoordFound.cols();
  int NITER=10;
  double TheMid=0;
  for (int iPt=0; iPt<nbPoint; iPt++)
    TheMid += zPosition(iPt);
  TheMid /= double(nbPoint);
  auto GetCoordinateMatrix=[&](double const& eMult, MyVector<double> const& V) -> MyMatrix<double> {
    MyMatrix<double> Omat=EulerAngleOrthogonalMatrix_vect(eMult, V);
    MyMatrix<double> eProd=CoordFound*Omat;
    double TheMidNew=0;
    for (int iPt=0; iPt<nbPoint; iPt++)
      TheMidNew += eProd(iPt,dim-1);
    TheMidNew /= double(nbPoint);
    double TheShift = TheMid - TheMidNew;
    for (int iPt=0; iPt<nbPoint; iPt++)
      eProd(iPt,dim-1) += TheShift;
    return eProd;
  };
  auto EvaluationPosition=[&](MyMatrix<double> const& CoordNew) -> double {
    double cost=0;
    for (int iPt=0; iPt<nbPoint; iPt++) {
      double posNew=CoordNew(iPt,dim-1);
      double posZ  =zPosition(iPt);
      cost += T_abs(posNew - posZ);
    }
    return cost;
  };
  auto ObjectiveFunction=[&](double const& eMult, MyVector<double> const& V) -> double {
    MyMatrix<double> NewCoordinate=GetCoordinateMatrix(eMult, V);
    return EvaluationPosition(NewCoordinate);
  };
  MyVector<double> RefVectAng=ZeroVector<double>(dim-1);
  double RefMult=1;
  double BestCost=ObjectiveFunction(RefMult, RefVectAng);
  for (int iSign=0; iSign<2; iSign++) {
    MyVector<double> VectAng=ZeroVector<double>(dim-1);
    double eMult;
    double CurrentCost=ObjectiveFunction(RefMult, VectAng);
    if (iSign == 0)
      eMult=1;
    else
      eMult=-1;
    int NSub=5;
    double InitShift=2*GetPI();
    double DecShift=1.5;
    for (int iter=0; iter<NITER; iter++) {
      for (int i=0; i<dim-1; i++) {
	MyVector<double> CopyVectAng=VectAng;
	for (int iSub=0; iSub<NSub; iSub++) {
	  MyVector<double> NewVectAng=CopyVectAng;
	  NewVectAng(i) += InitShift*double(iSub+1)/double(NSub);
	  double NewCost=ObjectiveFunction(eMult, NewVectAng);
	  if (NewCost < CurrentCost) {
	    CurrentCost=NewCost;
	    VectAng = NewVectAng;
	  }
	}
      }
      InitShift /= DecShift;
    }
    if (CurrentCost < BestCost) {
      RefVectAng=VectAng;
      RefMult=eMult;
    }
  }
# ifdef DEBUG
  std::cerr << "CoordFound=\n";
  WriteMatrix(std::cerr, CoordFound);
# endif
  MyMatrix<double> DistMatTest1=ComputeDistanceMatrix(CoordFound);
  MyMatrix<double> RetMatrix=GetCoordinateMatrix(RefMult, RefVectAng);
  MyMatrix<double> DistMatTest2=ComputeDistanceMatrix(RetMatrix);
# ifdef DEBUG
  std::cerr << "Error from PositionAdjust = " << TotalDistance_DistMat(DistMatTest1, DistMatTest2) << "\n";
# endif
  return RetMatrix;
}








/*
  Given an embedding Coord and a distance matrix DistMat we compute
  the cost functional
  sum_{i<j} { DM_{i,j} - || v_i - v_j|| }^2
*/
double CostFunctional(MyMatrix<double> const& DistMat, MyMatrix<double> const& Coord)
{
  int nbPoint=Coord.rows();
  int dim=Coord.cols();
  double cost=0;
  for (int iPt=0; iPt<nbPoint-1; iPt++)
    for (int jPt=iPt+1; jPt<nbPoint; jPt++) {
      double sum=0;
      for (int i=0; i<dim; i++) {
	double delta=Coord(iPt,i) - Coord(jPt,i);
	sum += delta*delta;
      }
      double diff=sqrt(sum) - DistMat(iPt,jPt);
      cost += diff*diff;
    }
  return cost;
}






MyMatrix<double> GradientSearchMetric(MyMatrix<double> const& DistMat, MyMatrix<double> const& CoordInit, int const& NITER)
{
  int nbPoint=DistMat.rows();
  int dim=CoordInit.cols();
  MyMatrix<double> WorkMat=CoordInit;
  MyMatrix<double> NewMat(nbPoint,dim);
  for (int iter=0; iter<NITER; iter++) {
    for (int iPt=0; iPt<nbPoint; iPt++) {
      for (int i=0; i<dim; i++)
	NewMat(iPt,i)=0;
      for (int jPt=0; jPt<nbPoint; jPt++)
	if (iPt != jPt) {
	  double sum=0;
	  for (int i=0; i<dim; i++) {
	    double delta=WorkMat(iPt,i) - WorkMat(jPt,i);
	    sum += delta*delta;
	  }
	  double dist=sqrt(sum);
	  double qFrac=DistMat(iPt,jPt)/dist;
	  for (int i=0; i<dim; i++) {
	    double delta=WorkMat(jPt,i) + (WorkMat(iPt,i) - WorkMat(jPt,i))*qFrac;
	    NewMat(iPt,i) += delta/double(nbPoint-1);
	  }
	}
    }
# ifdef DEBUG
    double cost1=CostFunctional(DistMat, WorkMat);
    double cost2=CostFunctional(DistMat, NewMat);
    std::cerr << "iter=" << iter << " cost(WorkMat)=" << cost1 << " cost(NewMat)=" << cost2 << "\n";
# endif
    WorkMat=NewMat;
  }
  return NewMat;
}





/*
  This data type contains the L1 L2 and Linfinity norm between two matrices.
  It is a common placeholder which is also hold for the coordinates distances as well.
 */
struct MatrixDist
{
  double L1norm;
  double L2norm;
  double LinfNorm;
};

/*
  This is for printing the error. Good for debugging purposes.
 */
std::ostream& operator<<(std::ostream& os, MatrixDist const& eR)
{
  os << "L1/L2/Linf=" << eR.L1norm << " / " << eR.L2norm << " / " << eR.LinfNorm;
  return os;
}

/*
  ComputeNorms takes two distance matrices M1 and M2.
  The output is the L1norm, L2norm and Linfinity norms.
  ---The L1norm is defined as
     L1(M1, M2) = sum_{i<j} |M1(i,j) - M2(i,j)|
  ---The L2norm is defined as
     L2(M1, M2) = sqrt( sum_{i<j} [M1(i,j) - M2(i,j)]^2 )
  ---The Linfinity norm is defined as
     Linf(M1, M2) = max_{i<j} |M1(i,j) - M2(i,j)|
 */
MatrixDist ComputeNorms(MyMatrix<double> const&M1, MyMatrix<double> const&M2)
{
  int nbRow=M1.rows();
  int nbCol=M1.cols();
  double L1norm=0;
  double sumSqr=0;
  double LinfNorm=0;
  for (int iRow=0; iRow<nbRow; iRow++)
    for (int iCol=0; iCol<nbCol; iCol++) {
      double delta=M1(iRow,iCol) - M2(iRow,iCol);
      double delta_a=T_abs(delta);
      L1norm += delta_a;
      sumSqr += delta*delta;
      if (delta_a > LinfNorm)
	LinfNorm=delta_a;
    }
  return {L1norm, sqrt(sumSqr), LinfNorm};
}

/*
  Given a positive definite matrix M we return a positive definite matrix P
  such that P*P = M
*/
MyMatrix<double> SquareRootPositiveDefMatrix(MyMatrix<double> const& M)
{
  Eigen::SelfAdjointEigenSolver<MyMatrix<double>> eig(M);
  MyVector<double> ListEig=eig.eigenvalues();
  MyMatrix<double> ListVect=eig.eigenvectors();
  int n=M.rows();
  MyMatrix<double> SqrRoot=ZeroMatrix<double>(n,n);
  for (int i=0; i<n; i++) {
    double eVal=ListEig(i);
    MyVector<double> eEigVect(n);
    for (int j=0; j<n; j++)
      eEigVect(j) = ListVect(j,i);
    double sum=0;
    for (int j=0; j<n; j++) {
      double delta=eEigVect(j);
      sum += delta*delta;
    }
    MyVector<double> eDiff=eVal*eEigVect - M*eEigVect;
    double sqrVal;
    if (eVal < 0)
      sqrVal=0;
    else
      sqrVal=sqrt(eVal);
    for (int iM=0; iM<n; iM++)
      for (int jM=0; jM<n; jM++)
	SqrRoot(iM,jM) += sqrVal*eEigVect(iM)*eEigVect(jM);
  }
  return SqrRoot;
}



/*
  The array for the QR decomposition
 */
struct QRdecomp {
  MyMatrix<double> Q;
  MyMatrix<double> R;
};




/*
  QR decomposition of a matrix M is to write it as
  M = Q*R with
  ---- Q orthogonal
  ---- R symmetric
            --
  Algorithm for computing them is following:
  We then have M' = R *Q' and M' * M = R * R.
  After computing square root this gives us R and then
  Q = M * inv R.
*/
QRdecomp ComputeQRdecomposition(MyMatrix<double> const& M)
{
  MyMatrix<double> Rsqr=M.transpose() * M;
  MyMatrix<double> R = SquareRootPositiveDefMatrix(Rsqr);
  MyMatrix<double> Q = M * R.inverse();
  /*
  int n=M.rows();
  MyMatrix<double> Cont1=Q*Q.transpose();
  MyMatrix<double> Cont2=R - R.transpose();
  MyMatrix<double> Cont3=Q*R;
  std::cerr << "Orthogonality Q*Q' " << ComputeNorms(Cont1, IdentityMat<double>(n)) << "\n";
  std::cerr << "Symmetry R - Rtr " << ComputeNorms(Cont2, ZeroMatrix<double>(n,n)) << "\n";
  std::cerr << "Expression M=QR " << ComputeNorms(Cont3, M) << "\n";*/
  return {Q, R};
}


/*
  Provide the same data as for Coordiante matrices.
  That is L1, L2 and Linf errors.
 */
MatrixDist ErrorCoordinateMatrices(MyMatrix<double> const&M1, MyMatrix<double> const&M2)
{
  int nbPoint=M1.rows();
  int dim=M1.cols();
  std::vector<double> ListError(nbPoint);
  double sum1=0;
  double sum2=0;
  for (int iRow=0; iRow<nbPoint; iRow++) {
    double sum=0;
    for (int i=0; i<dim; i++) {
      double delta=M1(iRow,i) - M2(iRow,i);
      sum += delta*delta;
    }
    double dist=sqrt(sum);
    ListError[iRow]=dist;
    sum1 += dist;
    sum2 += dist*dist;
  }
  double L1error=sum1 / double(nbPoint);
  double L2error=sqrt(sum2 / double(nbPoint));
  double Linf=VectorMax(ListError);
  return {L1error, L2error, Linf};
}



/*
  Given a set of positions V and a matrix M
  we compute the maximum error which is
  max_i | Z_i - V(i) |
*/
double ErrorZpositionMatrices(MyMatrix<double> const&M, MyVector<double> const&V)
{
  int nbPoint=M.rows();
  int dim=M.cols();
  std::vector<double> ListError(nbPoint);
  for (int iRow=0; iRow<nbPoint; iRow++) {
    double delta=M(iRow,dim-1) - V(iRow);
    ListError[iRow]=T_abs(delta);
  }
  return VectorMax(ListError);
}



/*
  Iterates over all subsets defining full dimensional simplices and print their
  determinant.
 */
void PrintAllDeterminants(std::ostream & os, MyMatrix<double> const& M, int const& soughtDim)
{
  std::cerr << "PrintAllDeterminants with soughtDim=" << soughtDim << "\n";
  int iterSize=soughtDim+1;
  std::vector<int> eList=BinomialStdvect_First(iterSize);
  int nbPoint=M.rows();
  while(true) {
    MyMatrix<double> Pmat(iterSize,iterSize);
    for (int i=0; i<iterSize; i++)
      Pmat(i,0)=1;
    for (int i=0; i<iterSize; i++) {
      int idx=eList[i];
      for (int iCol=0; iCol<soughtDim; iCol++)
	Pmat(i,iCol+1)=M(idx,iCol);
    }
    //    std::cerr << "Pmat=\n";
    //    WriteMatrix(std::cerr, Pmat);
    double det=DeterminantMat(Pmat);
    os << "eList=";
    for (auto & eVal : eList)
      os << " " << eVal;
    os << " det=" << det << "\n";
    bool test=BinomialStdvect_Increment(nbPoint, iterSize, eList);
    if (!test)
      break;
  }
}


/*
  This function sets one point to have coordinate (0,0, ...)
 */
void SetReferencePoint(MyMatrix<double> & M, int const& iRefPoint, int const& soughtDim)
{
  MyVector<double> V=GetMatrixRow(M, iRefPoint);
  int nbRow=M.rows();
  //  int nbCol=M.cols();
  for (int iRow=0; iRow<nbRow; iRow++)
    for (int iCol=0; iCol<soughtDim; iCol++)
      M(iRow,iCol) -= V(iCol);
}


/*
Find the two dimensional rotation that maps M2 to M1
First step is to remove the barycenter
Then we have functional of say
sum_i ( cos t M2(i,0) + sin t M2(i,1) - M1(i,0))^2
    + (-sin t M2(i,0) + cos t M2(i,1) - M1(i,1))^2
Expanded the functional has the form
  A cos^2 t + B sin^2 t + C cos t sin t + D cos t + E sin t + F
However, it quickly appears that in fact A = B and C = 0
so the functional simplifies to
   D cos t + E sin t + F2
   This function can then easily be optimized
 */
MyMatrix<double> TwoDimensionalAngleReduction(MyMatrix<double> const& M1, MyMatrix<double> const& M2)
{
  int nbPoint=M1.rows();
  int dim=M1.cols();
  int dim2=2;
  MyVector<double> eIso1=Isobarycenter(M1);
  std::cerr << "eIso1(r/c)=" << eIso1.rows() << " / " << eIso1.cols() << "\n";
  MyVector<double> eIso2=Isobarycenter(M2);
  std::cerr << "eIso2(r/c)=" << eIso2.rows() << " / " << eIso2.cols() << "\n";
  MyMatrix<double> M1bis(nbPoint,dim);
  MyMatrix<double> M2bis(nbPoint,dim);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<dim; j++) {
      M1bis(i,j) = M1(i,j) - eIso1(j);
      M2bis(i,j) = M2(i,j) - eIso2(j);
    }
  double D=0, E=0;
  for (int i=0; i<nbPoint; i++) {
    D += - M2bis(i,0) * M1bis(i,0) - M2bis(i,1) * M1bis(i,1);
    E += - M2bis(i,1) * M1bis(i,0) + M2bis(i,0) * M1bis(i,1);
  }
  double TheFrac=E / D;
  double Theta=atan(TheFrac);
  std::cerr << " TheFrac=" << TheFrac << " Theta=" << Theta << "\n";
  MyMatrix<double> Mrot(2,2);
  Mrot(0,0)=cos(Theta);
  Mrot(1,1)=cos(Theta);
  Mrot(1,0)=sin(Theta);
  Mrot(0,1)=-sin(Theta);
  MyMatrix<double> Mret(nbPoint,dim);
  for (int i=0; i<nbPoint; i++) {
    for (int j=0; j<dim2; j++)
      Mret(i,j) = eIso1(j) + M2bis(i,0) * Mrot(0,j) + M2bis(i,1) * Mrot(1,j);
    for (int j=dim2; j<dim; j++)
      Mret(i,j) = M2(i,j);
  }
  return Mret;
}




/*
  This code does the following given two embedding M1 and M2.
  It finds an orthogonal matrix P that maps M2 in a reasonably close
  manner to the matrix M1.
*/
MyMatrix<double> FindEmbeddingEquivalence_General(MyMatrix<double> const&M1, MyMatrix<double> const&M2, int const& soughtDim)
{
  int nbPoint=M1.rows();
  int dim=M1.cols();
#ifdef DEBUG  
  std::cerr << "dim=" << dim << " soughtDim=" << soughtDim << "\n";
  std::cerr << "|M1|=" << StringSizeMatrix(M1) << "|M2|=" << StringSizeMatrix(M2) << "\n";
  std::cerr << "nbPoint=" << nbPoint << "\n";
#endif
  MyVector<double> Iso1=Isobarycenter(M1);
  MyMatrix<double> P1(soughtDim,soughtDim);
  MyMatrix<double> P2(soughtDim,soughtDim);
  bool IsFirst=true;
  double TheError=0;
  MyMatrix<double> RetMat;
  int iterSize=soughtDim+1;
  std::vector<int> eList=BinomialStdvect_First(iterSize);
  bool IsAssigned=false;
  while(true) {
    int iFirst=eList[0];
    for (int iRow=0; iRow<soughtDim; iRow++) {
      int eRow=eList[iRow+1];
      for (int iCol=0; iCol<soughtDim; iCol++) {
	P1(iRow,iCol)=M1(eRow,iCol) - M1(iFirst,iCol);
	P2(iRow,iCol)=M2(eRow,iCol) - M2(iFirst,iCol);
      }
    }
    double det1=P1.determinant();
    double det2=P2.determinant();
    if (T_abs(det1) > 0 && T_abs(det2) > 0) {
      //      std::cerr << "det1=" << det1 << " det2=" << det2 << "\n";
      IsAssigned=true;
      MyMatrix<double> Pmat=P2.inverse() * P1;
      QRdecomp QRdec=ComputeQRdecomposition(Pmat);
      MyMatrix<double> Qred = QRdec.Q;
      MyMatrix<double> Q=IdentityMat<double>(dim);
      for (int i=0; i<soughtDim; i++)
	for (int j=0; j<soughtDim; j++)
	  Q(i,j) = Qred(i,j);
      MyMatrix<double> M2img = M2 * Q;
      MyVector<double> IsoM2img=Isobarycenter(M2img);
      MyVector<double> diff=Iso1 - IsoM2img;
      for (int iDim=soughtDim; iDim<dim; iDim++)
	diff(iDim)=0;
      for (int iRow=0; iRow<nbPoint; iRow++)
	M2img.row(iRow) += diff;
      double eError=ErrorCoordinateMatrices(M1, M2img).LinfNorm;
      if (IsFirst) {
	TheError=eError;
	IsFirst=false;
	RetMat=M2img;
	// double det=DeterminantMat(Q);
	//	std::cerr << "det=" << det << "\n";
      }
      else {
	if (TheError > eError) {
	  TheError=eError;
	  RetMat=M2img;
	  //	  std::cerr << "det=" << det << "\n";
	}
      }
    }
    bool test=BinomialStdvect_Increment(nbPoint, iterSize, eList);
    if (!test)
      break;
  }
#ifdef DEBUG
  std::cerr << "IsAssigned=" << IsAssigned << "\n";
#endif
  if (!IsAssigned) {
    return M1;
  }
  return TwoDimensionalAngleReduction(M1, RetMat);
}

MyMatrix<double> FindEmbeddingEquivalence(MyMatrix<double> const&M1, MyMatrix<double> const&M2)
{
  int dim=M1.cols();
  return FindEmbeddingEquivalence_General(M1, M2, dim);
}




/*
  Given a family of s vector in dimension n, it computes an orthonormal
  basis out of it.
*/
MyMatrix<double> GramSchmidtOrthogonalization(MyMatrix<double> const& M)
{
  int nbVect=M.rows();
  int dim=M.cols();
  MyVector<double> eVect=ZeroVector<double>(dim);
  MyMatrix<double> TheSpace(nbVect,dim);
  for (int iVect=0; iVect<nbVect; iVect++) {
    for (int i=0; i<dim; i++)
      eVect(i)=M(iVect,i);
    for (int jVect=0; jVect<iVect; jVect++) {
      double scal=0;
      for (int i=0; i<dim; i++)
	scal += eVect(i) * TheSpace(jVect,i);
      for (int i=0; i<dim; i++)
	eVect(i) -= scal * TheSpace(jVect,i);
    }
    for (int i=0; i<dim; i++)
      TheSpace(iVect,i)=eVect(i);
  }
  return TheSpace;
}


/*
  Given a family of d vector in dimension n, the function finds n-d vector
  that complements it to a full basis.
  The determinant is used to select the best completion
*/
MyMatrix<double> OptimalCompletionToBasis(MyMatrix<double> const& M)
{
  int n=M.cols();
  int nbVect=M.rows();
  int nbAdd=n - nbVect;
  double MaxDet=0;
  MyMatrix<double> RetMat;
  std::vector<int> eList=BinomialStdvect_First(nbAdd);
  while(true) {
    MyMatrix<double> TheMat(n,n);
    for (int iRow=0; iRow<nbVect; iRow++)
      for (int iCol=0; iCol<n; iCol++)
	TheMat(iRow,iCol)=M(iRow,iCol);
    for (int iAdd=0; iAdd<nbAdd; iAdd++) {
      int eCol=eList[iAdd];
      for (int iCol=0; iCol<n; iCol++) {
	double eVal=0;
	if (iCol == eCol)
	  eVal=1;
	TheMat(nbVect + iAdd, iCol)=eVal;
      }
    }
    double det=TheMat.determinant();
    double detA=T_abs(det);
    if (detA > MaxDet) {
      MaxDet=detA;
      RetMat=TheMat;
    }
    bool test=BinomialStdvect_Increment(n, nbAdd, eList);
    if (!test)
      break;
  }
  return RetMat;
}



/*
  Find an orthogonal matrix O mapping M1 to M2
 */
MyMatrix<double> OrthogonalTransformation(MyMatrix<double> const& M1, MyMatrix<double> const& M2)
{
  MyMatrix<double> M1_compl=OptimalCompletionToBasis(M1);
  MyMatrix<double> M2_compl=OptimalCompletionToBasis(M2);
  MyMatrix<double> O1=GramSchmidtOrthogonalization(M1_compl);
  MyMatrix<double> O2=GramSchmidtOrthogonalization(M2_compl);
  MyMatrix<double> Omat=O1.inverse() * O2;
  return Omat;
}




/*
  This is the main data structure used here.
  It contains all the information we need in order to make the optimization
 */
struct MetricEmbeddingGeneralized {
  int dim;
  MyMatrix<double> DistMat;
  MyMatrix<double> DistErrorMat;
  MyVector<double> Zposition;
  MyVector<double> Zerror;
  bool UseZposition;
  //  double ErrorDist;
  //  double ErrorZ;
  MyMatrix<int> MatAdj;
  std::vector<int> ListPointFixed;
  MyMatrix<double> CoordFixed;
  double ThresholdDistance;
};


// The function reads one embedding problem from data file.
MetricEmbeddingGeneralized ReadOneEmbeddingCase(std::istream & is)
{
  MetricEmbeddingGeneralized eEmbed;
  int dim;
  is >> dim;
  eEmbed.dim=dim;
  //
  eEmbed.DistMat=ReadMatrix<double>(is);
  eEmbed.DistErrorMat=ReadMatrix<double>(is);
  eEmbed.Zposition=ReadVector<double>(is);
  eEmbed.Zerror=ReadVector<double>(is);
  //
  bool UseZposition;
  is >> UseZposition;
  eEmbed.UseZposition=UseZposition;
  //
  double ThresholdDistance;
  is >> ThresholdDistance;
  eEmbed.ThresholdDistance=ThresholdDistance;
  //
  eEmbed.MatAdj=ReadMatrix<int>(is);
  //
  std::vector<int> ListPointFixed;
  is >> ListPointFixed;
  eEmbed.ListPointFixed=ListPointFixed;
  //
  eEmbed.CoordFixed=ReadMatrix<double>(is);
  //
  return eEmbed;
}




/*
  This is the generalized functional cost.
  We define a graph G on which we know the distances.
  In full it gives
  sum_{i connected to j} { DM_{i,j} - ||v_i - v_j|| }^2 / ErrorDist
                      + sum_i | Z_{v_i} - V_{i} | / ErrorZ
*/
double CostFunctionGeneralized(MetricEmbeddingGeneralized const& eEmbed, MyMatrix<double> const& Coord)
{
  int dim=eEmbed.dim;
  int nbPoint=eEmbed.DistMat.rows();
  double cost=0;
  for (int iPt=0; iPt<nbPoint; iPt++)
    for (int jPt=0; jPt<nbPoint; jPt++)
      if (eEmbed.MatAdj(iPt,jPt) == 1) {
	double sum=0;
	for (int i=0; i<dim; i++) {
	  double delta=Coord(iPt,i) - Coord(jPt,i);
	  sum += delta*delta;
	}
#ifdef DEBUG
	std::cerr << "iPt=" << iPt << " jPt=" << jPt << " nbPoint=" << nbPoint << "\n";
	std::cerr << "sum=" << sum << "\n";
	std::cerr << "eEmbed.DistMat(iPt,jPt)=" << eEmbed.DistMat(iPt,jPt) << "\n";
	std::cerr << "eEmbed.DistErrorMat(iPt,jPt)=" << eEmbed.DistErrorMat(iPt,jPt) << "\n";
#endif
	double deltaDist=sqrt(sum) - eEmbed.DistMat(iPt,jPt);
	double distErr=eEmbed.DistErrorMat(iPt,jPt);
	if (distErr > 0) {
	  double diff=deltaDist / distErr;
	  cost += diff*diff;
	}
      }
  if (eEmbed.UseZposition) {
    for (int iPt=0; iPt<nbPoint; iPt++) {
      double delta=Coord(iPt,dim-1) - eEmbed.Zposition(iPt);
      cost += delta*delta;
    }
  }
  return cost;
}


/*
  We have a distance matrix DistMat embeddded in dimension dim.
  OUTPUT the list of indexes of a simplex of maximum volume containing it.
 */
std::vector<int> GetBestInitialSimplex(MyMatrix<double> const& DistMat, int const& dim)
{
  int nbPoint=DistMat.rows();
  int iterSize=dim+1;
  std::vector<int> eList=BinomialStdvect_First(iterSize);
  double MaxDetFound= - 1;
  std::vector<int> SoughtSet;
  while(true) {
    if (dim == 2) {
      int i0=eList[0];
      int i1=eList[1];
      int i2=eList[2];
      double a=DistMat(i0, i1);
      double b=DistMat(i0, i2);
      double c=DistMat(i1, i2);
      double p=(a+b+c)/2;
      double TheDet=sqrt( p * ( p-a) * (p-b) * (p-c));
      if (TheDet > MaxDetFound) {
	MaxDetFound=TheDet;
	SoughtSet = eList;
      }
    }
    else {
      std::cerr << "Write your code here for computing volume from Gram matrix\n";
      throw TerminalException{1};
    }
    bool res=BinomialStdvect_Increment(nbPoint, iterSize, eList);
    if (!res)
      break;
  }
  return SoughtSet;
}



/*
  This function is for global trilateration of a distance matrix.
  We iterate over all points in order to get one embedding:
  INPUT:
  ---DistMat, the distance matrix
  ---DistErrorMat, the matrix of distance errors.
  ---MatAdj, specify when two points are adjacent or not.
  ---ListPointFixed, the list of points whose coordinate is known in advance.
  ---CoordFixed, the list of coordinates of the fixed points
  ---UseZposition, whether we use Z position or not.
  ---Zposition, the list of Z position that is available.
  ---Zerror the list of error on Z.
  ---dim: the dimension of the embedding
 */
MyMatrix<double> GlobalTrilaterationEmbedding(MyMatrix<double> const& DistMat, MyMatrix<double> const& DistErrorMat, MyMatrix<int> const& MatAdj, std::vector<int> ListPointFixed, MyMatrix<double> const& CoordFixed, bool const& UseZposition, MyVector<double> const& Zposition, MyVector<double> const& Zerror, int const& dim)
{
  int nbPoint=DistMat.rows();
  MyMatrix<double> Coord(nbPoint,dim);
  int nbFixedOrig=ListPointFixed.size();
  int nbFixed=0;
  std::vector<int> ListKnown;
  std::vector<int> Assigned(nbPoint,0);
  auto InsertPosition=[&](MyVector<double> const& ThePos, int const& ePos) -> void {
    Assigned[ePos]=1;
    ListKnown.push_back(ePos);
    for (int i=0; i<dim; i++)
      Coord(ePos,i)=ThePos(i);
    nbFixed++;
  };
  for (int iFixed=0; iFixed<nbFixedOrig; iFixed++) {
    int eFixed=ListPointFixed[iFixed];
    MyVector<double> eVect(dim);
    for (int i=0; i<dim; i++)
      eVect(i)=CoordFixed(iFixed,i);
    InsertPosition(eVect, eFixed);
  }
  auto GetOneAdjacentPoint=[&](std::vector<int> const& ListPt) -> int {
    int siz=ListPt.size();
    for (int iPt=0; iPt<nbPoint; iPt++)
      if (Assigned[iPt] == 0) {
	int sum=0;
	for (auto & ePt : ListPt)
	  if (MatAdj(iPt, ePt) == 1)
	    sum++;
	if (sum == siz)
	  return iPt;
      }
    return -1;
  };
  if (nbFixed == 0) {
    std::vector<int> eSet=GetBestInitialSimplex(DistMat, dim);
    MyMatrix<double> DistMatRed=ColRowSymmetricMatrix(DistMat, eSet);
    MyMatrix<double> Coord=CoordinatesFromSimplicialMatrix(DistMatRed);
    for (int i=0; i<=dim; i++) {
      MyVector<double> eVect=GetMatrixRow(Coord,i);
      InsertPosition(eVect,eSet[i]);
    }
  }
  if (nbFixed == 1) {
    int ePt=GetOneAdjacentPoint(ListKnown);
    int fPt=ListKnown[0];
    MyVector<double> eVect=GetMatrixRow(Coord,fPt);
    eVect(0) += DistMat(ePt,fPt);
    InsertPosition(eVect,ePt);
  }
  auto SingleInsert=[&]() -> void {
    int ePt=GetOneAdjacentPoint(ListKnown);
    std::vector<int> ListPt=ListKnown;
    ListPt.push_back(ePt);
    MyMatrix<double> DistMatRed=ColRowSymmetricMatrix(DistMat, ListPt);
    MyMatrix<double> CoordF=CoordinatesFromSimplicialMatrix(DistMatRed);
    MyMatrix<double> CoordF_red=SelectRow(CoordF, StdVectorFirstNentries(nbFixed));
    MyMatrix<double> CoordSel=SelectRow(Coord, ListKnown);
    MyMatrix<double> Omat=OrthogonalTransformation(CoordF_red, CoordSel);
    MyVector<double> eVect=GetMatrixRow(CoordF, nbFixed);
    MyVector<double> eVectImg=eVect*Omat.transpose();
    InsertPosition(eVectImg, ePt);
  };
  for (int iDim=2; iDim<dim; iDim++) 
    if (nbFixed == iDim)
      SingleInsert();
  auto IterateOverContainingNuple=[&]() -> void {
    std::vector<int> ListKnownCopy=ListKnown;
    std::vector<int> eList=BinomialStdvect_First(dim);
    int nbFixedWork=nbFixed;
    while(true) {
      MyMatrix<double> CoordMat(dim,dim);
      for (int iRow=0; iRow<dim; iRow++) {
	int eRow=ListKnownCopy[eList[iRow]];
	CoordMat.row(iRow) = Coord.row(eRow);
      }
      MyVector<double> vectDist(dim);
      auto ComputeRawError=[&](MyVector<double> const& eVectPos, int const& ePt) -> double {
	double cost=0;
	if (UseZposition) {
	  std::cerr << "dim=" << dim << " ePt=" << ePt << "\n";
	  std::cerr << "eVectPos.size()=" << eVectPos.size() << "\n";
	  std::cerr << "Zposition.size()=" << Zposition.size() << "\n";
	  double delta=(eVectPos(dim-1) - Zposition(ePt)) / Zerror(ePt);
	  std::cerr << "delta=" << delta << "\n";
	  cost += T_abs(delta);
	  std::cerr << "cost=" << cost << "\n";
	}
	for (int jPt=0; jPt<nbPoint; jPt++)
	  if (Assigned[jPt] == 1 && MatAdj(ePt,jPt) == 1) {
	    double sum=0;
	    for (int i=0; i<dim; i++) {
	      double diff=Coord(jPt,i) - eVectPos(i);
	      sum += diff*diff;
	    }
	    double delta=(sqrt(sum) - DistMat(ePt,jPt)) / DistErrorMat(ePt,jPt);
	    cost += T_abs(delta);
	  }
	return cost;
      };
      for (int iPt=0; iPt<nbPoint; iPt++) {
	if (Assigned[iPt] == 0) {
	  int sum=0;
	  for (int i=0; i<dim; i++) {
	    int ePt=eList[i];
	    int vPt=ListKnownCopy[ePt];
	    sum += MatAdj(vPt, iPt);
	    vectDist(i) = DistMat(vPt, iPt);
	  }
	  if (sum == dim) {
	    std::vector<MyVector<double>> ListVect=trilateration(CoordMat, vectDist);
	    double error0=ComputeRawError(ListVect[0], iPt);
	    double error1=ComputeRawError(ListVect[1], iPt);
	    if (error0 < error1)
	      InsertPosition(ListVect[0], iPt);
	    else
	      InsertPosition(ListVect[1], iPt);
	  }
	}
      }
      bool res=BinomialStdvect_Increment(nbFixedWork, dim, eList);
      if (!res)
	break;
    }
  };
  while(true) {
    IterateOverContainingNuple();
    if (nbFixed == nbPoint)
      break;
  }
  return Coord;
}


MyMatrix<double> GlobalTrilaterationEmbedding_specific(MyMatrix<double> const& DistMat, int const& dim)
{
  int nbPoint=DistMat.rows();
  MyMatrix<int> MatAdj(nbPoint, nbPoint);
  MyMatrix<double> DistErrorMat(nbPoint, nbPoint);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      int eVal=0;
      if (i != j)
	eVal=1;
      MatAdj(i,j)=eVal;
      DistErrorMat(i,j) = eVal * double(200);
    }
  std::vector<int> ListPointFixed;
  MyMatrix<double> CoordFixed;
  bool UseZposition=false;
  MyVector<double> Zposition;
  MyVector<double> Zerror;
  return GlobalTrilaterationEmbedding(DistMat, DistErrorMat, MatAdj, ListPointFixed, CoordFixed, UseZposition, Zposition, Zerror, dim);
}



struct RecOptionInitial {
  bool UsePreviousPosition;
  bool UseEigenvalueGuess;
};



/*
  Find initial set of coordinate in the generalized case
  We apply a number of different strategies:
  ---If no coordinate is known, then we apply first the eigenvalue guess
     of the embedding.
  ---If we have no fixed coordinates, then we can use rotations to find a
     better fit.
  ---If we have a number of fitted coordinate in the absolute case then the best
     is to use those fixed coordinate and trilateration to get a good initial
     guess.
*/
MyMatrix<double> InitialCoordinateGeneralized(MetricEmbeddingGeneralized const& eEmbed, RecOptionInitial const& RecOptIni)
{
  int nbPoint=eEmbed.DistMat.rows();
  int dim=eEmbed.dim;
  int nbFixedOrig=eEmbed.ListPointFixed.size();
# ifdef DEBUG
  std::cerr << "nbPoint=" << nbPoint << " dim=" << dim << " nbFixedOrig=" << nbFixedOrig << "\n";
# endif
  int nbZeroSet=0;
  for (int iPt=0; iPt<nbPoint-1; iPt++)
    for (int jPt=iPt+1; jPt<nbPoint; jPt++)
      if (eEmbed.MatAdj(iPt,jPt) == 0)
	nbZeroSet++;
# ifdef DEBUG
  std::cerr << "nbZeroSet=" << nbZeroSet << "\n";
# endif
  if (nbZeroSet == 0 && nbFixedOrig == 0) {
    auto FullDistanceGuess=[&](MyMatrix<double> const& inpDistMat, int const& inpDim) -> MyMatrix<double> {
      if (RecOptIni.UseEigenvalueGuess)
	return EigenvalueGuess(inpDistMat, inpDim);
      else
	return GlobalTrilaterationEmbedding_specific(inpDistMat, inpDim);
    };
    if (!eEmbed.UseZposition)
      return FullDistanceGuess(eEmbed.DistMat, dim);
    MyMatrix<double> DistMatRed(nbPoint,nbPoint);
    for (int iPt=0; iPt<nbPoint; iPt++)
      for (int jPt=0; jPt<nbPoint; jPt++) {
	double dist=eEmbed.DistMat(iPt, jPt);
	double deltaZ = eEmbed.Zposition(iPt) - eEmbed.Zposition(jPt);
	double diff=dist*dist - deltaZ*deltaZ;
	double distRed=sqrt(std::max(double(0), diff));
	DistMatRed(iPt, jPt)=distRed;
      }
    MyMatrix<double> CoordRed=FullDistanceGuess(DistMatRed, dim-1);
    MyMatrix<double> Coord(nbPoint,dim);
    for (int iPt=0; iPt<nbPoint; iPt++) {
      for (int i=0; i<dim-1; i++)
	Coord(iPt,i) = CoordRed(iPt,i);
      Coord(iPt, dim-1)=eEmbed.Zposition(iPt);
    }
    return Coord;
  }
  MyMatrix<double> Coord=GlobalTrilaterationEmbedding(eEmbed.DistMat, eEmbed.DistErrorMat, eEmbed.MatAdj, eEmbed.ListPointFixed, eEmbed.CoordFixed, eEmbed.UseZposition, eEmbed.Zposition, eEmbed.Zerror, dim);
  // We have several situations
  // ---We have some fixed points
  //    Then no adjustment by Zposition
  // ---We have Zposition but no fixed points
  //    Then apply the Orthogonal transformation for adjusting position
  // ---We have neither. Then no repositioning.
  if (eEmbed.UseZposition && nbFixedOrig == 0)
    Coord=PositionAdjustment(Coord, eEmbed.Zposition);
  return Coord;
}

/*
  This is the optimization procedure.
  It deals with the generalized case which covers:
  ---Use of absolute or relative cases
  ---Use of Z coordinate or not
  ---Use of fixed coordinates if available.
*/
MyMatrix<double> GradientSearchGeneralized(MetricEmbeddingGeneralized const& eEmbed, MyMatrix<double> const& CoordInit, int const& NITER, double const& qExponent)
{
  int nbPoint=eEmbed.DistMat.rows();
  int dim=CoordInit.cols();
  int nbFixed=eEmbed.ListPointFixed.size();
  MyMatrix<double> WorkMat=CoordInit;
  MyMatrix<double> NewMat(nbPoint,dim);
  MyVector<double> eVect(dim), sumShift(dim);
  double ErrorDist=0;
  int nbmeas=0;
  for (int iPt=0; iPt<nbPoint; iPt++)
    for (int jPt=0; jPt<nbPoint; jPt++)
      if (eEmbed.MatAdj(iPt, jPt) == 1) {
	ErrorDist += eEmbed.DistErrorMat(iPt,jPt);
	nbmeas++;
      }
  ErrorDist /= double(nbmeas);
  double ErrorZ=VectorSum(eEmbed.Zerror)/nbPoint;
  for (int iter=0; iter<NITER; iter++) {
    for (int iPt=0; iPt<nbPoint; iPt++) {
      for (int i=0; i<dim; i++) {
	eVect(i)=0;
	sumShift(i)=0;
      }
      for (int jPt=0; jPt<nbPoint; jPt++)
	if (eEmbed.MatAdj(iPt, jPt) == 1) {
	  double sum=0;
	  for (int i=0; i<dim; i++) {
	    double delta=WorkMat(iPt,i) - WorkMat(jPt,i);
	    sum += delta*delta;
	  }
	  double dist=sqrt(sum);
	  double qFrac;
	  double TheMult=pow(  fabs(eEmbed.DistMat(iPt,jPt) - dist), qExponent-2);
	  if (dist > eEmbed.ThresholdDistance) {
	    qFrac=eEmbed.DistMat(iPt,jPt)/dist;
	  }
	  else {
	    qFrac=0.9;
	  }
	  for (int i=0; i<dim; i++) {
	    double delta=(WorkMat(iPt,i) - WorkMat(jPt,i))*(qFrac-1)*TheMult;
	    sumShift(i) += double(dim);
	    eVect(i) += delta;
	  }
	}
      if (eEmbed.UseZposition) {
	double eFrac=ErrorDist/ErrorZ;
	double deltaZ = eEmbed.Zposition(iPt) - WorkMat(iPt,dim-1);
	eVect(dim-1) += deltaZ*eFrac;
	sumShift(dim-1) += eFrac;
      }
      for (int i=0; i<dim; i++)
	NewMat(iPt,i) = WorkMat(iPt,i) + eVect(i)/sumShift(i);
    }
    for (int iFixed=0; iFixed<nbFixed; iFixed++) {
      int eFixed=eEmbed.ListPointFixed[iFixed];
      NewMat.row(eFixed) = eEmbed.CoordFixed.row(iFixed);
    }
# ifdef DEBUG
    double cost1=CostFunctionGeneralized(eEmbed, WorkMat);
    double cost2=CostFunctionGeneralized(eEmbed, NewMat);
    std::cerr << "iter=" << iter << " cost(WorkMat)=" << cost1 << " cost(NewMat)=" << cost2 << "\n";
# endif
    WorkMat=NewMat;
  }
  return NewMat;
}



MyMatrix<double> GetEmbeddingGeneralized(MetricEmbeddingGeneralized const& eEmbed)
{
  RecOptionInitial RecOptIni;
  RecOptIni.UsePreviousPosition=false;
  RecOptIni.UseEigenvalueGuess=true;
  MyMatrix<double> CoordInit=InitialCoordinateGeneralized(eEmbed, RecOptIni);
  int NITER=20;
  double qExponent=2;
  MyMatrix<double> Coord=GradientSearchGeneralized(eEmbed, CoordInit, NITER, qExponent);
  return Coord;
}



struct SphereCoord {
  MyVector<double> x;
  double r;
};



/*
   Circumscribing set
   Equation is
   || x - a ||^2 = r^2
   or
   || x ||^2 - 2 a dot x + || a ||^2 - r^2 = 0
   || x ||^2 - 2 a dot x + alpha = 0
   or || x - c || ^2 = r^2
   The circle is about circumscribing the points p0, p1, ...., pS.
   the point a is expressed as
   a = p0 + lambda0 (p1-p0) + lambda1 (p2-p0) + .... + lambdaS (pS-p0)
   So the system of equation is for every 0 <= i <= S
   || p_i ||^2 -2 p_i dot p0  -2 sum_{I=0}^S lambdaI (p(I+1) - p0) dot p0 + alpha = 0
   This reduces the equation system to
   for 0 <= i <= S
   -2 lambda0 (p1 - p0) dot pi - 2 lambda1 (p2 - p0) dot pi + .... -2 lambda(S-1) (pS - p0) dot pi + alpha
                     = 2 p_i dot p0 - || p_i ||^2
 */
SphereCoord SimpleCircumscribingSphere(std::vector<MyVector<double>> const& ListPt)
{
  int eSiz=ListPt.size()-1;
  int dim=ListPt[0].size();
  MyMatrix<double> A(eSiz+1,eSiz+1);
  MyVector<double> B(eSiz+1);
  for (int iVect=0; iVect<=eSiz; iVect++) {
    double sum=0;
    for (int i=0; i<dim; i++) {
      double Vp0=ListPt[0](i);
      double Vpi=ListPt[iVect](i);
      sum += double(2) * Vpi * Vp0 - Vpi * Vpi;
    }
    B(iVect) = sum;
    for (int jVect=0; jVect<eSiz; jVect++) {
      double sum=0;
      for (int i=0; i<dim; i++) {
	double a=ListPt[jVect+1](i) - ListPt[0](i);
	double b=ListPt[iVect](i);
	sum += a*b;
      }
      A(iVect,jVect) = - double(2) * sum;
    }
    A(iVect,eSiz)=1;
  }
  Eigen::FullPivLU<MyMatrix<double>> solver;
  solver.compute(A);
  MyVector<double> lambda=solver.solve(B);
#ifdef DEBUG
  std::cerr << "-----------------------------------------------\n";
  std::cerr << "det(A)=" << A.determinant() << "\n";
  for (int iVect=0; iVect<=eSiz; iVect++) {
    double sumR=-B(iVect);
    for (int jVect=0; jVect<=eSiz; jVect++)
      sumR += A(iVect,jVect) * lambda(jVect);
    std::cerr << "iVect=" << iVect << " sumR=" << sumR << "\n";
  }
#endif
  MyVector<double> aPoint(dim);
  for (int i=0; i<dim; i++) {
    double eX=ListPt[0](i);
    for (int jVect=0; jVect<eSiz; jVect++) {
      double a=ListPt[jVect+1](i) - ListPt[0](i);
      eX += lambda(jVect)*a;
    }
    aPoint(i)=eX;
  }
#ifdef DEBUG
  std::cerr << "-------------------------------\n";
  for (int iPt=0; iPt<=eSiz; iPt++) {
    double sumB=0;
    for (int i=0; i<dim; i++) {
      double a=ListPt[iPt](i) - aPoint(i);
      sumB += a*a;
    }
    std::cerr << "iPt=" << iPt << " sumB=" << sumB << "\n";
  }
#endif
  double sum=0;
  for (int i=0; i<dim; i++) {
    double a=ListPt[0](i) - aPoint(i);
    sum += a*a;
  }
  double r=sqrt(sum);
  SphereCoord eCoor{aPoint,r};
  return eCoor;
}

/*
  Given a list of points ListVect in dimension dim, the function returns
  a sphere containing all the points and of minimal radius.
 */
SphereCoord GetCircumscribingCircle(std::vector<MyVector<double>> const& ListVect, int const& dim)
{
  int nbVect=ListVect.size();
  if (nbVect == 0) {
    MyVector<double> x(dim);
    for (int i=0; i<dim; i++)
      x(i)=-1;
    double r=-1;
    return {x,r};
  }
#ifdef DEBUG
  std::cerr << "nbVect=" << nbVect << "  dim=" << dim << "\n";
#endif
  double thr=0.0000001;
  auto IsCorrectCircumscribingSphere=[&](SphereCoord const& eCoor) -> bool {
    for (int iVect=0; iVect<nbVect; iVect++) {
      double delta= - eCoor.r * eCoor.r;
      for (int i=0; i<dim; i++) {
	double diff=eCoor.x(i) - ListVect[iVect](i);
	delta += diff*diff;
      }
      if (delta > thr)
	return false;
    }
    return true;
  };
  bool IsFirst=true;
  SphereCoord eCoorRet;
  auto FuncInsert=[&](SphereCoord const& eCoor) -> void {
    if (IsCorrectCircumscribingSphere(eCoor)) {
      if (IsFirst) {
	eCoorRet=eCoor;
	IsFirst=false;
      }
      else {
	if (eCoor.r < eCoorRet.r)
	  eCoorRet=eCoor;
      }
    }
  };
#ifdef DEBUG
  int nbCase=0;
#endif
  for (int eSiz=1; eSiz<=dim; eSiz++) {
#ifdef DEBUG
    std::cerr << "eSiz=" << eSiz << " dim=" << dim << "\n";
#endif
    std::vector<int> eList=BinomialStdvect_First(eSiz+1);
    while(true) {
#ifdef DEBUG
      nbCase++;
#endif
      std::vector<MyVector<double>> ListPt(eSiz+1);
      for (int i=0; i<=eSiz; i++) {
	int fVect=eList[i];
	ListPt[i]=ListVect[fVect];
      }
      SphereCoord eCoor=SimpleCircumscribingSphere(ListPt);
      FuncInsert(eCoor);
      bool test=BinomialStdvect_Increment(nbVect,eSiz+1,eList);
      if (!test)
	break;
    }
  }
# ifdef DEBUG
  std::cerr << "nbCase=" << nbCase << "\n";
# endif
  if (IsFirst) {
    std::cerr << "Error in the function GetCircumscribingCircle\n";
    throw TerminalException{1};
  }
  return eCoorRet;
}



/*
  This is a general type containing measurement 
 */
struct SingleMeas {
  int nature;  // 1 for distance 2 for altitude
  double error;
  double meas;
  MyVector<double> xPos;
};


/*
  This code is used for the extremal points.
  It takes the list of points and returns the list of points satisfying the constraints.
*/
std::vector<MyVector<double>> FindPossibleCoordinates(std::vector<SingleMeas> const& ListSimplicialSet)
{
  int nbNature1=0;
  int nbNature2=0;
  int dim=ListSimplicialSet.size();
  double zPosition=0;
  for (auto & eMeas : ListSimplicialSet) {
    if (eMeas.nature == 1)
      nbNature1++;
    if (eMeas.nature == 2) {
      nbNature2++;
      zPosition=eMeas.meas;
    }
  }
  if (nbNature2 == 0) {
    MyMatrix<double> Coord(dim,dim);
    MyVector<double> vectDist(dim);
    for (int i=0; i<dim; i++) {
      for (int j=0; j<dim; j++) {
	double rPos=ListSimplicialSet[i].xPos(j);
	Coord(i,j)=rPos;
      }
      double eMeas=ListSimplicialSet[i].meas;
      vectDist(i)=eMeas;
    }
    return trilateration(Coord, vectDist);
  }
  if (nbNature2 == 1) {
    MyMatrix<double> Coord(dim-1,dim-1);
    MyVector<double> vectDist(dim-1);
    int idx=0;
    //    double thr=0.00001;
    for (int i=0; i<dim; i++) {
      if (ListSimplicialSet[i].nature == 1) {
	for (int j=0; j<dim-1; j++) {
	  double rPos=ListSimplicialSet[i].xPos(j);
	  Coord(idx,j)=rPos;
	}
	double deltaZ=ListSimplicialSet[i].xPos(dim-1) - zPosition;
	double eMeas=ListSimplicialSet[i].meas;
	double NewMeas=sqrt(std::max(double(0), eMeas*eMeas - deltaZ*deltaZ));
	vectDist(idx)=NewMeas;
	idx++;
      }
    }
    std::vector<MyVector<double>> ListVectPos=trilateration(Coord, vectDist);
    std::vector<MyVector<double>> NewListVectPos;
    for (auto & eVectPos : ListVectPos) {
      MyVector<double> eNewVectPos(dim);
      for (int i=0; i<dim-1; i++)
	eNewVectPos(i)=eVectPos(i);
      eNewVectPos(dim-1)=zPosition;
      NewListVectPos.push_back(eNewVectPos);
    }
    return NewListVectPos;
  }
# ifdef DEBUG
  std::cerr << "We must have nbNature2 = 0 or 1\n";
  throw TerminalException{1};
# else
  return {};
# endif
}



/*
  This code is for listing 
  In input is:
  --- the list of measurement
     --- the measurements can be of altitude or of distance to a point
     --- the measurement must contain error estimate.
  In output we have
  --- A list of extremal point that satisfy the measurement within the estimate range.
       ----------
  The method is to iterate over all possible subsets of the set of measurement
  and set the measurement to d-e or d+e.
  This allow simple iteration
*/
std::vector<MyVector<double>> EnumerateExtremalPossiblePoints(std::vector<SingleMeas> const& ListSingleMeas)
{
  int nbMeas=ListSingleMeas.size();
#ifdef DEBUG
  std::cerr << "nbMeas=" << nbMeas << "\n";
#endif
  auto GetDim=[&]() -> int {
    for (auto & eSingleMeas : ListSingleMeas) {
      int eDim=eSingleMeas.xPos.size();
      if (eDim > 0)
	return eDim;
    }
# ifdef DEBUG
    std::cerr << "Should never reach that stage\n";
    throw TerminalException{1};
# else
    return -1;
# endif
  };
  int TheDim=GetDim();
  double tol=0.00001;
  auto IsInFeasibilityRegion=[&](MyVector<double> const& x, std::vector<SingleMeas> const& ListMeasSelect) -> bool {
    double zAltitude=x(TheDim-1);
    for (auto & eSingleMeas : ListMeasSelect) {
      if (eSingleMeas.nature == 2) {
	double eError=fabs(zAltitude - eSingleMeas.meas);
	if (eError > tol) {
# ifdef DEBUG
	  std::cerr << "IsInFeasibilityRegion, case 1\n";
	  std::cerr << "eError=" << eError << "\n";
# endif
	  return false;
	}
      }
      if (eSingleMeas.nature == 1) {
	double sum=0;
	for (int i=0; i<TheDim; i++) {
	  double diff=x(i) - eSingleMeas.xPos(i);
	  sum += diff*diff;
	}
	double dist=sqrt(sum);
	double eError=fabs(dist - eSingleMeas.meas);
	if (eError > tol) {
# ifdef DEBUG
	  std::cerr << "IsInFeasibilityRegion, case 2\n";
	  std::cerr << "  eError=" << eError << "\n";
	  std::cerr << "  dist=" << dist << " meas=" << eSingleMeas.meas << "\n";
# endif
	  return false;
	}
      }
    }
    return true;
  };
  auto CoherencyWithtMeasurement=[&](MyVector<double> const& x) -> bool {
    double zAltitude=x(TheDim-1);
    for (auto & eSingleMeas : ListSingleMeas) {
      if (eSingleMeas.nature == 2) {
	double eError=fabs(zAltitude - eSingleMeas.meas);
	if (eError > eSingleMeas.error + tol)
	  return false;
      }
      if (eSingleMeas.nature == 1) {
	double sum=0;
	for (int i=0; i<TheDim; i++) {
	  double diff=x(i) - eSingleMeas.xPos(i);
	  sum += diff*diff;
	}
	double eError=fabs(sqrt(sum) - eSingleMeas.meas);
	if (eError > eSingleMeas.error + tol)
	  return false;
      }
    }
    return true;
  };
  std::vector<MyVector<double>> ListExtremal;
  std::vector<int> Tvect=BinomialStdvect_First(TheDim);
  MyMatrix<int> MatChoice=BuildSet(TheDim,2);
  int nbChoice=MatChoice.rows();
#ifdef DEBUG
  int nbCases=0;
#endif
  while(true) {
#ifdef DEBUG
    std::cerr << "Tvect =";
    for (int i=0; i<TheDim; i++)
      std::cerr << " " << Tvect[i];
    std::cerr << "\n";
    nbCases++;
#endif
    for (int iChoice=0; iChoice<nbChoice; iChoice++) {
      std::vector<SingleMeas> ListMeasSelect(TheDim);
      for (int i=0; i<TheDim; i++) {
	SingleMeas eMeas=ListSingleMeas[Tvect[i]];
	int eVal=MatChoice(iChoice,i);
	if (eVal == 0)
	  eMeas.meas -= eMeas.error;
	if (eVal == 1)
	eMeas.meas += eMeas.error;
	ListMeasSelect[i]=eMeas;
      }
      std::vector<MyVector<double>> ListX=FindPossibleCoordinates(ListMeasSelect);
#ifdef DEBUG
      std::cerr << "|ListX|=" << ListX.size() << "\n";
#endif
      for (auto & x : ListX) {
#ifdef DEBUG
	std::cerr << "x=";
	for (int i=0; i<TheDim; i++) {
	  std::cerr << " " << x(i);
	}
	std::cerr << "\n";
#endif
	bool test1=IsInFeasibilityRegion(x, ListMeasSelect);
	if (test1) {
	  bool test2=CoherencyWithtMeasurement(x);
	  if (test2)
	    ListExtremal.push_back(x);
	}
      }
    }
    bool test=BinomialStdvect_Increment(nbMeas, TheDim, Tvect);
    if (!test)
      break;
  }
#ifdef DEBUG
  std::cerr << "We exit the loop nbCases=" << nbCases << "\n";
#endif
  return ListExtremal;
}







void TestAlgorithmQuality(std::ostream&os, MyMatrix<double> const& Coord, MyMatrix<double> const& NoisyDistMat, int const& dim, int const&NITER)
{
  MyMatrix<double> DistMat1=ComputeDistanceMatrix(Coord);
  os << "Coord=\n";
  WriteMatrix(os, Coord);
  os << "Total noise on input matrix = " << TotalDistance_DistMat(DistMat1, NoisyDistMat) << "\n";
  //
  MyMatrix<double> CoordInit=EigenvalueGuess(NoisyDistMat, dim);
  os << "CoordInit=\n";
  WriteMatrix(os, CoordInit);
  MyMatrix<double> DistMat2=ComputeDistanceMatrix(CoordInit);
  os << "Error on first guess = " << TotalDistance_DistMat(DistMat2, NoisyDistMat) << "\n";
  //
  MyMatrix<double> CoordProv1=GradientSearchMetric(NoisyDistMat, CoordInit, NITER);
  MyMatrix<double> DistMat3=ComputeDistanceMatrix(CoordProv1);
  os << "Error after gradient search on = " << TotalDistance_DistMat(DistMat3, NoisyDistMat) << "\n";
  //
  MyMatrix<double> CoordGuess=FindEmbeddingEquivalence(Coord, CoordProv1);
  //  MyMatrix<double> DistMat4=ComputeDistanceMatrix(CoordGuess);
  //  os << "Error unitary (should be zero) = " << TotalDistance_DistMat(DistMat3, DistMat4) << "\n";
  os << "CoordGuess=\n";
  WriteMatrix(os, CoordGuess);
  //
  MatrixDist ErrorCoord=ErrorCoordinateMatrices(CoordGuess, Coord);
  os << "Final error on distances = " << ErrorCoord << "\n";
}



void TestAdd1(MyMatrix<double> const& Coord)
{
  int nbPoint=Coord.rows();
  MyMatrix<double> DistMat1=ComputeDistanceMatrix(Coord);
  std::cerr << "DistMat1=\n";
  WriteMatrix(std::cerr, DistMat1);
  MyMatrix<double> NoisyDistMat(nbPoint,nbPoint);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      double eVal;
      if (i != j)
	eVal=DistMat1(i,j) + 1;
      else
	eVal=0;
      NoisyDistMat(i,j)=eVal;
    }
  int dim=3;
  int NITER=20;
  TestAlgorithmQuality(std::cerr, Coord, NoisyDistMat, dim, NITER);
}


FullNamelist NAMELIST_GetExtremalAbsolute()
{
  std::map<std::string, SingleBlock> ListBlock;
  // DATA
  std::map<std::string, int> ListIntValues1;
  std::map<std::string, bool> ListBoolValues1;
  std::map<std::string, double> ListDoubleValues1;
  std::map<std::string, std::string> ListStringValues1;
  std::map<std::string, std::vector<int> > ListListIntValues1;
  std::map<std::string, std::vector<double> > ListListDoubleValues1;
  ListStringValues1["FileCoordinate"]="unset.gram";
  ListBoolValues1["UseZposition"]=true;
  ListListDoubleValues1["RealPosition"]={};
  ListListDoubleValues1["ListDistPerturb"]={};
  ListListDoubleValues1["ListErrorDist"]={};
  ListDoubleValues1["Zperturb"]=0.4;
  ListDoubleValues1["ErrorZ"]=0.4;
  SingleBlock BlockDATA;
  BlockDATA.ListIntValues=ListIntValues1;
  BlockDATA.ListListIntValues=ListListIntValues1;
  BlockDATA.ListBoolValues=ListBoolValues1;
  BlockDATA.ListDoubleValues=ListDoubleValues1;
  BlockDATA.ListListDoubleValues=ListListDoubleValues1;
  BlockDATA.ListStringValues=ListStringValues1;
  BlockDATA.ListListIntValues=ListListIntValues1;
  ListBlock["DATA"]=BlockDATA;
  return {ListBlock, "undefined"};
}




void EnumerationExtremalPointsAbsolute(FullNamelist const& eFull)
{
  SingleBlock BlDATA=eFull.ListBlock.at("DATA");
  std::string FileCoordinate=BlDATA.ListStringValues.at("FileCoordinate");
  //
  std::ifstream INmat(FileCoordinate);
  MyMatrix<double> Coord=ReadMatrix<double>(INmat);
  int nbPoint=Coord.rows();
  int dim=Coord.cols();
  //
  std::vector<double> ListDistPerturb=BlDATA.ListListDoubleValues.at("ListDistPerturb");
  std::vector<double> ListErrorDist  =BlDATA.ListListDoubleValues.at("ListErrorDist");
  int nbDistPerturb=ListDistPerturb.size();
  int nbErrorDist=ListErrorDist.size();
  if (nbDistPerturb != nbPoint || nbErrorDist != nbPoint) {
    std::cerr << "nbPoint = " << nbPoint << "\n";
    std::cerr << "nbDistPerturb=" << nbDistPerturb << " nbErrorDist=" << nbErrorDist << "\n";
    std::cerr << "ListDistPerturb or ListErrorDist do not have the right length\n";
    throw TerminalException{1};
  }
  std::vector<double> RealPosition=BlDATA.ListListDoubleValues.at("RealPosition");
  //
  std::vector<SingleMeas> ListSingleMeas;
  for (int iPoint=0; iPoint<nbPoint; iPoint++) {
    MyVector<double> xPos=GetMatrixRow(Coord, iPoint);
    double sum=0;
    for (int i=0; i<dim; i++) {
      double diff=xPos(i) - RealPosition[i];
      sum += diff*diff;
    }
    double meas=sqrt(sum) + ListDistPerturb[iPoint];
    double error=ListErrorDist[iPoint];
    ListSingleMeas.push_back({1, error, meas, xPos});
  }
  //
  bool UseZposition=BlDATA.ListBoolValues.at("UseZposition");
  if (UseZposition) {
    double Zperturb=BlDATA.ListDoubleValues.at("Zperturb");
    double meas=RealPosition[dim-1] + Zperturb;
    double ErrorZ=BlDATA.ListDoubleValues.at("ErrorZ");
    ListSingleMeas.push_back({2,ErrorZ,meas,{}});
  }
  //
  std::vector<MyVector<double>> ListExtremal=EnumerateExtremalPossiblePoints(ListSingleMeas);
  int nbExtremal=ListExtremal.size();
  std::cerr << "nbExtremal=" << nbExtremal << "\n";
  for (auto & eExtremal : ListExtremal) {
    double sum=0;
    for (int i=0; i<dim; i++) {
      double diff=eExtremal(i) - RealPosition[i];
      sum += diff*diff;
    }
    double dist=sqrt(sum);
    std::cerr << "dist=" << dist << "\n";
  }
  //
}






FullNamelist NAMELIST_GetEmbeddingGeneralized()
{
  std::map<std::string, SingleBlock> ListBlock;
  // DATA
  std::map<std::string, int> ListIntValues1;
  std::map<std::string, bool> ListBoolValues1;
  std::map<std::string, double> ListDoubleValues1;
  std::map<std::string, std::string> ListStringValues1;
  std::map<std::string, std::vector<int> > ListListIntValues1;
  std::map<std::string, std::vector<double> > ListListDoubleValues1;
  ListStringValues1["FileCoordinate"]="unset.gram";
  ListBoolValues1["UseZposition"]=false;
  ListDoubleValues1["ErrorDist"]=1;
  ListDoubleValues1["ErrorZ"]=0.4;
  ListBoolValues1["AllDistanceKnown"]=true;
  ListStringValues1["FileAdjacency"]="unset.file";
  ListListIntValues1["ListPointFixed"]={};
  ListIntValues1["NITER"]=20;
  ListDoubleValues1["ValuePerturb"]=1;
  ListDoubleValues1["ThresholdDistance"]=0.1;
  SingleBlock BlockDATA;
  BlockDATA.ListIntValues=ListIntValues1;
  BlockDATA.ListListIntValues=ListListIntValues1;
  BlockDATA.ListBoolValues=ListBoolValues1;
  BlockDATA.ListDoubleValues=ListDoubleValues1;
  BlockDATA.ListStringValues=ListStringValues1;
  BlockDATA.ListListIntValues=ListListIntValues1;
  ListBlock["DATA"]=BlockDATA;
  return {ListBlock, "undefined"};
}



struct ResultGeoloc_absolute {
  bool HasSolution;
  SphereCoord sphCoord;
  MyVector<double> BestFit;
  double RawRadius;
};




/*
  This code is for the absolute localization
  In input is:
  --- the coordinate of the fixed points P_1, ...., P_m
  --- the list of distance d_1, ...., d_m = distance between P_i and P
  --- the list of errors in distance e_1, ...., e_m
  --- the measurement of altitude z of P
  --- the error in the measurement of altitude
  In output we have
  --- the coordinate of a circumscribing sphere around the points
  --- the best fit estimate of the coordinate of P
  --- the raw radius estimate of the circle centered on the best fit.
*/
ResultGeoloc_absolute Geoloc_AbsoluteComputation(MyMatrix<double> const& CoordMat, MyVector<double> const& ListDist, MyVector<double> const& ListDistError, double const& Zmeas, double const& ZerrorInput)
{
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 1\n";
  std::cerr << std::setprecision(9);
#endif
  int nbPoint=CoordMat.rows();
  int nbPointP1=nbPoint+1;
  int dim=CoordMat.cols();
  MyMatrix<double> DistMat(nbPoint+1,nbPoint+1);
  MyMatrix<double> DistErrorMat(nbPoint+1,nbPoint+1);
  for (int iPoint=0; iPoint<nbPoint; iPoint++)
    for (int jPoint=0; jPoint<nbPoint; jPoint++) {
      double sum=0;
      for (int i=0; i<dim; i++) {
	double diff=CoordMat(iPoint,i) - CoordMat(jPoint,i);
	sum += diff*diff;
      }
      DistMat(iPoint,jPoint)=sqrt(sum);
      DistErrorMat(iPoint,jPoint)=0;
    }
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 2\n";
#endif
  for (int iPoint=0; iPoint<nbPoint; iPoint++) {
    DistMat(iPoint,nbPoint) = ListDist(iPoint);
    DistMat(nbPoint,iPoint) = ListDist(iPoint);
    DistErrorMat(iPoint,nbPoint) = ListDistError(iPoint);
    DistErrorMat(nbPoint,iPoint) = ListDistError(iPoint);
  }
  DistMat(nbPoint,nbPoint)=0;
  DistErrorMat(nbPoint,nbPoint)=0;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 3\n";
#endif
  //
  MyVector<double> Zposition(nbPointP1);
  MyVector<double> Zerror(nbPointP1);
  for (int iPt=0; iPt<nbPoint; iPt++) {
    Zposition(iPt)=CoordMat(iPt,dim-1);
    Zerror(iPt)=ZerrorInput;
  }
  Zposition(nbPoint)=Zmeas;
  Zerror(nbPoint)=ZerrorInput;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 4\n";
#endif
  //
  MyMatrix<int> MatAdj(nbPointP1,nbPointP1);
  for (int i=0; i<nbPointP1; i++)
    for (int j=0; j<nbPointP1; j++) {
      int eVal;
      if (i == j)
	eVal=0;
      else
	eVal=1;
      MatAdj(i,j)=eVal;
    }
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 5\n";
#endif
  //
  double ThresholdDistance=0.1;
  //
  std::vector<int> ListPointFixed(nbPoint);
  for (int i=0; i<nbPoint; i++)
    ListPointFixed[i]=i;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 6\n";
#endif
  //
  MetricEmbeddingGeneralized eEmbed;
  eEmbed.dim=dim;
  eEmbed.DistMat=DistMat;
  eEmbed.DistErrorMat=DistErrorMat;
  eEmbed.Zposition=Zposition;
  eEmbed.Zerror=Zerror;
  eEmbed.UseZposition=true;
  eEmbed.MatAdj=MatAdj;
  eEmbed.ThresholdDistance=ThresholdDistance;
  eEmbed.ListPointFixed=ListPointFixed;
  eEmbed.CoordFixed=CoordMat;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 7\n";
#endif
  //
  RecOptionInitial RecOptIni;
  RecOptIni.UsePreviousPosition=false;
  RecOptIni.UseEigenvalueGuess=true;
  MyMatrix<double> CoordInit=InitialCoordinateGeneralized(eEmbed, RecOptIni);
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 8\n";
#endif
  double qExponent=2;
  int NITER=30;
  MyMatrix<double> CoordFound=GradientSearchGeneralized(eEmbed, CoordInit, NITER, qExponent);
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 9\n";
#endif

  MyVector<double> BestFit(dim);
  for (int i=0; i<dim; i++)
    BestFit(i)=CoordFound(nbPoint,i);
#ifdef DEBUG
  std::cerr << "CoordFound=";
  for (int i=0; i<dim; i++)
    std::cerr << " " << CoordFound(nbPoint,i);
  std::cerr << "\n";
#endif
  
  ResultGeoloc_absolute recReturn;
  recReturn.BestFit=BestFit;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 10\n";
#endif
  //
  // Circumscribing sphere
  //
  std::vector<SingleMeas> ListSingleMeas;
  for (int iPoint=0; iPoint<nbPoint; iPoint++) {
    MyVector<double> xPos=GetMatrixRow(CoordMat, iPoint);
    double meas=ListDist(iPoint);
    double error=ListDistError(iPoint);
    ListSingleMeas.push_back({1, error, meas, xPos});
  }
#ifdef DEBUG  
  std::cerr << "ZerrorInput=" << ZerrorInput << " Zmeas=" << Zmeas << "\n";
#endif
  ListSingleMeas.push_back({2,ZerrorInput,Zmeas,{}});
  std::vector<MyVector<double>> ListExtremal=EnumerateExtremalPossiblePoints(ListSingleMeas);
  int nbExtremal=ListExtremal.size();
#ifdef DEBUG  
  std::cerr << "nbExtremal=" << nbExtremal << " dim=" << dim << "\n";
#endif
  SphereCoord eSphCoord=GetCircumscribingCircle(ListExtremal, dim);
  recReturn.sphCoord=eSphCoord;
#ifdef DEBUG  
  std::cerr << "After GetCircumscribibgCircle\n";
#endif
  bool HasSolution=false;
  if (nbExtremal > 0) {
    HasSolution=true;
  }
  recReturn.HasSolution=HasSolution;
  //
  // Raw radiuses
  //
  double RawRadius=-1;
  for (auto & ePt : ListExtremal) {
    double sum=0;
    for (int i=0; i<dim; i++) {
      double diff=BestFit(i) - ePt(i);
      sum += diff*diff;
    }
    double eRad=sqrt(sum);
    if (eRad > RawRadius)
      RawRadius=eRad;
  }
  recReturn.RawRadius=RawRadius;
  //
#ifdef DEBUG  
  std::cerr << "Before return\n";
#endif
  return recReturn;
}



struct ResultGeoloc_relative {
  MyMatrix<double> BestFit;
};





ResultGeoloc_relative Geoloc_RelativeComputation(MyMatrix<double> const& DistMat_meas, MyMatrix<double> const& DistMat_error, MyVector<double> const& Zposition, MyVector<double> const& Zerror, bool const& IsFirst, MyMatrix<double> const& CoordPrevious, RecOptionInitial const& RecOptIni)
{
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 1\n";
  std::cerr << std::setprecision(9);
#endif
  int nbPoint=DistMat_meas.rows();
  int dim=3;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 2\n";
#endif
  //
  MyMatrix<int> MatAdj(nbPoint,nbPoint);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      int eVal;
      if (i == j)
	eVal=0;
      else
	eVal=1;
      MatAdj(i,j)=eVal;
    }
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 3\n";
#endif
  //
  double ThresholdDistance=0.1;
  //
  std::vector<int> ListPointFixed;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 4\n";
#endif
  //
  MyMatrix<double> CoordFixed(0, dim);
  //
  MetricEmbeddingGeneralized eEmbed;
  eEmbed.dim=dim;
  eEmbed.DistMat=DistMat_meas;
  eEmbed.DistErrorMat=DistMat_error;
  eEmbed.Zposition=Zposition;
  eEmbed.Zerror=Zerror;
  eEmbed.UseZposition=true;
  eEmbed.MatAdj=MatAdj;
  eEmbed.ThresholdDistance=ThresholdDistance;
  eEmbed.ListPointFixed={};
  eEmbed.CoordFixed=CoordFixed;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 5\n";
#endif
  //
  MyMatrix<double> CoordInit;
  if (RecOptIni.UsePreviousPosition && !IsFirst) {
    CoordInit = CoordPrevious;
  }
  else {
    CoordInit=InitialCoordinateGeneralized(eEmbed, RecOptIni);
  }
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 6\n";
  std::cerr << "CoordInit=\n";
  WriteMatrix(std::cerr, CoordInit);
#endif
  double qExponent=2;
  int NITER=30;
  MyMatrix<double> BestFit=GradientSearchGeneralized(eEmbed, CoordInit, NITER, qExponent);
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 7\n";
#endif
  
  ResultGeoloc_relative recReturn;
  recReturn.BestFit=BestFit;
  return recReturn;
}











ResultGeoloc_relative Geoloc_RelativeComputation_NoZ(MyMatrix<double> const& DistMat_meas, MyMatrix<double> const& DistMat_error)
{
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 1\n";
  std::cerr << std::setprecision(9);
#endif
  int nbPoint=DistMat_meas.rows();
  int dim=3;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 2\n";
#endif
  //
  MyMatrix<int> MatAdj(nbPoint,nbPoint);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      int eVal;
      if (i == j)
	eVal=0;
      else
	eVal=1;
      MatAdj(i,j)=eVal;
    }
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 3\n";
#endif
  //
  double ThresholdDistance=0.1;
  //
  std::vector<int> ListPointFixed;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 4\n";
#endif
  //
  MyMatrix<double> CoordFixed(0, dim);
  //
  MetricEmbeddingGeneralized eEmbed;
  eEmbed.dim=dim;
  eEmbed.DistMat=DistMat_meas;
  eEmbed.DistErrorMat=DistMat_error;
  eEmbed.UseZposition=false;
  eEmbed.MatAdj=MatAdj;
  eEmbed.ThresholdDistance=ThresholdDistance;
  eEmbed.ListPointFixed={};
  eEmbed.CoordFixed=CoordFixed;
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 5\n";
#endif
  //
  RecOptionInitial RecOptIni;
  RecOptIni.UsePreviousPosition=false;
  RecOptIni.UseEigenvalueGuess=true;
  MyMatrix<double> CoordInit=InitialCoordinateGeneralized(eEmbed, RecOptIni);
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 6\n";
#endif
  double qExponent=2;
  int NITER=30;
  MyMatrix<double> BestFit=GradientSearchGeneralized(eEmbed, CoordInit, NITER, qExponent);
#ifdef DEBUG
  std::cerr << "ComputeGeneralTrilateration, step 7\n";
#endif
  
  ResultGeoloc_relative recReturn;
  recReturn.BestFit=BestFit;
  return recReturn;
}





void TestAdd1Generalized(FullNamelist const& eFull)
{
  SingleBlock BlDATA=eFull.ListBlock.at("DATA");
  std::string FileCoordinate=BlDATA.ListStringValues.at("FileCoordinate");
  //
  if (!IsExistingFile(FileCoordinate)) {
    std::cerr << "Missing file. FileCoordinate = " << FileCoordinate << "\n";
    throw TerminalException{1};
  }
  std::ifstream INmat(FileCoordinate);
  MyMatrix<double> Coord=ReadMatrix<double>(INmat);
  int nbPoint=Coord.rows();
  int dim=Coord.cols();
  //
  bool AllDistanceKnown=BlDATA.ListBoolValues.at("AllDistanceKnown");
  MyMatrix<int> MatAdj(nbPoint,nbPoint);
  if (AllDistanceKnown) {
    for (int i=0; i<nbPoint; i++)
      for (int j=0; j<nbPoint; j++) {
	int eVal;
	if (i == j)
	  eVal=0;
	else
	  eVal=1;
	MatAdj(i,j)=eVal;
      }
  }
  else {
    std::string FileAdjacency=BlDATA.ListStringValues.at("FileAdjacency");
    if (!IsExistingFile(FileAdjacency)) {
      std::cerr << "Missing file. FileAdjacency = " << FileAdjacency << "\n";
      throw TerminalException{1};
    }
    std::ifstream INadj(FileAdjacency);
    MatAdj=ReadMatrix<int>(INadj);
  }
  //
  MyVector<double> Zposition(nbPoint);
  for (int iPt=0; iPt<nbPoint; iPt++)
    Zposition(iPt)=Coord(iPt,dim-1);
  //
  double ErrorDist=BlDATA.ListDoubleValues.at("ErrorDist");
  double ErrorZ=BlDATA.ListDoubleValues.at("ErrorZ");
  double ThresholdDistance=BlDATA.ListDoubleValues.at("ThresholdDistance");
  MyMatrix<double> DistErrorMat(nbPoint,nbPoint);
  for (int iPt=0; iPt<nbPoint; iPt++)
    for (int jPt=0; jPt<nbPoint; jPt++)
      DistErrorMat(iPt,jPt)=ErrorDist;
  MyVector<double> Zerror(nbPoint);
  for (int iPt=0; iPt<nbPoint; iPt++)
    Zerror(iPt)=ErrorZ;
  //
  std::vector<int> ListPointFixed=BlDATA.ListListIntValues.at("ListPointFixed");
  int nbPointFixed=ListPointFixed.size();
  for (int iFixed=0; iFixed<nbPointFixed-1; iFixed++)
    for (int jFixed=iFixed+1; jFixed<nbPointFixed; jFixed++) {
      int eFixed=ListPointFixed[iFixed];
      int fFixed=ListPointFixed[jFixed];
      MatAdj(eFixed,fFixed)=0;
      MatAdj(fFixed,eFixed)=0;
    }


  MyMatrix<double> CoordFixed(nbPointFixed,dim);
  for (int iFixed=0; iFixed<nbPointFixed; iFixed++) {
    int eFixed=ListPointFixed[iFixed];
    CoordFixed.row(iFixed) = Coord.row(eFixed);
  }
  //
  double ValuePerturb=BlDATA.ListDoubleValues.at("ValuePerturb");
  int NITER=BlDATA.ListIntValues.at("NITER");
  bool UseZposition=BlDATA.ListBoolValues.at("UseZposition");
  //
  MyMatrix<double> DistMat1=ComputeDistanceMatrix(Coord);
  MyMatrix<double> NoisyDistMat(nbPoint,nbPoint);
  for (int i=0; i<nbPoint; i++)
    for (int j=0; j<nbPoint; j++) {
      double eVal;
      if (MatAdj(i,j) == 1)
	eVal=DistMat1(i,j) + ValuePerturb;
      else
	eVal=0;
      NoisyDistMat(i,j)=eVal;
    }
  //
  MetricEmbeddingGeneralized eEmbed;
  eEmbed.dim=dim;
  eEmbed.DistMat=NoisyDistMat;
  eEmbed.DistErrorMat=DistErrorMat;
  eEmbed.Zposition=Zposition;
  eEmbed.Zerror=Zerror;
  eEmbed.UseZposition=UseZposition;
  //  eEmbed.ErrorDist=ErrorDist;
  //  eEmbed.ErrorZ=ErrorZ;
  eEmbed.MatAdj=MatAdj;
  eEmbed.ThresholdDistance=ThresholdDistance;
  eEmbed.ListPointFixed=ListPointFixed;
  eEmbed.CoordFixed=CoordFixed;
  //
  std::ostream &os = std::cerr;
  //
  os << "Total noise on input matrix = " << TotalDistance_DistMat(DistMat1, NoisyDistMat) << "\n";
  //
  RecOptionInitial RecOptIni;
  RecOptIni.UsePreviousPosition=false;
  RecOptIni.UseEigenvalueGuess=true;
  MyMatrix<double> CoordInit=InitialCoordinateGeneralized(eEmbed, RecOptIni);
  os << "CoordInit=\n";
  WriteMatrix(os, CoordInit);
  MyMatrix<double> DistMat2=ComputeDistanceMatrix(CoordInit);
  os << "Error on first guess = " << TotalDistance_DistMat(DistMat2, NoisyDistMat) << "\n";
  //
  double qExponent=2;
  MyMatrix<double> CoordFound=GradientSearchGeneralized(eEmbed, CoordInit, NITER, qExponent);
  os << "CoordFound=\n";
  WriteMatrix(os, CoordFound);
  MyMatrix<double> DistMat3=ComputeDistanceMatrix(CoordFound);
  os << "Error after gradient search on = " << TotalDistance_DistMat(DistMat3, NoisyDistMat) << "\n";
  //
  MyMatrix<double> CoordGuess;
  if (nbPointFixed == 0) {
    CoordGuess=FindEmbeddingEquivalence(Coord, CoordFound);
  }
  else {
    CoordGuess=CoordFound;
  }
  //  MyMatrix<double> DistMat4=ComputeDistanceMatrix(CoordGuess);
  //  os << "Error unitary (should be zero) = " << TotalDistance_DistMat(DistMat3, DistMat4) << "\n";
  os << "DistMat3 / NoisyDistMat : " << ComputeNorms(DistMat3, NoisyDistMat) << "\n";
  os << "CoordGuess=\n";
  WriteMatrix(os, CoordGuess);
  //
  MatrixDist ErrorCoord=ErrorCoordinateMatrices(CoordGuess, Coord);
  os << "Final error on distances = " << ErrorCoord << "\n";
  //
  double ErrorZcoord=ErrorZpositionMatrices(CoordGuess, eEmbed.Zposition);
  os << "Final error on vertical position = " << ErrorZcoord << "\n";
}










#endif
