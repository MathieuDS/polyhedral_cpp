#include "Metric.h"

int main(int argc, char *argv[])
{
  try {
    if (argc != 3) {
      fprintf(stderr, "Number of argument is = %d\n", argc);
      fprintf(stderr, "This program is used as\n");
      fprintf(stderr, "GeneralProgEmbedding [inputEmbedCase] [outCoord]\n");
      return -1;
    }
    // reading the matrix
    std::ifstream INmat(argv[1]);
    MetricEmbeddingGeneralized eEmbed=ReadOneEmbeddingCase(INmat);
    MyMatrix<double> Coord=GetEmbeddingGeneralized(eEmbed);
    std::ofstream OUTres(argv[2]);
    WriteMatrixMatlab(OUTres, Coord);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
