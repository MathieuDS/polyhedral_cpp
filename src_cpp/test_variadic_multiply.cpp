#include <iostream>

auto VariadicMultiply()
{
  return 1;
}

template<typename T1, typename... T>
auto VariadicMultiply(T1 t, T... ts)
{
  return t * VariadicMultiply(ts...);
}

int main()
{
  std::cout << VariadicMultiply<int,int>(10,10,10,10) << "\n";
}
