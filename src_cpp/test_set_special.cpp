#include <cstddef>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <functional>
#include <algorithm>
#include "gmpxx.h"



template<typename T>
struct SimpleOrbitFacetInv {
  int size;
  mpz_class eOrbitSize;
  T eVal;
};

template<typename T>
bool operator==(SimpleOrbitFacetInv<T> const& x, SimpleOrbitFacetInv<T> const& y)
{
  if (x.size != y.size)
    return false;
  if (x.eOrbitSize != y.eOrbitSize)
    return false;
  if (x.eVal != y.eVal)
    return false;
  return true;
}

template<typename T>
bool operator<(SimpleOrbitFacetInv<T> const& eOrb, SimpleOrbitFacetInv<T> const& fOrb)
{
  if (eOrb.size < fOrb.size)
    return true;
  if (fOrb.size > fOrb.size)
    return false;
  if (eOrb.eOrbitSize < fOrb.eOrbitSize)
    return true;
  if (eOrb.eOrbitSize > fOrb.eOrbitSize)
    return false;
  return eOrb.eVal < fOrb.eVal;
}






int main ()
{
  std::vector<SimpleOrbitFacetInv<mpq_class>> ListInv;
  ListInv.push_back({25,96,13263827111/8});
  ListInv.push_back({40,24,2694005506});
  ListInv.push_back({36,72,4856914241/2});
  ListInv.push_back({20,1152,5297779867/4});
  ListInv.push_back({36,24,4856914241/2});
  ListInv.push_back({20,384,5272026691/4});
  ListInv.push_back({20,96,5265596701/4});
  ListInv.push_back({28,96,1865180224});
  ListInv.push_back({20,384,5272026691/4});
  ListInv.push_back({20,48,5265596701/4});
  ListInv.push_back({28,96,1865180224});
  ListInv.push_back({32,96,2150036236});
  ListInv.push_back({25,48,13263827111/8});
  ListInv.push_back({25,48,13263827111/8});
  ListInv.push_back({30,48,4040971949/2});

  std::vector<int> ListInt{9,12,14,6,8,7,11,5,4,3,2,1};
  std::function<int(int const&)> GetPos=[&](int const& eVal) -> int {
    for (int i=0; i<int(ListInt.size()); i++)
      if (ListInt[i] == eVal)
	return i;
    std::cerr << "Major error\n";
    exit(1);
  };
  //  std::set<int,std::function<bool(int const&,int const&)> > ListPendingCases([&](int const&i, int const&j) -> bool {return ListInv[i] < ListInv[j];});
  std::set<int,std::function<bool(int const&,int const&)> > ListPendingCases([&](int const&i, int const&j) -> bool {return GetPos(i) < GetPos(j);});
  for (auto eVal : ListInt)
    ListPendingCases.insert(eVal);
  std::cerr << "ListPendingCases=";
  for (auto & eVal : ListPendingCases)
    std::cerr << " " << eVal;
  std::cerr << "\n";
  for (int i=0; i<int(ListInt.size()); i++) {
    int eVal=ListInt[i];
    size_t reply=ListPendingCases.erase(eVal);
    std::cerr << "reply=" << reply << "\n";
  }
  
}

