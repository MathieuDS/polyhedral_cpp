#include <iostream>
#include <vector>
#include <algorithm>

void FindTHR(std::vector<int> const& xs, int const& threshold)
{
  /*
  std::vector<int>::const_iterator i = xs.begin();
  for (; i != xs.end(); ++i)
    if (*i >= threshold)
      break;
  */
  auto i = find_if(xs.begin(), xs.end(), [&threshold](int i) -> bool {return i >= threshold;});
  if (i == xs.end())
    std::cerr << "Not found\n";
  std::cerr << "Found i=" << *i << "\n";
}


int main(int argc, char* argv[])
{
  std::vector<int> xs{1,2,3};
  FindTHR(xs, 4);
}
