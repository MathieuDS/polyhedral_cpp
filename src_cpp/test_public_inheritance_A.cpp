#include <iostream>
struct subclass {
private:
  int val;
public:
  subclass() : val(0)
  {
    std::cerr << "subclass constructor\n";
  }
  ~subclass()
  {
    std::cerr << "subclass destructor\n";
  }
  int get_A()
  {
    std::cerr << "subclass get_A\n";
    return val;
  }
  void set_A(int _val)
  {
    std::cerr << "subclass set_A\n";
    val = _val;
  }

  int get_B()
  {
    std::cerr << "subclass get_B\n";
    return val;
  }
  void set_B(int _val)
  {
    std::cerr << "subclass set_B\n";
    val = _val;
  }

};



struct overclass : subclass {
public:
  overclass() : subclass() 
  {
    std::cerr << "overclass constructor\n";
  }
  ~overclass()
  {
    std::cerr << "overclass destructor\n";
  }
  
  int get_B()
  {
    std::cerr << "overclass get_B\n";
    return subclass::get_B();
  }
  void set_B(int _val)
  {
    std::cerr << "overclass set_B\n";
    subclass::set_B(_val);
  }


};



int main()
{
  overclass inst;
  inst.set_A(32);
  std::cerr << "1 : val=" << inst.get_B() << "\n";
  inst.set_B(42);
  std::cerr << "2 : val=" << inst.get_A() << "\n";

}
