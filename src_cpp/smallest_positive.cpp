#include <iostream>
#include <iomanip>
#include <cmath>


int main()
{
  //  using T=float;
  using T=double;
  T val = 0;
  T val_pos = std::nextafter(val, 1.f);
  std::cerr << std::setprecision(20) << "val_pos=" << val_pos << "\n";
}
