#include <string>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

// This test shows that assignation of data from a pointer leads to the data
// being copied anyway.

int main ()
{
  int n=5;
  std::vector<int> V(n, 0);
  int *ptr = V.data();
  for (int i=0; i<n; i++)
    ptr[i] = 1;
  for (int i=0; i<n; i++)
    std::cerr << "i=" << i << " V[i]=" << V[i] << "\n";
}

