#include <iostream>
#include <set>
#include <string>
#include <functional>


int test_sum(int a, int b)
{
  int sum=0;
  int idx=a;
  while(true) {
    sum = sum + idx;
    if (idx == b)
      break;
    idx = idx + 1;
  }
  return sum;
}


int main ()
{
  std::cerr << "sum (1,3) result=" << test_sum(1, 3) << "\n";
}

