#include <iostream>
#include <set>
#include <string>
#include <functional>


void perimeter_area(int a)
{
  int ePerim = 4*a;
  int eArea = a*a;
  std::cerr << "perimeter=" << ePerim << "\n";
  std::cerr << "area=" << eArea << "\n";
}


int main ()
{
  perimeter_area(3);
}

