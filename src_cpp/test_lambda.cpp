#include <iostream>
#include <functional>


int main ()
{
  std::function<void(int&,int&)> f=[](int & a, int&b) -> void {
    a=2*a;
    b=3*b;
  };
  int a=2;
  int b=3;
  f(a,b);
  std::cerr << "a=" << a << "\n";
  std::cerr << "b=" << b << "\n";
}

