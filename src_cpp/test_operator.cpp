#include <iostream>
#include <string>



struct cont {
public:
  void operator()(double const& val)
  {
    std::cerr << "operator() : double with val=" << val << "\n";
  }
  void operator()(int const& val)
  {
    std::cerr << "operator() : int with val=" << val << "\n";
    (*this)((double)val);
  }
  void operator()(std::string const& val)
  {
    std::cerr << "operator() : std::string with val=" << val << "\n";
  }
};


int main()
{
  cont inst;
  inst(3);
  inst(3.14);
  inst("abcd");
}
