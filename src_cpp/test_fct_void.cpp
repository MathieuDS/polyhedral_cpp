#include <iostream>
#include <functional>

extern int a[];
typedef decltype(a) T1;
int a[10];
typedef decltype(a) T2;

int main ()
{
  std::cerr << "test=" << std::is_same_v<int,int> << "\n";
  std::cerr << "test=" << std::is_same<void,void>::value << "\n";
}

