#include <iostream>

#ifdef MEMCHECK
# define MACRO_SET(a,b) a = b;
#else
# define MACRO_SET(a,b)
#endif

int main()
{
  int a = 4;
  MACRO_SET(a,11+4)
  std::cerr << "a=" << a << "\n";
}

