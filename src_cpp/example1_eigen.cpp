#include "Temp_common.h"
#include <Eigen/Dense>
#include <Eigen/LU>

template <typename T> using MyMatrix=Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;

MyMatrix<double> HilbertMatrix(int const& n)
{
  MyMatrix<double> eMat(n, n);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      eMat(i,j)=double(1)/(double(i+j+1));
  return eMat;
}

int main ()
{
  for (int n=1; n<10; n++) {
    MyMatrix<double> B=HilbertMatrix(n);
    int nRow=B.rows();
    int nCol=B.cols();
    for (int iRow=0; iRow<nRow; iRow++)
      for (int iCol=0; iCol<nCol; iCol++)
	std::cerr << "iRow/iCol=" << iRow << "," << iCol << " eVal=" << B(iRow,iCol) << "\n";
  }
  return 0;
}

