#include <iostream>
#include <stdlib.h>
#include <set>
#include <ctime>
#include <unordered_set>
#include <functional>

int main(int argc, char* argv[])
{
  auto Timings=[&](int const n) -> void {
    std::clock_t start1 = std::clock();
    std::set<int> eSet;
    for (int i=0; i<n; i++) {
      int val = rand();
      eSet.insert(val);
    }
    std::cout << "------------ n=" << n << "-------------------\n";
    std::cout << "   |eSet|=" << eSet.size() << "\n";
    std::clock_t stop1 = std::clock();
    double time1 =  (double(stop1 - start1) / CLOCKS_PER_SEC);
    std::cerr << "   time(set) =        " << time1 << "\n";
    //
    std::clock_t start2 = std::clock();
    std::unordered_set<int> eSet2;
    for (int i=0; i<n; i++) {
      int val = rand();
      eSet2.insert(val);
    }
    std::cout << "   |eSet2|=" << eSet2.size() << "\n";
    std::clock_t stop2 = std::clock();
    double time2 =  (double(stop2 - start2) / CLOCKS_PER_SEC);
    double quot = time1 / time2;
    std::cerr << "   time(unord) =    " << time2 << " quot=" << quot << "\n";
  };
  Timings(100);
  Timings(1000);
  Timings(10000);
  Timings(100000);
  Timings(1000000);
  Timings(10000000);
  Timings(100000000);
  Timings(1000000000);
}
