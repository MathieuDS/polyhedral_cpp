// Copyright: Mathieu Dutour Sikiric
#include <iostream>
#include <vector>

struct data {
  std::vector<int> V;
};


data GetStruct(int n) {
  std::vector<int> V(n, 0);
  return {V};
}


int main() {
  data eData = GetStruct(10);
  std::cerr << "eData.V[0]=" << eData.V[0] << "\n";
}

