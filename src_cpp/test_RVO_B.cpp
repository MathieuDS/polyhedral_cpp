#include <iostream>
#include <string>

struct C {
public:
  int* ptr;
  C(const size_t& len)
  {
    ptr = (int*) malloc(len * sizeof(int));
  }

  ~C()
  {
    std::cerr << "Passing by the destructor\n";
    free(ptr);
  }
  C() = delete;
  C(const C&) = delete;
  C(C && x) = delete;
  C(C && x) {
    std::cerr << "Passing by the && constructor\n";
    std::cerr << "Curiously, it is not invoked, even if it exists\n";
    ptr = x.ptr;
    x.ptr = nullptr;
    }
  C& operator=(const C&) = delete;
};

C get_object(size_t len)
{
  C obj(len);
  return obj;
}

int main()
{
  size_t len=1000;
  C obj_A = get_object(len);
  for (size_t i=0; i<len; i++)
    obj_A.ptr[i] = i;
  size_t sum=0;
  for (size_t i=0; i<len; i++)
    sum += obj_A.ptr[i];
  std::cerr << "sum=" << sum << "\n";
}
