#include <iostream>
#include <limits>

int main()
{

  double v1 = std::numeric_limits<double>::quiet_NaN();
  double v2 = 3;
  double v3 = std::max(v1, v2);
  double v4 = std::min(v1, v2);
  std::cout << "v3=" << v3 << " v4=" << v4 << "\n";
  bool test = std::numeric_limits<double>::has_quiet_NaN;
  std::cout << "test=" << test << "\n";
}
