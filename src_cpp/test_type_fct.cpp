#include <iostream>


template<typename F>
void process_function(F f)
{
  for (int i=0; i<10; i++) {
    std::cerr << "i=" << i << " f(i)=" << f(i) << "\n";
  }
}


int f(int val)
{
  return 2*val + val * val;
}


int main()
{
  //  using T_F = (int*)(int);
  process_function<decltype(f)>(f);
}
