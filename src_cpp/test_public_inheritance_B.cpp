#include <iostream>
#include <string>

struct subclass {
private:
  int val_i;
  std::string val_s;
public:
  subclass(int val_i, std::string val_s) : val_i(val_i), val_s(val_s)
  {
    std::cerr << "subclass constructor\n";
  }
  ~subclass()
  {
    std::cerr << "subclass destructor\n";
  }
  int get_val_i()
  {
    std::cerr << "subclass get_val_i\n";
    return val_i;
  }
  std::string get_val_s()
  {
    std::cerr << "subclass get_val_s\n";
    return val_s;
  }
};


template<typename T, typename... Args>
struct overclass : subclass {
public:
  double val_d;
public:
  overclass(double val_d, Args &... args) : val_d(val_d), subclass(args...)
  {
    std::cerr << "overclass constructor\n";
  }
  ~overclass()
  {
    std::cerr << "overclass destructor\n";
  }
  double get_val_d()
  {
    std::cerr << "overclass get_val_d\n";
    return val_d;
  }
};



int main()
{
  double a = 3.14;
  int b = 3;
  std::string c = "pi constant";
  overclass<double,int,std::string> inst(a, b, c);
  std::cerr << "inst val_d=" << inst.get_val_d() << " val_i=" << inst.get_val_i() << " val_s=" << inst.get_val_s() << "\n";
}
