#include <iostream>
#include <variant>
#include <vector>


struct typ1 {
  int val;
};



struct typ2 {
  int val;
};


void print(const typ1& x, int u)
{
  std::cerr << "print(typ1) x=" << x.val << " u=" << u << "\n";
}

void print(const typ2& x, int u)
{
  std::cerr << "print(typ2) x=" << x.val << " u=" << u << "\n";
}


template <typename T, typename... Args>
struct is_one_of:
  std::disjunction<std::is_same<std::decay_t<T>, Args>...> {};


int main()
{
  using T = std::variant<typ1,typ2>;

  std::vector<T> V;
  for (int i=0; i<10; i++) {
    size_t idx = rand() % 2;
    int val = rand() % 100;
    if (idx == 0) {
      V.push_back(typ1{val});
    }
    if (idx == 1) {
      V.push_back(typ2{val});
    }
  }
  int u=0;
  for (auto & x : V) {
    std::visit([&](auto&& arg) {
      static_assert(is_one_of<decltype(arg), typ1, typ2>{}, "Non matching type.");
      using T = std::decay_t<decltype(arg)>;
      if constexpr (std::is_same_v<T, typ1>)
                     print(arg, u);
      else if constexpr (std::is_same_v<T, typ2>)
                     print(arg, u);
      else
        std::cout << "Not allowed type\n";
    }, x);
    u++;
  }
}

