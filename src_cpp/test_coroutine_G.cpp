#include <boost/coroutine/all.hpp>
#include <iostream>

using namespace boost::coroutines;

void cooperativeA(coroutine<void>::push_type &sink)
{
  std::cout << "Hello";
  sink();
  std::cout << "world";
}

void cooperativeB(coroutine<void>::push_type &sink)
{
  std::cout << "Hello";
  sink();
  std::cout << "world";
}

int main()
{
  coroutine<void>::pull_type source{cooperativeA, cooperativeB};
  std::cout << ", ";
  source();
  std::cout << "!\n";
}

