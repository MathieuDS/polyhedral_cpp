#include <iostream>
#include <vector>
#include <concepts>

struct contain {
  using Telt=int;
  int x;
};




template<typename T>
concept Hashable = requires(T a)
{
  { std::hash<T>{}(a) } -> std::convertible_to<std::size_t>;
  std::hash<T>{}(a);
};

template<typename T>
concept Addable = requires(T a) {
   a += a;
   { a + a } -> std::constructible_from<T>;
};


template<typename T>
T sum_vector(std::vector<T> const& V) requires Addable<T> && Hashable<typename contain::Telt>
{
  T e_sum;
  for (auto & eVal : V)
    e_sum += eVal;
  return e_sum;
}



template<typename T>
concept Fieldable = requires(T a, T b) {
   a + b;
   a * b;
   a - b;
   0;
};


template<typename T>
T strange_operation(std::vector<T> const& V) requires Fieldable<T>
{
  T e_res;
  for (auto & eVal : V)
    e_res = e_res + e_res * eVal + eVal;
  return e_res;
}








int main ()
{
  std::vector<float> V_float{1,2,3};
  std::vector<int> V_int{1,2,3};
  std::vector<std::string> V_str{"a", "BCK", "123"};
  std::cerr << "sum(V_int)=" << sum_vector(V_int) << "\n";
  std::cerr << "sum(V_str)=" << sum_vector(V_str) << "\n";
  std::cerr << "strange(V_str)=" << strange_operation(V_float) << "\n";
  //  std::cerr << "sum(V_float)=" << sum_vector(V_float) << "\n";     // does not compile
}

