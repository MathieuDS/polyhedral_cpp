#include <iostream>
#include <functional>
#include <queue>



int main()
{

  std::priority_queue<int, std::vector<int>, std::function<bool(int,int)>> pq;
  std::function<bool(int,int)> fct = [](int a, int b) -> bool {
    return a > b;
  };
  pq = std::priority_queue<int, std::vector<int>, std::function<bool(int,int)>>(fct);

  for (int i=0; i<10; i++) {
    int val = rand() % 100;
    std::cerr << "i=" << i << " val=" << val << "\n";
    pq.push(val);
  }
  //
  while (!pq.empty()) {
    std::cerr << " " << pq.top();
    pq.pop();
  }
  std::cerr << '\n';
}
