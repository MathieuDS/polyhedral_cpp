#include <iostream>
struct subclass {
public:
  subclass(std::string str)
  {
    std::cerr << "subclass constructor\n";
  }
  ~subclass()
  {
    std::cerr << "subclass destructor\n";
  }
  int operator()(int val)
  {
    std::cerr << "subclass : operator() : val=" << val << "\n";
    return val;
  }
};


template<typename T>
struct overclass : T {
public:
  overclass(std::string str) : T(str)
  {
    std::cerr << "overclass constructor\n";
  }
  ~overclass()
  {
    std::cerr << "overclass destructor\n";
  }
  int operator()(int val)
  {
    std::cerr << "overclass : operator() : val=" << val << "\n";
    return T::operator()(val);
  }
};



int main()
{
  overclass<subclass> inst("ABCD");
  inst(32);
}
