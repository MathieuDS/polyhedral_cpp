#include <functional>
#include <boost/coroutine2/coroutine.hpp>
#include <boost/coroutine/all.hpp>
#include <iostream>


void cooperative_A(boost::coroutines2::coroutine<int>::push_type & sink, int idx)
{
  std::cout << "[coop_A] First step\n";
  sink(42 + idx);
  std::cout << "[coop_A] Second step\n";
}


void cooperative_B(boost::coroutines2::coroutine<long>::push_type & sink, boost::coroutines2::coroutine<int>::pull_type & source)
{
  std::cout << "[coop_B] First step\n";
  int val = source.get();
  source();
  long val_l = val;
  sink(val_l);
  std::cout << "[coop_B] Second step\n";
}





int main()
{
  int idx = 10;
  auto f=[&](boost::coroutines2::coroutine<int>::push_type & sink) -> void {
    cooperative_A(sink, idx);
  };
  boost::coroutines2::coroutine<int>::pull_type source_A{f};
  std::cout << "[main] step 1\n";
  auto g=[&](boost::coroutines2::coroutine<long>::push_type & sink) -> void {
    cooperative_B(sink, source_A);
  };
  std::cout << "[main] step 2\n";
  boost::coroutines2::coroutine<long>::pull_type source_B{g};
  std::cout << "[main] step 3\n";
  long val = source_B.get();
  source_B();
  std::cout << "[main] val=" << val << "\n";
}

