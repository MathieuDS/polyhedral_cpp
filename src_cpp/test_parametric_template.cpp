#include <iostream>

template<typename T, T x_const>
struct TestClass {
  T x;
  TestClass(T _x) : x(_x) {}
  T get_val(bool test) {
    if (test) {
      return x_const;
    } else {
      return x;
    }
  }
};



int main()
{
  TestClass<int,3> x1(4);
  std::cerr << "x1.get_val(true)=" << x1.get_val(true) << "\n";
  std::cerr << "x1.get_val(false)=" << x1.get_val(false) << "\n";
}
