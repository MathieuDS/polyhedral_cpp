#include<vector>
#include<algorithm>


int main()
{
  std::vector<int> V{1,2,3,4,5};
  // This is to check that it is part of C++11 (and not later C++14, C++17)
  std::nth_element(V.begin(), V.begin(), V.end());
}
