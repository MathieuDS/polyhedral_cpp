#include <iostream>


class Identity {
public:
  virtual void PrintFirst() {
    std::cerr << "PrintFirst : Identity\n";
  }
  virtual void PrintSecond() {
    std::cerr << "PrintSecond : Identity\n";
  }
};


class French : public Identity
{
  virtual void PrintFirst() {
    std::cerr << "PrintFirst : French\n";
  }
  virtual void PrintSecond() {
    std::cerr << "PrintSecond : French\n";
  }
};


class Croatian : public Identity
{
  virtual void PrintFirst() {
    std::cerr << "PrintFirst : Croatian\n";
  }
  /*
  virtual void PrintSecond() {
    std::cerr << "PrintSecond : Croatian\n";
    }*/
};


int main(int argc, char** argv)
{
  Identity *ePRC;
  French *ePRC_fr;
  Croatian *ePRC_hr;
  ePRC = new Identity();
  ePRC_fr = new French();
  ePRC_hr = new Croatian();
  //
  ePRC->PrintFirst();
  ePRC->PrintSecond();
  delete ePRC;
  //
  ePRC = ePRC_fr;
  ePRC->PrintFirst();
  ePRC->PrintSecond();
  delete ePRC_fr;
  //
  ePRC = ePRC_hr;
  ePRC->PrintFirst();
  ePRC->PrintSecond();
  delete ePRC_hr;
}
