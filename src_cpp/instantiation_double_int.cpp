#include <cmath>


template <typename T>
inline typename std::enable_if<!std::is_floating_point<T>::value, bool>::type
isnan_alltype(T const& val) {
    return false;
}

template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value, bool>::type
isnan_alltype(T const& val) {
    return std::isnan(val);
}

template<typename T>
T sum(T v1, T v2)
{
    if (!isnan_alltype(v2))
      return v1 + v2;
    return v1;
}

double sum_d(double v1, double v2)
{
    return sum(v1, v2);
}

int sum_i(int v1, int v2)
{
    return sum(v1, v2);
}

int main() {
    return 0;
}

