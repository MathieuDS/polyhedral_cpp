#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <functional>

int main(int argc, char* argv[])
{

  auto hash_fct=[&](size_t eVal) -> size_t {
    return std::hash<size_t>()(eVal % 128);
  };
  auto equal_fct=[&](size_t eVal1, size_t eVal2) -> bool {
    return eVal1 == eVal2;
  };
  //
  std::unordered_map<size_t, size_t, decltype(hash_fct), decltype(equal_fct)> eVect({}, hash_fct, equal_fct);
  for (size_t i=0; i<10; i++) {
    size_t& group = eVect[i];
    std::cerr << "1: i=" << i << " group=" << group << "\n";
    group = 1;
  }
  for (size_t i=0; i<10; i++) {
    std::cerr << "2: i=" << i << " group=" << eVect[i] << "\n";
  }


  
}
