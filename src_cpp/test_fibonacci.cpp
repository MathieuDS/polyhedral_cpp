#include <iostream>
#include <vector>

int main(int argc, char* argv[])
{
  int n=60;
  std::vector<unsigned int> eVect(n);
  eVect[0]=0;
  eVect[1]=1;
  for (int i=2; i<n; i++)
    eVect[i] = eVect[i-1] + eVect[i-2];
  for (int i=0; i<n; i++)
    std::cerr << "i=" << i << " fib=" << eVect[i] << "\n";
}
