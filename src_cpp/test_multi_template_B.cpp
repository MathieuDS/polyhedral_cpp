#include <string>
#include <iostream>



template<typename T, template<typename E, typename F> typename U>
struct contain {
  int fct(int x)
  {
    return x;
  }
};


int main()
{
  contain<int,std::tuple><int,float,double> cont;
  std::cerr << "val=" << cont.fct(1) << "\n";
}
