#include <iostream>
#include <map>



int main()
{

  std::map<int,int> l;


  int tot_sum_a = 0;
  for (int i=0; i<100; i++) {
    int pos = rand() % 10;
    int val = rand() % 100;

    auto iter = l.find(pos);
    if (iter == l.end()) {
      l[pos] = 0;
      iter = l.find(pos);
    }
    iter->second += val;
    tot_sum_a += val;
  }

  // iterating over all entries
  int tot_sum_b = 0;
  for (auto kv : l) {
    tot_sum_b += kv.second;
  }
  std::cerr << "tot_sum_a=" << tot_sum_a << " tot_sum_b=" << tot_sum_b << "\n";
}
