#include <iostream>
#include <vector>
#include <unordered_set>
#include <functional>

int main(int argc, char* argv[])
{
  size_t n=600;
  std::vector<size_t> V(n);
  for (size_t i=0; i<n; i++) {
    size_t eVal = rand() % 128;
    V[i] = eVal;
  }


  auto hash_fct=[&](size_t eVal) -> size_t {
    return std::hash<size_t>()(eVal % 128);
  };
  auto equal_fct=[&](size_t eVal1, size_t eVal2) -> bool {
    size_t fVal1 = eVal1 % n;
    size_t fVal2 = eVal2 % n;
    return V[fVal1] == V[fVal2];
  };
  //
  std::cout << "sizeof(size_t)=" << sizeof(size_t) << "\n";
  std::cout << "sizeof(int64_t)=" << sizeof(int64_t) << "\n";
  std::cout << "sizeof(uint64_t)=" << sizeof(uint64_t) << "\n";
  using PointHash = std::unordered_multiset<size_t, decltype(hash_fct), decltype(equal_fct)>;

  PointHash eVect({}, hash_fct, equal_fct);

  for (size_t i=0; i<1000; i++) {
    eVect.insert(i);
  }
}
