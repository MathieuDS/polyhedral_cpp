#include <fstream>
#include <iostream>
#include <sstream>
#include <unsupported/Eigen/CXX11/Tensor>


template <typename T> using MyMatrix=Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;

Eigen::Tensor<double, 3> GetSingleTensor(int const& siz)
{
  Eigen::Tensor<double,3> eT(siz, siz, siz);
  for (int i=0; i<siz; i++)
    for (int j=0; j<siz; j++)
      for (int k=0; k<siz; k++) {
	double eVal=double(i+1)*double(j+1)*double(k+1);
	eT(i,j,k)=eVal;
      }
  return eT;
}

/*
void PrintTensor(Eigen::Tensor<double> const& eT)
{
  int dim=eT.index();
  std::cout << "dim=" << dim << "\n";
  }*/

int main ()
{
  int n=4;
  Eigen::Tensor<double,3> eWorkT=GetSingleTensor(n);
  std::cout << "eWorkT(0,0,0)=" << eWorkT(0,0,0) << "\n";
  PrintTensor(eWorkT);
}

