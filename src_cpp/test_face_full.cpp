#include <bitset>
#include <boost/dynamic_bitset.hpp>
#include <iostream>

typedef boost::dynamic_bitset<> Face;

int main()
{
  int n=4;
  Face f(n, true); // It does not do what we want to set up all bits to true. Just the first one.
  for (int i=0; i<n; i++) {
    std::cerr << "i=" << i << " f[i])=" << f[i] << "\n";
  }
}
