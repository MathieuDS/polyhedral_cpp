#include <iostream>
#include <string>

struct C {
  C(std::string const& e) : val(e)
  {
    std::cout << "Constructing object with val=" << val << "\n";
  }

  C(C const& obj)
  {
    std::cout << "Invoking the copy constructor for object of value val=" << obj.val << "\n";
    val = obj.val;
  }

  ~C()
  {
    std::cout << "destroying object of value val=" << val << "\n";
  }

private:
  std::string val;
};

C get_object()
{
  C obj("get_object");
  return obj;
}

int main()
{
  C obj_A("obj_A");
  obj_A = get_object();
}
