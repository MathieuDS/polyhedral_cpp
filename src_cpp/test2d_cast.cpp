#include "Temp_common.h"
#include "gmpxx.h"
#include <Eigen/Dense>
#include <Eigen/LU>


template <typename T> using MyMatrix=Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;

template<typename T>
MyMatrix<T> HilbertMatrix(int const& n)
{
  MyMatrix<T> eMat(n, n);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      eMat(i,j) = i+j+1;
  std::cerr << "Assignation finished\n";
  return eMat;
}

int main ()
{
  std::cerr << "Before GetMatrix\n";
  MyMatrix<int> A=HilbertMatrix<int>(4);
  using T = mpz_class;
  MyMatrix<T> B = A.cast<T>();
  int nRow=B.rows();
  int nCol=B.cols();
  for (int iRow=0; iRow<nRow; iRow++)
    for (int iCol=0; iCol<nCol; iCol++)
      std::cerr << "iRow/iCol=" << iRow << "," << iCol << " eVal=" << B(iRow,iCol) << "\n";
  return 0;
}

