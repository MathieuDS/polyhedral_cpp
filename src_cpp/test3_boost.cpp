#include "boost/multi_array.hpp"
int main ()
{
  int n=4;
  boost::multi_array<double, 2> B;
  boost::multi_array<double, 2> A(boost::extents[n][n]);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      A[i][j]=double(1)/(double(i+j+1));
  B=A;
  return 0;
}

