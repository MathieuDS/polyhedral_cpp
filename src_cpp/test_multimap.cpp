#include "Temp_common.h"

int main()
{
  std::multimap <int, int> m;        // empty multimap container

  // insert elements in random order
  m.insert(std::pair <int, int> (1, 40));
  m.insert(std::pair <int, int> (2, 30));
  m.insert(std::pair <int, int> (3, 60));
  m.insert(std::pair <int, int> (4, 20));
  m.insert(std::pair <int, int> (5, 50));
  m.insert(std::pair <int, int> (6, 50));
  m.insert(std::pair <int, int> (6, 10));
  // printing multimap m
  std::multimap <int, int> :: iterator itr;
  std::cout << "\nThe multimap m is : \n";
  std::cout << "\tKEY\tELEMENT\n";
  for (itr = m.begin(); itr != m.end(); ++itr) {
    std::cout  << '\t' << itr->first <<  '\t' << itr->second << '\n';
  }
  std::cout << "\n";
  //
  //  std::vector<int> LVal = m[6];
  //  std::cout << "|LVal|=" << LVal.size() << "\n";
  
}
