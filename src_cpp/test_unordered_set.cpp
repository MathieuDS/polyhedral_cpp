#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <functional>

int main(int argc, char* argv[])
{

  auto hash_fct=[&](size_t eVal) -> size_t {
    return std::hash<size_t>()(eVal % 128);
  };
  auto equal_fct=[&](size_t eVal1, size_t eVal2) -> bool {
    return eVal1 == eVal2;
  };
  //
  std::unordered_set<size_t, decltype(hash_fct), decltype(equal_fct)> eSet({}, hash_fct, equal_fct);
  for (size_t i=0; i<10; i++) {
    eSet.insert(i);
  }
  std::cout << "|eSet|=" << eSet.size() << "\n";
}
