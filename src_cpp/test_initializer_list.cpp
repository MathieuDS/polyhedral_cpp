#include <vector>
#include <iostream>


struct test {
  std::vector<int> V;
  int val;
  test(std::initializer_list<int> l) : V(l), val(*(l.begin()))
  {
  }


  std::vector<int> getV() const
  {
    return V;
  }
  
  int getval() const
  {
    return val;
  }

  

};



void print_v(std::vector<int> const& V)
{
  std::cerr << "V =";
  for (auto & eVal : V)
    std::cerr << " " << eVal;
  std::cerr << "\n";
}



int main()
{
  test t{1,2,3};
  std::vector<int> V = t.getV();
  print_v(V);
  std::cerr << "val = " << t.getval() << "\n";


  
}
