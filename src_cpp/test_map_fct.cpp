#include <iostream>
#include <unordered_map>
#include <map>
#include <functional>


int main ()
{
  struct Utype {
    int nb;
  };
  std::function<bool(Utype const&,Utype const&)> f=[](Utype const& x, Utype const& y) -> bool {
    if (x.nb < y.nb)
      return true;
    return false;
  };
  std::map<Utype, std::vector<int>, std::function<bool(Utype const&,Utype const&)>> eMap(f);

}

