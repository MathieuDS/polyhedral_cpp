#include <bitset>
#include <boost/dynamic_bitset.hpp>
#include <iostream>

typedef boost::dynamic_bitset<> Face;

int main(int argc, char* argv[])
{
  std::vector<Face> orb;
  for (int i=0; i<10; i++) {
    std::cerr << "i=" << i << "\n";
    Face eFace(4);
    orb.push_back(eFace);
  }
  std::cerr << "Normal termination\n";
  
}
