#include <iostream>
#include <map>
#include <cassert>

enum Values {
  RELIABLE_NOT_REAL_TIME,
  RELIABLE_REAL_TIME,
  UNRELIABLE
};

template<Values val>
int fct(int const& x) {
  if constexpr(val == Values::RELIABLE_NOT_REAL_TIME) {
    return 2*x;
  } else {
    if constexpr(val == Values::RELIABLE_REAL_TIME) {
      return 3*x;
    } else {
      return 4*x;
    }
  }
}


int main()
{
  std::cerr << "val " << fct<Values::RELIABLE_NOT_REAL_TIME>(2) << "\n";
  std::cerr << "val " << fct<Values::RELIABLE_REAL_TIME>(2) << "\n";
  std::cerr << "val " << fct<Values::UNRELIABLE>(2) << "\n";
}
