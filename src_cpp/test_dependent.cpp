#include <iostream>



template<typename T2>
struct data {
  using T = T2;
  T2 val1;
  T2 val2;
};


int main()
{
  data<int>::T val = 1;
  std::cerr << "val=" << val << "\n";
}
