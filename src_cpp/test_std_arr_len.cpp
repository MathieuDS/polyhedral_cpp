#include <iostream>
#include <array>


using T = std::array<int,8>;

static constexpr int len = T().size();


template<typename T>
class contain {
private:
  static constexpr int len_priv = T().size();
  int arr[len_priv];
  using val = 5;
public:
  contain(int val)
  {
    arr[0] = val;
  }
};


int main()
{
  contain<T> arr(4);
  
}
