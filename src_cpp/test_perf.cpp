#include <iostream>
#include <cmath>

int main(int argc, char* argv[])
{
  //  using Tfloat=float;
  using Tfloat=double;
  Tfloat sum=0;
  for (int i=0; i<10000000; i++) {
    Tfloat val=Tfloat(i);
    sum += val*val + std::cos(val) + std::sin(val);
  }
  std::cerr << "sum=" << sum << "\n";
}
