#include <iostream>

struct refStruct {
  int& val;
  refStruct(int& _val) : val(_val)
  {
  }
};



int main()
{
  {
    int val2=5;
    refStruct ref1(val2);
    val2=6;
    std::cerr << "ref1.val=" << ref1.val << "\n";
  }
  //
  {
    int val2=5;
    int val3=6;
    refStruct ref1(val2);
    ref1.val = val3;
    std::cerr << "ref1.val=" << ref1.val << "\n";
  }
  //
  {
    int val2 = 5;
    int val3 = 15;
    refStruct ref1(val2);
    refStruct ref2(val2);
    ref2.val = val3;
    val2 = 6;
    val3 = 16;
    std::cerr << "ref1.val=" << ref1.val << "  ref2.val=" << ref2.val << "\n";
  }

}

