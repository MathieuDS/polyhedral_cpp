#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <functional>





int main()
{
  std::unordered_map<std::string,int> map;
  using T_iter = std::unordered_map<std::string,int>::iterator;

  map.emplace("A", 3);
  map.emplace(std::make_pair("B", 3));
  std::pair<T_iter,bool> e_pair = map.emplace("C", 3);
}
