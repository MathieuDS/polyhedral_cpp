#include "gmpxx.h"
#include <iostream>

template <typename T>
struct is_ring_field {
};

template <>
struct is_ring_field<int> {
  static const bool value = false;
};

template <>
struct is_ring_field<mpq_class> {
  static const bool value = true;
};

template<typename T>
inline typename std::enable_if<is_ring_field<T>::value,int>::type RankMat(T const& Input)
{
  return 10;
}

template<typename T>
inline typename std::enable_if<(not is_ring_field<T>::value),int>::type RankMat(T const& Input)
{
  return 20;
}

int main()
{
  int M1=10;
  mpq_class M2=3;
  std::cerr << "FICT rank(M1)=" << RankMat(M1) << "\n";
  std::cerr << "FICT rank(M2)=" << RankMat(M2) << "\n";
}
