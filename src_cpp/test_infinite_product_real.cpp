#include <cmath>
#include <iomanip>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc != 4) {
    std::cerr << "function is used as\n";
    std::cerr << "test_infinite_product [u] [siz] [m]\n";
    std::cerr << "The function computed is\n";
    exit(1);
  }
  std::cerr << std::setprecision(90);

  double u;
  (void)sscanf(argv[1], "%lf", &u);
  int siz;
  (void)sscanf(argv[2], "%d", &siz);
  int M;
  (void)sscanf(argv[3], "%d", &M);

  std::cerr << "u=" << u << "\n";
  std::cerr << "siz=" << siz << "\n";
  std::cerr << "M=" << M << "\n";
  //
  auto FCT=[&](double const& tIN) -> double {
    double retVal=std::pow(u, tIN*(tIN-1) /2);
    double x = std::pow(u, tIN);
    double xI = double(1) / x;
    double uPow=1;

    for (int i=0; i<M; i++) {
      double uProd = uPow * u;
      retVal *= (1 + xI * uProd) * (1 + x * uPow);
      uPow = uProd;
    }
    return retVal;
  };
  double eMin=0;
  double eMax=0;
  for (int h=0; h<siz; h++) {
    double tIN = double(h) / double(siz);
    double eVal=FCT(tIN);
    if (h == 0) {
      eMin=eVal;
      eMax=eVal;
    }
    else {
      if (eVal < eMin)
	eMin=eVal;
      if (eVal > eMax)
	eMax=eVal;
    }
    std::cerr << "tIN=" << tIN << " w=" << FCT(tIN) << "\n";
  }
  double delta=eMax - eMin;
  std::cerr << "delta=" << delta << "\n";
}
