#include "NumberTheory.h"
#include <iostream>
#include <set>
#include <functional>

struct Utype {
  mpz_class nb;
  int u;
  std::set<mpq_class> z;
};


template<typename T>
struct Vtype {
  T nb;
  int u;
  std::set<mpq_class> z;
};

int main ()
{
  std::set<Utype> eSet1;
  /* This code compiles for one solve reason
     The eSet1 is not used at all, and so we do not need to make any comparison
  mpz_class nb=1;
  int u=4;
  std::set<mpq_class> z;
  Utype eUtype{nb, u, z};
  eSet1.insert(eUtype); */

  
  std::set<Vtype<mpz_class>> eSet2;
}

