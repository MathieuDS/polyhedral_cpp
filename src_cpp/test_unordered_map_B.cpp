#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <functional>

struct token {
  uint8_t val8;
  uint16_t val16;
  uint64_t val64;
  bool operator==(const token& y) const
  {
    return (val8 == y.val8) && (val16 == y.val16) && (val64 == y.val64);
  }
};

namespace std {
  template<>
  struct hash<token>
  {
    size_t operator()(const token& tk) const
    {
      return std::hash<uint8_t>()(tk.val8) + std::hash<uint16_t>()(tk.val16) + std::hash<uint64_t>()(tk.val64);
    }
  };
}


int main(int argc, char* argv[])
{
  std::unordered_map<token,int> map;
}
