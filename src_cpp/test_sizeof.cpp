#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>

int main(int argc, char** argv)
{
  std::cerr << "sizeof(int)=" << sizeof(int) << "\n";
  std::cerr << "sizeof(size_t)=" << sizeof(size_t) << "\n";
  std::cerr << "sizeof(short)=" << sizeof(short) << "\n";
  //
  std::cerr << "sizeof(int8_t)=" << sizeof(int8_t) << "\n";
  std::cerr << "sizeof(uint8_t)=" << sizeof(uint8_t) << "\n";
  std::cerr << "sizeof(int16_t)=" << sizeof(int16_t) << "\n";
  std::cerr << "sizeof(uint16_t)=" << sizeof(uint16_t) << "\n";
  std::cerr << "sizeof(int32_t)=" << sizeof(int32_t) << "\n";
  std::cerr << "sizeof(uint32_t)=" << sizeof(uint32_t) << "\n";
  std::cerr << "sizeof(int64_t)=" << sizeof(int64_t) << "\n";
  std::cerr << "sizeof(uint64_t)=" << sizeof(uint64_t) << "\n";
  std::cerr << "sizeof(float)=" << sizeof(float) << "\n";
  std::cerr << "sizeof(double)=" << sizeof(double) << "\n";

}
