#include <fstream>
#include <iostream>
#include <sstream>
#include <Eigen/Dense>
#include <Eigen/LU>


template <typename T> using MyMatrix=Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;

void PrintMatrix(MyMatrix<double> const& eMat)
{
  int nbRow=eMat.rows();
  int nbCol=eMat.cols();
  for (int iRow=0; iRow<nbRow; iRow++) {
    for (int iCol=0; iCol<nbCol; iCol++)
      std::cerr << " " << eMat(iRow, iCol);
    std::cerr << "\n";
  }
}

int main ()
{
  int n=4;
  MyMatrix<double> M1(n,n);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      M1(i,j)=double(i+1)*double(j+1);
  MyMatrix<double> M2(n,n);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      M2(i,j)=double(n-i)*double(n-j);
  std::cerr << "Before M1=\n";
  PrintMatrix(M1);
  std::cerr << "Before M2=\n";
  PrintMatrix(M2);
  //
  M1.cwiseMax(M2);
  std::cerr << "After operation M1=\n";
  PrintMatrix(M1);
  std::cerr << "After operation M2=\n";
  PrintMatrix(M2);
  
}

