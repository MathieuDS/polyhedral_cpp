#include <iostream>
#include <sstream>
int main() {
  // How to effectively read character by character
  std::string strW = "Hello";
  std::istringstream is(strW);
  char c;
  while(!is.eof()) {
    is.get(c);
    if (is.gcount() == 1)
      std::cerr << "Character c=" << c << "\n";
  }
}

