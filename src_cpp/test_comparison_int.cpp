#include <iostream>


int main()
{
  auto f1=[](int const& test) -> int {
    if (!test) return test;
    return 42;
  };
  auto f2=[](int const& test) -> int {
    if (test != 0) return test;
    return 42;
  };
  auto f3=[](int const& test) -> int {
    if (test) return test;
    return 42;
  };
  for (int i=-1; i<=1; i++)
    std::cout << "i=" << i << " f1=" << f1(i) << " f2=" << f2(i) << " f3=" << f3(i) << "\n";
}
