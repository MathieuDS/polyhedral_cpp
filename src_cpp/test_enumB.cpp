#include <iostream>
#include <map>
#include <cassert>

enum class Values {
  BUY ,
  SELL
};


int main()
{
  std::map<int,Values> M;
  for (int i=0; i<20; i++) {
    if (rand() % 2 == 0) {
      M[i] = Values::BUY;
    } else {
      M[i] = Values::SELL;
    }
  }

  // f1 does not work since the compiler cannot identify all the code
  // path and so it returns a possibility of having an error.
  //
  // The key reason appears to be that the enum can take values out of
  // their range. This is undefined behavior it seems.
  //
  // Replacing the enum by enum Values { BUY, SELL} does not address the
  // problem.

  /*
  auto f1=[](Values & u) -> int {
    if (u == Values('B'))
      return 0;
    if (u == Values('S'))
      return 1;

  };
  */
  auto f2=[](Values & u) -> int {
    switch (u) {
    case Values::BUY:
      return 0;
    case Values::SELL:
      return 1;
      //    default:
      //      assert(false);
    }
  };
  for (auto kv : M) {
    //    std::cerr << "kv.first=" << kv.first << " kv.second=" << kv.second << " f(kv.second)=" << f2(kv.second) << "\n";
    std::cerr << "kv.first=" << kv.first << " f(kv.second)=" << f2(kv.second) << "\n";
  }

  
}
