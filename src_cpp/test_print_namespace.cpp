#include <iostream>
#include <string>

void print()
{
  std::cerr << "print()\n";
}



namespace test {

  void print()
  {
    std::cerr << "test::print()\n";
  }


  void test_print()
  {
    print();
  }
}



int main()
{
  test::test_print();
}
