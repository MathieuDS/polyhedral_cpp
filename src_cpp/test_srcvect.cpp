#include <vector>
#include <algorithm>
#include <iostream>

int main()
{
  std::vector<int> V;
  for (int i=0; i<10; i++)
    V.push_back(rand() % 100);
  std::cerr << "V =";
  for (auto &eV : V)
    std::cerr << " " << eV;
  std::cerr << "\n";
  //
  int rem = V[5];
  std::cerr << "  rem=" << rem << "\n";
  auto it = std::remove(V.begin(), V.end(), rem);
  V.erase(it);
  //
  std::cerr << "V =";
  for (auto &eV : V)
    std::cerr << " " << eV;
  std::cerr << "\n";
}
