#include <iostream>
#include "MAT_Matrix.h"









MyVector<double> Sample_Point_Sphere()
{


  
}



int main()
{
    std::random_device rd{};
    std::mt19937 gen{rd()};
    // values near the mean are the most likely
    // standard deviation affects the dispersion of generated values from the mean
    std::normal_distribution<> d{0,2};


    MyMatrix<double> M(4,4);
    for (int i=0; i<4; i++)
      M(i,0) = 1;
    uint64_t n_iter = 10000;
    uint64_t n_match = 0;
    for(uint64_t n=0; n<; ++n) {
      for (int j=0; j<4; j++) {
        double val1 = d(gen);
        double val2 = d(gen);
        double val3 = d(gen);
        double norm = sqrt(val1 * val1 + val2 * val2 + val3 * val3);

        M(j,1) = val1 / norm;
        M(j,2) = val2 / norm;
        M(j,3) = val3 / norm;
      }
      MyMatrix<double> Minv = M.inv();
      bool is_ok = true;
      for (int j=0; j<4; j++)
        if (M(i,0) < 0)
          is_ok = false;
      if (is_ok)
        n_match++;
    }
    double frac = (double(n_match) / double(n_iter));
    std::cerr << "frac=" << frac << "\n";
}
