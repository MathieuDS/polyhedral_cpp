#include "Temp_common.h"
#include "fmpqxx.h"
flint::fmpqxx TheProductB(int const&x, flint::fmpqxx const&y)
{
  flint::fmpqxx xb, eProd;
  xb=x;
  eProd=xb*y;
  return eProd;
}

flint::fmpqxx TheProduct(flint::fmpqxx const& a1, flint::fmpqxx a2)
{
  flint::fmpqxx x;
  x=a1*a2;
  return x;
}


int main(int argc, char *argv[])
{
  flint::fmpqxx a, b, c;
  int a2=3;
  a=a2;
  b=5;
  c=a/b;
  //  flint::fmpqxx eProd=TheProduct(a, b);
  flint::fmpqxx eProd=TheProduct(a, b);

  std::cerr << "a=" << a << "\n";
  std::cerr << "b=" << b << "\n";
  std::cerr << "c=" << c << "\n";
  std::cerr << "eProd=" << eProd << "\n";

  std::cerr << "Completion of the program\n";
}
