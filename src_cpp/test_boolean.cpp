#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>


int main ()
{
  std::cerr.setf(std::ios::boolalpha);
  for (int i=0; i<2; i++) {
    for (int j=0; j<2; j++) {
      bool i_b = i;
      bool j_b = j;
      bool ret_b = i_b ^ j_b;
      bool ret_c = i_b * j_b;
      std::cerr << "i=" << i_b << "   j=" << j_b << " i^j  =" << ret_b << " i*j=" << ret_c << "\n";
    }
  }
}

