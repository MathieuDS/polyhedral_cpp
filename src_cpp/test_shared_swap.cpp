#include <memory>
#include <variant>
#include <iostream>


//#define DO_PRINT

int main()
{
  //  using T = int;
  using T = std::variant<int,double>;


  std::shared_ptr<T> eval = std::make_shared<T>(4);
  std::shared_ptr<T> fval = std::make_shared<T>(5);

#ifdef DO_PRINT
  std::cerr << "*eval=" << *eval << "\n";
  std::cerr << "*fval=" << *fval << "\n";
#endif


  eval.swap(fval);
#ifdef DO_PRINT
  std::cerr << "*eval=" << *eval << "\n";
  std::cerr << "*fval=" << *fval << "\n";
#endif

}
