#include <boost/coroutine/all.hpp>
#include <iostream>

using namespace boost::coroutines;

void cooperative(coroutine<int>::push_type &sink)
{
  std::cout << "[coop] First step\n";
  sink(42);
  std::cout << "[coop] Second step\n";
}

int main()
{
  coroutine<int>::pull_type source{cooperative};
  std::cout << "[main] first\n";
  int val = source.get();
  source();
  std::cout << "[main] val=" << val << "\n";
}

