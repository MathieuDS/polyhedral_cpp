#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>

typedef Eigen::SparseMatrix<double, Eigen::ColMajor> MyMatrix;

int main(int argc, char** argv){

  MyMatrix A_sparse(10, 10);
  std::vector<Eigen::Block<MyMatrix> > sparse_block_vector;
  Eigen::Block<MyMatrix> sparse_block = A_sparse.block(0, 0, 5, 5);
  sparse_block_vector.push_back(sparse_block);

}
