// shared_ptr::operator= example
#include <iostream>
#include <memory>

int main () {
  std::shared_ptr<int> foo;
  std::shared_ptr<int> bar (new int(10));

  foo = bar;                          // copy

  std::cout << "1: *foo: " << *foo << '\n';
  std::cout << "1: *bar: " << *bar << '\n';

  bar = std::make_shared<int> (20);   // move

  std::cout << "2: *foo: " << *foo << '\n';
  std::cout << "2: *bar: " << *bar << '\n';

  return 0;
}
