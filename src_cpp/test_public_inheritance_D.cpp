#include <iostream>
#include <string>

struct subclass {
public:
  void operator()(int val)
  {
    std::cerr << "subclass : operator() : int : val=" << val << "\n";
  }
  void operator()(std::string val)
  {
    std::cerr << "subclass : operator() : string : val=" << val << "\n";
  }
  //
  void test(int val)
  {
    std::cerr << "subclass : test : int : val=" << val << "\n";
  }
  void test(std::string val)
  {
    std::cerr << "subclass : test : string : val=" << val << "\n";
  }
};


struct overclass : subclass {
public:
  using subclass::operator();
  using subclass::test;
  void operator()(int val)
  {
    std::cerr << "overclass : operator() : int : val=" << val << "\n";
    subclass()(val);
  }
  void test(int val)
  {
    std::cerr << "overclass : operator() : int : val=" << val << "\n";
    subclass::test(val);
  }
};



int main()
{
  overclass inst;
  inst(4);
  std::string s("ABCD");
  inst(s);
  inst.test(s);
}
