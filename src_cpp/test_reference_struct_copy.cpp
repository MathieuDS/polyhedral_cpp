#include <iostream>

struct refStruct {
  int& val;
  refStruct(int& _val) : val(_val)
  {
  }
  refStruct operator=(refStruct& ref)
  {
    return refStruct(ref.val);
  }
};



int main()
{
  {
    int val2=5;
    refStruct ref1(val2);
    refStruct ref2 = ref1;
    val2=6;
    std::cerr << "ref2.val=" << ref2.val << "\n";
  }
}

