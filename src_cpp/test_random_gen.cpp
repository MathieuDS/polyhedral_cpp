#include <unordered_set>
#include <random>
#include <iostream>

std::unordered_set<int> pickSet(int N, int k, std::mt19937& gen)
{
  std::cout << "Beginning of pickSet\n";
    std::unordered_set<int> elems;
    for (int r = N - k; r < N; ++r) {
      std::cout << "Before std::uniform_int_distribution\n";
        int v = std::uniform_int_distribution<>(0, r)(gen);
        std::cout << "r=" << r << " v=" << v << " elems=";
        for (auto & eVal : elems)
          std::cout << " " << eVal;
        std::cout << "\n";

        // there are two cases.
        // v is not in candidates ==> add it
        // v is in candidates ==> well, r is definitely not, because
        // this is the first iteration in the loop that we could've
        // picked something that big.

        if (!elems.insert(v).second) {
            elems.insert(r);
        }
    }
    return elems;
}


int main()
{
  std::cout << "Beginning of main\n";
    std::random_device rd;
    std::mt19937 gen(rd());

    int N = 10;
    int k = 10;
    std::unordered_set<int> eList = pickSet(N, k, gen);
    for (auto & eVal : eList)
      std::cout << " " << eVal;
    std::cout << "\n";
}
