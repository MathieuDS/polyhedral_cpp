#include <functional>
#include <boost/coroutine2/coroutine.hpp>
#include <boost/coroutine/all.hpp>
#include <iostream>


void cooperative(boost::coroutines2::coroutine<int>::push_type & sink, int idx)
{
  std::cout << "[coop] First step\n";
  sink(42 + idx);
  std::cout << "[coop] Second step\n";
}

int main()
{
  int idx = 10;
  boost::coroutines2::coroutine<int>::pull_type source{std::bind(cooperative, std::placeholders::_1, idx)};
  std::cout << "[main] first\n";
  int val = source.get();
  source();
  std::cout << "[main] val=" << val << "\n";
}

