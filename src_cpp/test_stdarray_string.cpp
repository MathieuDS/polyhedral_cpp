#include <iostream>
#include <string>
#include <array>


void set_array(std::array<char, 8> & arr, std::string const& str)
{
  for (int i=0; i<8; i++)
    arr[i] = '\0';
  for (size_t u=0; u<str.size(); u++) {
    std::string str_s = str.substr(u,1);
    char echar = str_s[0];
    arr[u] = echar;
  }
}

std::string get_string(std::array<char,8> const& arr)
{
  std::string str;
  for (size_t u=0; u<arr.size(); u++) {
    char echar =arr[u];
    if (echar == '\0')
      return str;
    str += echar;
  }
  return str;
}





int main()
{
  std::array<char,8> arr;
  std::string str = "AAPL";
  set_array(arr, str);
  std::string strB = get_string(arr);
  std::cerr << "str=" << str << " strB=" << strB << "\n";
}
