#include <iostream>
#include <map>


enum Values : char {
  BUY = 'B',
  SELL = 'S'
};


int main()
{
  std::map<int,bool> M;
  for (int i=0; i<20; i++) {
    if (rand() % 2 == 0) {
      M[i] = false;
    } else {
      M[i] = true;
    }
  }


  auto f1=[](bool & u) -> int {
    if (u == true)
      return 0;
    if (u == false)
      return 1;

  };
  auto f2=[](Values & u) -> int {
    switch (u) {
    case Values::BUY:
      return 0;
    case Values::SELL:
      return 1;
    }
  };
  for (auto kv : M) {
    std::cerr << "kv.first=" << kv.first << " kv.second=" << kv.second << " f(kv.second)=" << f1(kv.second) << "\n";
  }

  
}
