#include "Temp_common.h"

void PrintVector(std::vector<int> && eVect)
{
  for (auto &eVal : eVect)
    std::cerr << "eVal=" << eVal << "\n";
}

void AppendVector(std::vector<std::vector<int>> & v, std::vector<int> && eVect)
{
  v.push_back(std::move(eVect));
}



int main ()
{
  std::vector<int> testV={1,2,3};
  std::cerr << "First one\n";
  //  PrintVector(std::move(testV));
  std::cerr << "After operatior\n";
  //  PrintVector(std::move(testV));

  std::vector<std::vector<int>> u;
  AppendVector(u, std::move(testV));
  std::cerr << "final\n";
  PrintVector(std::move(testV));


}

