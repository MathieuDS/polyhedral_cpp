#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>

int f() __attribute__((noinline));
#ifdef RETURN
int f(){
  if(rand()%2){
    return -1;
  }else{
    return 1;
  }
}
#else
int f(){
  if(rand()%2)
    throw std::runtime_error("-1");
  return 1;
}
#endif

int main(int argc, char** argv){
  int ret;
  int it = atoi(argv[1]);
  for(int i=0; i<it; i++){
#ifdef RETURN
    if((ret=f())<0){ perror("f"); }
#else
    try{ ret=f(); }catch(const std::exception& e){
      fputs(e.what(), stderr);
    }
#endif
  }
}
