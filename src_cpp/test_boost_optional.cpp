#include <iostream>
#include <boost/optional.hpp>



int main()
{
  boost::optional<int> val1;
  boost::optional<int> val2();
  boost::optional<int> val3(42);
  boost::optional<int> val4 = boost::optional<int>();
  //
  if (val1)
    std::cerr << "val1 does match\n";
  else
    std::cerr << "val1 DOES NOTmatch\n";
  //
  if (val2)
    std::cerr << "val2 does match\n";
  else
    std::cerr << "val2 DOES NOTmatch\n";
  //
  if (val3)
    std::cerr << "val3 does match\n";
  else
    std::cerr << "val3 DOES NOTmatch\n";
  //
  if (val4)
    std::cerr << "val4 does match\n";
  else
    std::cerr << "val4 DOES NOTmatch\n";
  //

}
