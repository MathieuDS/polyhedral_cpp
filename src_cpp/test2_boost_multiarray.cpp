#include "boost/multi_array.hpp"
#include <iostream>

boost::multi_array<double, 2> GetMatrix(int const& n)
{
  boost::multi_array<double, 2> eMat(boost::extents[n][n]);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      eMat[i][j]=double(1)/(double(i+j+1));
  return eMat;
}

int main ()
{
  boost::multi_array<double, 2> B;
  B=GetMatrix(4);
  int nRow=B.shape()[0];
  int nCol=B.shape()[1];
  for (int iRow=0; iRow<nRow; iRow++)
    for (int iCol=0; iCol<nCol; iCol++) {
      std::cerr << "iRow/iCol=" << iRow << "," << iCol << " eVal=" << B[iRow][iCol] << "\n";
    }
  return 0;
}

