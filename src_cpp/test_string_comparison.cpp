#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstring>



std::string random_string( size_t length )
{
  //  srand ( time(NULL) );
  auto randchar = []() -> char {
    const char charset[] = "abcdefghijklmnopqrstuvwxyz";
    const size_t max_index = (sizeof(charset) - 1);
    return charset[ rand() % max_index ];
  };
  std::string str(length,0);
  std::generate_n( str.begin(), length, randchar );
  return str;
}

std::string random_string_randlen( size_t length )
{
  int len = rand() % length;
  return random_string(len);
}




bool string_compar(std::string const& str1, std::string const& str2)
{
  int len1=str1.size();
  int len2=str2.size();
  int minlen = std::min(len1, len2);
  int test = std::strncmp(str2.data(), str1.data(), minlen);
  if (test != 0) {
    return test > 0;
  }
  if (len1 > len2) return false;
  if (len1 < len2) return true;
  return false;
}

int main()
{
  for (int iter=0; iter < 100000; iter++) {
    std::string str1 = random_string_randlen(5);
    std::string str2 = random_string_randlen(5);
    bool test1 = string_compar(str1, str2);
    bool test2 = str1 < str2;
    if (test1 != test2) {
      std::cout << "iter=" << iter << " str1=" << str1 << " str2=" << str2 << " test1=" << test1 << " test2=" << test2 << "\n";
    }
    std::string strmin = std::min(str1, str2);
    std::string strmax = std::max(str1, str2);
    if (strmin > strmax) {
      std::cout << "ERROR : strmin=" << strmin << " strmax=" << strmax << "\n";
    }
  }

  
}
