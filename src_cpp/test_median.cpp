#include <algorithm>
#include <vector>
#include <set>
#include <iostream>

template<typename T>
int partition(std::vector<T> & arr, int low, int high)
{
  int mid = (low + high)/2;
  if (mid != high)
    std::swap(arr[high], arr[mid]);
  T pivot = arr[high];
  int i = (low - 1);
  for (int j = low; j <= high - 1; j++) {
    if (arr[j] <= pivot) {
      i++;
      std::swap(arr[i], arr[j]);
    }
  }
  std::swap(arr[i + 1], arr[high]);
  return (i+1);
}


template<typename T>
T kthSmallest(std::vector<T> & a, int k)
{
    int left = 0;
    int right = a.size() - 1;
    while (left <= right) {
        int pivotIndex = partition(a, left, right);
        if (pivotIndex == k)
            return a[pivotIndex];
        else if (pivotIndex > k)
            right = pivotIndex - 1;
        else
            left = pivotIndex + 1;
    }
    return -1;
}


template<typename T>
T KthSmallestBruteForce(std::vector<T> & a, int k)
{
  std::sort(a.begin(), a.end());
  std::cerr << "a =";
  for (auto & eVal : a)
    std::cerr << " " << eVal;
  std::cerr << "\n";
  return a[k];
}


template<typename T>
void TestConsistency(std::vector<T> const& arr, int k)
{
  std::cerr << "--------------------------------------\n";
  std::vector<T> arr1 = arr;
  std::vector<T> arr2 = arr;
  std::cerr << "arr =";
  for (auto & eVal : arr)
    std::cerr << " " << eVal;
  std::cerr << "\n";
  T MedVal1 = kthSmallest(arr1, k);
  T MedVal2 = KthSmallestBruteForce(arr2, k);
  if (MedVal1 != MedVal2) {
    std::cerr << "Error. MedVal1=" << MedVal1 << "   MedVal2=" << MedVal2 << "\n";
    exit(1);
  }
}


std::vector<int> GetRandomVector(int const& n, int const& k)
{
  while(true) {
    std::vector<int> V(n);
    std::set<int> eS;
    for (int i=0; i<n; i++) {
      int eVal = rand() % k;
      V[i] = eVal;
      eS.insert(eVal);
    }
    if (int(eS.size()) == n)
      return V;
  }
}


int main()
{
  int n=11;
  int k=1000;
  while(true) {
    std::vector<int> V = GetRandomVector(n, k);
    int kMid = 0;
    TestConsistency(V, kMid);
  }

}
