#include <iostream>
#include <functional>

extern int a[];
typedef decltype(a) T1;
int a[10];
typedef decltype(a) T2;

int main ()
{
  static_assert(std::is_same<T1, int[]>::value, "");
  static_assert(std::is_same<T2, int[10]>::value, "");
  static_assert(!std::is_same<T1, T2>::value, "");
}

