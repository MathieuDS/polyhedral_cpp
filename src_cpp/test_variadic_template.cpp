#include <iostream>



template<typename F, typename... Targs>
void iterate(int n_iter, F f, Targs... args) {
  for (int iter=0; iter<n_iter; iter++) {
    f(args...);
  }
}

void f_vals(int val1, int val2)
{
  std::cerr << "val1=" << val1 << " val2=" << val2 << "\n";
}

void f_hello(std::string name)
{
  std::cerr << "Hello " << name << "\n";
}



int main()
{
  iterate(3, f_vals, 3, 4);
  iterate(2, f_hello, "Robert");
}
