#include <iostream>
#include <vector>
#include <array>


constexpr int fibonacciA(int n)
{
  if (n < 2)
    return 1;
  return fibonacciA(n-1) + fibonacciA(n-2);
}


int fibonacciB(int n)
{
  if (n < 2)
    return 1;
  std::vector<int> V(n+1);
  V[0] = 1;
  V[1] = 1;
  for (int i=2; i<=n; i++)
    V[i] = V[i-1] + V[i-2];
  return V[n];
}

// fibonacciC

template <unsigned int n>
struct fibonacciC {
  enum { value = fibonacciC<n - 1>::value + fibonacciC<n - 2>::value };
};

template <>
struct fibonacciC<0> {
  enum { value = 1 };
};

template <>
struct fibonacciC<1> {
  enum { value = 1 };
};

// fibonacciD

template <unsigned int n>
struct fibonacciD {
  static const unsigned int value = fibonacciD<n - 1>::value + fibonacciD<n - 2>::value;
};

template <>
struct fibonacciD<0> {
  static const unsigned int value = 1;
};

template <>
struct fibonacciD<1> {
  static const unsigned int value = 1;
};

// fibonacciE

template<unsigned int n>
constexpr unsigned int fibonacciE()
{
  if (n < 2)
    return 1;
  std::array<unsigned int,n + 1> V;
  V[0] = 1;
  V[1] = 1;
  for (unsigned int i=2; i<=n; i++)
    V[i] = V[i-1] + V[i-2];
  return V[n];
}






constexpr int valA = fibonacciA(10);
int valB = fibonacciB(10);
int valC = fibonacciC<10>::value;
int valD = fibonacciD<10>::value;
constexpr int valE = fibonacciE<10>();



int main()
{
  std::cerr << "valA = " << valA << "\n";
  std::cerr << "valB = " << valB << "\n";
  std::cerr << "valC = " << valC << "\n";
  std::cerr << "valD = " << valD << "\n";
  std::cerr << "valE = " << valE << "\n";
}
