#include <iostream>
#include <algorithm>
#include <vector>
#include <functional>
#include <gfx/timsort.hpp>

struct Utype {
  int nb;
  int value;
};


void test_function(int const& nb1, int const& nb2)
{
  std::vector<Utype> eL1;
  std::vector<Utype> eL2;
  auto getString=[&](std::vector<Utype> const& eL) -> std::string {
    std::string eL_str;
    for (size_t i=0; i<eL.size(); i++) {
      eL_str += " [" + std::to_string(eL[i].nb) + "," + std::to_string(eL[i].value) + "]";
    }
    return eL_str;
  };
  for (int i=0; i<nb1; i++) {
    int nb = rand() % nb2;
    int value = rand() % nb2;
    eL1.push_back({nb,value});
    eL2.push_back({nb,value});
  }
  
  std::function<bool(Utype const&,Utype const&)> f=[](Utype const& x, Utype const& y) -> bool {
    return x.nb < y.nb;
  };
  gfx::timsort(eL1.begin(), eL1.end(), f);
  std::stable_sort(eL2.begin(), eL2.end(), f);
  //
  for (int i=0; i<nb1; i++) {
    if (eL1[i].nb != eL2[i].nb) {
      std::cerr << "Found a BUG 1\n";
      exit(1);
    }
    if (eL1[i].value != eL2[i].value) {
      std::cerr << "i=" << i << "Found a BUG 2\n";
      exit(1);
    }
  }
}

int main ()
{
  size_t idx = 0;
  while(true) {
    idx++;
    test_function(100, 10);
    test_function(1000, 10);
    test_function(100, 100);
    test_function(100, 2);
    test_function(10, 100);
    std::cerr << "idx=" << idx << "\n";
  }

}

