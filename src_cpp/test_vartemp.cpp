#include "Temp_common.h"


int main ()
{
  std::vector<int> aList={5};
  std::vector<int> bList={23, 34, 56};
  std::vector<int> cList={223, 456, 459};
  std::vector<int> finList = Concatenation(aList, bList, cList);
  std::cerr << "|finList|=" << finList.size() << "\n";
  for (auto & eVal : finList)
    std::cerr << " " << eVal;
  std::cerr << "\n";
}

