#include <map>
#include <functional>
#include <iostream>


int main()
{
  std::function<bool(int,int)> fctComp = [&](int val1, int val2) -> bool {
    return val1 < val2;
  };

  using T = std::map<int, int, std::function<bool(int,int)>>;
  T  map(fctComp);


  std::cerr << "compar(2, 5)=" << map.key_comp()(2, 5) << "\n";
  std::cerr << "compar(10, 5)=" << map.key_comp()(10, 5) << "\n";

  /*
    Conclusions:
    --- We cannot do something like map.end()-- and expect to get a valid iterator
    --- We can erase the iterator after usage.
   */
}
