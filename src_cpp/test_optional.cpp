#include <string>
#include <iostream>
#include <optional>


std::string PrintOption(std::optional<int> const& eR)
{
  if (eR) {
    int val = *eR;
    return std::to_string(val);
  }
  else {
    return "missing";
  }
}


int main ()
{
  std::optional<int> V1 = 4;
  std::cerr << "V1=" << PrintOption(V1) << "\n";
  //
  std::optional<int> V2;
  std::cerr << "V2=" << PrintOption(V2) << "\n";
  //
  std::optional<int> V3 = {};
  std::cerr << "V3=" << PrintOption(V3) << "\n";
}

