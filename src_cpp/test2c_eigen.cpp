#include "Temp_common.h"
#include <Eigen/Dense>
#include <Eigen/LU>


template <typename T> using MyMatrix=Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>;

MyMatrix<double> HilbertMatrix(int const& n)
{
  MyMatrix<double> eMat(n, n);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      eMat(i,j)=double(1)/(double(i+j+1));
  std::cerr << "Assignation finished\n";
  return eMat;
}

int main ()
{
  std::cerr << "Before GetMatrix\n";
  MyMatrix<double> B=HilbertMatrix(4);
  std::cerr << "After GetMatrix\n";
  Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic> eMat;
  int nRow=B.rows();
  int nCol=B.cols();
  for (int iRow=0; iRow<nRow; iRow++)
    for (int iCol=0; iCol<nCol; iCol++)
      std::cerr << "iRow/iCol=" << iRow << "," << iCol << " eVal=" << B(iRow,iCol) << "\n";
  return 0;
}

