#include <iostream>


struct cls {


  template<typename T>
  void print(T val)
  {
    std::cerr << "print : " << sizeof(T) << " val=" << val << "\n";
  }
};




int main()
{
  cls TheEnt;

  TheEnt.print(1);
  TheEnt.print<long>(32);
  TheEnt.print<short>(42);

}
