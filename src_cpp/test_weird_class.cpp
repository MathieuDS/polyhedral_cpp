#include <iostream>

struct contain {
  using Tin = int;
  using Tout = int;
  Tout sqr(Tin x)
  {
    return x * x;
  }
  contain(int _x) : x(_x)
  {
  }
  int get_x() const
  {
    return x;
  }
private:
  int x;
};


int cube(contain u)
{
  int x = u.get_x();
  return x * x * x;
}



int main()
{
  contain ah(2);
  std::cout << cube(ah) << "\n";
  contain::Tin eval = 2;
  std::cout << ah.sqr(eval) << "\n";
}

