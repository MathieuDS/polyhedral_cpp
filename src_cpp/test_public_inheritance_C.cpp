#include <iostream>
#include <string>

struct subclass {
public:
  void operator()(int val)
  {
    std::cerr << "subclass : operator() : int : val=" << val << "\n";
  }
};


struct overclass : subclass {
public:
  void operator()(int val)
  {
    std::cerr << "overclass : operator() : int : val=" << val << "\n";
    subclass()(val);
  }
};



int main()
{
  overclass inst;
  inst(4);
}
