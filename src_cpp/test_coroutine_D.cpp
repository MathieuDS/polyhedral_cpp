#include <iostream>
#include <tuple>
#include <string>
#include <functional>
#include <boost/coroutine2/coroutine.hpp>
#include <boost/coroutine/all.hpp>

typedef boost::coroutines2::coroutine< void > coro_t;
typedef boost::coroutines2::coroutine< int > coro_t1;
typedef boost::coroutines2::coroutine<std::tuple<int, std::string>> coro_t2;

void func2(coro_t2::pull_type &source)
{ int count=0;
  auto args = source.get();
  int x = std::get<0>(args);
    std::cout<< std::get<1>(args) << '\n';
    x%2==0 ? std::cout<<"even number"<< '\n' : std::cout<<"odd number"<< '\n';
	
    source(); 
    args = source.get();
    
     int x1 = std::get<0>(args);
    std::cout<< std::get<1>(args) << '\n';
     for(int i=2;i<x1;i++)
	{
		if(x1%i==0)
		{
			count++;
			break;
		}
	}
	count==0 ? std::cout<<"prime number"<< '\n' : std::cout<<"not prime number"<< '\n';
	
 

}
void func1(coro_t1::push_type &sink, int i)
{
  int j = i;
 
  for (int k=0; k<=5;k++)
  j+=5;
      
  sink(j);
  
  for (int k=5; k>=3;k--)
  j-=5;
    
  sink(j);
  
}

void func(coro_t::push_type &sink)
{ int a;
  std::cout << "enter the value";
  sink();
  std::cin>>a;
 std::cout<<a;
}

int main()
{
  coro_t::pull_type source{func};
  using std::placeholders::_1;
  coro_t1::pull_type source1{std::bind(func1, _1, 0)};
  coro_t2::push_type sink{func2};
  std::cout << ": ";
  source();
  std::cout << "!\n";
   std::cout<<"Returning values:"<<std::endl;  
  std::cout << source1.get() << '\n';
  source1();
  std::cout << source1.get() << '\n';
  source1();
    
  sink(std::make_tuple(4, "1. Even/Odd"));
  sink(std::make_tuple(7, "2.Prime or not"));
  
}
