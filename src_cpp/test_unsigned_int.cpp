#include <iostream>

int main(int argc, char* argv[])
{
  // I thought this was undefined behavior. But maybe it is not.
  // Is the 2-complement actually part of the standard.
  unsigned int l2;
  l2 = -2048;
  std::cerr << "l2=" << l2 << "\n";
}
