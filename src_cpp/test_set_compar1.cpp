#include <iostream>
#include <set>
#include <functional>


int main ()
{
  struct Utype {
    int nb;
  };
  std::function<bool(Utype const&,Utype const&)> f=[](Utype const& x, Utype const& y) -> bool {
    if (x.nb < y.nb)
      return true;
    return false;
  };
  std::set<Utype, decltype(f)> eSet1(f);
  std::set<Utype, std::function<bool(Utype const&,Utype const&)>> eSet2(f);
}

