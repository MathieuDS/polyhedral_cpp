#include <iostream>
#include <map>


int main()
{
  std::map<int,int> map;
  for (int i=0; i<10; i++) {
    int val1 = rand() % 100;
    int val2 = rand() % 100;
    map[val1] = val2;
  }
  auto iter = map.begin();
  while(iter) {
    std::cerr << "iter=" << iter->first << " / " << iter->second << "\n";
    iter++;
  }

}
