// shared_ptr::operator= example
#include <iostream>
#include <memory>

int main () {
  std::shared_ptr<int> v1 = std::make_shared<int>(1);
  std::shared_ptr<int> v2 = std::make_shared<int>(1);
  std::shared_ptr<int> v3 = v1;

  bool test12 = v1 == v2;
  bool test13 = v1 == v3;

  std::cout << "test12=" << test12 << " test13=" << test13 << "\n";

  return 0;
}
