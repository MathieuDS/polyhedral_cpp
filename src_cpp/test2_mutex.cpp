#include <thread>
#include <mutex>
#include <iostream>
#include <functional>

struct foo {
public:
  foo() {
    x=0;
  }
  void safe_increment()
  {
    std::lock_guard<std::mutex> lk(mul);
    x++;
  }
  int GetX()
  {
    std::lock_guard<std::mutex> lk(mul);
    return x;
  }
private:
  int x;
  std::mutex mul;
};

int main()
{
  foo eFoo;
  std::function<void(void)> eIncr=[&]() -> void {
    eFoo.safe_increment();
  };
  std::thread t1(eIncr);
  std::thread t2(eIncr);
  t1.join();
  t2.join();
  std::cerr << "x=" << eFoo.GetX() << "\n";
}
