#include <random>
#include <iostream>


int main()
{
    std::random_device rd;
    std::mt19937 gen1(rd());
    std::mt19937 gen2(1234567890);
    std::default_random_engine gen3(0);

    for (int64_t i = 0; i < 10; i++) {
        size_t pos = std::uniform_int_distribution<>(0, 10)(gen3);
        std::cout << "i=" << i << " pos=" << pos << "\n";
    }
}
