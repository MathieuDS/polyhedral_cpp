#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc != 5) {
    std::cerr << "function is used as\n";
    std::cerr << "test_infinite_product [u] [xReal] [xImag] [m]\n";
    std::cerr << "The function computed is\n";
    exit(1);
  }
  std::cerr << std::setprecision(90);

  double u;
  (void)sscanf(argv[1], "%lf", &u);
  double tReal;
  (void)sscanf(argv[2], "%lf", &tReal);
  double tImag;
  (void)sscanf(argv[3], "%lf", &tImag);
  int M;
  (void)sscanf(argv[4], "%d", &M);
  std::complex<double> t(tReal, tImag);
  std::cerr << "u=" << u << "\n";
  std::cerr << "t=" << t << "\n";
  std::cerr << "M=" << M << "\n";
  //
  double pi=std::acos(-1);
  double s = 2 * pi / log(u);
  std::cerr << "s=" << s << "\n";
  // compute u^{xINP}
  double lnU=log(u);
  auto PowFCT=[&](std::complex<double> const& xINP) -> std::complex<double> {
    std::complex<double> eProd = xINP * lnU;
    
    return std::exp(eProd);
  };
  auto FCTcomplex=[&](std::complex<double> const& tIN) -> std::complex<double> {
    std::complex<double> One(double(1), double(0));
    std::complex<double> Two(double(2), double(0));
    std::complex<double> eProd = tIN * (tIN - One) / Two;
    std::complex<double> ExpoTerm=PowFCT(eProd);
    std::complex<double> x = PowFCT(tIN);
    std::complex<double> xI = std::complex<double>(1,0) / x;
    std::cerr << " x=" << x << "\n";
    std::cerr << "xI=" << xI << "\n";
    double uPow=1;
    std::complex<double> InfProd(double(1), double(0));
    for (int i=0; i<M; i++) {
      //      std::cerr << "i=" << i << " uPow=" << uPow << " retVal=" << retVal << "\n";
      double uProd = uPow * u;
      InfProd *= (One + xI * uProd) * (One + x * uPow);
      uPow = uProd;
    }
    std::cerr << "InfProd=" << InfProd << " ExpoTerm=" << ExpoTerm << "\n";
    std::complex<double> retVal = InfProd * ExpoTerm;
    return retVal;
  };
  std::complex<double> RES = FCTcomplex(t);
  std::cerr << " w=" << RES << "\n";
}
