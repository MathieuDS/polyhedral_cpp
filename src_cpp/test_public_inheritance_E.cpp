#include <iostream>
#include <string>

struct subclass {
public:
  void operator()(int val)
  {
    std::cerr << "subclass : operator() : int : val=" << val << "\n";
  }
  void operator()(std::string val)
  {
    std::cerr << "subclass : operator() : string : val=" << val << "\n";
  }
};


struct overclass : subclass {
public:
  using subclass::operator();
  void operator()(int val)
  {
    std::cerr << "overclass : operator() : int : val=" << val << "\n";
    subclass()(val);
  }
};



int main()
{
  overclass inst;
  int val_i = 4;
  std::string val_s("ABCD");
  inst(val_i);
  inst(val_s);
}
