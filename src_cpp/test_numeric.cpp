#include "Temp_common.h"

template<typename T>
void PrintNumericLimit()
{
  std::cerr << "min=" << std::numeric_limits<T>::min() << " max=" << std::numeric_limits<T>::max() << "\n";
}


int main ()
{
  std::cerr << "uint32_t : ";
  PrintNumericLimit<uint32_t>();
  std::cerr << "uint64_t : ";
  PrintNumericLimit<uint64_t>();
  std::cerr << "int64_t : ";
  PrintNumericLimit<int64_t>();
}

