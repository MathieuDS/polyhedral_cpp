#include <iostream>






template<typename T>
const T* get_ptr()
{
  std::cerr << "sizeof(T)=" << sizeof(T) << "\n";
  return nullptr;
}


template<typename Tin>
struct cls {
public:
  Tin val;

  template<typename T>
  const T* try_peek()
  {
    std::cerr << "Doing bad things sizeof(T)=" << sizeof(T) << "\n";
    return (T*)(&val);
  }

};




int main()
{
  //  const int* ptr = get_ptr<int>();
  cls<int> exmp;
  const int* ptr = exmp.template try_peek<int>();
}

