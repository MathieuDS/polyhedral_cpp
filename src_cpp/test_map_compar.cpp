#include <map>
#include <functional>
#include <iostream>


int main()
{
  std::function<bool(int,int)> fctComp = [&](int val1, int val2) -> bool {
    return val1 < val2;
  };

  using T = std::map<int, int, std::function<bool(int,int)>>;
  T  map(fctComp);

  for (int i=0; i<10; i++) {
    map[i*i] = 10 - i;
  }
  for (auto& kv : map) {
    std::cerr << "kv=" << kv.first << " / " << kv.second << "\n";
  }
  T::iterator iter1 = map.begin();
  std::cerr << "iter1=" << iter1->first << " / " << iter1->second << "\n";
  map.erase(iter1);

  T::iterator iter2 = map.begin();
  std::cerr << "iter2=" << iter2->first << " / " << iter2->second << "\n";

  /*
    Conclusions:
    --- We cannot do something like map.end()-- and expect to get a valid iterator
    --- We can erase the iterator after usage.
   */
}
