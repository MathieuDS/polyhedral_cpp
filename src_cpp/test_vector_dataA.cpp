#include <string>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

// This test shows that assignation of data from a pointer leads to the data
// being copied anyway.

int main ()
{
  int* data;
  int n=5;
  data = new int[n];
  for (int i=0; i<n; i++)
    data[i] = i;

  std::vector<int> v(data, data+n);
  for (int i=0; i<n; i++)
    v[i] = 100;
  for (int i=0; i<n; i++)
    std::cerr << "i=" << i << " data=" << data[i] << "\n";
}

