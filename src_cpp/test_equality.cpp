#include <iostream>
#include <set>
#include <string>
#include <functional>


std::string test_equal(int a, int b)
{
  if (a == b)
    {
      return "je su jednaki";
    }
  else
    {
      return "nisu jednaki";
    }
}


int main ()
{
  std::cerr << "1: result=" << test_equal(5, 5) << "\n";
  std::cerr << "2: result=" << test_equal(5, 6) << "\n";
}

