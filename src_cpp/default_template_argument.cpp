#include <iostream>



template<int val = 32>
void print()
{
  std::cerr << "val=" << val << "\n";
}



int main()
{
  print<>();
  print<42>();
}
