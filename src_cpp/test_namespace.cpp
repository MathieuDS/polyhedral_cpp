#include <iostream>

namespace test { namespace AAAA {
    int get_val_a(int const& val) {
      return 2*val + val * val;
    }
  } }


namespace test::BBBB {
  int get_val_b(int const& val) {
    return 2*val + 3 * val * val;
  }
}



int main()
{
  for (int i=0; i<10; i++) {
    std::cerr << "i=" << i << " val_a=" << test::AAAA::get_val_a(i) << " val_b=" << test::BBBB::get_val_b(i) << "\n";
  }
}
