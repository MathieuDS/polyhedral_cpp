#include <iostream>
#include <vector>
#include <concepts>

struct contain {
  int x;
};

struct broken_contain {
  int y;
};




template<typename T>
concept HasX = requires(T a)
{
  a.x;
};

template<HasX T>
int sqr(T const& u)
{
  return u.x * u.x;
}








int main ()
{
  contain ah{2};
  broken_contain bh{2};
  std::cerr << "sum(V_int)=" << sqr(ah) << "\n";
  //  std::cerr << "sum(V_int)=" << sqr(bh) << "\n";  // Does not compile as desired.

  // To avoid compiler warning
  std::cerr << "bh.y=" << bh.y << "\n";
}

