#include <iostream>


struct TheClass {
private:
  int val;
public:
  TheClass(int const& eVal)
  {
    val = eVal;
  }
  int getval() const
  {
    int valRet = val + 1;
    return valRet;
  }
  int getval()
  {
    int valRet = val;
    return valRet;
  }
};



int main(int argc, char* argv[])
{
  TheClass eRec(2);
  std::cerr << "val=" << eRec.getval() << "\n";
}

