#include <iostream>
#include <vector>

// The test is interesting in order to test
// the overflow and its consequences.

// Very inefficient code. Print statement in order to avoid optimization.
// But it turns out to work ok with the swap. The overflow cancels each other
// and get us a correct result.
template<typename T>
void swap(T & a, T & b)
{
  a = a + b; // u = a + b
  std::cerr << "1: Now a=" << a << "\n";
  b = a - b; // Now b = a
  std::cerr << "2: Now b=" << b << "\n";
  a = a - b; // Now a = b
  std::cerr << "3: Now a=" << a << "\n";
}



int main(int argc, char* argv[])
{
  unsigned int val1=34;
  unsigned int val2=45;
  swap(val1, val2);
  std::cerr << "val1=" << val1 << " val2=" << val2 << "\n";


  unsigned int idx1=1836311903;
  unsigned int idx2=2971215073;
  swap(idx1, idx2);
  std::cerr << "idx1=" << idx1 << " idx2=" << idx2 << "\n";
}
