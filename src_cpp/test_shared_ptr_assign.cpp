// shared_ptr::operator= example
#include <iostream>
#include <memory>

int main () {
  std::shared_ptr<int> v1 = std::make_shared<int>(1);
  std::shared_ptr<int> v2 = v1;
  std::shared_ptr<int> v3 = v2;
  std::shared_ptr<int> v4 = std::make_shared<int>(*v1);
  *v4 = 5;

  v2 = std::make_shared<int>(3);

  std::cout << "1: *v1: " << *v1 << '\n';
  std::cout << "1: *v2: " << *v2 << '\n';
  std::cout << "1: *v3: " << *v3 << '\n';
  std::cout << "1: *v4: " << *v4 << '\n';
  return 0;
}
