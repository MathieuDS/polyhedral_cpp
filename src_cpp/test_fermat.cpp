#include <iostream>
#include <functional>
#include "gmpxx.h"
int main (int argc, char* argv[])
{
  using T=mpz_class;
  T n=3;
  auto comp_pow=[](T const& a, T const& nb) -> T {
    T ret = 1;
    for (T i=0; i<nb; i++)
      ret *= a;
    return ret;
  };
  while(true) {
    for (T x=1; x<=n; x++)
      for (T y=1; y<=n; y++)
        for (T z=1; z<=n; z++)
          {
            T term1 = comp_pow(x, n);
            T term2 = comp_pow(y, n) + comp_pow(z, n);
            if (term1 == term2) {
              std::cerr << "CounterExample n=" << n << " x=" << x << " y=" << y << " z=" << z << "\n";
              exit(1);
            }
          }
    n++;
  }
}

