#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>


struct PolyhedralInv {
  int dim;
  std::string eStr;
};



bool operator==(PolyhedralInv const& x, PolyhedralInv const& y)
{
  if (x.dim != y.dim)
    return false;
  if (x.eStr != y.eStr)
    return false;
  return true;
}


int main(int argc, char** argv)
{
  PolyhedralInv fInv_t{1, "bs"};
  PolyhedralInv eInv_t{0, "bst"};
  if (fInv_t == eInv_t) {
    std::cerr << "They are equal\n";
  }
  else {
    std::cerr << "They are NOT equal\n";
  }
}
