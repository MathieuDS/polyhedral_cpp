#include "Temp_common.h"
#include <Eigen/Dense>
#include <Eigen/LU>


Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic> HilbertMatrix(int const& n)
{
  Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic> eMat(n, n);
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      eMat(i,j)=double(1)/(double(i+j+1));
  std::cerr << "Assignation finished\n";
  return eMat;
}

int main ()
{
  std::cerr << "Before GetMatrix\n";
  Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic> B=HilbertMatrix(4);
  std::cerr << "After GetMatrix\n";
  int nRow=B.rows();
  int nCol=B.cols();
  for (int iRow=0; iRow<nRow; iRow++)
    for (int iCol=0; iCol<nCol; iCol++)
      std::cerr << "iRow/iCol=" << iRow << "," << iCol << " eVal=" << B(iRow,iCol) << "\n";
  return 0;
}

