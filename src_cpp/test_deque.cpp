#include <deque>
#include <iostream>



int main()
{
  std::deque<int> l;
  for (size_t i=0; i<10; i++) {
    int val = rand() % 100;
    l.push_back(val);
    std::cerr << "i=" << i << " val=" << val << "\n";
  }

  std::cerr << "l =";
  for (auto & n : l)
    std::cerr << " " << n;
  std::cerr << "\n";

  for (auto & n : l) {
    if (n < 50)
      n = 0;
  }

  std::cerr << "l =";
  for (auto & n : l)
    std::cerr << " " << n;
  std::cerr << "\n";

  std::cerr << "l =";
  while (!l.empty()) {
    const int v = l.front();
    std::cerr << " " << v;
    l.pop_front();
  }
  std::cerr << "\n";

  auto iter = l.begin();
  std::cerr << "l =";
  while (iter != l.end()) {
    std::cerr << "  A " << *iter;
    
    iter++;
  }
  std::cerr << "\n";
  
}
