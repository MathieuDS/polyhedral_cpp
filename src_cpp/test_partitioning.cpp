#include <vector>
#include <algorithm>
#include <iostream>


static int64_t dist_get_start(int64_t total, int num_pes, int node_id) {
    int64_t res = total % num_pes;
    int64_t blk = (total - res) / num_pes;
    return node_id * blk + std::min(int64_t(node_id), res);
}

static int64_t dist_get_end(int64_t total, int num_pes, int node_id) {
    return dist_get_start(total, num_pes, node_id+1);
}

/*
static int64_t dist_get_node_portion(int64_t total, int num_pes, int node_id) {
    return dist_get_end(total, num_pes, node_id) -
           dist_get_start(total, num_pes, node_id);
}
*/

static int64_t index_rank(int64_t total, int num_pes, int index) {
    int64_t res = total % num_pes;
    int64_t blk = (total - res) / num_pes;
    int64_t crit = (blk + 1) * res;
    if (index < crit) {
      return index / (blk+1);
    }
    else {
      return res + (index - crit) / blk;
    }
}

int main()
{
  int64_t total =1000;
  int num_pes = 17;
  std::vector<int> V(total, -1);
  for (int node_id=0; node_id<num_pes; node_id++) {
    int64_t start = dist_get_start(total, num_pes, node_id);
    int64_t end = dist_get_end(total, num_pes, node_id);
    std::cout << "node_id=" << node_id << " start=" << start << " end=" << end << "\n";
    for (int pos=start; pos<end; pos++) {
      V[pos] = node_id;
    }
  }
  int nb_check=0;
  for (int i=0; i<total; i++) {
    int e_node = index_rank(total, num_pes, i);
    int f_node = V[i];
    nb_check++;
    if (e_node != f_node) {
      std::cout << "Error e_node=" << e_node << " f_node=" << f_node << "\n";
    }
  }
  std::cout << "nb_check=" << nb_check << "\n";
}
