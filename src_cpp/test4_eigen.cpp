#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>

int main(int argc, char** argv){

  Eigen::SparseMatrix<double, Eigen::ColMajor> A_sparse(10, 10);
  std::vector< Eigen::Block<Eigen::SparseMatrix<double, Eigen::ColMajor> > > sparse_block_vector;
  Eigen::Block<Eigen::SparseMatrix<double, Eigen::ColMajor> > sparse_block = A_sparse.block(0, 0, 5, 5);
  sparse_block_vector.push_back(sparse_block);

}
