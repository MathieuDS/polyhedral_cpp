#ifndef ENUMERATION_FEASIBLE_PENTAGON
#define ENUMERATION_FEASIBLE_PENTAGON

#include "MAT_Matrix.h"
#include "PlaneGraph.h"
#include "POLY_LinearProgramming.h"
#include "Namelist.h"



FullNamelist NAMELIST_GetStandard_PENTAGON_enum()
{
  std::map<std::string, SingleBlock> ListBlock;
  // PROC
  std::map<std::string, int> ListIntValues1;
  std::map<std::string, bool> ListBoolValues1;
  std::map<std::string, double> ListDoubleValues1;
  std::map<std::string, std::string> ListStringValues1;
  std::map<std::string, std::vector<std::string>> ListListStringValues1;
  ListStringValues1["PentagonType"]="cannot write it for you";
  ListStringValues1["PlaneFile"]="PLori_16_1";
  ListStringValues1["OutFile"]="result.txt";
  ListStringValues1["PrefixRealPaver"]="unset";
  ListIntValues1["MAX_NBITER"]=-1;
  ListIntValues1["MAX_NBSOL"]=-1;
  ListBoolValues1["DoFull"]=true;
  ListBoolValues1["OutputRealPaver"]=false;
  SingleBlock BlockPROC;
  BlockPROC.ListIntValues=ListIntValues1;
  BlockPROC.ListBoolValues=ListBoolValues1;
  BlockPROC.ListDoubleValues=ListDoubleValues1;
  BlockPROC.ListStringValues=ListStringValues1;
  BlockPROC.ListListStringValues=ListListStringValues1;
  ListBlock["PROC"]=BlockPROC;
  // Merging all data
  return {ListBlock, "undefined"};
}









struct CombinSolution {
  std::vector<int> DEpositionVert;
  std::vector<int> DEpositionEdge;
  std::vector<int> ListChosenIChoice;
};




std::vector<std::vector<int>> GetNontrivialSymmetryOfNgon(std::vector<int> const& arrN)
{
  int N=arrN.size();
  std::vector<std::vector<int>> ListSym;
  auto IsTrivial=[&](std::vector<int> const& V) -> bool {
    for (int i=0; i<N; i++)
      if (V[i] != i)
	return false;
    return true;
  };
  int nbDistVal=VectorMax(arrN) + 1;
  auto IsCorrectSymmetry=[&](std::vector<int> const& VS) -> bool {
    std::vector<int> Mapping(nbDistVal,-1);
    for (int i=0; i<N; i++) {
      int iImg=VS[i];
      int eVal=arrN[i];
      int fVal=arrN[iImg];
      if (Mapping[eVal] == -1) {
	Mapping[eVal]=fVal;
      }
      else {
	if (Mapping[eVal] != fVal)
	  return false;
      }
    }
    return true;
  };
  auto FuncInsert=[&](std::vector<int> const& VS) -> void {
    if (IsTrivial(VS))
      return;
    std::cerr << "VS=";
    for (auto & eVal : VS)
      std::cerr << " " << eVal;
    std::cerr << "\n";
    bool test=IsCorrectSymmetry(VS);
    std::cerr << "test=" << test << "\n";
    if (test)
      ListSym.push_back(VS);
  };
  for (int i=0; i<N; i++) {
    std::vector<int> VS(N);
    int pos=i;
    for (int j=0; j<N; j++) {
      VS[j]=pos;
      pos=NextIdx(N, pos);
    }
    FuncInsert(VS);
  }
  for (int i=0; i<N; i++) {
    std::vector<int> VS(N);
    int pos=i;
    for (int j=0; j<N; j++) {
      VS[j]=pos;
      pos=PrevIdx(N, pos);
    }
    FuncInsert(VS);
  }
  return ListSym;
}



std::vector<int> MappingNgonSymmetryToAngle(std::vector<int> const& eSym)
{
  int N=eSym.size();
  std::vector<std::vector<int>> ListPairAng;
  for (int i=0; i<N; i++) {
    int iPrev=PrevIdx(N,i);
    if (iPrev > i)
      ListPairAng.push_back({i, iPrev});
    else
      ListPairAng.push_back({iPrev, i});
  }
  std::vector<int> eSymAng(N);
  for (int i=0; i<N; i++) {
    int i0=ListPairAng[i][0];
    int i1=ListPairAng[i][1];
    int i0img=eSym[i0];
    int i1img=eSym[i1];
    std::vector<int> ePairImg;
    if (i0img < i1img)
      ePairImg = {i0img, i1img};
    else
      ePairImg = {i1img, i0img};
    int iPos=-1;
    for (int j=0; j<N; j++)
      if (ePairImg == ListPairAng[j])
	iPos=j;
    if (iPos == -1) {
      std::cerr << "Inconsistency in MappingNgonSymmetryToAngle\n";
      throw TerminalException{1};
    }
    eSymAng[i]=iPos;
  }
  return eSymAng;
}


template<typename T>
MyMatrix<T> GetListIneq_Large(int const& N)
{
  std::vector<MyVector<T>> ListIneq_vect;
  for (int i=0; i<N; i++) {
    MyVector<T> eIneq1=ZeroVector<T>(1+N);
    eIneq1(i+1)=1;
    ListIneq_vect.push_back(eIneq1);
    //
    MyVector<T> eIneq2=ZeroVector<T>(1+N);
    eIneq2(0)=1;
    eIneq2(i+1)=-1;
    ListIneq_vect.push_back(eIneq2);
  }
  MyMatrix<T> ListIneq=MatrixFromVectorFamily(ListIneq_vect);
  return ListIneq;
}

template<typename T>
MyVector<T> GetSingleEqua_Large(int const& N, int const& nbFace)
{
  MyVector<T> SingleEqua=ZeroVector<T>(1+N);
  for (int i=0; i<N; i++)
    SingleEqua(i+1)=1;
  T nbFace_T=nbFace;
  SingleEqua(0) = - (N - 2 + 4/nbFace_T); // need to change value for the case of 
  return SingleEqua;
}






template<typename T>
void EnumerationFeasiblePentagon(PlanGraphOriented const& PLori, std::vector<int> const& arrN, std::function<void(CombinSolution const&)> const& FuncInsert, FullNamelist const& eFull)
{
  std::vector<std::vector<int>> ListSym = GetNontrivialSymmetryOfNgon(arrN);
  std::cerr << "|ListSym|=" << ListSym.size() << "\n";
  std::vector<std::vector<int>> ListSymAng;
  for (auto & eSym : ListSym)
    ListSymAng.push_back(MappingNgonSymmetryToAngle(eSym));
  auto VEFori=PlanGraphOrientedToVEF(PLori);
  FlagStructure FS=PlanGraphOrientedToABC(PLori);
  TheGroupFormat GRP=AutomorphismGroupFlagStructure(FS);
  std::cerr << "|GRP|=" << GRP.size << "\n";
  SingleBlock BlPROC=eFull.ListBlock.at("PROC");
  bool DoFull=BlPROC.ListBoolValues.at("DoFull");
  int MAX_NBITER = BlPROC.ListIntValues.at("MAX_NBITER");
  int MAX_NBSOL  = BlPROC.ListIntValues.at("MAX_NBSOL");
  int N=arrN.size();
  int nbChoice=2*N;
  int MaxNbChoice;
  if (DoFull) {
    MaxNbChoice=nbChoice;
  }
  else {
    MaxNbChoice=N;
  }
  std::cerr << "N=" << N << " nbChoice=" << nbChoice << " MaxNbChoice=" << MaxNbChoice << "\n";
  int nbFace=VEFori.nbFace;
  int nbVert=VEFori.nbVert;
  int nbDE=PLori.nbP;
  std::cerr << "nbDE=" << nbDE << " nbFace=" << nbFace << " nbVert=" << nbVert << "\n";
  MyMatrix<int> AdjFace(nbFace,5);
  for (int iFace=0; iFace<nbFace; iFace++) {
    //    std::cerr << "iFace=" << iFace << " adj=";
    for (int i=0; i<N; i++) {
      int eDE=VEFori.FaceSet[iFace][i];
      //      std::cerr << "iFace=" << iFace << " i=" << i << " eDE=" << eDE << "\n";
      int rDE=PLori.invers.at(eDE);
      //      std::cerr << "    rDE=" << rDE << "\n";
      int jFace=VEFori.ListOriginFace[rDE];
      //      std::cerr << "iFace=" << iFace << " i=" << i << "\n";
      AdjFace(iFace,i)=jFace;
      //      std::cerr << " " << jFace;
    }
    //    std::cerr << "\n";
  }
  MyMatrix<int> ListChoiceEdge(nbChoice,N);
  MyMatrix<int> ListChoiceVert(nbChoice,N);
  for (int iChoice=0; iChoice<N; iChoice++) {
    int pos=iChoice;
    for (int i=0; i<N; i++) {
      ListChoiceEdge(iChoice,i)=pos;
      ListChoiceVert(iChoice,i)=pos;
      pos=NextIdx(N, pos);
    }
  }
  for (int iChoice=N; iChoice<nbChoice; iChoice++) {
    int posEdge=iChoice-N;
    int posVert=iChoice+1-N;
    if (posVert >= N)
      posVert -= N;
    for (int i=0; i<N; i++) {
      posEdge--;
      posVert--;
      if (posEdge < 0)
	posEdge += N;
      if (posVert < 0)
	posVert += N;
      ListChoiceEdge(iChoice,i)=posEdge;
      ListChoiceVert(iChoice,i)=posVert;
    }
  }
  std::cerr << "ListChoiceEdge/Vert built\n";

  std::cerr << "AdjFace has been built\n";
  struct FeasibleLevel {
    int ChosenIFace;
    int ChosenIChoice;
    int relNbChoice;
    std::vector<int> FaceStatus;
    std::vector<int> DEpositionEdge;
    std::vector<int> DEpositionVert;
  };
  std::vector<FeasibleLevel> ListLevel(nbFace);
  std::vector<int> FaceStatus(nbFace,0);
  std::vector<int> DEpositionEdge(nbDE,-1);
  std::vector<int> DEpositionVert(nbDE,-1);
  for (int iFace=0; iFace<nbFace; iFace++)
    ListLevel[iFace]={-1, -1, MaxNbChoice, FaceStatus, DEpositionEdge, DEpositionVert};
  int TheLevel=0;
  std::vector<MyVector<T>> ListIneq_vect;
  for (int i=0; i<N; i++) {
    MyVector<T> eVect1=ZeroVector<T>(2+N);
    eVect1(0)=-1;
    eVect1(i+2)=1;
    ListIneq_vect.push_back(eVect1);
    //
    MyVector<T> eVect2=ZeroVector<T>(2+N);
    eVect2(0)=-1;
    eVect2(1)=1;
    eVect2(i+2)=-1;
    ListIneq_vect.push_back(eVect2);
  }
  MyMatrix<T> ListIneq=MatrixFromVectorFamily(ListIneq_vect);
  std::cerr << "ListIneq built\n";
  //
  MyVector<T> ToBeMinimized=ZeroVector<T>(2+N);
  ToBeMinimized(1)=1;
  //
  MyVector<T> SingleEqua=ZeroVector<T>(2+N);
  for (int i=0; i<N; i++)
    SingleEqua(i+2)=1;
  T nbFace_T=nbFace;
  SingleEqua(1) = - (N - 2 + 4/nbFace_T); // need to change value for the case of 
  
  auto GetEmptyFace=[&]() -> int {
    //    std::cerr << "GetEmptyFace TheLevel=" << TheLevel << "\n";
    if (TheLevel == 0)
      return 0;
    std::vector<int> NbFaceAdj(nbFace,0);
    //    std::cerr << "TheLevel=" << TheLevel << "\n";
    for (int eLevel=0; eLevel<TheLevel; eLevel++) {
      int iChosenFace=ListLevel[eLevel].ChosenIFace;
      for (int i=0; i<N; i++) {
	//	std::cerr << "iChosenFace=" << iChosenFace << "i=" << i << "\n";
	int jFace=AdjFace(iChosenFace,i);
	if (ListLevel[TheLevel-1].FaceStatus[jFace] == 0)
	  NbFaceAdj[jFace]++;
      }
    }
    int eMax=VectorMax(NbFaceAdj);
    /*
    std::cerr << "FaceStatus=";
    for (int jFace=0; jFace<nbFace; jFace++)
      std::cerr << " " << ListLevel[TheLevel-1].FaceStatus[jFace];
    std::cerr << "\n";
    std::cerr << "NbFaceAdj =";
    for (int jFace=0; jFace<nbFace; jFace++)
      std::cerr << " " << NbFaceAdj[jFace];
      std::cerr << "\n";*/
    for (int iFace=0; iFace<nbFace; iFace++)
      if (NbFaceAdj[iFace] == eMax)
	return iFace;
    return -400;
  };
  MyMatrix<T> ListIneq_Large = GetListIneq_Large<T>(N);
  MyVector<T> SingleEqua_Large = GetSingleEqua_Large<T>(N, nbFace);
  // Representation of the symmetries
  std::vector<std::vector<int>> ListPerm(5);
  for (int u=0; u<5; u++) {
    std::vector<int> ePerm(5);
    for (int v=0; v<5; v++) {
      int vImg = 2*u - v;
      if (vImg < 0)
	vImg += 5;
      if (vImg >= 5)
	vImg -= 5;
      ePerm[v] = vImg;
    }
    ListPerm[u] = ePerm;
  }
  std::vector<int> ListPossForSymm;
  for (int u=0; u<5; u++) {
    
  }
  auto IsStateInvariant=[&](std::vector<int> const& ListPositionVert) -> bool {
    std::vector<MyVector<T>> ListEqua_vect{SingleEqua_Large};
    for (int iVert=0; iVert<nbVert; iVert++) {
      MyVector<T> eEqua=ZeroVector<T>(1+N);
      eEqua(0)=-2;
      bool IsFull=true;
      for (auto & eDE : VEFori.VertSet[iVert]) {
        int iDEvert=ListPositionVert[eDE];
	if (iDEvert == -1)
	  IsFull=false;
	else
	  eEqua(1+iDEvert)++;
      }
      if (IsFull)
	ListEqua_vect.push_back(eEqua);
    }
    MyMatrix<T> ListEqua=MatrixFromVectorFamily(ListEqua_vect);
    //
    MyMatrix<T> NSP=NullspaceTrMat(ListEqua);
    int nbNSP=NSP.rows();
    auto TestNSP=[&](int const& eCh) -> bool {
      for (int iNSP=0; iNSP<nbNSP; iNSP++) {
	for (int u=0; u<5; u++) {
	  int uImg=ListPerm[eCh][u];
	  if (NSP(iNSP, u) != NSP(iNSP, uImg))
	    return false;
	}
      }
      return true;
    };
    for (auto & eCh : ListPossForSymm) {
      if (TestNSP(eCh))
	return true;
    }
    return false;
  };
  auto AssignmentLevel=[&](int const& hLevel, int const& iFace, int const& iChoice) -> bool {
    //    std::cerr << "AssignmentLevel hLevel=" << hLevel << " iFace=" << iFace << " iChoice=" << iChoice << "\n";
    ListLevel[hLevel].ChosenIFace=iFace;
    ListLevel[hLevel].ChosenIChoice=iChoice;
    if (hLevel > 0) {
      ListLevel[hLevel].FaceStatus = ListLevel[hLevel-1].FaceStatus;
      ListLevel[hLevel].DEpositionEdge = ListLevel[hLevel-1].DEpositionEdge;
      ListLevel[hLevel].DEpositionVert = ListLevel[hLevel-1].DEpositionVert;
    }
    ListLevel[hLevel].FaceStatus[iFace]=1;
    /*
    std::cerr << "  FaceStatus=";
    for (int jFace=0; jFace<nbFace; jFace++)
      std::cerr << " " << ListLevel[hLevel].FaceStatus[jFace];
      std::cerr << "\n";*/
    for (int i=0; i<N; i++) {
      int relIChoiceEdge=ListChoiceEdge(iChoice,i);
      int relIChoiceVert=ListChoiceVert(iChoice,i);
      int iDE=VEFori.FaceSet[iFace][i];
      ListLevel[hLevel].DEpositionEdge[iDE]=relIChoiceEdge;
      ListLevel[hLevel].DEpositionVert[iDE]=relIChoiceVert;
      int val=arrN[relIChoiceEdge];
      //
      int rDE=PLori.invers.at(iDE);
      int rIChoiceEdge=ListLevel[hLevel].DEpositionEdge[rDE];
      if (rIChoiceEdge != -1) {
	int Rval=arrN[rIChoiceEdge];
	if (val != Rval) {
	  //	  std::cerr << "Return false for wrongly matching edges\n";
	  return false;
	}
      }
    }
    //    std::cerr << "We pass the combinatorial checks\n";
    std::vector<MyVector<T>> ListEqua_vect{SingleEqua};
    for (int iVert=0; iVert<nbVert; iVert++) {
      MyVector<T> eEqua=ZeroVector<T>(2+N);
      eEqua(1)=-2;
      bool IsFull=true;
      for (auto & eDE : VEFori.VertSet[iVert]) {
        int iDEvert=ListLevel[hLevel].DEpositionVert[eDE];
	//	std::cerr << "eDE=" << eDE << "  iDEvert=" << iDEvert << "\n";
	if (iDEvert == -1) {
	  IsFull=false;
	}
	else {
	  eEqua(2+iDEvert)++;
	}
      }
      if (IsFull)
	ListEqua_vect.push_back(eEqua);
    }
    MyMatrix<T> ListEqua=MatrixFromVectorFamily(ListEqua_vect);
    /*
    std::cerr << "ListIneq=\n";
    WriteMatrix(std::cerr, ListIneq);
    std::cerr << "ListEqua=\n";
    WriteMatrix(std::cerr, ListEqua);
    std::cerr << "ToBeMinimized=\n";
    WriteVector(std::cerr, ToBeMinimized);*/
    //    std::cerr << "Before TestRealizability\n";
    bool testPoly=TestRealizabilityInequalitiesEqualities(ListIneq, ListEqua, ToBeMinimized);
    //    std::cerr << "testPoly=" << testPoly << "\n";
    return testPoly;
  };
  auto GoUpNextInTree_sma=[&]() -> bool {
    int iFace=ListLevel[TheLevel-1].ChosenIFace;
    int iChoice=ListLevel[TheLevel-1].ChosenIChoice;
    while(true) {
      iChoice++;
      if (iChoice == MaxNbChoice)
	break;
      //      std::cerr << "GoUpNextInTree_sma, call AssignmentLevel with TheLevel=" << TheLevel << "\n";
      bool test=AssignmentLevel(TheLevel-1, iFace, iChoice);
      if (test)
	return true;
    }
    return false;
  };
  auto GoUpNextInTree_big=[&]() -> bool {
    while(true) {
      if (TheLevel == 0)
	break;
      bool test=GoUpNextInTree_sma();
      if (test)
	return true;
      TheLevel--;
    }
    return false;
  };
  auto NextInTree=[&]() -> bool {
    if (TheLevel == nbFace) {
      //      std::cerr << "We are at maximum level. Move on\n";
      return GoUpNextInTree_big();
    }
    int iFace=GetEmptyFace();
    //    std::cerr << "Result of GetEmptyFace = " << iFace << " TheLevel=" << TheLevel << "\n";
    int iChoice=0;
    while(true) {
      //      std::cerr << "NextInTree, call AssignmentLevel with TheLevel=" << TheLevel << " iFace=" << iFace << " iChoice=" << iChoice << "\n";
      bool test=AssignmentLevel(TheLevel, iFace, iChoice);
      //      std::cerr << "NextInTree, test=" << test << "\n";
      if (test) {
	TheLevel++;
	return true;
      }
      iChoice++;
      if (iChoice == MaxNbChoice)
	break;
    }
    return GoUpNextInTree_big();
  };
  int nbIter=0;
  int nbSol=0;
  while(true) {
    nbIter++;
    //    std::cerr << "----------------------------------  nbIter = " << nbIter << "\n";
    bool test=NextInTree();
    //    std::cerr << "Now TheLevel=" << TheLevel << "\n";
    if (TheLevel == nbFace) {
      /*
      for (int iLevel=0; iLevel<TheLevel; iLevel++) {
	int iFace=ListLevel[TheLevel-1].ChosenIFace;
	std::cerr << " iLevel=" << iLevel << " iFace=" << iFace << "\n";
	}*/
      auto DEpositionVert = ListLevel[TheLevel-1].DEpositionVert;
      auto DEpositionEdge = ListLevel[TheLevel-1].DEpositionEdge;
      std::vector<int> ListChosenIChoice(nbFace);
      for (int iLevel=0; iLevel<TheLevel; iLevel++)
	ListChosenIChoice[iLevel] = ListLevel[iLevel].ChosenIChoice;
      CombinSolution eSol{DEpositionVert, DEpositionEdge, ListChosenIChoice};
      FuncInsert(eSol);
      nbSol++;
      std::cerr << "Now nbSol=" << nbSol << " nbIter=" << nbIter << "\n";
      if (MAX_NBSOL != -1)
	if (nbSol > MAX_NBSOL)
	  return;
    }
    if (MAX_NBITER != -1)
      if (nbIter > MAX_NBITER)
	return;
    if (!test)
      break;
  }
  std::cerr << "nbIter=" << nbIter << "\n";
}








template<typename T>
void WriteInput_RealPaver(std::string const& FileName, PlanGraphOriented const& PLori, VEForiented const& VEFori, CombinSolution const& eSol, std::vector<int> const& arrN)
{
  //  std::cerr << "WriteInput_RealPaver, step 1\n";
  int N=arrN.size();
  int nbDE=PLori.nbP;
  if (N != 5) {
    std::cerr << "We have N=" << N << "\n";
    std::cerr << "The code maybe shoudl be generalized\n";
    throw TerminalException{1};
  }
  int nbVert=VEFori.nbVert;
  int nbFace=VEFori.nbFace;
  std::cerr << "nbVert=" << nbVert << " nbFace=" << nbFace << "\n";
  MyMatrix<T> ListIneq_Large = GetListIneq_Large<T>(N);
  //  std::cerr << "WriteInput_RealPaver, step 2\n";
  //
  MyVector<T> SingleEqua = GetSingleEqua_Large<T>(N, nbFace);
  //
  std::vector<MyVector<T>> ListEqua_vect{SingleEqua};
  for (int iVert=0; iVert<nbVert; iVert++) {
    //    std::cerr << "iVert=" << iVert << "\n";
    MyVector<T> eVect=ZeroVector<T>(1+N);
    eVect(0) = - 2;
    for (auto & eDE : VEFori.VertSet[iVert]) {
      int iDE=eSol.DEpositionVert[eDE];
      //      std::cerr << "  eDE=" << eDE << " iDE=" << iDE << "\n";
      eVect[iDE+1]++;
    }
    /*    std::cerr << "eVect=";
	  WriteVector(std::cerr, eVect);*/
    ListEqua_vect.push_back(eVect);
  }
  MyMatrix<T> ListEqua=MatrixFromVectorFamily(ListEqua_vect);
  //  std::cerr << "WriteInput_RealPaver, step 3\n";

  EmbeddedPolytope<T> DescEmbed=ComputeEmbeddedPolytope(ListIneq_Large, ListEqua);
  //  std::cerr << "WriteInput_RealPaver, step 4\n";
  int nbParamAng=DescEmbed.LinSpace.rows() - 1;
  std::vector<T> ListMinParam(nbParamAng);
  std::vector<T> ListMaxParam(nbParamAng);
  for (int iParamAng=0; iParamAng<nbParamAng; iParamAng++) {
    MyVector<T> ToBeMinimized1=ZeroVector<T>(nbParamAng+1);
    ToBeMinimized1(1+iParamAng)=1;
    LpSolution<T> eSol1=CDD_LinearProgramming(DescEmbed.ListIneq, ToBeMinimized1);
    ListMinParam[iParamAng]=eSol1.OptimalValue;
    //
    MyVector<T> ToBeMinimized2=ZeroVector<T>(nbParamAng+1);
    ToBeMinimized2(1+iParamAng)=-1;
    LpSolution<T> eSol2=CDD_LinearProgramming(DescEmbed.ListIneq, ToBeMinimized2);
    ListMaxParam[iParamAng]=-eSol2.OptimalValue;
  }
  //  std::cerr << "WriteInput_RealPaver, step 5\n";
  int nbEdgeDist=VectorMax(arrN) + 1;
  std::ofstream os(FileName);
  os << "/*  nbParamAng=" << nbParamAng << " nbEdgeDist=" << nbEdgeDist << "\n";
  os << "ListChosenIChoice=";
  for (int iFace=0; iFace<nbFace; iFace++)
    os << " " << eSol.ListChosenIChoice[iFace];
  os << "\n";
  os << "AnglePosition=";
  for (int i=0; i<N; i++)
    os << " " << DescEmbed.LinSpace(0,i+1);
  os << "\n";
  os << "AnglePositionVert=";
  for (int iDE=0; iDE<nbDE; iDE++) {
    int iDE5=eSol.DEpositionVert[iDE];
    T eVal=DescEmbed.LinSpace(0,iDE5+1);
    os << " " << eVal;
  }
  os << "\n";
  os << "    ListIneq=\n";
  WriteMatrix(os, ListIneq_Large);
  os << "    ListEqua=\n";
  WriteMatrix(os, ListEqua);
  os << "    DescEmbed.ListIneq=\n";
  WriteMatrix(os, DescEmbed.ListIneq);
  os << "    DescEmbed.LinSpace=\n";
  WriteMatrix(os, DescEmbed.LinSpace);
  os << " */\n";
  os << "Variables\n";
  int idxVar=0;
  for (int iParamAng=0; iParamAng<nbParamAng; iParamAng++) {
    idxVar++;
    os << "  x" << idxVar << " in ]" << ListMinParam[iParamAng] << "," << ListMaxParam[iParamAng] << "[,\n";
  }
  for (int iEdgeDist=0; iEdgeDist<nbEdgeDist; iEdgeDist++) {
    idxVar++;
    os << "  x" << idxVar << " in [0,@pi]";
    if (iEdgeDist < nbEdgeDist -1) {
      os << ",";
    }
    else {
      os << ";";
    }
    os << "\n";
  }
  os << "\n\n";
  //  std::cerr << "WriteInput_RealPaver, step 6\n";
  os << "Constraints\n";
  int nbIneq=DescEmbed.ListIneq.rows();
  for (int iIneq=0; iIneq<nbIneq; iIneq++) {
    bool HasNonZero=false;
    for (int iParamAng=0; iParamAng<nbParamAng; iParamAng++) {
      T eVal=DescEmbed.ListIneq(iIneq, 1+iParamAng);
      if (eVal != 0)
	HasNonZero=true;
    }
    if (HasNonZero) {
      os << "  " << DescEmbed.ListIneq(iIneq,0);
      for (int iParamAng=0; iParamAng<nbParamAng; iParamAng++) {
	T eVal=DescEmbed.ListIneq(iIneq, 1+iParamAng);
	int idxVar = 1 + iParamAng;
	if (eVal != 0) {
	  if (eVal > 0) {
	    os << " + " << eVal << "*x" << idxVar;
	  }
	  else {
	    T fVal = -eVal;
	    os << " - " << fVal << "*x" << idxVar;
	  }
	}
      }
      os << " >= 0,\n";
    }
  }
  //  std::cerr << "WriteInput_RealPaver, step 7\n";
  std::vector<std::string> ListExpressionAngle(N);
  for (int i=0; i<N; i++) {
    std::ostringstream osB;
    osB << "@pi * (" << DescEmbed.LinSpace(0,i+1);
    for (int iParamAng=1; iParamAng<=nbParamAng; iParamAng++) {
      T eVal=DescEmbed.LinSpace(iParamAng,i+1);
      if (eVal != 0) {
	if (eVal > 0) {
	  osB << " + " << eVal << "*x" << iParamAng;
	}
	else {
	  T fVal=-eVal;
	  osB << " - " << fVal << "*x" << iParamAng;
	}
      }
    }
    osB << ")";
    ListExpressionAngle[i] = osB.str();
  }
  //  std::cerr << "WriteInput_RealPaver, step 8\n";
  std::vector<std::string> ListExpressionEdgeLength(N);
  for (int i=0; i<N; i++) {
    int iEdgeDist=arrN[i];
    int idxVar=iEdgeDist + 1 + nbParamAng;
    std::ostringstream osB;
    osB << "x" << idxVar;
    ListExpressionEdgeLength[i]=osB.str();
  }
  //
  // Now the edge matching equations
  //
  //  std::cerr << "WriteInput_RealPaver, step 9\n";
  for (int i=0; i<5; i++) {
    int i0=i;
    int i1=NextIdx(5,i0);
    int i2=NextIdx(5,i1);
    int i3=NextIdx(5,i2);
    int i4=NextIdx(5,i3);
    //
    std::string Dist_a=ListExpressionEdgeLength[i0];
    std::string Dist_b=ListExpressionEdgeLength[i1];
    std::string Dist_c=ListExpressionEdgeLength[i2];
    std::string Dist_d=ListExpressionEdgeLength[i3];
    std::string Dist_e=ListExpressionEdgeLength[i4];
    //
    std::string Ang_alpha=ListExpressionAngle[i0];
    std::string Ang_phi=ListExpressionAngle[i1];
    std::string Ang_psi=ListExpressionAngle[i2];
    std::string Ang_beta=ListExpressionAngle[i3];
    std::string Ang_theta=ListExpressionAngle[i4];
    //
    std::string eEqua1 = "cos(" + Dist_d + ") * cos(" + Dist_e + ") + sin(" + Dist_d + ") * sin(" + Dist_e + ") * cos(" + Ang_theta + ")";
    std::string eEqua2 = "cos(" + Dist_a + ") * cos(" + Dist_b + ") * cos(" + Dist_c + ") + sin(" + Dist_b + ") * (sin(" + Dist_a + ") * cos(" + Dist_c + ") * cos(" + Ang_phi + ") + cos(" + Dist_a + ") * sin(" + Dist_c + ") * cos(" + Ang_psi + ") ) + sin(" + Dist_a + ") * sin(" + Dist_c + ") * ( sin(" + Ang_phi + ") * sin(" + Ang_psi + ") - cos(" + Dist_b + ") * cos(" + Ang_phi + ") * cos(" + Ang_psi + ") )";
    os << "  " << eEqua1 << "   =   " << eEqua2;
    if (i < N-1) {
      os << ",";
    }
    else {
      os << ";";
    }
    os << "\n";
  }
  //  std::cerr << "WriteInput_RealPaver, step 10\n";
}






#endif
