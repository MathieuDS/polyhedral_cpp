#include "EnumerationFeasible.h"

int main(int argc, char *argv[])
{
  try {
    FullNamelist eFull=NAMELIST_GetStandard_PENTAGON_enum();
    if (argc != 2) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "PENT_EnumerateFeasibleSolution [FileOption.nml]\n";
      std::cerr << "\n";
      std::cerr << "Example of format for FileOption.nml :\n";
      NAMELIST_WriteNamelistFile(std::cerr, eFull);
      return -1;
    }
    std::string eFileName=argv[1];
    NAMELIST_ReadNamelistFile(eFileName, eFull);
    //
    SingleBlock BlPROC=eFull.ListBlock.at("PROC");
    std::string optPentType=BlPROC.ListStringValues.at("PentagonType");
    std::string PlaneFile=BlPROC.ListStringValues.at("PlaneFile");
    std::string OutFile=BlPROC.ListStringValues.at("OutFile");
    std::string PrefixRealPaver=BlPROC.ListStringValues.at("PrefixRealPaver");
    bool OutputRealPaver=BlPROC.ListBoolValues.at("OutputRealPaver");
    //
    std::ifstream is(PlaneFile);
    auto PLori=ReadPlanGraphOrientedStream(is);
    std::cerr << "We have PLori\n";
    int nbDE=PLori.nbP;
    auto VEFori=PlanGraphOrientedToVEF(PLori);
    std::cerr << "We have VEFori\n";
    //
    std::cerr << "optPentType=" << optPentType << "\n";
    std::vector<int> arr5{};
    if (optPentType == "aaaab")
      arr5={0,0,0,0,1};
    if (optPentType == "aaabb")
      arr5={0,0,0,1,1};
    if (optPentType == "aaabc")
      arr5={0,0,0,1,2};
    if (optPentType == "aabbc")
      arr5={0,0,1,1,2};
    if (arr5.size() == 0) {
      std::cerr << "Failed assignment of arr5\n";
      std::cerr << "Allowed values are aaaab, aaabb, aaabc, aabbc\n";
      throw TerminalException{1};
    }
    std::cerr << "We have |arr5| = " << arr5.size() << "\n";
    //
    std::ofstream os(OutFile);
    int nbSol=0;
    std::function<void(CombinSolution const&)> f=[&](CombinSolution const& xSol) -> void {
      os << "1\n";
      for (int i=0; i<nbDE; i++)
	os << " " << xSol.DEpositionVert[i];
      os << "\n";
      for (int i=0; i<nbDE; i++)
	os << " " << xSol.DEpositionEdge[i];
      os << "\n";
      nbSol++;
      //
      if (OutputRealPaver) {
	std::string FileName = PrefixRealPaver + IntToString(nbSol) + ".input";
	WriteInput_RealPaver<mpq_class>(FileName, PLori, VEFori, xSol, arr5);
      }
      
    };
    EnumerationFeasiblePentagon<mpq_class>(PLori, arr5, f, eFull);
    std::cerr << "We have full enumeration nbSol = " << nbSol << "\n";
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
  std::cerr << "Completion of the program\n";
}
