package main
import "fmt"

func main() {
	var s = []int{3, 5, 7, 9, 11, 13, 17}
	var s2 = []int{3, 5}
        copy(s2, s)

	// Short hand declaration
	t := []int{2, 4, 8, 16, 32, 64}

	fmt.Println("s = ", s)
	fmt.Println("t = ", t)
}
