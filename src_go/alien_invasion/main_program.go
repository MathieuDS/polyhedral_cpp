package main

import "os"
import "fmt"
import "strings"
import "strconv"
import "io/ioutil"
import "math/rand"

type city struct {
    name string
    //
    south_idx int
    north_idx int
    west_idx int
    east_idx int
    //
    aliveness bool
}



func check(e error) {
    if e != nil {
        panic(e)
    }
}

func insert_city(eName string, list_city []city) []city {
  nbCity := len(list_city)
  for iCity:=0; iCity<nbCity; iCity++ {
    if list_city[iCity].name == eName {
      return list_city
    }
  }
  return append(list_city, city{eName, -1, -1, -1, -1, true})
}

func get_city_index(eName string, list_city []city) int {
  nbCity := len(list_city)
  for iCity:=0; iCity<nbCity; iCity++ {
    if list_city[iCity].name == eName {
      return iCity
    }
  }
  return -1
}




func main() {
    //
    // Number of arguments check
    //
    fmt.Println("number of arguments =", len(os.Args))
    if len(os.Args) != 3 {
      panic("The program is run as main_program [Grid] [N]")
    }
    N, err := strconv.Atoi(os.Args[2])
    check(err)
    //
    // FileName containing the Maze
    //
    FileName := os.Args[1]
    fmt.Println("FileName = ", FileName)
    // check file existence
    _, err_stat := os.Stat(FileName)
    if err_stat != nil {
        if os.IsNotExist(err_stat) {
            panic("File does not exist.")
        }
    }
    //
    // Reading of the maze
    // 1: Reading all the city
    // 2: assigning the directions afterwards
    //
    list_city := []city{}
    f, err := ioutil.ReadFile(FileName)
    check(err)
    lines := strings.Split(string(f), "\n")
    nbLine := len(lines)
    fmt.Println("|lines|=", nbLine)
    for iLine:=0; iLine<nbLine; iLine++ {
      eLine := lines[iLine]
      if len(eLine)>0 {
        entry := strings.Split(eLine, " ")
        list_city = insert_city(entry[0], list_city)
        for iEnt:=1; iEnt<len(entry); iEnt++ {
          ePair := strings.Split(entry[iEnt], "=")
          if len(ePair) != 2 {
            strError := "iEnt=" + strconv.Itoa(iEnt) + " entry[iEnt]=" + entry[iEnt] + " and we should have |ePair|=" + strconv.Itoa(len(ePair))
            panic(strError)
          }
          list_city = insert_city(ePair[1], list_city)
        }
      }
    }
    nbCity := len(list_city)
    fmt.Println("|list_city|=", nbCity)
    //
    for iLine:=0; iLine<nbLine; iLine++ {
      eLine := lines[iLine]
      if len(eLine)>0 {
        entry := strings.Split(eLine, " ")
        iCity := get_city_index(entry[0], list_city)
        for iEnt:=1; iEnt<len(entry); iEnt++ {
          ePair := strings.Split(entry[iEnt], "=")
          eDir  := ePair[0]
          jCity := get_city_index(ePair[1], list_city);
          isAssigned := false
          //
          if eDir == "north" {
            if list_city[iCity].north_idx != -1 && list_city[iCity].north_idx != jCity {
              panic("Inconsistency in the input file")
            }
            list_city[iCity].north_idx = jCity
            if list_city[jCity].south_idx != -1 && list_city[jCity].south_idx != iCity {
              panic("Inconsistency in the input file")
            }
            list_city[jCity].south_idx = iCity
            isAssigned = true
          }
          //
          if eDir == "south" {
            if list_city[iCity].south_idx != -1 && list_city[iCity].south_idx != jCity {
              panic("Inconsistency in the input file")
            }
            list_city[iCity].south_idx = jCity
            if list_city[jCity].north_idx != -1 && list_city[jCity].north_idx != iCity {
              panic("Inconsistency in the input file")
            }
            list_city[jCity].north_idx = iCity
            isAssigned = true
          }
          //
          if eDir == "west" {
            if list_city[iCity].west_idx != -1 && list_city[iCity].west_idx != jCity {
              panic("Inconsistency in the input file")
            }
            list_city[iCity].west_idx = jCity
            if list_city[jCity].east_idx != -1 && list_city[jCity].east_idx != iCity {
              panic("Inconsistency in the input file")
            }
            list_city[jCity].east_idx = iCity
            isAssigned = true
          }
          //
          if eDir == "east" {
            if list_city[iCity].east_idx != -1 && list_city[iCity].east_idx != jCity {
              panic("Inconsistency in the input file")
            }
            list_city[iCity].east_idx = jCity
            if list_city[jCity].west_idx != -1 && list_city[jCity].west_idx != iCity {
              panic("Inconsistency in the input file")
            }
            list_city[jCity].west_idx = iCity
            isAssigned = true
          }
          //
          if isAssigned == false {
            panic("No matching entry for the direction")
          }
        }
      }
    }
    //
    // Initial assignation of the aliens
    //
    alien_positioning := []int{}
    alien_liveness := []int{}
    for iN:=0; iN<N; iN++ {
      iCity := rand.Intn(nbCity)
      fmt.Println("Alien iN=", iN, " initial position iCity=", iCity)
      alien_positioning = append(alien_positioning, iCity)
      alien_liveness = append(alien_liveness, 1)
    }
    //
    // Random running.
    //
    iter := 0
    maxIter := 10000
    for iter < maxIter {
      //
      // First step: Destroying the cities
      //
      nb_alien := []int{}
      for iCity:=0; iCity<nbCity; iCity++ {
        nb_alien = append(nb_alien, 0)
      }
      for iN:=0; iN<N; iN++ {
        if alien_liveness[iN] == 1 {
          iCity := alien_positioning[iN]
          nb_alien[iCity] = nb_alien[iCity] + 1
        }
      }
      for iCity:=0; iCity<nbCity; iCity++ {
        if nb_alien[iCity] > 1 {
          // the message
          var message = list_city[iCity].name + " has been destroyed by"
          nb_found := 0
          for iN:=0; iN<N; iN++ {
            if iCity == alien_positioning[iN] && alien_liveness[iN] == 1 {
              alien_liveness[iN] = 0
              if nb_found > 0 {
                message = message + " and"
              }
              message = message + " alien " + strconv.Itoa(iN)
              nb_found = nb_found + 1
            }
          }
          fmt.Println(message)
          list_city[iCity].aliveness = false
          //
          // Updating the routing map
          for iDir:=0; iDir<4; iDir++ {
            iCityAdj := -1
            if iDir==0 {
              iCityAdj = list_city[iCity].south_idx
            }
            if iDir==1 {
              iCityAdj = list_city[iCity].north_idx
            }
            if iDir==2 {
              iCityAdj = list_city[iCity].west_idx
            }
            if iDir==3 {
              iCityAdj = list_city[iCity].east_idx
            }
            if iCityAdj != -1 {
              if iDir==0 {
                list_city[iCity].south_idx = -1
                list_city[iCityAdj].north_idx = -1
              }
              if iDir==1 {
                list_city[iCity].north_idx = -1
                list_city[iCityAdj].south_idx = -1
              }
              if iDir==2 {
                list_city[iCity].west_idx = -1
                list_city[iCityAdj].east_idx = -1
              }
              if iDir==3 {
                list_city[iCity].east_idx = -1
                list_city[iCityAdj].west_idx = -1
              }
            }
          }
        }
      }
      //
      // Second step: Randomly moving (if at all possible)
      //
      for iN:=0; iN<N; iN++ {
        iCity:=alien_positioning[iN]
        nbChoice:=0
        if list_city[iCity].south_idx != -1 {
          nbChoice++
        }
        if list_city[iCity].north_idx != -1 {
          nbChoice++
        }
        if list_city[iCity].west_idx != -1 {
          nbChoice++
        }
        if list_city[iCity].east_idx != -1 {
          nbChoice++
        }
        if nbChoice > 0 {
          iPos := rand.Intn(nbChoice)
          iCurrent := 0
          iCityAdj := -1
          if list_city[iCity].south_idx != -1 {
            if iPos == iCurrent {
              iCityAdj = list_city[iCity].south_idx
            }
            iCurrent++
          }
          if list_city[iCity].north_idx != -1 {
            if iPos == iCurrent {
              iCityAdj = list_city[iCity].north_idx
            }
            iCurrent++
          }
          if list_city[iCity].west_idx != -1 {
            if iPos == iCurrent {
              iCityAdj = list_city[iCity].west_idx
            }
            iCurrent++
          }
          if list_city[iCity].east_idx != -1 {
            if iPos == iCurrent {
              iCityAdj = list_city[iCity].east_idx
            }
            iCurrent++
          }
          if iCityAdj == -1 {
            panic("We should have a non-trivial adjacent city")
          }
          alien_positioning[iN] = iCityAdj
        }
      }
      print_positions := true
      if print_positions == true {
        strPos := "list_positions ="
        for iN:=0; iN<N; iN++ {
          strPos += " " + strconv.Itoa(alien_positioning[iN])
        }
        fmt.Println(strPos)
      }
      iter = iter + 1
    }
    //
    // Now printing the output
    //
    for iCity:=0; iCity<nbCity; iCity++ {
      if list_city[iCity].aliveness {
        var message = list_city[iCity].name
        if list_city[iCity].south_idx != -1 {
          iCityAdj := list_city[iCity].south_idx
          message += " south=" + list_city[iCityAdj].name
        }
        if list_city[iCity].north_idx != -1 {
          iCityAdj := list_city[iCity].north_idx
          message += " north=" + list_city[iCityAdj].name
        }
        if list_city[iCity].west_idx != -1 {
          iCityAdj := list_city[iCity].west_idx
          message += " west=" + list_city[iCityAdj].name
        }
        if list_city[iCity].east_idx != -1 {
          iCityAdj := list_city[iCity].east_idx
          message += " east=" + list_city[iCityAdj].name
        }
        fmt.Println(message)
      }
    }
}
