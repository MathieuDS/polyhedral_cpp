This is the README file for the exercise alien invasion.

Basic checks are made on the input files:
---The name of the city should not contain space in their name nor = sign
   which is reasonable.
---The directions are indicated as "south", "north", "east", "west" which is
   fairly reasonable. No capitalization allowed.
---We cannot have two directions like "south=city1", "south=city2" though this
   is not checked in the code.
---The assignation are symmetric. A "city1 south=city2" will implicity define
   a "city2 north=city1"



There is no assumption that at the beginning the aliens are at different places.
So, at the first step, we may have city destruction without movement.

The program is run as
go run main_program.go examples/Basic  7
