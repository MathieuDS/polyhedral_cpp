#include "SimpleSettings.h"
#include <QStringList>
#include <QDebug>
#include <QDateTime>
#include <QFile>


SimpleSettings *SimpleSettings::m_pInstance     = 0;

SimpleSettings *SimpleSettings::getInstance()
{
    if(!m_pInstance)
        m_pInstance = new SimpleSettings();
    return m_pInstance;
}

SimpleSettings::SimpleSettings()
{
    m_sSettingsFile = QString("/etc/psbconfig.ini");
    m_pSettings     = NULL;
}

void SimpleSettings::setConfigFile(const QString & filename) {
    m_sSettingsFile = filename;
}

void SimpleSettings::load(uint16_t id) {
    if(m_pSettings != NULL)
        delete(m_pSettings);

    QString m_save = QString("/etc/psbconfig-%1-node-%2.ini")
            .arg(QDateTime::currentDateTime().toString("yyyyMMdd-hh-mm"))
            .arg(id);
    QFile::copy(m_sSettingsFile, m_save);

    qDebug() << Q_FUNC_INFO << QString("SettingsManager - reading configuration from %1 writing to %2")
                .arg(m_sSettingsFile)
                .arg(m_save);
    m_pSettings = new QSettings(m_save, QSettings::IniFormat);
    printSettings();
}

QVariant SimpleSettings::value(const QString & key, const QVariant & defaultValue)
{
    if(!m_pSettings)
        load(0);
    return m_pSettings->value(key, defaultValue);
}

void SimpleSettings::setValue(const QString & key, const QVariant & value)
{
    if(!m_pSettings)
        load(0);
    m_pSettings->setValue(key,value);
    m_pSettings->sync();
}


void SimpleSettings::printqlist( const QString &key, const QList<QVariant> &list, QString &conf) {
    conf.append(QString("\t%1=").arg(key));
    for(int i = 0; i < (int)list.size(); i++) {
        conf.append(QString("%1, ").arg(list[i].toString()));
    }
    conf.append("\n");
}

void SimpleSettings::printSettings()
{
    QString conf = QString("dump current settings:\n");
    foreach (const QString &group, m_pSettings->childGroups()) {
        conf.append(QString("[%1]\n").arg(group));
        m_pSettings->beginGroup(group);
        foreach (const QString &key, m_pSettings->childKeys()) {
            if((key == "start_coordinate") || (key == "mobile_nodes") || (key == "reference_nodes"))
                printqlist(key, m_pSettings->value(key).toList(), conf);
            else
                conf.append(QString("\t%1=%2\n").arg(key, m_pSettings->value(key).toString()));
        }
        m_pSettings->endGroup();
    }
    qDebug() << Q_FUNC_INFO << conf;
}
