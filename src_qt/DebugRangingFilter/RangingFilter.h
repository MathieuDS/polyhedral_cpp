#ifndef RANGINGFILTER_H
#define RANGINGFILTER_H

#include <QObject>
#include "SimpleSettings.h"

#define SETTING_FILTER_SMOOTH_METHOD              QString("Filter/smooth_method")
#define SETTING_FILTER_SMOOTH_METHOD_DEFAULT      1

#define SETTING_FILTER_CALIB_METHOD               QString("Filter/calibration_method")
#define SETTING_FILTER_CALIB_METHOD_DEFAULT       1

#define SETTING_FILTER_USE_KALMAN                 QString("Filter/use_kalman")
#define SETTING_FILTER_USE_KALMAN_DEFAULT         1

#define SETTING_FILTER_DIST_MIN                   QString("Filter/distance_min")
#define SETTING_FILTER_DIST_MIN_DEFAULT           10

#define SETTING_FILTER_DIST_MAX                   QString("Filter/distance_max")
#define SETTING_FILTER_DIST_MAX_DEFAULT           8000

#define SETTING_FILTER_DQF_MIN                    QString("Filter/dqf_min")
#define SETTING_FILTER_DQF_MIN_DEFAULT            -1

#define SETTING_FILTER_KALMAN_ALPHA               QString("Filter/kalman_param_alpha")
#define SETTING_FILTER_KALMAN_ALPHA_DEFAULT       0.3

#define SETTING_FILTER_MOV_AVG_N                  QString("Filter/moving_average_param_n")
#define SETTING_FILTER_MOV_AVG_N_DEFAULT          3

#define SETTING_FILTER_DIST_VARI_MAX              QString("Filter/distance_variation_max")
#define SETTING_FILTER_DIST_VARI_MAX_DEFAULT      -1

#define SETTING_FILTER_DIST_VARI_MAX_CALC         QString("Filter/distance_variation_max_calculated")
#define SETTING_FILTER_DIST_VARI_MAX_CALC_DEFAULT -1

#define SETTING_FILTER_DIST_MOTION                QString("Filter/use_motion_filter_for_distance")
#define SETTING_FILTER_DIST_MOTION_DEFAULT        false

enum FilterID {
    NONE         = 0,
    KALMAN       = 1,
    AVERAGE      = 2,
    OFFSET       = 3,
    CAPVARIATION = 4
};

/**
 * \class RangingFilter
 * \brief Abstract Filter Class
 */
class RangingFilter : public QObject
{
    Q_OBJECT

public:
    virtual double compute(double distance) = 0;
    virtual void   reset();

protected:
    RangingFilter();
};

/**
 * \class RangingFilterDqf
 * \brief Refuse measure if DQF is too low
 */
class RangingFilterDqf : public RangingFilter
{
public:
    RangingFilterDqf();
    virtual double compute(double distance);
    virtual double compute(double distance, uint8_t dqf);
};

/**
 * \class RangingFilterKalman
 * \brief Perform Bound on measure
 */
class RangingFilterBound : public RangingFilter
{
public:
    RangingFilterBound();
    virtual double compute(double distance);
};

/**
 * \class RangingFilterKalman
 * \brief Perform Kalman Filtering
 */
class RangingFilterOffset : public RangingFilter
{
public:
    RangingFilterOffset();
    virtual void   setOffset(double offset);
    virtual double compute(double distance);
    virtual void   reset();

private:
    double offset;
};

/**
 * \class RangingFilterKalmanVriable
 * \brief Perform Kalman Filtering with variable coefficient
 */
class RangingFilterKalmanVariable : public RangingFilter
{
public:
    RangingFilterKalmanVariable();
    virtual std::pair<double,double> addMeasErr(double distance, double distance_err, double speed, unsigned long long time1, unsigned long long time2);
    virtual double compute(double distance);
    virtual void   reset();
    bool isInitialised();

private:
    double q; /**< process noise covariance */
    double r; /**< measurement noise covariance */
    double x; /**< value */
    double p; /**< estimation error covariance */
    double k; /**< kalman gain */
    double err_meas;
    double err_est;
    bool   initialised;
};



/**
 * \class RangingFilterKalmanVriable
 * \brief Perform Kalman Filtering with variable coefficient
 */
class RangingFilterExponential : public RangingFilter
{
public:
    RangingFilterExponential();
    virtual double compute(double distance);
    virtual void   reset();
    bool isInitialised();

private:
    double x; /**< value */
    double k; /**< exponential coefficient */
    bool   initialised;
};



/**
 * \class RangingFilterKalman
 * \brief Perform Kalman Filtering
 */
class RangingFilterKalman : public RangingFilter
{
public:
    RangingFilterKalman();
    virtual double compute(double measure);
    virtual void   reset();
    bool isInitialised();

private:
    double q; /**< process noise covariance */
    double r; /**< measurement noise covariance */
    double x; /**< value */
    double p; /**< estimation error covariance */
    double k; /**< kalman gain */
    bool   initialised;
};

/**
 * \class RangingFilterAverage
 * \brief Perform Average value
 */
class RangingFilterAverage : public RangingFilter
{
public:
    RangingFilterAverage();
    virtual double compute(double distance);
    virtual void   reset();

private:
    double sum;
    int    samples;
};

/**
 * \class RangingFilterCapVariation
 * \brief Cap Distance Variation
 */
class RangingFilterCapVariation : public RangingFilter
{
public:
    RangingFilterCapVariation();
    virtual double compute(double distance);
    virtual void   reset();

private:
    double last;
    int    refusedCounter;
};

/**
 * \class RangingFilterCapVariation
 * \brief Cap Distance Variation
 */
class RangingFilterCapVariationFromCalculatedDistance : public RangingFilter
{
public:
    RangingFilterCapVariationFromCalculatedDistance();
    virtual double compute(double distance, double calculated);
    virtual void   reset();

private:
    int    refusedCounter;
};

/**
 * \class RangingFilterCapVariation
 * \brief Cap Distance Variation
 */
class RangingFilterMotion : public RangingFilter
{
public:
    RangingFilterMotion();
    virtual double compute(double distance);
    virtual double compute(double distance, bool motion_status_1, bool motion_status_2);
};

/**
 * \class RangingFilterLastCorrect
 * \brief Returns last correct value
 */
class RangingFilterLastCorrect : public RangingFilter
{
public:
    RangingFilterLastCorrect();
    virtual double compute(double distance);
    virtual void   reset();

private:
    double last;
};


class RangingFilterProcessing
{
public:
    double                   value();
    double                   value_error();

    RangingFilterProcessing *insert(double value, uint8_t dqf, unsigned long long time, double speed);
    RangingFilterProcessing *dqfFilter();
    RangingFilterProcessing *bound();
    RangingFilterProcessing *kalman();
    RangingFilterProcessing *kalmanVariable();
    RangingFilterProcessing *exponential();
    RangingFilterProcessing *average();
    RangingFilterProcessing *offset();
    RangingFilterProcessing *capVariation();
    RangingFilterProcessing *motion(bool motion_status_1, bool motion_status_2);
    RangingFilterProcessing *lastCorrect();
    void reset();

    RangingFilterDqf            dqfFilterArr;
    RangingFilterBound          boundFilter;
    RangingFilterKalman         kalmanFilter;
    RangingFilterKalmanVariable kalmanFilterVariable;
    RangingFilterExponential    filterExponential;
    RangingFilterAverage        averageFilter;
    RangingFilterOffset         offsetFilter;
    RangingFilterCapVariation   capVariationFilter;
    RangingFilterMotion         motionFilter;
    RangingFilterLastCorrect    lastCorrectFilter;

private:
    double sample;
    uint8_t sample_dqf;
    unsigned long long sample_time;
    unsigned long long current_time;
    double sample_error;
    double sample_speed;
};

#endif // RANGINGFILTER_H
