#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "RangingFilter.h"

int main(int argc, char *argv[])
{
  RangingFilterProcessing *filter;
  filter = new RangingFilterProcessing();
  double measure=733;
  uint8_t dqf=66;
  unsigned long long time=1493912308992;
  double speed=0;


  filter->insert(measure, dqf, time, speed)
    ->dqfFilter()->offset()
    ->exponential()
    ->bound()->capVariation();
  std::cerr << "value=" << filter->value() << "\n";
  return 1;
}
