#include "RangingFilter.h"
#include <QDebug>

double  RangingFilterProcessing::value() {
    return sample;
}

double  RangingFilterProcessing::value_error() {
    return sample_error;
}

void RangingFilterProcessing::reset() {
    dqfFilterArr.reset();
    boundFilter.reset();
    kalmanFilter.reset();
    kalmanFilterVariable.reset();
    filterExponential.reset();
    averageFilter.reset();
    offsetFilter.reset();
    capVariationFilter.reset();
    sample = -1;
    sample_dqf = 50;
    sample_time = 0;
    sample_error = 200;
    sample_speed = 0;
    sample_time = 0;
    current_time = 0;
}

RangingFilterProcessing *RangingFilterProcessing::insert(double measure, uint8_t dqf, unsigned long long time, double speed_est) {
    qDebug() << Q_FUNC_INFO << QString("insert(in) : measure=%1 dqf=%2 time=%3 speed=%4").arg(measure).arg(dqf).arg(time).arg(speed_est);
    sample = measure;
    sample_dqf = dqf;
    sample_time = time;
    double dqf_d = double(dqf);
    double dqf_coef = (100 - dqf_d) / 100;
    double meas_coef = std::min(double(measure), double(3000));
    double est_error = std::max(double(40), -double(82) + double(588.56) * dqf_coef + double(0.03821) * meas_coef);
    sample_error = est_error;
    sample_speed = speed_est;
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::dqfFilter() {
  sample = dqfFilterArr.compute(sample, sample_dqf);
  return this;
}

RangingFilterProcessing *RangingFilterProcessing::bound() {
    sample = boundFilter.compute(sample);
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::kalman() {
    sample = kalmanFilter.compute(sample);
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::exponential() {
    sample = filterExponential.compute(sample);
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::kalmanVariable() {
  std::pair<double,double> ResKal=kalmanFilterVariable.addMeasErr(sample, sample_error, sample_speed, current_time, sample_time);
  sample = ResKal.first;
  sample_error = ResKal.second;
  current_time = sample_time;
  return this;
}

RangingFilterProcessing *RangingFilterProcessing::average() {
    sample = averageFilter.compute(sample);
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::offset() {
    sample = offsetFilter.compute(sample);
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::capVariation() {
    sample = capVariationFilter.compute(sample);
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::motion(bool motion_status_1, bool motion_status_2) {
    sample = motionFilter.compute(sample, motion_status_1, motion_status_2);
    return this;
}

RangingFilterProcessing *RangingFilterProcessing::lastCorrect() {
    sample = lastCorrectFilter.compute(sample);
    return this;
}


RangingFilter::RangingFilter() {
}

double RangingFilter::compute(double distance) {
    return distance;
}

void RangingFilter::reset() {
}

RangingFilterDqf::RangingFilterDqf() {
}

double RangingFilterDqf::compute(double distance) {
    return distance;
}

double RangingFilterDqf::compute(double distance, uint8_t dqf) {
    qDebug() << Q_FUNC_INFO << QString("compute(in) : RangingFilterDqf distance=%1 dqf=%2").arg(distance).arg(dqf);
    if((distance == -1) || (dqf == 0xff))
      return -1;
    int dqf_min = SimpleSettings::getInstance()->value(SETTING_FILTER_DQF_MIN, SETTING_FILTER_DQF_MIN_DEFAULT).toInt();
    if((dqf_min > -1) && (dqf < dqf_min))
      return -1;
    return distance;
}

RangingFilterBound::RangingFilterBound() {
}

double RangingFilterBound::compute(double distance) {
    qDebug() << Q_FUNC_INFO << QString("compute(in) : RangingFilterBound distance=%1").arg(distance);
    if(distance == -1)
        return -1;

    int min = SimpleSettings::getInstance()->value(SETTING_FILTER_DIST_MIN, SETTING_FILTER_DIST_MIN_DEFAULT).toInt();
    int max = SimpleSettings::getInstance()->value(SETTING_FILTER_DIST_MAX, SETTING_FILTER_DIST_MAX_DEFAULT).toInt();
    qDebug() << Q_FUNC_INFO << QString("RangingFilterBound min=%1 max=%2").arg(min).arg(max);

    if((min > -1) && (distance < min)) {
      qDebug() << Q_FUNC_INFO << QString("OUT : Case 1 : RangingFilterBound min=%1").arg(min);
      return min;
    }
    if((max > -1) && (distance > max)) {
      qDebug() << Q_FUNC_INFO << QString("OUT : Case 2 : RangingFilterBound max=%1").arg(max);
      return max;
    }
    qDebug() << Q_FUNC_INFO << QString("OUT : Case 3 : RangingFilterBound distance=%1").arg(distance);
    return distance;
}

RangingFilterOffset::RangingFilterOffset() {
    this->offset = 0;
}

void RangingFilterOffset::setOffset(double offset) {
    this->offset = offset;
}

double RangingFilterOffset::compute(double distance) {
    if(distance == -1)
        return -1;
    return (distance - offset);
}

void RangingFilterOffset::reset() {
    this->offset = 0;
}

//
// RangingFilterKalman
//

RangingFilterKalman::RangingFilterKalman() {
    initialised = false;
}

double RangingFilterKalman::compute(double measure) {
    qDebug() << Q_FUNC_INFO << QString("compute(in) : RangingFilterKalman measure=%1").arg(measure);
    if(!SimpleSettings::getInstance()->value(SETTING_FILTER_USE_KALMAN, SETTING_FILTER_USE_KALMAN_DEFAULT).toBool())
        return measure;

    if((measure == -1) && (!initialised))
        return -1;

    if(!initialised) {
        q = SimpleSettings::getInstance()->value(SETTING_FILTER_KALMAN_ALPHA, SETTING_FILTER_KALMAN_ALPHA_DEFAULT).toDouble();
        r = 4;
        p = 0.5;
        k = 0;
        x = measure;
        initialised = true;
        qWarning() << Q_FUNC_INFO << QString(" range kalman update (k,x,p,q,r): %1 %2 %3 %4 %5")
                      .arg(k).arg(x).arg(p).arg(q).arg(r);
    } else {
        if(measure == -1)
            measure = x; // todo we can do better
        p = p + q;
        k = p / (p + r);
        x = x + k * ( measure - x);
        p = (1 - k) * p;
        qWarning() << Q_FUNC_INFO << QString(" range kalman update (raw,k,x,p,q,r): %1 %2 %3 %4 %5 %6")
                      .arg(measure).arg(k).arg(x).arg(p).arg(q).arg(r);
    }
    qDebug() << Q_FUNC_INFO << QString("compute(out) : RangingFilterKalman measure=%1").arg(measure);
    return x;
}

bool RangingFilterKalman::isInitialised() {
    return this->initialised;
}

void RangingFilterKalman::reset() {
    this->initialised = false;
}

//
// RangingFilterKalmanVariable
//

RangingFilterKalmanVariable::RangingFilterKalmanVariable() {
    initialised = false;
}

double RangingFilterKalmanVariable::compute(double distance) {
  return distance;
}

std::pair<double,double> RangingFilterKalmanVariable::addMeasErr(double measure, double measure_error, double speed, unsigned long long time1, unsigned long long time2) {
    if(!SimpleSettings::getInstance()->value(SETTING_FILTER_USE_KALMAN, SETTING_FILTER_USE_KALMAN_DEFAULT).toBool())
      return {measure, measure_error};

    if((measure == -1) && (!initialised))
      return {-1, measure_error};

    r = measure_error * measure_error;
    double deltaX = 5;
    if(!initialised) {
      //        q = SimpleSettings::getInstance()->value(SETTING_FILTER_KALMAN_ALPHA, SETTING_FILTER_KALMAN_ALPHA_DEFAULT).toDouble();
        p = deltaX * deltaX;
        k = 0;
        x = measure;
        initialised = true;
        qWarning() << Q_FUNC_INFO << QString(" range kalman update (k,x,p,q,r): %1 %2 %3 %4 %5")
                      .arg(k).arg(x).arg(p).arg(q).arg(r);
    } else {
        if(measure == -1)
            measure = x; // todo we can do better
	deltaX += fabs(speed) * double(time2 - time1);
	q = deltaX * deltaX;
        p = p + q;
        k = p / (p + r);
        x = x + k * ( measure - x);
        p = (1 - k) * p;
    }
    qWarning() << Q_FUNC_INFO << QString(" range kalman update (raw,k,x,p,q,r): %1 %2 %3 %4 %5 %6")
      .arg(measure).arg(k).arg(x).arg(p).arg(q).arg(r);
    err_est = sqrt(p);
    return {x, err_est};
}

bool RangingFilterKalmanVariable::isInitialised() {
    return this->initialised;
}

void RangingFilterKalmanVariable::reset() {
    this->initialised = false;
}

//
// RangingFilterExponential
//

RangingFilterExponential::RangingFilterExponential() {
    initialised = false;
}

double RangingFilterExponential::compute(double measure) {
    if((measure == -1) && (!initialised))
        return -1;

    if(!initialised) {
        k = 0.8;
        x = measure;
        initialised = true;
        qWarning() << Q_FUNC_INFO << QString(" range kalman update (k,x): %1 %2 %3 %4 %5")
	  .arg(k).arg(x);
    } else {
        if(measure == -1)
            measure = x;
        x += k * ( measure - x);
        qWarning() << Q_FUNC_INFO << QString(" range kalman update (raw,k,x): %1 %2 %3")
	  .arg(measure).arg(k).arg(x);
    }
    return x;
}

bool RangingFilterExponential::isInitialised() {
    return this->initialised;
}

void RangingFilterExponential::reset() {
    this->initialised = false;
}

//
// RangingFilterAverage
//

RangingFilterAverage::RangingFilterAverage() {
    reset();
}

double RangingFilterAverage::compute(double distance) {
    if((distance == -1) && (samples == 0))
        return -1;

    if(distance != -1) {
        sum += distance;
        samples++;
    }
    return (sum/samples);
}

void RangingFilterAverage::reset() {
    sum     = 0;
    samples = 0;
}

//
// RangingFilterCapVariation
//

RangingFilterCapVariation::RangingFilterCapVariation() {
    last = -1;
    refusedCounter = 0;
}

double RangingFilterCapVariation::compute(double distance) {
    qDebug() << Q_FUNC_INFO << QString("compute(in) : RangingFilterCapVariation distance=%1").arg(distance);
    if(distance == -1)
        return -1;

    double max = SimpleSettings::getInstance()->value(SETTING_FILTER_DIST_VARI_MAX, SETTING_FILTER_DIST_VARI_MAX_DEFAULT).toInt();
    if(max == -1)
        return distance;

    if(last == -1) {
        last = distance;
        refusedCounter = 0;
        return distance;
    }

    double variation = abs(distance - last);
    if(variation > (max*(refusedCounter+1))) {
        refusedCounter++;
        if( refusedCounter <= 3) {
            qWarning() << Q_FUNC_INFO << QString("distance variation too high [%1/3], discarded").arg(refusedCounter);
            return -1;
        }
        qWarning() << Q_FUNC_INFO << "distance variation too high [3/3] but accept anyway and reset counter";
        refusedCounter = 0;
    }
    last = distance;
    qDebug() << Q_FUNC_INFO << QString("compute(out) : RangingFilterCapVariation distance=%1").arg(distance);
    return distance;
}

void RangingFilterCapVariation::reset() {
    last = -1;
    refusedCounter = 0;
}

//
// RangingFilterCapVariationFromCalculatedDistance
//

RangingFilterCapVariationFromCalculatedDistance::RangingFilterCapVariationFromCalculatedDistance() {
    refusedCounter = 0;
}

double RangingFilterCapVariationFromCalculatedDistance::compute(double distance, double calculated) {
    if(distance == -1)
        return -1;

    double max = SimpleSettings::getInstance()->value(SETTING_FILTER_DIST_VARI_MAX_CALC, SETTING_FILTER_DIST_VARI_MAX_CALC_DEFAULT).toInt();
    if(max == -1)
        return distance;

    double variation = abs(distance - calculated);
    if(variation > (max*(refusedCounter+1))) {
        refusedCounter++;
        if( refusedCounter <= 3) {
            qWarning() << Q_FUNC_INFO << QString("distance variation too high [%1/3], discarded").arg(refusedCounter);
            return -1;
        }
        qWarning() << Q_FUNC_INFO << "distance variation too high [3/3] but accept anyway and reset counter";
        refusedCounter = 0;
    }
    return distance;
}

void RangingFilterCapVariationFromCalculatedDistance::reset() {
    refusedCounter = 0;
}

//
// RangingFilterMotion
//

RangingFilterMotion::RangingFilterMotion() {
}

double RangingFilterMotion::compute(double distance) {
    return distance;
}

double RangingFilterMotion::compute(double distance, bool motion_status_1, bool motion_status_2) {
    qDebug() << Q_FUNC_INFO << QString("compute(in) : RangingFilterMotion distance=%1").arg(distance);
    if(!SimpleSettings::getInstance()->value(SETTING_FILTER_DIST_MOTION, SETTING_FILTER_DIST_MOTION_DEFAULT).toBool())
        return distance;
    if(!motion_status_1 && !motion_status_2)
        return -1;
    return distance;
}

//
// RangingFilterLastCorrect
//

RangingFilterLastCorrect::RangingFilterLastCorrect() {
    last = -1;
}

double RangingFilterLastCorrect::compute(double distance) {
    if((distance == -1) && (last == -1))
        return -1;
    if(distance == -1)
        return last;
    last = distance;
    return distance;
}

void RangingFilterLastCorrect::reset() {
    last = -1;
}
