#ifndef SIMPLESETTINGS_H
#define SIMPLESETTINGS_H

#include <QString>
#include <QObject>
#include <QSettings>
#include <QVariant>
#include <stdint.h>

class SimpleSettings : public QObject
{
    Q_OBJECT

public:
    static SimpleSettings *getInstance();

    void     setConfigFile(const QString & filename);
    void     load(uint16_t id);

    void	 setValue(const QString & key, const QVariant & value);
    QVariant value(const QString & key, const QVariant & defaultValue = QVariant());
    void     printSettings();


private:
    static SimpleSettings* m_pInstance;
    SimpleSettings();

    QSettings*             m_pSettings;
    QString                m_sSettingsFile;

    void printqlist( const QString &key, const QList<QVariant> &list, QString &conf);

signals:

public slots:

};

#endif // SIMPLESETTINGS_H
