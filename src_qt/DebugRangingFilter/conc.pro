QT += widgets

HEADERS       = RangingFilter.h SimpleSettings.h
SOURCES       = RangingFilter.cpp SimpleSettings.cpp \
                main.cpp

CLOCAL = 
                
QMAKE_CXXFLAGS += -std=c++11 $(CLOCAL)

INCLUDEPATH += $(MPFR_INCDIR)
INCLUDEPATH += $(EIGEN_PATH)
INCLUDEPATH += $(IPOPT_INCDIR)

LIBS += $(GMP_CXX_LINK)
LIBS += $(MPFR_LINK)
LIBS += $(IPOPT_LINK)



# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/mainwindows/menus
INSTALLS += target
