/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>

#include "mainwindow.h"
#include "Temp_Chemical_Solution.h"

//! [0]
MainWindow::MainWindow()
{
    QWidget *widget = new QWidget;
    setCentralWidget(widget);
//! [0]

//! [1]
    QWidget *topFiller = new QWidget;
    topFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    infoLabel = new QLabel(tr("No reaction file nor concentrations loaded"));
    infoLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    infoLabel->setAlignment(Qt::AlignCenter);
    infoLabel->setWordWrap(true);
    //    infoLabel->setTextFormat(Qt::PlainText);
    infoLabel->setTextFormat(Qt::RichText);
    LabelSetText();

    QWidget *bottomFiller = new QWidget;
    bottomFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(5);
    layout->addWidget(topFiller);
    layout->addWidget(infoLabel);
    layout->addWidget(bottomFiller);
    widget->setLayout(layout);
//! [1]

//! [2]
    createActions();
    createMenus();

    QString message = tr("A context menu is available by right-clicking");
    statusBar()->showMessage(message);

    setWindowTitle(tr("Chemical Concentration"));
    setMinimumSize(160, 160);
    resize(480, 320);
}
//! [2]

void MainWindow::ErrorMessage(std::vector<std::string> const& TheList)
{
  std::string TotalStr;
  for (auto & eStr : TheList) {
    TotalStr=TotalStr + eStr;
  }
  QString TheStr=QString::fromStdString(TotalStr);
  QMessageBox::about(this, tr("Error Message"), TheStr);
}

void MainWindow::LabelSetText()
{
  QString eStr1, eStr2, eStr3;
  if (HaveReact == false) {
    eStr1="        Have Reaction file = <b>false</b>";
  }
  else {
    QString fStr1, fStr2, fStr3;
    fStr1="        Have Reaction file = <b>true</b>";
    int nbReact=eDesc.ListChemicalReact.size();
    int nbChem=eDesc.ListNature.size();
    fStr2="           number reaction = <b>" + QString::number(nbReact) + "</b>";
    fStr3=" number chemical reactants = <b>" + QString::number(nbChem) + "</b>";
    eStr1=fStr1 + "<br>" + fStr2 + "<br>" + fStr3;
  }
  if (HaveInitConc == false) {
    eStr2="Have Initial concentration file = <b>false</b>";
  }
  else {
    eStr2="Have Initial concentration file = <b>true</b>";
  }
  if (HaveFinalConc == false) {
    eStr3="        Have Final concentration = <b>false</b>";
  }
  else {
    eStr3="        Have Final concentration = <b>true</b>";
  }
  infoLabel->setText(eStr1 + "<hr>" + eStr2 + "<hr>" + eStr3);
  //  infoLabel->setText(tr(eStr1 + "\n" + eStr2 + "\n" + eStr3));
}


void MainWindow::LoadReactionFile()
{
  try {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Reaction File"),QDir::homePath());
    std::string filename_react=fileName.toStdString();
    std::ifstream STreact;
    STreact.open(filename_react.c_str() );
    if (STreact.is_open() == false) {
      std::vector<std::string> eErr{"File cannot be opened"};
      throw eErr;
    }
    eDesc=ReadChemicalSolutionFile<double>(STreact);
    STreact.close();
    HaveReact=true;
    HaveInitConc=false;
    HaveFinalConc=false;
  }
  catch (std::vector<std::string> & e) {
    ErrorMessage(e);
    HaveReact=false;
  }
  LabelSetText();
}

void MainWindow::LoadInitialConcentrationFile()
{
  try {
    if (HaveReact == false) {
      std::vector<std::string> LStr{"Load reaction file first"};
      throw LStr;
    }
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Initial Concentration File"),QDir::homePath());
    std::string filename_init_conc=fileName.toStdString();
    std::ifstream STinit;
    STinit.open(filename_init_conc.c_str() );
    if (STinit.is_open() == false) {
      std::vector<std::string> eErr{"File cannot be opened"};
      throw eErr;
    }
    ListInitConc=ReadConcentrationFile<double>(eDesc, STinit);
    STinit.close();
    HaveInitConc=true;
    HaveFinalConc=false;
  }
  catch (std::vector<std::string> & e) {
    ErrorMessage(e);
    HaveInitConc=false;
  }
  LabelSetText();
}

void MainWindow::SaveFinalConcentration()
{
  try {
    if (HaveFinalConc == false) {
      std::vector<std::string> LStr{"Run the solver first"};
      throw LStr;
    }
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
						    "untitled.csv",
						    tr("Comma Separated File (*.csv)"));
    std::string filename_final_conc=fileName.toStdString();
    std::ofstream STfinal;
    STfinal.open(filename_final_conc.c_str() );
    PrintFinalState(eDesc, ListInitConc, ListFinalConc, STfinal);
    STfinal.close();
  }
  catch (std::vector<std::string> & e) {
    ErrorMessage(e);
    HaveInitConc=false;
  }
  LabelSetText();
}

void MainWindow::WriteFinalCompleteReport()
{
  try {
    if (HaveFinalConc == false) {
      std::vector<std::string> LStr{"Run the solver first"};
      throw LStr;
    }
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
						    "untitled.txt",
						    tr("Text File (*.txt)"));
    std::string filename_final_conc=fileName.toStdString();
    std::ofstream STfinal;
    STfinal.open(filename_final_conc.c_str() );
    PrintFinalState(eDesc, ListInitConc, ListFinalConc, STfinal);
    STfinal.close();
  }
  catch (std::vector<std::string> & e) {
    ErrorMessage(e);
    HaveInitConc=false;
  }
  LabelSetText();
}









void MainWindow::SetSolverParameter()
{
  LabelSetText();
}

void MainWindow::RunSolver()
{
  try {
    if (HaveReact == false || HaveInitConc == false) {
      std::vector<std::string> LStr{"Load reaction file and initial filefirst"};
      throw LStr;
    }
    double TheTol=1e-30;
    ListFinalConc=SolveSolutionSystem<double>(eDesc, ListInitConc, TheTol);
    HaveFinalConc=true;
  }
  catch (std::vector<std::string> & e) {
    ErrorMessage(e);
    HaveFinalConc=false;
  }
  LabelSetText();
}

void MainWindow::about()
{
  LabelSetText();
  QMessageBox::about(this, tr("About Menu"),
		     tr("The program serves to solve concentration computations "));
}

//! [4]
void MainWindow::createActions()
{
//! [5]
  loadReactFileAct = new QAction(tr("&Load Reaction File"), this);
  loadReactFileAct->setShortcuts(QKeySequence::New);
  loadReactFileAct->setStatusTip(tr("Load components and concentrations"));
  connect(loadReactFileAct, SIGNAL(triggered()), this, SLOT(LoadReactionFile()));
//! [4]

  loadInitConcFileAct = new QAction(tr("&Load Initial Concentration File"), this);
  loadInitConcFileAct->setShortcuts(QKeySequence::Open);
  loadInitConcFileAct->setStatusTip(tr("Load an initial concentration file"));
  connect(loadInitConcFileAct, SIGNAL(triggered()), this, SLOT(LoadInitialConcentrationFile()));
//! [5]

  saveFinalConcFileAct = new QAction(tr("&Save Final Concentration"), this);
  saveFinalConcFileAct->setShortcuts(QKeySequence::Save);
  saveFinalConcFileAct->setStatusTip(tr("Save the document to disk"));
  connect(saveFinalConcFileAct, SIGNAL(triggered()), this, SLOT(SaveFinalConcentration()));

  exitAct = new QAction(tr("E&xit"), this);
  exitAct->setShortcuts(QKeySequence::Quit);
  exitAct->setStatusTip(tr("Exit the application"));
  connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));
  
  setSolverAct = new QAction(tr("&Set Solver Parameter"), this);
  setSolverAct->setShortcuts(QKeySequence::Undo);
  setSolverAct->setStatusTip(tr("Undo the last operation"));
  connect(setSolverAct, SIGNAL(triggered()), this, SLOT(SetSolver()));
  
  runSolverAct = new QAction(tr("&Run Solver"), this);
  runSolverAct->setShortcuts(QKeySequence::Redo);
  runSolverAct->setStatusTip(tr("Redo the last operation"));
  connect(runSolverAct, SIGNAL(triggered()), this, SLOT(RunSolver()));
  
  aboutAct = new QAction(tr("&About"), this);
  aboutAct->setStatusTip(tr("Show the application's About box"));
  connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
}
//! [7]

//! [8]
void MainWindow::createMenus()
{
//! [9] //! [10]
  fileMenu = menuBar()->addMenu(tr("&File"));
  fileMenu->addAction(loadReactFileAct);
//! [9]
  fileMenu->addAction(loadInitConcFileAct);
//! [10]
  fileMenu->addAction(saveFinalConcFileAct);
//! [11]
  fileMenu->addSeparator();
//! [11]
  fileMenu->addAction(exitAct);

  solverMenu = menuBar()->addMenu(tr("&Solver"));
  solverMenu->addAction(setSolverAct);
  solverMenu->addAction(runSolverAct);
  
  helpMenu = menuBar()->addMenu(tr("&Help"));
  helpMenu->addAction(aboutAct);
}
//! [12]
