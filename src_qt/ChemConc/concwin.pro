QT += widgets

HEADERS       = mainwindow.h
SOURCES       = mainwindow.cpp \
                main.cpp

CLOCAL = -I../src -I../src_basic -I../src_chem
                
QMAKE_CXXFLAGS += -std=c++11 $(CLOCAL) -DWINDOWS

INCLUDEPATH += $(WINDOWS_GMP_INCDIR)
INCLUDEPATH += $(WINDOWS_MPFR_INCDIR)
INCLUDEPATH += $(WINDOWS_EIGEN_PATH)
INCLUDEPATH += $(WINDOWS_IPOPT_INCDIR)

LIBS += $(WINDOWS_GMP_CXX_LINK)
LIBS += $(WINDOWS_MPFR_LINK)
LIBS += $(WINDOWS_IPOPT_LINK)



