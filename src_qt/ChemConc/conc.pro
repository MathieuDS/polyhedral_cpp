QT += widgets

HEADERS       = mainwindow.h
SOURCES       = mainwindow.cpp \
                main.cpp

CLOCAL = -I../src -I../src_basic -I../src_chem -I../src_number
                
QMAKE_CXXFLAGS += -std=c++11 $(CLOCAL)

INCLUDEPATH += $(MPFR_INCDIR)
INCLUDEPATH += $(EIGEN_PATH)
INCLUDEPATH += $(IPOPT_INCDIR)

LIBS += $(GMP_CXX_LINK)
LIBS += $(MPFR_LINK)
LIBS += $(IPOPT_LINK)



# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/mainwindows/menus
INSTALLS += target
