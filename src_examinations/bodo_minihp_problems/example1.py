import numba
from minihp_initial import (gen_range32, gen_rand, gen_rand_int32, arr_sum,
    simple_join, MiniHPTestPipeline)

def example1():
    n = 10
    k1 = gen_rand_int32(6, n)
    k2 = gen_rand_int32(6, n)
    print("keys1:")
    print(k1)
    print("keys2:")
    print(k2)
    d1 = gen_rand(n)
    d2 = gen_range32(n)
    o1, o2 = simple_join(k1, k2, d1, d2)
    print("out1:")
    print(o1)
    print("out2:")
    print(o2)
    return


numba.jit(pipeline_class=MiniHPTestPipeline)(example1)()

## mpiexec --outfile-pattern=output%r.log -n 2 python -u example1.py
