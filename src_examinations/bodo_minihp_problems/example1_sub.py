import numba
from minihp_initial import (gen_range32, gen_rand, gen_rand_int32, arr_sum,
    simple_join, MiniHPTestPipeline)


# ISSUES:
# ---We can test only simple routine.
# STRUCTURE:
# ---The block contains the code (in "body")
#    The "loc"
# ---Types for the Instructions: Inst, Stmt(Inst), Expr(Inst), Yield(Inst)
#    many classes depending on Stmt:
#    ---"print(...)" statement correspond to "class Print(...)"
#


def example2(m):
    print("m:")
    print(m)
    return


def example1(m):
    n = 10
    k1 = gen_rand_int32(6,n)
    k1 = gen_rand_int32(6,n)
    k2 = gen_rand_int32(6,n)
    print("keys1:")
    print(k1)
    print("keys2:")
    print(k2)
    if n>255:
        aval = 257
    else:
        aval = 259
    d1 = gen_rand(n)
    d2 = gen_range32(n)
    o1, o2 = simple_join(k1, k2, d1, d2)
    print("out1:")
    print(o1)
    print("out2:")
    print(o2)
    return


numba.jit(pipeline_class=MiniHPTestPipeline)(example1)(100)

## mpiexec --outfile-pattern=output%r.log -n 2 python -u example1.py
