import numba
from minihp_initial_orig import (gen_range32, gen_rand, gen_rand_int32, arr_sum,
    simple_join, MiniHPTestPipeline)


n = 4
k1 = gen_rand_int32(6, n)
k2 = gen_rand_int32(6, n)
print("keys1:")
print(k1)
print("keys2:")
print(k2)
d1 = gen_rand(n)
d2 = gen_range32(n)
d3 = gen_range32(n)[2]
o1, o2, o3 = simple_join(k1, k2, d1, d2, d3)
print("out1:")
print(o1)
print("out2:")
print(o2)
print("out3:")
print(o3)
