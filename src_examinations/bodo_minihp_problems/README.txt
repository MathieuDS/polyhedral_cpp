Bodo MiniHP Compiler Problems
-----------------------------

MiniHP is a simple compiler infrastructure designed to simulate parts of Bodo's analytics engine.
It takes a Python function, compiles it to C++ and executes it.
Only a few operations (defined below) are supported.
MiniHP uses Numba for initial translation steps and MPI for parallelization.
Arrays are divided to equal chunks among processors (MPI ranks) automatically.


Installation
------------

Install Anaconda if not already installed. You can use Miniconda for faster installation:

On Linux:
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
chmod +x miniconda.sh
./miniconda.sh
export PATH=$HOME/miniconda3/bin:$PATH

On Mac:
Install Miniconda from here: https://conda.io/en/latest/miniconda.html

Create a new Conda environment and activate it:

conda create -n MHP -c defaults -c conda-forge numba mpi4py
conda activate MHP

Install compilers:
On Linux:
conda install gcc_linux-64 gxx_linux-64

On Mac:
conda install clang_osx-64 clangxx_osx-64

Try a simple example:

python example0.py
mpiexec -n 2 python example0.py


Background
----------

Single Program Multiple Data (SPMD) parallelism paradigm:
The same program runs on all processors but on different chunks of data.
It is different than client/server and master/executor paradigms in that
all processors are usually "equal" and can communicate with each other.
https://www.coursera.org/lecture/distributed-programming-in-java/3-1-single-program-multiple-data-spmd-model-wod4J

MPI tutorials:
Basic communication: https://mpitutorial.com/tutorials/mpi-send-and-receive/
Collective communication: https://mpitutorial.com/tutorials/mpi-broadcast-and-collective-communication/
Alltoall (shuffle): https://computing.llnl.gov/tutorials/mpi/images/MPI_Alltoall.gif
https://computing.llnl.gov/tutorials/mpi/man/MPI_Alltoallv.txt
https://www.eecis.udel.edu/~pollock/372/f06/alltoallv.ppt
https://stackoverflow.com/questions/15049190/difference-between-mpi-allgather-and-mpi-alltoall-functions


Parallel Join:
https://15721.courses.cs.cmu.edu/spring2017/slides/18-hashjoins.pdf


Liveness analysis:
https://suif.stanford.edu/~courses/cs243/lectures/l2.pdf
https://www.seas.harvard.edu/courses/cs252/2011sp/slides/Lec02-Dataflow.pdf


Simple Parallel Join Problem
----------------------------

The goal is to generate parallel C++ code for "simple_join()" function
(see sequential python implementation in minihp_initial.py).
It takes key arrays (int32 keys) for the left and right tables, and data arrays for
the right table as input. Left table doesn't have data arrays for simplicity.
All of this data is distributed (each process has a chunk of global data for each array).
It returns the data arrays of the joined table (keys are ignored for simplicity).
Output arrays are distributed as well, but chunk sizes may not be equal among processors.
Order of output data doesn't matter.

For example, let's assume we have two MPI processes and right table has one data array:

Process 0:
left_keys = [0, 1, 1]
right keys = [1, 0, 4]
data = [1, 2, 3]
out_data = [2, 2, 4]

Process 1:
left_keys = [2, 1, 0]
right keys = [2, 5, 3]
data = [4, 5, 6]
out_data = [1, 1, 1]

For implementation, use simple hash partitioning (key % num_processes)
to shuffle the data, and then perform local join. Use of STL for simpler code is encouraged.
Add implementation in _gen_join() inside minihp_initial.py and try the examples.


Liveness Analysis Problem
-------------------------

Implement liveness analysis for MiniHP (fill compute_lives function in minihp_initial.py).
It should provide live variables at the entrance of each basic block.
Optimizations like using bitvector for variables and techniques for faster convergence are not
necessary.



MiniHP Operations
-----------------

gen_range32(n): generates an array filled with int32 numbers 1 to n-1
gen_rand(n): generates an array filled random float64 numbers
gen_rand_int32(h, n): generates an array filled random int32 numbers in range [0, h)
arr_sum(A): sums values of an array
simple_join(keys1, keys2, *data_arrs): joins two tables, assuming that the
    left table only has keys and no data. data_arrs is a variable number of
    data arrays for the right table. Returns joined table's data as tuple of arrays.


IR Definition
-------------

Here are the relevant IR nodes and their attributes for MiniHP.

ir.FunctionIR: IR of a function
    blocks: dictionary mapping block lables to ir.Block values

ir.Block: a basic block
    body: list of statements (assignment, print, jump, branch)

ir.Jump: jump to another basic block
    target: label of other basic block

ir.Branch: jump to block based on condition
    cond: condition variable
    truebr: label of target block if cond is true
    falsebr: label of target block if cond is false

ir.Assign: an assignment
    target: target variable (left-hand side) of assignment
    value: value to assign, could be ir.Var, ir.Const, or ir.Expr

ir.Expr: an expression
    op: operation of Expr, determines its other attributes
    op == call: a function call
        func: function call variable
        args: arguments (tuple of variables)
    op == binop: a binary operation
        fn: function to apply, such as operator.add for "+"
        lhs: left argument variable
        rhs: right argument variable
    op == exhaust_iter: exhaust values of iterator (can treat it as simple assignment here)
        value: variable of value to exhaust
    op == static_getitem: get item from tuple
        value: variable of tuple value
        index: constant integer value for index

ir.Print: print node
    args: arguments to be printed. Tuple of variables.

ir.Const: a constant value
    value: constant value

ir.Global: a global value
    value: constant value

ir.Var: a variable
    name: name of variable (use only variable name for comparison or keeping in data structures)

Feel free to ignore "cast" and ir.Return nodes since we don't return values in MiniHP.
