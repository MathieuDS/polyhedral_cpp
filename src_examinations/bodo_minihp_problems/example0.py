import numba
from minihp_initial import (gen_range32, gen_rand, gen_rand_int32, arr_sum,
    simple_join, MiniHPTestPipeline)


def example0():
    n = 10
    k1 = gen_range32(n)
    s = arr_sum(k1)
    print(s)
    return

numba.jit(pipeline_class=MiniHPTestPipeline)(example0)()
