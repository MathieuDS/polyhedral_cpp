import os
import operator
import ctypes
import numpy as np
from mpi4py import MPI
import numba

from numba import types, ir
from numba.ir_utils import guard, find_callname, compute_cfg_from_blocks
from numba.typing import signature
from numba.typing.templates import infer_global, AbstractTemplate
np.random.seed(0)


###### function definitions ######


def gen_range32(n):
    """generates an array filled with int32 numbers 1 to n-1
    """
    return np.arange(n, dtype=np.int32)


def gen_rand(n):
    """generates an array filled random float64 numbers
    """
    return np.random.ranf(n)


def gen_rand_int32(h, n):
    """generates an array filled random int32 numbers
    """
    return np.random.randint(0, h, n, dtype=np.int32)


def arr_sum(A):
    """sums values of an array
    """
    return A.sum()


def simple_join(keys1, keys2, *data_arrs):
    """joins two tables, assuming that the left table only has keys and no
    data. data_arrs is a variable number of data arrays for the right table.
    """
    l1 = len(keys1)
    l2 = len(keys2)
    j_table = set(keys1)
    print("j_table=", j_table)
    
    out_len = 0
    for i in range(l2):
        if keys2[i] in j_table:
            out_len += 1
    print("out_len=", out_len);

    out_data_arrs = tuple(np.empty(out_len, d.dtype) for d in data_arrs)
    print("out_data_arrs=", out_data_arrs);

    out_ind = 0
    for i in range(l2):
        k = keys2[i]
        if k in j_table:
            for out_data, data in zip(out_data_arrs, data_arrs):
                out_data[out_ind] = data[i]
            out_ind += 1

    return out_data_arrs


############ typers #############


def unliteral_all(args):
    return tuple(types.unliteral(a) for a in args)


@infer_global(gen_range32)
class ArangeTyper(AbstractTemplate):
    def generic(self, args, kws):
        assert not kws
        assert len(args) == 1
        ret_typ = types.Array(types.int32, 1, 'C')
        return signature(ret_typ, *unliteral_all(args))


@infer_global(gen_rand)
class RandTyper(AbstractTemplate):
    def generic(self, args, kws):
        assert not kws
        assert len(args) == 1
        ret_typ = types.Array(types.float64, 1, 'C')
        return signature(ret_typ, *unliteral_all(args))


@infer_global(gen_rand_int32)
class Rand32Typer(AbstractTemplate):
    def generic(self, args, kws):
        assert not kws
        assert len(args) == 2
        ret_typ = types.Array(types.int32, 1, 'C')
        return signature(ret_typ, *unliteral_all(args))


@infer_global(arr_sum)
class SumTyper(AbstractTemplate):
    def generic(self, args, kws):
        assert not kws
        assert len(args) == 1
        dtype = args[0].dtype
        return signature(dtype, *unliteral_all(args))


@infer_global(simple_join)
class JoinTyper(AbstractTemplate):
    def generic(self, args, kws):
        assert not kws
        data_args = args[2:]
        ret_typ = types.Tuple(data_args)
        return signature(ret_typ, *unliteral_all(args))


############# minihp compiler pipeline ###############


class MiniHPTestPipeline(numba.compiler.BasePipeline):
    """compiler pipeline for MiniHP
    """
    def define_pipelines(self, pm):
        name = 'minihp_test'
        pm.create_pipeline(name)
        self.add_preprocessing_stage(pm)
        # self.add_pre_typing_stage(pm)
        pm.add_stage(self.stage_generic_rewrites, "nopython rewrites")
        self.add_typing_stage(pm)
        pm.add_stage(self.stage_minihp, "mini hp")
        self.add_lowering_stage(pm)
        self.add_cleanup_stage(pm)


    def stage_minihp(self):
        """minihp compiler pipeline after Numba's type inference.
        generates cpp code and executes the function
        """
        # ignore del nodes
        numba.ir_utils.remove_dels(self.func_ir.blocks)
        # self.func_ir.dump()
        # lives = self.compute_lives(self.func_ir.blocks)
        # print(lives)
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        fname = self.func_id.func_name
        # generate CPP code only on rank 0 (assuming single node)
        if rank == 0:
            # get CPP code
            code = self.lower_cpp()
            # compile CPP
            os.system("echo '{}' > tmp{}.cpp".format(code, fname))
            os.system(("$CXX -shared -fPIC -std=c++11 -I$CONDA_PREFIX/include -o"
                " libtmp{}.so tmp{}.cpp -L$CONDA_PREFIX/lib -lmpi").format(
                fname, fname))
        comm.barrier()
        libname = "./libtmp{}.so".format(fname)
        libtmp = ctypes.cdll.LoadLibrary(libname)
        # run user function
        libtmp.user_func()
        comm.barrier()
        if rank == 0:
            # cleanup tmp files
            if os.path.exists(libname):
                os.remove(libname)
            if os.path.exists("tmp{}.cpp".format(fname)):
                os.remove("tmp{}.cpp".format(fname))
        comm.barrier()
        # replace ir with dummy code to avoid Numba errors
        dummy_ir = numba.ir_utils.compile_to_numba_ir(lambda:None, {},
            self.typingctx, (), self.typemap, self.calltypes)
        self.func_ir.blocks = dummy_ir.blocks

    def compute_lives(self, blocks):
        cfg = compute_cfg_from_blocks(blocks)
        # TODO: implement liveness here
        # cfg.successors() & cfg.predecessors() are available

    def lower_cpp(self):
        code = self._gen_cpp_init()  # includes and global functions

        # generate join calls by finding their types from calltypes
        for call, typ in self.calltypes.items():
            if (isinstance(call, ir.Expr) and call.op == 'call' and
                    self.typemap[call.func.name].typing_key.__name__
                        == 'simple_join'):
                code += self._gen_join(call.func.name, typ.args)

        code += "\nextern \"C\" void user_func()\n{\n"
        # code += "   MPI_Init(NULL, NULL);\n"
        code += "   MPI_Comm_size(MPI_COMM_WORLD, &n_pes);\n"
        code += "   MPI_Comm_rank(MPI_COMM_WORLD, &rank);\n"

        # declare types of variables
        for vname, typ in self.typemap.items():
            ctyp = _to_c_typ(typ)
            if ctyp is None:  # ignore variables like functions
                continue
            code += "   {} {};\n".format(
                ctyp, _sanitize_varname(vname))

        code += self._gen_cpp_body()
        # code += "   MPI_Finalize();\n}\n"
        code += "}\n"
        # print(code)
        return code

    def _gen_cpp_body(self):
        code = ""
        for label, block in self.func_ir.blocks.items():
            code += "L{}:\n".format(label)
            for stmt in block.body:
                if isinstance(stmt, ir.Assign):
                    expr_str = None
                    rhs = stmt.value
                    if isinstance(rhs, ir.Var):
                        expr_str = _sanitize_varname(rhs.name)
                    elif isinstance(rhs, ir.Const):
                        if rhs.value is None or isinstance(rhs.value, str):
                            continue
                        expr_str = str(rhs.value)
                    elif isinstance(rhs, ir.Expr):
                        expr_str = self._gen_expr(rhs)

                    if expr_str is not None:
                        code += "   {} = {};\n".format(
                            _sanitize_varname(stmt.target.name), expr_str)
                elif isinstance(stmt, ir.Print):
                    assert len(stmt.args) == 1, "print() supports 1 arg only"
                    arg_typ = self.typemap[stmt.args[0].name]
                    vname = _sanitize_varname(stmt.args[0].name)
                    if isinstance(arg_typ, types.Array):
                        code += "   for (auto v: {})\n".format(vname)
                        code += "      std::cout << v << \" \";\n"
                        code += "   std::cout<< std::endl;\n"
                    elif isinstance(arg_typ, types.StringLiteral):
                        code += "   std::cout<< \"{}\" << std::endl;\n".format(
                        arg_typ._literal_value)
                    else:
                        code += "   std::cout<< {} << std::endl;\n".format(
                            vname)
                elif isinstance(stmt, ir.Jump):
                    code += "  goto L{};\n".format(stmt.target)
                elif isinstance(stmt, ir.Branch):
                    cond = _sanitize_varname(stmt.cond.name)
                    code += "  if ({}) goto L{}; else goto L{};\n".format(
                        cond, stmt.truebr, stmt.falsebr)
        return code

    def _gen_expr(self, rhs):
        expr_str = None
        if rhs.op == 'call':
            func = guard(find_callname, self.func_ir, rhs)
            assert func is not None
            if func[0] == 'gen_rand':
                assert len(rhs.args) == 1
                expr_str = self._lower_rand(rhs.args[0].name)
            elif func[0] == 'gen_rand_int32':
                assert len(rhs.args) == 2
                expr_str = self._lower_rand_int32(
                    rhs.args[0].name, rhs.args[1].name)
            elif func[0] == 'arr_sum':
                assert len(rhs.args) == 1
                expr_str = self._lower_sum(rhs.args[0].name)
            elif func[0] == 'gen_range32':
                assert len(rhs.args) == 1
                expr_str = self._lower_range32(
                    rhs.args[0].name)
            else:
                func_name = _sanitize_varname(rhs.func.name)
                expr_str = "{}({})".format(func_name,
                    ", ".join(_sanitize_varname(v.name) for v in rhs.args))
        elif rhs.op == 'exhaust_iter':
            expr_str = _sanitize_varname(rhs.value.name)
        elif rhs.op == 'static_getitem':
            val_typ = self.typemap[rhs.value.name]
            val_name = _sanitize_varname(rhs.value.name)
            if isinstance(val_typ, types.Tuple):
                expr_str = "std::get<{}>({})".format(
                    rhs.index,
                    val_name)
            elif isinstance(val_typ, types.Array):
                expr_str = "{}[{}]".format(val_name, rhs.index)
        elif rhs.op == 'binop':
            arg0 = _sanitize_varname(rhs.lhs.name)
            arg1 = _sanitize_varname(rhs.rhs.name)
            op_map = {operator.add: '+', operator.lt: '<', operator.gt: '>'}
            expr_str = "({} {} {})".format(arg0, op_map[rhs.fn], arg1)

        return expr_str

    def _gen_cpp_init(self):
        return ("#include <iostream>\n#include <vector>\n#include <random>\n"
                "#include <algorithm>\n"
                "#include <unordered_set>\n"
                "#include \"mpi.h\"\n"
                "int rank;\n"
                "int n_pes;\n"
                "static int64_t get_start(int64_t total, int num_pes, int node_id)\n"
                "{\n"
                    "int64_t div_chunk = (int64_t)ceil(total/((double)num_pes));\n"
                    "int64_t start = std::min(total, node_id*div_chunk);\n"
                    "return start;\n"
                "}\n"
                "static int64_t get_end(int64_t total, int num_pes, int node_id)\n"
                "{\n"
                "    int64_t div_chunk = (int64_t)ceil(total/((double)num_pes));\n"
                "    int64_t end = std::min(total, (node_id+1)*div_chunk);\n"
                "    return end;\n"
                "}\n"
                "static int64_t get_node_portion(int64_t total, int num_pes, int node_id)\n"
                "{\n"
                    "return get_end(total, num_pes, node_id) -\n"
                    "    get_start(total, num_pes, node_id);\n"
                "}\n"
                "std::vector<double> _gen_random(int64_t size)"
                "{\n"
                "   size = get_node_portion(size, n_pes, rank);\n"
                "   std::random_device rd;\n"
                "   std::default_random_engine r_engine(rd());\n"
                "   std::uniform_real_distribution<> r_dist(0, 1);\n"
                "   std::vector<double> data(size);\n"
                "   std::generate(data.begin(), data.end(), "
                "[&r_engine,&r_dist]() { return r_dist(r_engine); });\n"
                "   return data;\n"
                "}\n"
                "std::vector<int> _gen_random_int32(int64_t high, int64_t size)"
                "{\n"
                "   size = get_node_portion(size, n_pes, rank);\n"
                "   std::random_device rd;\n"
                "   std::default_random_engine r_engine(rd());\n"
                "   std::uniform_int_distribution<> r_dist(0, high);\n"
                "   std::vector<int> data(size);\n"
                "   std::generate(data.begin(), data.end(), "
                "[&r_engine,&r_dist]() { return r_dist(r_engine); });\n"
                "   return data;\n"
                "}\n"
                "std::vector<int> _gen_range32(int64_t size)"
                "{\n"
                "   int64_t start = get_start(size, n_pes, rank);\n"
                "   int64_t end = get_end(size, n_pes, rank);\n"
                "   size = get_node_portion(size, n_pes, rank);\n"
                "   std::vector<int> data(size);\n"
                "   for(uint64_t i=start; i<end; i++)\n"
                "      data[i-start] = static_cast<int>(i);\n"
                "   return data;\n"
                "}\n"
                "int allreduce_scalar(int val)\n"
                "{\n"
                "  int ret;\n"
                "  MPI_Allreduce(&val, &ret, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);\n"
                "  return ret;\n"
                "}\n"
                "int64_t allreduce_scalar(int64_t val)\n"
                "{\n"
                "  int64_t ret;\n"
                "  MPI_Allreduce(&val, &ret, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);\n"
                "  return ret;\n"
                "}\n"
                "double allreduce_scalar(double val)\n"
                "{\n"
                "  double ret;\n"
                "  MPI_Allreduce(&val, &ret, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);\n"
                "  return ret;\n"
                "}\n"
                )

    def _lower_rand(self, argname):
        cvar = _sanitize_varname(argname)
        return "_gen_random({})".format(cvar)

    def _lower_rand_int32(self, hname, argname):
        hname = _sanitize_varname(hname)
        cvar = _sanitize_varname(argname)
        return "_gen_random_int32({}, {})".format(hname, cvar)

    def _lower_sum(self, argname):
        cvar = _sanitize_varname(argname)
        zero = self.typemap[argname].dtype(0)
        return "allreduce_scalar(std::accumulate({}.begin(), {}.end(), {}))".format(
            cvar, cvar, zero)

    def _lower_range32(self, argname):
        cvar = _sanitize_varname(argname)
        return "_gen_range32({})".format(cvar)

    def _lower_join(self, func_name, args):
        func_name = _sanitize_varname(func_name)
        return "{}({})".format(func_name,
            ", ".join(_sanitize_varname(v.name) for v in args))

    def _gen_join(self, func_name, arg_typs):
        func_name = _sanitize_varname(func_name)
        assert len(arg_typs) >= 2
        data_typs = arg_typs[2:]

        # return out_data_arrs
        key_args = "std::vector<int> keys1, std::vector<int> keys2, "
        data_args = ", ".join(
            _to_c_typ(t) + " data{}".format(i) for i,t in enumerate(data_typs))
        ret_typ = "std::tuple<{}>".format(
            ", ".join(_to_c_typ(t) for t in data_typs))
        args = key_args + data_args
        code = "{} {}({})\n{{\n".format(ret_typ, func_name, args)
        code += "   {} out_data_arrs;\n".format(ret_typ)

        # Code example:
        # std::tuple<int,double,float> func_name(std::vector<int> keys1, std::vector<int> key2,
        # int data0, double data1, float data2)
        # {
        
        # TODO: add C++ implementation to "code" here
        # _to_c_typ() converts python type to C++ type
        # _to_mpi_typ() converts python type MPI type, only for scalars types.
        # use t.dtype to get scalar data type for array types.

        return code


def _to_c_typ(typ):
    typ = types.unliteral(typ)
    if isinstance(typ, types.Array):
        return "std::vector<{}>".format(_to_c_typ(typ.dtype))
    elif typ == types.int32:
        return "int"
    elif typ == types.int64:
        return "int64_t"
    elif typ == types.float64:
        return "double"
    elif typ == types.bool_:
        return "bool"
    elif isinstance(typ, types.Function):
        pass
    elif typ == types.none:
        pass
    elif isinstance(typ, types.Tuple):
        dtypes = ",".join(_to_c_typ(t) for t in typ.types)
        return "std::tuple<{}>".format(dtypes)
    elif typ == types.unicode_type:
        pass
    else:
        raise ValueError("Unsupported type {}".format(typ))


def _to_mpi_typ(typ):
    if typ == types.int32:
        return "MPI_INT"
    elif typ == types.int64:
        return "MPI_LONG_LONG_INT"
    elif typ == types.float64:
        return "MPI_DOUBLE"
    else:
        raise ValueError("Unsupported mpi type {}".format(typ))


def _sanitize_varname(varname):
    return varname.replace('$', '_').replace('.', '_')
