#ifndef SHORTEST_PATH_CODE_H
#define SHORTEST_PATH_CODE_H

#include <vector>
#include <set>
#include <map>
#include <utility>
#include <string>

#include <bitset>
#include <boost/dynamic_bitset.hpp>
using Face=boost::dynamic_bitset<>;

using Vertex=int;
using Label=std::string;


struct ClassVertexLabel {
public:
  ClassVertexLabel() {
    nbVertex=0;
    nbLabel=0;
  }
  void CreateVertex(Vertex const& a)
  {
#ifdef SAFE_RUN
    auto iter=VertexToIdx.find(a);
    if (iter != VertexToIdx.end()) {
      std::cerr << "Vertex was already created\n";
      std::cerr << "Wrong function call\n";
      exit(1);
    }
#endif
    VertexToIdx[a] = nbVertex;
    //    std::cerr << "a=" << a << " matched to " << nbVertex << "\n";
    VertexFromIdx.push_back(a);
    ListLabelByVertex.push_back({});
    ListAdjacentTo.push_back({});
    ListAdjacentFrom.push_back({});
    nbVertex++;
  }
  void CreateEdge(Vertex const& a, Vertex const& b)
  {
#ifdef SAFE_RUN
    auto iter1=VertexToIdx.find(a);
    if (iter1 == VertexToIdx.end()) {
      std::cerr << "Vertex a=" << a << " is missing\n";
      std::cerr << "Wrong function call\n";
      exit(1);
    }
    auto iter2=VertexToIdx.find(b);
    if (iter2 == VertexToIdx.end()) {
      std::cerr << "Vertex b=" << b << " is missing\n";
      std::cerr << "Wrong function call\n";
      exit(1);
    }
    int a_idx_test = VertexToIdx.at(a);
    int b_idx_test = VertexToIdx.at(b);
    for (auto & eVal : ListAdjacentTo[a_idx_test])
      if (eVal == b_idx_test) {
	std::cerr << "Vertex a=" << a << " is already adjacent to vertex b=" << b << "\n";
	exit(1);
      }
#endif
    //    std::cerr << "a=" << a << " b=" << b << "\n";
    int a_idx = VertexToIdx.at(a);
    int b_idx = VertexToIdx.at(b);
    //    std::cerr << "a_idx=" << a_idx << " b_idx=" << b_idx << "\n";
    ListAdjacentTo[a_idx].push_back(b_idx);
    ListAdjacentFrom[b_idx].push_back(a_idx);
  }
  void AddLabel(Vertex const& a, Label const& l)
  {
    int a_idx = VertexToIdx.at(a);
    int l_idx;
    auto iter = LabelInfo.find(l);
    if (iter == LabelInfo.end()) {
      l_idx=GetEmptyLabelIdx();
      LabelInfo[l] = {1, l_idx};
    }
    else {
      l_idx=iter->second.second;
      iter->second.first++;
    }
#ifdef VECTOR_VERTEX_LABEL
# ifdef SAFE_RUN
    for (auto & eVal : ListLabelByVertex[a_idx])
      if (eVal == l_idx) {
	std::cerr << "Vertex a=" << a << " already has label l=" << l << "\n";
	exit(1);
      }
# endif
    ListLabelByVertex[a_idx].push_back(l_idx);
#endif
#ifdef SET_VERTEX_LABEL
# ifdef SAFE_RUN
    if (ListLabelByVertex[a_idx].find(l_idx) != ListLabelByVertex[a_idx].end()) {
      std::cerr << "Vertex a=" << a << " already has label l=" << l << "\n";
      exit(1);
    }
# endif
    ListLabelByVertex[a_idx].insert(l_idx);
#endif
  }
  void RemoveLabel(Vertex const& a, Label const& l)
  {
    auto iter=LabelInfo.find(l);
#ifdef SAFE_RUN
    if (iter == LabelInfo.end()) {
      std::cerr << "Label l=" << l << " is missing\n";
      exit(1);
    }
#endif
    int idx=iter->second.second;
    iter->second.first--;
    if (iter->second.first == 0) {
      LabelUsedStatus[idx] = 0;
      LabelInfo.erase(iter);
    }
#ifdef SAFE_RUN
    auto iter_test=VertexToIdx.find(a);
    if (iter_test == VertexToIdx.end()) {
      std::cerr << "The input Vertex a=" << a << " is actually not a vertex\n";
      exit(1);
    }
#endif
    int a_idx = VertexToIdx.at(a);
#ifdef VECTOR_VERTEX_LABEL
    for (auto & eVal : ListLabelByVertex[a_idx])
      if (eVal == idx) {
	eVal = -1;
	return;
      }
# ifdef SAFE_RUN
    std::cerr << "We fail to find label l=" << l << " in the list of label of vertex a=" << a << "\n";
    exit(1);
# endif
#endif
#ifdef SET_VERTEX_LABEL
    ListLabelByVertex[a_idx].erase(idx);
#endif
  }
  std::vector<Vertex> ShortestPath(Vertex const& a, Vertex const& b, Label const& l) const
  {
    if (a == b) {
      return {a};
    }
    int a_idx=VertexToIdx.at(a);
    int b_idx=VertexToIdx.at(b);
    int l_idx=LabelInfo.at(l).second;
    //
    Face ListStatusA(nbVertex);
    Face ListStatusB(nbVertex);
    std::vector<int> ListPrevA(nbVertex,-1);
    std::vector<int> ListNextB(nbVertex,-1);
    ListStatusA[a_idx] = 1;
    ListStatusB[b_idx] = 1;
    std::vector<int> WorkNodeA{a_idx};
    std::vector<int> WorkNodeB{b_idx};

    auto GetOneShortestPath=[&](int const& eNode) -> std::vector<Vertex> {
      std::vector<Vertex> LVert1;
      int eVert1=eNode;
      while(true) {
	int ePrec=ListPrevA[eVert1];
	if (ePrec == -1) {
	  LVert1.push_back(a);
	  break;
	}
	LVert1.push_back(VertexFromIdx[eVert1]);
	eVert1 = ePrec;
      }
      //
      int len=LVert1.size();
      std::vector<Vertex> Lreturn(len-1);
      for (int i=0; i<len-1; i++) {
	Lreturn[i] = LVert1[len-1-i];
      }
      //
      int eVert2=eNode;
      while(true) {
	int eNext=ListNextB[eVert2];
	if (eNext == -1) {
	  Lreturn.push_back(b);
	  break;
	}
	Lreturn.push_back(VertexFromIdx[eVert2]);
	eVert2 = eNext;
      }
      return Lreturn;
    };
    auto IsMatchingVertex=[&](int const& eNode) -> bool {
#ifdef VECTOR_VERTEX_LABEL      
      for (auto & eLabel_idx : ListLabelByVertex[eNode])
	if (eLabel_idx == l_idx)
	  return true;
      return false;
#endif
#ifdef SET_VERTEX_LABEL
      return ListLabelByVertex[eNode].find(l_idx) != ListLabelByVertex[eNode].end();
#endif
    };
    while(true) {
      std::vector<int> NewWorkNodeA;
      for (auto & eNode1 : WorkNodeA) {
	if (IsMatchingVertex(eNode1)) {
	  for (auto & eNodeNext : ListAdjacentTo[eNode1]) {
	    if (ListStatusB[eNodeNext] == 1) {
	      ListPrevA[eNodeNext] = eNode1;
	      return GetOneShortestPath(eNodeNext);
	    }
	    if (ListStatusA[eNodeNext] == 0) {
	      ListPrevA[eNodeNext] = eNode1;
	      ListStatusA[eNodeNext] = 1;
	      NewWorkNodeA.push_back(eNodeNext);
	    }
	  }
	}
      }
#ifdef SAFE_RUN
      if (NewWorkNodeA.size() == 0) {
	std::cerr << "The vertices a=" << a << " and b=" << b << " are not connected\n";
	std::cerr << "Therefore no shortest path is possible\n";
	exit(1);
      }
#endif
      WorkNodeA=NewWorkNodeA;
      //
      std::vector<int> NewWorkNodeB;
      for (auto & eNode2 : WorkNodeB) {
	if (IsMatchingVertex(eNode2)) {
	  for (auto & eNodePrev : ListAdjacentFrom[eNode2]) {
	    if (ListStatusA[eNodePrev] == 1) {
	      ListNextB[eNodePrev] = eNode2;
	      return GetOneShortestPath(eNodePrev);
	    }
	    if (ListStatusB[eNodePrev] == 0) {
	      ListNextB[eNodePrev] = eNode2;
	      ListStatusB[eNodePrev] = 1;
	      NewWorkNodeB.push_back(eNodePrev);
	    }
	  }
	}
      }
      WorkNodeB=NewWorkNodeB;
    }
  }
private:
  int GetEmptyLabelIdx()
  {
    int len=LabelUsedStatus.size();
    for (int i=0; i<len; i++) {
      if (LabelUsedStatus[i] == 0) {
	LabelUsedStatus[i]=1;
	return i;
      }
    }
    LabelUsedStatus.push_back(1);
    return len;
  }
  // The labels are coded into index for better speed
  // We need some fairly complex stuff because labels can be added or deleted and we want to do it fast.
  int nbLabel;
  std::map<Label, std::pair<int,int>> LabelInfo; // provides the number of use as well as the index
  std::vector<Label> LabelFromIdx;
  std::vector<int> LabelUsedStatus;
  // Vertices are encoded into index for better speed
  int nbVertex;
  std::map<Vertex,int> VertexToIdx;
  std::vector<Vertex> VertexFromIdx;
  // The vertex properties
#ifdef VECTOR_VERTEX_LABEL
  std::vector<std::vector<int>> ListLabelByVertex;
#endif
#ifdef SET_VERTEX_LABEL
  std::vector<std::set<int>> ListLabelByVertex; // Maybe use a set?
#endif
  std::vector<std::vector<int>> ListAdjacentTo;
  std::vector<std::vector<int>> ListAdjacentFrom;
};




#endif
