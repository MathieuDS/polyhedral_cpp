ExportGRAPE_to_MemgraphShortest:=function(eFile, GRA, PrintLength)
  local nbVert, ListMatch, iVert, pos, output, eAdj, eVert1, eVert2, fVert1, fVert2, nbDirEdge, iter, dist;
  nbVert:=GRA.order;
  ListMatch:=[];
  for iVert in [1..nbVert]
  do
    while(true)
    do
      pos:=Random([1..100*nbVert]);
      if Position(ListMatch, pos)=fail then
        Add(ListMatch, pos);
	break;
      fi;
    od;
  od;
  #
  RemoveFileIfExist(eFile);
  output:=OutputTextFile(eFile, true);
  # vertices
  for iVert in [1..nbVert]
  do
    AppendTo(output, "v ", ListMatch[iVert], "\n");
  od;
  # edges
  nbDirEdge:=0;
  for iVert in [1..nbVert]
  do
    for eAdj in Adjacency(GRA, iVert)
    do
      nbDirEdge:=nbDirEdge+1;
      AppendTo(output, "e ", ListMatch[iVert], " ", ListMatch[eAdj], "\n");
    od;
  od;
  # label
  for iVert in [1..nbVert]
  do
    AppendTo(output, "a ", ListMatch[iVert], " test\n");
  od;
  for iter in [1..100]
  do
    eVert1:=Random([1..nbVert]);
    eVert2:=Random([1..nbVert]);
    fVert1:=ListMatch[eVert1];
    fVert2:=ListMatch[eVert2];
    if PrintLength then
      dist:=Distance(GRA, eVert1, eVert2);
      AppendTo(output, "s ", fVert1, " ", fVert2, " ", dist, " test\n");
    else
      AppendTo(output, "s ", fVert1, " ", fVert2, " test\n");
    fi;
  od;
  AppendTo(output, "f\n");
  CloseStream(output);
  Print("eFile=", eFile, " nbDirEdge=", nbDirEdge, "\n");
end;


ExportGRAPE_Nlabels:=function(eFile, GRA, Nlabel, PrintLength)
  local nbVert, ListMatch, iVert, pos, output, nbDirEdge, iLabel, eVert1, eVert2, fVert1, fVert2, dist, eAdj, iter;
  nbVert:=GRA.order;
  ListMatch:=[];
  for iVert in [1..nbVert]
  do
    while(true)
    do
      pos:=Random([1..100*nbVert]);
      if Position(ListMatch, pos)=fail then
        Add(ListMatch, pos);
        break;
      fi;
    od;
  od;
  #
  # Now the write down
  #
  RemoveFileIfExist(eFile);
  output:=OutputTextFile(eFile, true);
  # vertices
  for iVert in [1..nbVert]
  do
    AppendTo(output, "v ", ListMatch[iVert], "\n");
  od;
  # edges
  nbDirEdge:=0;
  for iVert in [1..nbVert]
  do
    for eAdj in Adjacency(GRA, iVert)
    do
      nbDirEdge:=nbDirEdge+1;
      AppendTo(output, "e ", ListMatch[iVert], " ", ListMatch[eAdj], "\n");
    od;
  od;
  # label
  for iVert in [1..nbVert]
  do
    for iLabel in [1..Nlabel]
    do
      AppendTo(output, "a ", ListMatch[iVert], " test", iLabel, "\n");
    od;
  od;
  for iVert in [1..nbVert]
  do
    for iLabel in [2..Nlabel]
    do
      AppendTo(output, "r ", ListMatch[iVert], " test", iLabel, "\n");
    od;
  od;
  for iter in [1..100]
  do
    eVert1:=Random([1..nbVert]);
    eVert2:=Random([1..nbVert]);
    fVert1:=ListMatch[eVert1];
    fVert2:=ListMatch[eVert2];
    if PrintLength then
      dist:=Distance(GRA, eVert1, eVert2);
      AppendTo(output, "s ", fVert1, " ", fVert2, " ", dist, " test1\n");
    else
      AppendTo(output, "s ", fVert1, " ", fVert2, " test\n");
    fi;
  od;
  AppendTo(output, "f\n");
  CloseStream(output);
  Print("eFile=", eFile, " Nlabel=", Nlabel, " nbDirEdge=", nbDirEdge, "\n");
end;






PL:=ArchimedeanPolyhedra("Dodecahedron");

PrintPerfTest1:=true;
if PrintPerfTest1 then
  #
#  PL1:=GoldbergConstruction(PL, 4, 3);
#  GRA1:=PlanGraphToGRAPE(PL1);
#  ExportGRAPE_to_MemgraphShortest("PERF_Test1", GRA1, false);
  #
#  PL2:=GoldbergConstruction(PL, 5, 3);
#  GRA2:=PlanGraphToGRAPE(PL2);
#  ExportGRAPE_to_MemgraphShortest("PERF_Test2", GRA2, false);
  #
#  PL3:=GoldbergConstruction(PL, 7, 6);
#  GRA3:=PlanGraphToGRAPE(PL3);
#  ExportGRAPE_to_MemgraphShortest("PERF_Test3", GRA3, false);
  #
#  PL4:=GoldbergConstruction(PL, 9, 6);
#  GRA4:=PlanGraphToGRAPE(PL4);
#  ExportGRAPE_to_MemgraphShortest("PERF_Test4", GRA4, false);
  #
#  PL5:=GoldbergConstruction(PL, 13, 8);
#  GRA5:=PlanGraphToGRAPE(PL5);
#  ExportGRAPE_to_MemgraphShortest("PERF_Test5", GRA5, false);
  #
#  PL6:=GoldbergConstruction(PL, 17, 10);
#  GRA6:=PlanGraphToGRAPE(PL6);
#  ExportGRAPE_to_MemgraphShortest("PERF_Test6", GRA6, false);
  #
#  PL7:=GoldbergConstruction(PL, 21, 14);
#  GRA7:=PlanGraphToGRAPE(PL7);
#  ExportGRAPE_to_MemgraphShortest("PERF_Test7", GRA7, false);
  #
  PL8:=GoldbergConstruction(PL, 31, 14);
  GRA8:=PlanGraphToGRAPE(PL8);
  ExportGRAPE_to_MemgraphShortest("PERF_Test8", GRA8, false);
  #
fi;


PrintCheckCase1:=true;
if PrintCheckCase1 then
  #
  PL1:=GoldbergConstruction(PL, 2, 1);
  GRA1:=PlanGraphToGRAPE(PL1);
  ExportGRAPE_to_MemgraphShortest("CHECK_Test1", GRA1, true);
  #
  PL2:=GoldbergConstruction(PL, 1, 1);
  GRA2:=PlanGraphToGRAPE(PL2);
  ExportGRAPE_to_MemgraphShortest("CHECK_Test2", GRA2, true);
  #
  PL3:=GoldbergConstruction(PL, 1, 0);
  GRA3:=PlanGraphToGRAPE(PL3);
  ExportGRAPE_to_MemgraphShortest("CHECK_Test3", GRA3, true);
  #
  PL4:=GoldbergConstruction(PL, 3, 1);
  GRA4:=PlanGraphToGRAPE(PL4);
  ExportGRAPE_to_MemgraphShortest("CHECK_Test4", GRA4, true);
  #
  PL5:=GoldbergConstruction(PL, 4, 1);
  GRA5:=PlanGraphToGRAPE(PL5);
  ExportGRAPE_to_MemgraphShortest("CHECK_Test5", GRA5, true);
  #
fi;


PrintLabelCase1:=true;
if PrintLabelCase1 then
  #
  PL1:=GoldbergConstruction(PL, 2, 1);
  GRA1:=PlanGraphToGRAPE(PL1);
  ExportGRAPE_Nlabels("LABEL_Test1", GRA1, 5, true);
fi;


