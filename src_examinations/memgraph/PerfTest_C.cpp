#include <iostream>
#include <fstream>

#include "ShortestPathCode.h"



int main(int argc, char* argv[])
{
  if (argc != 3) {
    std::cerr << "Program is run as PerfTest_C [NBvert] [NBlabel]\n";
    exit(1);
  }
  int NBvert;
  (void)sscanf(argv[1], "%d", &NBvert);
  int NBlabel;
  (void)sscanf(argv[2], "%d", &NBlabel);
  //
  ClassVertexLabel CVL;
  for (int iVert=0; iVert<NBvert; iVert++) {
    Vertex a = iVert;
    CVL.CreateVertex(a);
  }
  int nbIter=100;
  for (int iter=0; iter<nbIter; iter++) {
    for (int iVert=0; iVert<NBvert; iVert++) {
      Vertex a = iVert;
      for (int iLabel=0; iLabel<NBlabel; iLabel++) {
	Label l = "test_string_" + std::to_string(iLabel);
	CVL.AddLabel(a,l);
      }
    }
    //
    for (int iVert=0; iVert<NBvert; iVert++) {
      Vertex a = iVert;
      for (int iLabel=0; iLabel<NBlabel; iLabel++) {
	Label l = "test_string_" + std::to_string(iLabel);
	CVL.RemoveLabel(a,l);
      }
    }
  }
  std::cerr << "Normal termination of the program\n";
}
