#include <iostream>
#include <fstream>

#include "ShortestPathCode.h"






int main(int argc, char* argv[])
{
  if (argc != 3) {
    std::cerr << "Program is run as FileTest [FileI] [NB]\n";
    exit(1);
  }
  std::string eFile=argv[1];
  std::ifstream is(eFile);
  if (!is.is_open()) {
    std::cerr << "Error opening eFile=" << eFile << "\n";
    exit(1);
  }
  ClassVertexLabel CVL;
  std::vector<Vertex> ListVert;
  while(true) {
    char eChar;
    is.get(eChar);
    //
    if (eChar == 'f') {
      break;
    }
    //
    if (eChar == 'v') {
      Vertex a;
      is >> a;
      ListVert.push_back(a);
      CVL.CreateVertex(a);
    }
    //
    if (eChar == 'e') {
      Vertex a;
      Vertex b;
      is >> a;
      is >> b;
      //      std::cerr << "e : a=" << a << " b=" << b << "\n";
      CVL.CreateEdge(a,b);
    }
    //
    if (eChar == 'a') {
      Vertex a;
      is >> a;
      Label l;
      is >> l;
      CVL.AddLabel(a,l);
    }
    //
    if (eChar == 'r') {
      Vertex a;
      is >> a;
      Label l;
      is >> l;
      CVL.RemoveLabel(a,l);
    }
    //
    if (eChar == 's') {
      Vertex a, b;
      is >> a;
      is >> b;
      Label l;
      is >> l;
      std::vector<Vertex> TheShortest = CVL.ShortestPath(a, b, l);
      std::cerr << "|TheShortest|=" << TheShortest.size() << "\n";
    }
    
  }
  //
  int NB;
  (void)sscanf(argv[2], "%d", &NB);
  int nbVert=ListVert.size();
  std::string l_str="test";
  for (int iter=0; iter<NB; iter++) {
    int pos1=rand() % nbVert;
    int pos2=rand() % nbVert;
    int eVert1=ListVert[pos1];
    int eVert2=ListVert[pos2];
    std::vector<Vertex> TheShortest = CVL.ShortestPath(eVert1, eVert2, l_str);
    std::cerr << "iter=" << iter << " |TheShortest|=" << TheShortest.size() << "\n";
  }
  std::cerr << "Normal termination of the program\n";
}
