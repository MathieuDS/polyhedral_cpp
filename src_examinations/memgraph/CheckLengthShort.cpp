#include <iostream>
#include <fstream>

#include "ShortestPathCode.h"






int main(int argc, char* argv[])
{
  if (argc != 2) {
    std::cerr << "Program is run as FileTest [FileI]\n";
    exit(1);
  }
  std::string eFile=argv[1];
  std::ifstream is(eFile);
  if (!is.is_open()) {
    std::cerr << "Error opening eFile=" << eFile << "\n";
    exit(1);
  }
  ClassVertexLabel CVL;
  int nbError=0;
  while(true) {
    char eChar;
    is.get(eChar);
    //
    if (eChar == 'f') {
      break;
    }
    //
    if (eChar == 'v') {
      Vertex a;
      is >> a;
      CVL.CreateVertex(a);
    }
    //
    if (eChar == 'e') {
      Vertex a;
      Vertex b;
      is >> a;
      is >> b;
      CVL.CreateEdge(a,b);
    }
    //
    if (eChar == 'a') {
      Vertex a;
      is >> a;
      Label l;
      is >> l;
      CVL.AddLabel(a,l);
    }
    //
    if (eChar == 'r') {
      Vertex a;
      is >> a;
      Label l;
      is >> l;
      CVL.RemoveLabel(a,l);
    }
    //
    if (eChar == 's') {
      Vertex a, b;
      is >> a;
      is >> b;
      int dist;
      is >> dist;
      Label l;
      is >> l;
      std::vector<Vertex> TheShortest = CVL.ShortestPath(a, b, l);
      int diff=int(TheShortest.size()) - 1 - dist;
      if (diff != 0) {
	std::cerr << "Discrepancy between the distances\n";
	std::cerr << "|TheShortest|=" << TheShortest.size() << "  dist=" << dist << "\n";
	std::cerr << "a=" << a << " b=" << b << "\n";
	std::cerr << "PATH =";
	for (auto & eVal : TheShortest)
	  std::cerr << " " << eVal;
	std::cerr << "\n";
	nbError++;
      }
      std::cerr << "|TheShortest|=" << TheShortest.size() << "\n";
    }
  }
  std::cerr << "nbError = " << nbError << "\n";
  std::cerr << "Normal termination of the program\n";
}
