#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <set>

struct InfoMachine {
  int Pi_PriceAcquisition;
  int Ri_ResoldPrice;
  int Gi_ProfitPerDay;
};

bool operator==(InfoMachine const& eInf1, InfoMachine const& eInf2)
{
  if (eInf1.Pi_PriceAcquisition != eInf2.Pi_PriceAcquisition)
    return false;
  if (eInf1.Ri_ResoldPrice != eInf2.Ri_ResoldPrice)
    return false;
  if (eInf1.Gi_ProfitPerDay != eInf2.Gi_ProfitPerDay)
    return false;
  return true;
}


struct DayStructure {
  int eDay;
  std::vector<int> ListIglobal;
};

struct DescProblem {
  int N_nbMachineForSale;
  int C_nbDollar;
  int D_nbDayRestruct;
  std::vector<DayStructure> ListDayStructure;
  std::vector<InfoMachine> ListInfoMachineTotal;
  int positionZero;
};




std::vector<DescProblem> ReadDescProblem(std::string const& eFile)
{
  std::ifstream is(eFile);
  std::vector<DescProblem> ListProblem;
  while(true) {
    int N_nbMachineForSale;
    int C_nbDollar;
    int D_nbDayRestruct;
    is >> N_nbMachineForSale;
    is >> C_nbDollar;
    is >> D_nbDayRestruct;
    std::cerr << "Reading N_nbMachineForSale=" << N_nbMachineForSale << " C_nbDollar=" << C_nbDollar << " D_nbDayRestruct=" << D_nbDayRestruct << "\n";
    if (N_nbMachineForSale == 0 && C_nbDollar == 0 && D_nbDayRestruct == 0)
      break;
    D_nbDayRestruct++;
    std::vector<int> ListDayTotal;
    std::set<int> SetDayTotal;
    std::vector<InfoMachine> ListInfoMachineTotal;
    for (int iN=0; iN<N_nbMachineForSale; iN++) {
      int eDay;
      int Pi_PriceAcquisition;
      int Ri_ResoldPrice;
      int Gi_ProfitPerDay;
      is >> eDay;
      is >> Pi_PriceAcquisition;
      is >> Ri_ResoldPrice;
      is >> Gi_ProfitPerDay;
      //
      std::cerr << "Machine iN=" << iN << " Price=" << Pi_PriceAcquisition << " resold=" << Ri_ResoldPrice << " gain=" << Gi_ProfitPerDay << "\n";
      ListDayTotal.push_back(eDay);
      SetDayTotal.insert(eDay);
      ListInfoMachineTotal.push_back({Pi_PriceAcquisition, Ri_ResoldPrice, Gi_ProfitPerDay});
    }
    // We need to insert the zero solution
    InfoMachine eInfoZero{0,0,0};
    int positionZero=-1;
    for (int i=0; i<int(ListInfoMachineTotal.size()); i++) {
      if (ListInfoMachineTotal[i] == eInfoZero)
	positionZero = i;
    }
    if (positionZero == -1) {
      ListDayTotal.push_back(0);
      SetDayTotal.insert(0);
      ListInfoMachineTotal.push_back(eInfoZero);
      positionZero = ListInfoMachineTotal.size() - 1;
    }
    //
    std::vector<DayStructure> ListDayStructure;
    int idx=0;
    for (auto & eDay : SetDayTotal) {
      std::cerr << "  idx=" << idx << " eDay=" << eDay << "\n";
      idx++;
      std::vector<int> ListIglobal;
      for (int iN=0; iN<N_nbMachineForSale; iN++)
	if (ListDayTotal[iN] == eDay)
	  ListIglobal.push_back(iN);
      ListDayStructure.push_back({eDay, ListIglobal});
    }
    DescProblem eProblem {N_nbMachineForSale, C_nbDollar, D_nbDayRestruct, ListDayStructure, ListInfoMachineTotal, positionZero};
    ListProblem.push_back(eProblem);
  }
  return ListProblem;
}


std::pair<std::vector<int>,int> MaximumValue(DescProblem const& eProblem)
{
  //  int N_nbMachineForSale = eProblem.N_nbMachineForSale;
  int C_nbDollar = eProblem.C_nbDollar;
  int D_nbDayRestruct = eProblem.D_nbDayRestruct;

  int nbDayTotal=eProblem.ListDayStructure.size();
  //  std::cerr << "nbDayTotal=" << nbDayTotal << "\n";
  int nbDaySearch=0;
  for (int iDay=0; iDay<nbDayTotal; iDay++)
    if (eProblem.ListDayStructure[iDay].eDay < D_nbDayRestruct)
      nbDaySearch = iDay + 1;
  //  std::cerr << "nbDaySearch=" << nbDaySearch << "\n";
  
  std::vector<int> ListDayRelevant(nbDaySearch+1);
  for (int iDay=0; iDay<nbDaySearch; iDay++)
    ListDayRelevant[iDay] = eProblem.ListDayStructure[iDay].eDay;
  ListDayRelevant[nbDaySearch] = D_nbDayRestruct;
  //  std::cerr << "------------------- ListDay ------------------------\n";
  //  for (int iDay=0; iDay<=nbDaySearch; iDay++)
  //    std::cerr << "iDay=" << iDay << " eDay=" << ListDayRelevant[iDay] << "\n";
  // List of sizes.
  std::vector<int> ListSizes(nbDaySearch);
  for (int iDay=0; iDay<nbDaySearch; iDay++)
    ListSizes[iDay] = eProblem.ListDayStructure[iDay].ListIglobal.size();
  //  std::cerr << "------------------- ListSizes -----------------------\n";
  //  for (int iDay=0; iDay<nbDaySearch; iDay++)
  //    std::cerr << "iDay=" << iDay << " eSize=" << ListSizes[iDay] << "\n";
  // We have nbDaySearch
  // Each index i with 0 <= i < nbDaySearch
  // corresponds to a period between 
  // ListDayRelevant[iDay] and ListDayRelevant[iDay+1]
  // 
  // What is different with usual case is that we can still go further in the tree search
  // Value -1 for 
  
  std::vector<int> Solution_iNlocal(nbDaySearch,-1);
  std::vector<int> Solution_iNglobal(nbDaySearch,-1);
  std::vector<int> LocalSum(nbDaySearch+1,0);
  LocalSum[0] = C_nbDollar;
  //
  
  int nbDayAssigned=0;
  // This function sets the values assuming that everything is valid
  //
  auto SetValues=[&](int const& eDayAssigned, int const& iNlocal) -> void {
    int iNglobalPrev;
    if (eDayAssigned == 0) {
      iNglobalPrev = eProblem.positionZero;
    }
    else {
      iNglobalPrev = Solution_iNglobal[eDayAssigned - 1];
    }
    //    std::cerr << "Beginning of SetValues eDayAssigned=" << eDayAssigned << " iNlocal=" << iNlocal << " iNglobalPrev=" << iNglobalPrev << "\n";
    int ChangingCost=0;
    int iNglobal = iNglobalPrev;
    int CorrPeriod=0;
    if (iNlocal != -1) {
      iNglobal = eProblem.ListDayStructure[eDayAssigned].ListIglobal[iNlocal];
      ChangingCost = eProblem.ListInfoMachineTotal[iNglobalPrev].Ri_ResoldPrice - eProblem.ListInfoMachineTotal[iNglobal].Pi_PriceAcquisition;
      CorrPeriod=-1;
    }
    Solution_iNlocal[eDayAssigned] = iNlocal;
    Solution_iNglobal[eDayAssigned] = iNglobal;
    int lenPeriod=ListDayRelevant[eDayAssigned+1] - ListDayRelevant[eDayAssigned] + CorrPeriod;
    int GainPeriod = lenPeriod * eProblem.ListInfoMachineTotal[iNglobal].Gi_ProfitPerDay;
    LocalSum[eDayAssigned + 1] = LocalSum[eDayAssigned] + ChangingCost + GainPeriod;
    //    std::cerr << "Assigning eDayAssigned=" << eDayAssigned << "\n";
    //    std::cerr << "   iNglobalPrev=" << iNglobalPrev << " iNglobal=" << iNglobal << "\n";
    //    std::cerr << "   lenPeriod=" << lenPeriod << " GainPeriod=" << GainPeriod << " ChangingCost=" << ChangingCost << "\n";
    //    std::cerr << "   LocalSum[eDay]=" << LocalSum[eDayAssigned] << " LocalSum[eDay+1]=" << LocalSum[eDayAssigned+1] << "\n";
  };


  auto GoUpNextInTree=[&]() -> bool {
    while(true) {
      if (nbDayAssigned == 0)
	break;
      //      std::cerr << "nbDayAssigned=" << nbDayAssigned << "\n";
      int iNlocal=Solution_iNlocal[nbDayAssigned-1];
      int iNglobal=Solution_iNglobal[nbDayAssigned-1];
      //      std::cerr << "iNlocal=" << iNlocal << " iNglobal=" << iNglobal << "\n";
      int LatentProfit = eProblem.ListInfoMachineTotal[iNglobal].Ri_ResoldPrice;
      while(true) {
	if (iNlocal == ListSizes[nbDayAssigned-1] - 1)
	  break;
	iNlocal++;
	int iNglobalNew=eProblem.ListDayStructure[nbDayAssigned-1].ListIglobal[iNlocal];
	//	std::cerr << "Now iNlocal=" << iNlocal << " iNglobalNew=" << iNglobalNew << "\n";
	if (LocalSum[nbDayAssigned-1] + LatentProfit >= eProblem.ListInfoMachineTotal[iNglobalNew].Pi_PriceAcquisition) {
	  //	  std::cerr << "Before call to setValues 1\n";
	  SetValues(nbDayAssigned-1, iNlocal);
	  return true;
	}
      }
      nbDayAssigned--;
    }
    return false;
  };




  
  auto NextInTree=[&]() -> bool {
    //    std::cerr << "NextInTree, beginning nbDayAssigned=" << nbDayAssigned << "\n";
    if (nbDayAssigned == nbDaySearch) {
      //      std::cerr << "Before call to GoUpNextInTree\n";
      return GoUpNextInTree();
    }
    //    std::cerr << "After the GoUpNextInTree\n";
    int iNglobalPrev;
    if (nbDayAssigned == 0) {
      iNglobalPrev = eProblem.positionZero;
    }
    else {
      iNglobalPrev = Solution_iNglobal[nbDayAssigned - 1];
    }
    //    std::cerr << "iNglobalPrev=" << iNglobalPrev << "\n";
    //    std::cerr << "Before call to SetValues 2\n";
    SetValues(nbDayAssigned, -1);
    //    std::cerr << "After SetValues\n";
    nbDayAssigned++;
    //    std::cerr << "Now nbDayAssigned=" << nbDayAssigned << "\n";
    //    std::cerr << "-----------------------------------------------\n";
    return true;
  };
  int BestValue = 0;
  std::vector<int> BestSol;
  while(true) {
    if (nbDayAssigned == nbDaySearch) {
      int iNglobalFinal = Solution_iNglobal[nbDayAssigned-1];
      int eResold = eProblem.ListInfoMachineTotal[iNglobalFinal].Ri_ResoldPrice;
      //      std::cerr << "iNglobalFinal=" << iNglobalFinal << " eResold=" << eResold << "\n";
      int FinalValue = LocalSum[nbDayAssigned] + eResold;
      //      std::cerr << "FinalValue=" << FinalValue << "\n";
      if (FinalValue > BestValue) {
	BestValue = FinalValue;
	BestSol = Solution_iNglobal;
      }
    }
    //
    /*
    for (int iDay=0; iDay<nbDayAssigned; iDay++) {
      std::cerr << "iDay=" << iDay << " iNglobal=" << Solution_iNglobal[iDay] << "\n";
    }
    std::cerr << "LocalSum =";
    for (int iDay=0; iDay<=nbDayAssigned; iDay++)
      std::cerr << " " << LocalSum[iDay];
    std::cerr << "\n";
    */
    //
    bool test = NextInTree();
    if (!test)
      break;
  }
  return {BestSol, BestValue};
}



void PrintSolution(std::ostream & os, DescProblem const& eProblem, std::vector<int> const& Solution_iNglobal)
{
  int D_nbDayRestruct = eProblem.D_nbDayRestruct;
  //
  int nbDayTotal=eProblem.ListDayStructure.size();
  int nbDaySearch=0;
  for (int iDay=0; iDay<nbDayTotal; iDay++)
    if (eProblem.ListDayStructure[iDay].eDay < D_nbDayRestruct)
      nbDaySearch = iDay + 1;

  
  std::vector<int> ListDayRelevant(nbDaySearch+1);
  for (int iDay=0; iDay<nbDaySearch; iDay++)
    ListDayRelevant[iDay] = eProblem.ListDayStructure[iDay].eDay;
  ListDayRelevant[nbDaySearch] = D_nbDayRestruct;
  // List of sizes.
  std::vector<int> ListSizes(nbDaySearch);
  for (int iDay=0; iDay<nbDaySearch; iDay++)
    ListSizes[iDay] = eProblem.ListDayStructure[iDay].ListIglobal.size();
  //
  // 
  //
  int CurrentMoney = eProblem.C_nbDollar;
  int len=Solution_iNglobal.size();
  int iNglobal = eProblem.positionZero;
  std::cerr << "Initial CurrentMoney=" << CurrentMoney << "\n";
  for (int iDay=0; iDay<len; iDay++) {
    int iNglobalLocal   = Solution_iNglobal[iDay];
    int day2 = ListDayRelevant[iDay+1];
    int day1 = ListDayRelevant[iDay];
    os << "iPeriod=" << iDay << " from day " << day1 << " to day " << day2 << "\n";
    int CorrPeriod=0;
    if (iNglobal != iNglobalLocal) {
      int LatentProfit = eProblem.ListInfoMachineTotal[iNglobal].Ri_ResoldPrice;
      CurrentMoney += LatentProfit;
      os << " at day " << day1 << " selling machine " << iNglobal << " for " << LatentProfit << ". Now CurrentMoney=" << CurrentMoney << "\n";
      CorrPeriod=-1;
      //
      int NeededExpense=eProblem.ListInfoMachineTotal[iNglobalLocal].Pi_PriceAcquisition;
      CurrentMoney -= NeededExpense;
      os << " at day " << day1 << " buying machine " << iNglobalLocal << " for " << NeededExpense << ". Now CurrentMoney=" << CurrentMoney << "\n";
    }
    int lenPeriod  = day2 - day1 + CorrPeriod;
    int profitPerDay = eProblem.ListInfoMachineTotal[iNglobalLocal].Gi_ProfitPerDay;
    int GainPeriod = lenPeriod * profitPerDay;
    CurrentMoney += GainPeriod;
    os << "Operating machine " << iNglobalLocal << " for " << lenPeriod << " day(s) gaining " << GainPeriod << ". Now CurrentMoney=" << CurrentMoney << "\n";
    os << "          lenPeriod=" << lenPeriod << " profitPerDay=" << profitPerDay << " GainPeriod=" << GainPeriod << "\n";
    iNglobal = iNglobalLocal;
  }
  int dayFinal = ListDayRelevant[nbDaySearch];
  int LatentProfit = eProblem.ListInfoMachineTotal[iNglobal].Ri_ResoldPrice;
  CurrentMoney += LatentProfit; 
  os << "FINAL: at day " << dayFinal << " selling machine " << iNglobal << " for " << LatentProfit << ". Now CurrentMoney=" << CurrentMoney << "\n";
}




int main(int argc, char *argv[])
{
  std::string eFile = argv[1];
  std::vector<DescProblem> ListProblem = ReadDescProblem(eFile);
  int nbProblem=ListProblem.size();
  for (int iProblem=0; iProblem<nbProblem; iProblem++) {
    std::cerr << "iProblem=" << iProblem << "\n";
    DescProblem eProblem = ListProblem[iProblem];
    std::pair<std::vector<int>,int> ePair = MaximumValue(eProblem);
    std::cerr << "SumFinal ePair.second=" << ePair.second << "\n";
    PrintSolution(std::cerr, eProblem, ePair.first);
  }
}


