#include <iostream>
#include <string>
#include <utility>

struct TerminalException {
  int retVal;
};



std::pair<std::string,int> MostFrequent(std::string const& strIn)
{
  int siz=strIn.size();
  if (siz == 0) {
    throw TerminalException{1};
  }
  auto GetLen=[&](int const& start) -> int {
    std::string eChar=strIn.substr(start,1);
    for (int u=start+1; u<siz; u++) {
      std::string fChar=strIn.substr(u,1);
      //      std::cerr << "   start=" << start << " u=" << u << " eChar=" << eChar << " fChar=" << fChar << "\n";
      if (fChar != eChar) {
	int retVal=u - start;
	//	std::cerr << "      retVal=" << retVal << "\n";
	return retVal;
      }
    }
    return siz-start;
  };
  int pos=0;
  std::string eMost=strIn.substr(0,1);
  int lenMost=GetLen(0);
  //  std::cerr << "eMost=" << eMost << " lenMost=" << lenMost << "\n";
  int len=lenMost;
  while(true) {
    pos += len;
    if (pos >= siz) {
      break;
    }
    std::string eChar=strIn.substr(pos,1);
    //    std::cerr << "pos=" << pos << "    eChar=" << eChar << "\n";
    len = GetLen(pos);
    //    std::cerr << "    len=" << len << "\n";
    if (len > lenMost) {
      eMost = eChar;
      lenMost = len;
    }
  }
  return {eMost,lenMost};
}



int main(int argc, char *argv[])
{
  std::string strIn="aabcccababaa";
  for (int u=0; u<int(strIn.size()); u++) {
    std::cerr << "u=" << u << " char=" << strIn.substr(u,1) << "\n";
  }
  std::pair<std::string,int> ePair = MostFrequent(strIn);
  std::cerr << "ePair.first=" << ePair.first << " ePair.second=" << ePair.second << "\n";
}
