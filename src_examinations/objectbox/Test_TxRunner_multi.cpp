#include "Dummy_implement_transaction.h"
#include "TxRunner.h"
#include <functional>
#include <vector>

#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>


struct TerminalException {
  int eVal;
};

int main(int argc, char *argv[])
{
  try {
    Database db;
    TxRunner eTX(db);
    int nbThread=10;
    std::atomic<int> nb(100);
    //
    std::atomic<int> nbThreadRunning(0);
    std::condition_variable cv;
    std::mutex mtx_cv;
    auto WaitStuck=[&]() -> void {
      std::unique_lock<std::mutex> lk(mtx_cv);
      cv.wait(lk, [&]{return nbThreadRunning == 0;});
    };

    
    std::function<void(Database&)> fun=[&](Database & db) -> void {
      if (nb == 0)
	return;
      std::cerr << "Running with nb=" << nb << "\n";
      if (nb == 5) throw TerminalException{5};
      nb--;
      eTX.runTransactional(fun);
    };
    auto SpannThread=[&]() -> void {
      nbThreadRunning++;
      eTX.runTransactional(fun);
      nbThreadRunning--;
      cv.notify_one();
    };

    std::vector<std::thread> ListThreads;
    for (int iThread=0; iThread<nbThread; iThread++) {
      ListThreads.push_back(std::thread(SpannThread));
      ListThreads[ListThreads.size()-1].detach();
    }
    WaitStuck();


    
  }
  catch (TerminalException const& e) {
    std::cerr << "Terminal exception with val=" << e.eVal << "\n";
  }
}
