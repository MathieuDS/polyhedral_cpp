#include "Dummy_implement_transaction.h"


thread_local int NbRunning;
thread_local Transaction* trs;

struct TxRunner {
public:
  /// db is provided by the caller and is guaranteed to be valid for the live time of TxRunner
  TxRunner(Database& db) : eDB(db)
  {
    nbTrans=0;
    NbRunning=0;
    trs=nullptr;
  }

  int intCount(std::string const& key) {
    return nbTrans;
  }
  
  int getUInt(std::string const& key) {
    nbTrans++;
    return db.getUInt();
  }
  
  void setUInt(std::string const& key, uint64_t const& val) {
    nbTrans++;
    db.setUInt();
  }

  /// Runs the given function inside a transaction, which is committed after the given function returns (normally).
  /// The given function may throw an exception, in which case the transaction is aborted (not committed).
  /// FUNCTION is any lambda accepting a reference to the Database object as parameter.
  template <typename FUNCTION>
  void runTransactional(FUNCTION fun) {
    if (NbRunning == 0) {
      trs = new Transaction(eDB);
      NbRunning++;
      trs->start();
    }
    bool IsOK=true;
    try {
      fun(eDB);
    }
    catch (...) {
      IsOK=false;
    }
    NbRunning--;
    if (!IsOK) {
      trs->abort();
      NbRunning=0;
      delete trs;
    }
    else {
      if (NbRunning == 0) {
	trs->commit();
	delete trs;
      }
    }
  }
private:
  int nbTrans;
  Database & eDB;
};
