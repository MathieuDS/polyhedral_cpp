#include "Dummy_implement_transaction.h"
#include "TxRunner.h"
#include <functional>

struct TerminalException {
  int eVal;
};

int main(int argc, char *argv[])
{
  Database db;
  TxRunner eTX(db);

  int nb=10;
  std::function<void(Database&)> fun=[&](Database & db) -> void {
    if (nb == 0)
      return;
    std::cerr << "Running with nb=" << nb << "\n";
    if (nb == 5) throw TerminalException{5};
    nb--;
    eTX.runTransactional(fun);
  };

  eTX.runTransactional(fun);

  
}
