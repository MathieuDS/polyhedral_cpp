#ifndef INCLUDE_DUMMY
#define INCLUDE_DUMMY

#include <iostream>
#include <cstdlib>

struct Database {
public:
  Database()
  {
    std::cerr << "Database has been created\n";
  }
  void SignalStart()
  {
    std::cerr << "Starting of Database\n";
  }
  void SignalCommit()
  {
    std::cerr << "Committing of Database\n";
  }
  void SignalAbort()
  {
    std::cerr << "Aborting of Database\n";
  }
  uint64_t getUInt(const std::string& key) const
  {
    std::cerr << "getUInt operation\n";
  }
  void putUInt(const std::string& key, uint64_t value)
  {
    std::cerr << "putUInt operation\n";
  }
};

class Transaction {
  public:
  Transaction(Database& db) : eDB(db)
  {
    std::cerr << "Creating Transaction\n";
    IsStarted=false;
    IsCommited=false;
    IsAborted=false;
  }
  ~Transaction() {
    std::cerr << "Destroying Transaction\n";
    std::cerr << "At the end IsStarted=" << IsStarted << " IsCommited=" << IsCommited << " IsAborted=" << IsAborted << "\n";
  }
  
  /// Starts the TX. Illegal to call if the TX already started.
  void start() {
    if (IsStarted) {
      std::cerr << "Starting a transaction already started. Illegal\n";
      exit(1);
    }
    IsStarted=true;
    eDB.SignalStart();
  }
  
  /// Commits a started TX. Illegal to call if TX was not yet started or is already committed/aborted.
  void commit() {
    if (IsCommited || IsAborted) {
      std::cerr << "Illegal to commit a transaction that is already commited/aborted\n";
      exit(1);
    }
    if (!IsStarted) {
      std::cerr << "Illegal to commit a transaction that is not started\n";
      exit(1);
    }
    IsCommited=true;
    eDB.SignalCommit();
  }
  
  /// Aborts a started TX. Illegal to call if TX was not yet started or is already committed/aborted.
  void abort()
  {
    if (IsCommited || IsAborted) {
      std::cerr << "Illegal to commit a transaction that is already commited/aborted\n";
      exit(1);
    }
    if (!IsStarted) {
      std::cerr << "Illegal to commit a transaction that is not started\n";
      exit(1);
    }
    IsAborted=true;
    eDB.SignalAbort();
  }
private:
  Database & eDB;
  bool IsStarted;
  bool IsCommited;
  bool IsAborted;
};



#endif
