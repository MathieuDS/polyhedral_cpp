import java.util.ArrayList;
import java.lang.Float;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.io.PrintWriter;

public class Standalone_SharedObj {

    public static void main(String[] args) {
	try {
	    int argv=args.length;
	    if (argv != 1) {
		System.out.print("The program is used as\n");
		System.out.print("java Standalone_SharedObj [nbThread]\n");
		throw new IOException("wrong input");
	    }
	    String strNbThread=args[0];
	    int nbThread=Integer.parseInt(strNbThread);
	    System.out.print("strNbThread = " + strNbThread + "\n");
	    
	    SharedObject sharObj = new SharedObject();
	    //
	    Thread[] ArrThr = new Thread[nbThread];
	    for (int iThread=0; iThread<nbThread; iThread++) {
		ArrThr[iThread] = new Thread() {
			public void run() {
			    Object o =sharObj.retrieve();
			}
		    };
		System.out.print("Assigning Thread = " + iThread + "\n");
	    }
	    for (int iThread=0; iThread<nbThread; iThread++) {
		ArrThr[iThread].start();
		System.out.print("Starting Thread = " + iThread + "\n");
	    }
	    for (int iThread=0; iThread<nbThread; iThread++) {
		try {
		    ArrThr[iThread].join();
		    System.out.print("Joining Thread = " + iThread + "\n");
		}
		catch (InterruptedException e) {
		    System.out.println("Exception while joining iThread=" + iThread);
		}
	    }
	    
	}
	catch(IOException e) {
	    System.out.println("Error during IO operations");
	}
    }

}
