import java.util.ArrayList;
import java.lang.Float;
import java.io.IOException;
import java.util.Random;
import java.util.HashMap;
import java.util.ConcurrentModificationException;

public class Standalone_HashMap2 {

    public static void main(String[] args) {
	try {
	    int argv=args.length;
	    if (argv != 2) {
		System.out.print("Program is run as\n");
		System.out.print("java Standalone_HashMap2 [nbThread] [nbMult]\n");
		throw new IOException("Number of arguments should be 2");
	    }
	    String strNbThread=args[0];
	    String strNbMult=args[1];
	    int nbThread=Integer.parseInt(strNbThread);
	    int nbMult=Integer.parseInt(strNbMult);
	    System.out.print("nbThread = " + nbThread + " nbMult=" + nbMult + "\n");

	    HashMap<String,Integer> map = new HashMap<String,Integer>();
	    int nb=nbMult * nbThread;
	    for (int i=0; i<nb; i++) {
		Integer nInt = i;
		String nStr = Integer.toString(i);
		map.put(nStr, nInt);
	    }
	    //
	    Thread[] ArrThr = new Thread[nbThread];
	    for (int iThread=0; iThread<nbThread; iThread++) {
		ArrThr[iThread] = new Thread(new Runnable() {
			private Integer myThread;
			public Runnable init(Integer jThread) {
			    this.myThread = jThread;
			    return this;
			}
			public void run() {
			    for (int iMult=0; iMult<nbMult; iMult++) {
				int pos = myThread + iMult * nbThread;
				String posStr = Integer.toString(pos);
				map.remove(posStr);
			    }
			}
		    }.init(iThread));
		System.out.print("Assigning Thread = " + iThread + "\n");
	    }
	    for (int iThread=0; iThread<nbThread; iThread++) {
		ArrThr[iThread].start();
		System.out.print("Starting Thread = " + iThread + "\n");
	    }
	    for (int iThread=0; iThread<nbThread; iThread++) {
		try {
		    ArrThr[iThread].join();
		    System.out.print("Joining Thread = " + iThread + "\n");
		}
		catch (InterruptedException e) {
		    System.out.println("Exception while joining iThread=" + iThread);
		}
	    }
	    
	}
	catch(IOException e) {
	    System.out.println("Error during IO operations");
	}
	catch(ConcurrentModificationException e) {
	    System.out.println("Concurrent exception in the HashMap. Proof of concept");
	}
    }

}
