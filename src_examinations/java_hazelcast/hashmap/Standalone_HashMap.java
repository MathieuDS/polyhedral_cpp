import java.util.ArrayList;
import java.lang.Float;
import java.io.IOException;
import java.util.Random;
import java.util.HashMap;
import java.util.ConcurrentModificationException;

public class Standalone_HashMap {

    public static void main(String[] args) {
	try {
	    int argv=args.length;
	    if (argv != 1) {
		System.out.print("Number of arguments should be 1\n");
		throw new IOException("Number of arguments should be 2");
	    }
	    String strNbThread=args[0];
	    int nbThread=Integer.parseInt(strNbThread);
	    System.out.print("nbThread = " + nbThread + "\n");

	    HashMap<Integer,String> map = new HashMap<Integer,String>();
	    //
	    Thread[] ArrThr = new Thread[nbThread];
	    for (int iThread=0; iThread<nbThread; iThread++) {
		ArrThr[iThread] = new Thread(new Runnable() {
			private Integer myThread;
			public Runnable init(Integer iVal) {
			    this.myThread = iVal;
			    return this;
			}
			public void run() {
			    Random rand = new Random();
			    while(true) {
				int n=rand.nextInt(50) + 1;
				Integer nInt = n;
				String nStr = Integer.toString(n);
				map.put(nInt, nStr);
				System.out.print("myThread=" + myThread + " nInt=" + nInt + " nStr=" + nStr + "\n");
				//
			    }
			}
		    }.init(iThread));
		System.out.print("Assigning Thread = " + iThread + "\n");
	    }
	    for (int iThread=0; iThread<nbThread; iThread++) {
		ArrThr[iThread].start();
		System.out.print("Starting Thread = " + iThread + "\n");
	    }
	    for (int iThread=0; iThread<nbThread; iThread++) {
		try {
		    ArrThr[iThread].join();
		    System.out.print("Joining Thread = " + iThread + "\n");
		}
		catch (InterruptedException e) {
		    System.out.println("Exception while joining iThread=" + iThread);
		}
	    }
	    
	}
	catch(IOException e) {
	    System.out.println("Error during IO operations");
	}
	catch(ConcurrentModificationException e) {
	    System.out.println("Concurrent exception in the HashMap. Proof of concept");
	}
    }

}
