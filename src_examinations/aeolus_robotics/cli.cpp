#include "FundamentalIncludes.h"
#include "Parsing.h"
#include "Filling.h"


void DISPLAY_IMAGE(const cv::Mat &image) {
  std::string win_name = "Input";
  cv::namedWindow(win_name, cv::WINDOW_AUTOSIZE);
  cv::imshow(win_name, image);
  cv::waitKey(0);
}


void DISPLAY_PIXELS(const cv::Mat &image)
{
  std::string win_name = "Pixels";
  cv::namedWindow(win_name, cv::WINDOW_AUTOSIZE);
  cv::imshow(win_name, image);
  cv::waitKey(0);
}


void SAVE_PIXELS(std::string const& outFile, cv::Mat const& image)
{
  try {
    imwrite(outFile, image);
  }
  catch (...) {
    std::cerr << "Some error happened while trying to save image to outFile=" << outFile << "\n";
    std::cerr << "Please correct\n";
    throw TerminalException{1};
  }
}


int main(int argc, char **argv) {
  try {
    if (argc < 9) {
      std::cerr << "cli is used as\n";
      std::cerr << "\n";
      std::cerr << "cli <Image_Path> <Output_Path> <MethodRegion> <MethodPerimeter> <ErrorSimilarity> <x=a> <y=b> <meth=c> ..... <x=a> <y=b> <meth=c>\n";
      std::cerr << "That is we should have\n";
      std::cerr << "Image_Path as input figure\n";
      std::cerr << "Output_Path as output figure\n";
      std::cerr << "MethodRegion as integer (for example 1 or 2)\n";
      std::cerr << "MethodPerimeter as integer (for example 1 or 2)\n";
      std::cerr << "ErrorSimilarity as the maximum error for the color distance (say 20)\n";
      std::cerr << "\n";
      std::cerr << "Then this is followed by a list of points to put and method to apply serially.\n";
      std::cerr << "That is first x\n";
      std::cerr << "Then y\n";
      std::cerr << "And finally the method which can be 1 (for selecting region) or 2 (for selecting perimeter)\n";
      return -1;
    }
    ParsedInputFields eInput = ParseTheInput(argc, argv);
    std::cerr << "Input parsed\n";
    cv::Mat image;
    try {
      image = cv::imread(eInput.FileIn, 1);
    }
    catch (...) {
      std::cerr << "Unable to read FileIn=" << eInput.FileIn << "\n";
    }
    std::cerr << "After reading of image\n";
    //    
    if (!image.data) {
      printf("No image data (warning: OpenCV recognize files by extensions)\n");
      return -1;
    }
    //    
    std::cerr << "Before display of image\n";
    DISPLAY_IMAGE(image);
    std::cerr << "After display of image\n";
    //
    cv::Mat outPixels = APPLY_SEQUENCE_OPERATION(image, eInput.GraverBasisRegion, eInput.GraverBasisPerimeter, eInput.MaximumError, eInput.ListOper);
    std::cerr << "Now we have outPixels\n";
    //
    DISPLAY_PIXELS(outPixels);
    std::cerr << "After Display_pixels\n";
    //
    SAVE_PIXELS(eInput.FileOut, outPixels);
    std::cerr << "After Save_pixels\n";
  }
  catch (TerminalException const& e) {
    return e.eVal;
  }
  std::cerr << "Normal termination of the program cli\n";
  return 0;
}
