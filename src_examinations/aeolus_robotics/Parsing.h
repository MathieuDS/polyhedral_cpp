#ifndef PARSING_H
#define PARSING_H

#include "FundamentalIncludes.h"

struct SingleOper {
  int x;
  int y;
  int TheOper; // 1 for region, 2 for perimeter
};



struct ParsedInputFields {
  std::string FileIn;
  std::string FileOut;
  std::vector<int> GraverBasisRegion; // Method used for filling the zone
  std::vector<int> GraverBasisPerimeter; // Method used for computing the perimeter
  double MaximumError;
  //
  std::vector<SingleOper> ListOper;
};


ParsedInputFields ParseTheInput(int argc, char* argv[]);

#endif
