#ifndef INCLUDE_REGION_PERIMETER_H
#define INCLUDE_REGION_PERIMETER_H
#include "Parsing.h"



struct FundRegion {
  int nRows;
  int nCols;
  std::vector<int> ListPos;
};


FundRegion FIND_REGION(cv::Mat const& Mat, int const& x, int const& y, std::vector<int> const& GraverBasis, cv::Vec3b const& RGBrefcol, int const& optColor, int const& MaxDist);


FundRegion FIND_PERIMETER(FundRegion const& eRegion, std::vector<int> const& GraverBasis);


FundRegion FIND_SMOOTH_PERIMETER(FundRegion const& eRegion, std::vector<int> const& GraverBasis);


cv::Mat APPLY_SEQUENCE_OPERATION(cv::Mat const& Mat, std::vector<int> const& GraverBasisRegion, std::vector<int> const& GraverBasisPerimeter, double const& MaximumDistance, std::vector<SingleOper> const& ListOper);


// internal functions


FundRegion STEP_EXPANSION(FundRegion const& eRegion, std::vector<int> const& GraverBasis);

FundRegion STEP_REDUCTION(FundRegion const& eRegion, std::vector<int> const& GraverBasis);

FundRegion REMOVE_SMALL_CONN_COMPONENTS(FundRegion const& eRegion, std::vector<int> const& GraverBasis, int const& MinSize);


FundRegion FIND_SMOOTH_PERIMETER_KERNEL(FundRegion const& eRegion, std::vector<int> const& GraverBasis, int const& MinLenSegment, int const& SmoothLen);





#endif
