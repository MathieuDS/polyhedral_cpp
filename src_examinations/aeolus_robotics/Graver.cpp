#include "Graver.h"

std::vector<int> GetGraverBasis(int const& opt)
{
  if (opt == 1) {
    return {1,0 , -1,0 , 0,1 , 0,-1};
  }
  if (opt == 2) {
    return {1,0 , -1,0 , 0,1 , 0,-1 , 1,1 , 1,-1 , -1,1 , -1,-1};
  }
  if (opt == 3) {
    return {1,0 , -1,0 , 0,1  , 0,-1  ,
	    1,1 , 1,-1 , -1,1 , -1,-1 ,
	    1,2 , -1,2 , 1,-2 , -1,-2 ,
            2,1 , 2,-1 , -2,1 , -2,-1};
  }
  if (opt == 4) {
    return {1,0 , -1,0 , 0,1  , 0,-1  ,
	    1,1 , 1,-1 , -1,1 , -1,-1 ,
	    1,2 , -1,2 , 1,-2 , -1,-2 ,
            2,1 , 2,-1 , -2,1 , -2,-1 ,
            0,2 , 0,-2 , 2,0  , -2,0};
  }
  std::cerr << "The allowed choices for the set of Graver basis are 1, 2, 3 and 4\n";
  std::cerr << "Here the value that you choose is opt="	<< opt << "\n";
  throw TerminalException{1};
}
