#include "Parsing.h"
#include "Graver.h"


ParsedInputFields ParseTheInput(int argc, char* argv[])
{
  std::string FileIn  = argv[1];
  std::string FileOut = argv[2];
  //
  int ChoiceMethodRegion;
  int ChoiceMethodPerimeter;
  std::istringstream(argv[3]) >> ChoiceMethodRegion;
  std::istringstream(argv[4]) >> ChoiceMethodPerimeter;
  std::vector<int> GraverBasisRegion    = GetGraverBasis(ChoiceMethodRegion);
  std::vector<int> GraverBasisPerimeter = GetGraverBasis(ChoiceMethodPerimeter);
  //
  double MaximumError;
  std::istringstream(std::string(argv[5])) >> MaximumError;
  //
  std::vector<SingleOper> ListOper;
  //
  int res=argc % 3;
  if (res != 0) {
    std::cerr << "The number of arguments should be divisible by 3\n";
    throw TerminalException{1};
  }
  int nbOper = (argc - 6) / 3;
  for (int iOper=0; iOper<nbOper; iOper++) {
    std::istringstream is4(argv[6 + iOper * 3]);
    std::istringstream is5(argv[6 + iOper * 3 + 1]);
    std::istringstream is6(argv[6 + iOper * 3 + 2]);
    //
    int x, y, oper;
    is4 >> x;
    is5 >> y;
    is6 >> oper;
    if (oper < 1 && oper > 6) {
      std::cerr << "Only two possibilities for oper between 1 and 6\n";
      std::cerr << "here we have oper=" << oper << " please correct\n";
      throw TerminalException{1};
    }
    ListOper.push_back({x,y,oper});
  }
  return {FileIn, FileOut, GraverBasisRegion, GraverBasisPerimeter, MaximumError, ListOper};
}
