#ifndef INCLUDE_COLOR_H
#define INCLUDE_COLOR_H

#include "FundamentalIncludes.h"

double ComputeEuclideanColorDistance(cv::Vec3b const& col1, cv::Vec3b const& col2);

#endif
