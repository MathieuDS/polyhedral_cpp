#ifndef FUNDAMENTAL_INCLUDES_H
#define FUNDAMENTAL_INCLUDES_H

// Standard include statements for I/O.
#include <stdio.h>
#include <string>

// IO stream
#include <fstream>
#include <iostream>
#include <sstream>


// Standard containers
#include <vector>


// OpenCV library fundamentals.
#include <opencv2/opencv.hpp>
//using namespace cv;


// Some local definitions for the output.
struct TerminalException {
  int eVal;
};

#endif
