#include "FundamentalIncludes.h"
#include "Filling.h"
#include "Color.h"

//
FundRegion FIND_REGION(cv::Mat const& mat, int const& x, int const& y, std::vector<int> const& GraverBasis, cv::Vec3b const& RGBrefcol, int const& optColor, double const& MaxDist)
{
  int nRows=mat.rows;
  int nCols=mat.cols;
  if (x < 0 || x >= nRows || y < 0 || y >= nCols) {
    std::cerr << "We have nRows=" << nRows << " nCols=" << nCols << "\n";
    std::cerr << "But the point x=" << x << " y=" << y << " is outside of the allowed range\n";
    throw TerminalException{1};
  }
  //
  int sizBasis=GraverBasis.size()/2;
  std::vector<int> ListPoint, ListPointNew;
  std::vector<int> FinalListPoint;
  std::vector<int> StatusVect(nRows*nCols, 1);
  auto FuncInsert=[&](int const& ThePt) -> void {
    FinalListPoint.push_back(ThePt);
    StatusVect[ThePt] = 0;
    //    std::cerr << "Inserting ThePt=" << ThePt << "\n";
  };
  int ePt=x * nCols + y;
  ListPoint = {ePt};
  FuncInsert(ePt);
  while(true) {
    for (auto & fPt : ListPoint) {
      int eY = fPt % nCols;
      int eX = (fPt - eY) / nCols;
      //      std::cerr << "eX=" << eX << " eY=" << eY << "\n";
      cv::Vec3b eColor = mat.at<cv::Vec3b>(eX, eY);
      for (int u=0; u<sizBasis; u++) {
	int eXnew = eX + GraverBasis[2*u];
	int eYnew = eY + GraverBasis[2*u + 1];
	//	std::cerr << "eXnew=" << eXnew << " eYnew=" << eYnew << "\n";
	if (eXnew >= 0 && eYnew >= 0 && eXnew < nRows && eYnew < nCols) {

	  cv::Vec3b eColorNew = mat.at<cv::Vec3b>(eXnew, eYnew);
	  double eDist;
	  if (optColor == 1) {
	    eDist = ComputeEuclideanColorDistance(eColorNew, RGBrefcol);
	  }
	  else {
	    eDist = ComputeEuclideanColorDistance(eColorNew, eColor);
	  }
	  //	  std::cerr << "eDist=" << eDist << "\n";
	  if (eDist < MaxDist) {
	    int NewPt = eXnew * nCols + eYnew;
	    if (StatusVect[NewPt] == 1) {
	      FuncInsert(NewPt);
	      ListPointNew.push_back(NewPt);
	    }
	  }
	}
      }
    }
    //    std::cerr << "|ListPointNew|=" << ListPointNew.size() << "\n";
    if (ListPointNew.size() == 0) {
      break;
    }
    ListPoint = ListPointNew;
    ListPointNew.clear();
    //
  }
  std::cerr << "FIND_REGION : nRows=" << nRows << " nCols=" << nCols << "  sizBasis=" << sizBasis << "  |FinalListPoint|=" << FinalListPoint.size() << "\n";
  return {nRows, nCols, FinalListPoint};
}




/*! \brief This function takes a region as input and returns the perimeter
 *
 *  We use linear time algorithm for doing this computation.
 *  The computation relies on Graver basis set of vectors for finding adjacent points.
 *  This method works for 
 */
FundRegion FIND_PERIMETER(FundRegion const& eRegion, std::vector<int> const& GraverBasis)
{
  int nRows=eRegion.nRows;
  int nCols=eRegion.nCols;
  std::vector<int> ListReturn;
  std::vector<int> ListStatus(nRows*nCols, 0);
  for (auto & ePt : eRegion.ListPos)
    ListStatus[ePt]=1;
  int sizBasis=GraverBasis.size()/2;
  for (auto & ePt : eRegion.ListPos) {
    bool IsPerimeterPoint=false;
    int eY = ePt % nCols;
    int eX = (ePt - eY) / nCols;
    for (int u=0; u<sizBasis; u++) {
      int eXnew = eX + GraverBasis[2*u];
      int eYnew = eY + GraverBasis[2*u + 1];
      if (eXnew >= 0 && eYnew >= 0 && eXnew < nRows && eYnew < nCols) {
	int NewPt = eXnew * nCols + eYnew;
	if (ListStatus[NewPt] == 0)
	  IsPerimeterPoint=true;
      }
    }
    if (IsPerimeterPoint)
      ListReturn.push_back(ePt);
  }
  std::cerr << "FIND_PERIMETER  |eRegion.ListPos|=" << eRegion.ListPos.size() << "   |ListReturn|=" << ListReturn.size() << "\n";
  return {nRows, nCols, ListReturn};
}


/*! \brief This function looks at the Region put in argument 
 *         and returns the region obtained by adding point at distance 1 from it
 *
 *  We use linear time algorithm for doing this computation
 *  The computation relies on Graver basis set of vectors for finding which are in the same component.
 *  The interest of it is that it removes holes in the picture.
 */
FundRegion STEP_EXPANSION(FundRegion const& eRegion, std::vector<int> const& GraverBasis)
{
  int nRows=eRegion.nRows;
  int nCols=eRegion.nCols;
  std::vector<int> ListStatus(nRows*nCols, 0);
  for (auto & ePt : eRegion.ListPos)
    ListStatus[ePt]=1;
  int sizBasis=GraverBasis.size()/2;
  for (auto & ePt : eRegion.ListPos) {
    int eY = ePt % nCols;
    int eX = (ePt - eY) / nCols;
    for (int u=0; u<sizBasis; u++) {
      int eXnew = eX + GraverBasis[2*u];
      int eYnew = eY + GraverBasis[2*u + 1];
      if (eXnew >= 0 && eYnew >= 0 && eXnew < nRows && eYnew < nCols) {
	int NewPt = eXnew * nCols + eYnew;
	ListStatus[NewPt]=1;
      }
    }
  }
  std::vector<int> ListReturn;
  for (int iPt=0; iPt<nRows*nCols; iPt++)
    if (ListStatus[iPt] == 1)
      ListReturn.push_back(iPt);
  std::cerr << "STEP_EXPANSION |eRegion.ListPos|=" << eRegion.ListPos.size() << "   |ListReturn|=" << ListReturn.size() << "\n";
  return {nRows, nCols, ListReturn};
}



/*! \brief This function looks at the Region put in argument 
 *         and returns the region obtained by removing the perimeter.
 *
 *  We use linear time algorithm for doing this computation
 *  The computation relies on Graver basis set of vectors for finding which are in the same component.
 *  The interest of it is that it removes holes in the picture.
 */
FundRegion STEP_REDUCTION(FundRegion const& eRegion, std::vector<int> const& GraverBasis)
{
  FundRegion ePerim = FIND_PERIMETER(eRegion, GraverBasis);
  int nRows=eRegion.nRows;
  int nCols=eRegion.nCols;
  int eProd = nRows * nCols;
  //
  std::vector<int> ListStatus(eProd, 0);
  for (auto & ePt : ePerim.ListPos)
    ListStatus[ePt]=1;
  std::vector<int> ListReturn;
  for (auto & ePt : eRegion.ListPos)
    if (ListStatus[ePt] == 0)
      ListReturn.push_back(ePt);
  std::cerr << "STEP_REDUCTION |eRegion.ListPos|=" << eRegion.ListPos.size() << "   |ListReturn|=" << ListReturn.size() << "\n";
  return {nRows, nCols, ListReturn};
}






/*! \brief This function looks at the Region put in argument 
 *         and returns the region obtained in connected components of size at least MinSize
 *
 *  We use linear time algorithm for doing this computation
 *  The computation relies on Graver basis set of vectors for finding which are in the same component.
 *  This eliminates small region that can be interpreted as artifacts of the perimeter process.
 */
FundRegion REMOVE_SMALL_CONN_COMPONENTS(FundRegion const& eRegion, std::vector<int> const& GraverBasis, int const& MinSize)
{
  int nRows=eRegion.nRows;
  int nCols=eRegion.nCols;
  int eProd=nRows * nCols;
  
  int sizReg=eRegion.ListPos.size();
  std::vector<int> MappingPoint(eProd,-1);
  for (int iPt=0; iPt<sizReg; iPt++) {
    int ePt=eRegion.ListPos[iPt];
    MappingPoint[ePt] = iPt;
  }
  std::vector<int> ListStatusConn(sizReg, -1);
  int sizBasis=GraverBasis.size()/2;
  int iRegion=0;
  int StartPoint=0;
  std::vector<int> RegionStatus;
  int nbRegionOK=0;
  while(true) {
    int FindPoint=-1;
    while(true) {
      if (ListStatusConn[StartPoint] == -1) {
	FindPoint = StartPoint;
	break;
      }
      StartPoint++;
      if (StartPoint == sizReg)
	break;
    }
    if (FindPoint == -1)
      break;
    int sizConn=0;
    //    std::vector<int> ListPointConn;
    auto SetPoint=[&](int const& ePt) -> void {
      ListStatusConn[ePt] = iRegion;
      //      ListPointConn.push_back(ePt);
      sizConn++;
    };
    SetPoint(FindPoint);
    std::vector<int> ListPoint, ListPointNew;
    ListPoint.push_back(FindPoint);
    while(true) {
      for (auto & iPt : ListPoint) {
	int ePt = eRegion.ListPos[iPt];
	int eY = ePt % nCols;
	int eX = (ePt - eY) / nCols;
	for (int u=0; u<sizBasis; u++) {
	  int eXnew = eX + GraverBasis[2*u];
	  int eYnew = eY + GraverBasis[2*u + 1];
	  if (eXnew >= 0 && eYnew >= 0 && eXnew < nRows && eYnew < nCols) {
	    int NewPt = eXnew * nCols + eYnew;
	    int eMap = MappingPoint[NewPt];
	    if (eMap != -1) {
	      if (ListStatusConn[eMap] == -1) {
		SetPoint(eMap);
		ListPointNew.push_back(eMap);
	      }
	    }
	  }
	}
      }
      if (ListPointNew.size() == 0)
	break;
      ListPoint = ListPointNew;
      ListPointNew.clear();
    }
    int eVal=1;
    if (sizConn < MinSize) {
      eVal=0;
    }
    nbRegionOK += eVal;
    if (eVal == 1) {
      std::cerr << "iRegion=" << iRegion << " sizConn=" << sizConn << "\n";
    }
    RegionStatus.push_back(eVal);
    iRegion++;
  }
  std::cerr << "iRegion=" << iRegion << " nbRegionOK=" << nbRegionOK << "\n";
  std::vector<int> ListReturn;
  for (int iPt=0; iPt<sizReg; iPt++) {
    int iRegion = ListStatusConn[iPt];
    int eVal = RegionStatus[iRegion];
    if (eVal == 1)
      ListReturn.push_back(eRegion.ListPos[iPt]);
  }
  std::cerr << "REMOVE_SMALL_CONN_COMPONENTS    [ListPos|=" << sizReg << " |ListReturn|=" << ListReturn.size() << "\n";
  return {nRows, nCols, ListReturn};
}




/*! \brief This function looks at the Region put in argument 
 *         and returns a smoothed version of the boundary.
 *
 *  We use linear time algorithm for doing this computation
 *  The computation relies on Graver basis set of vectors for finding which are in the same component.
 *
 *  The difficulty of the computation is that we have a boundary and we need to identify the 1-dimensional
 *  line in order to get the boundary and apply stuff like Catmull-Rom as recommended.
 *  The boundary does not have to be a simple line. It can have a loop, and another loop attached
 *  This is the signification of test3.png
 */
FundRegion FIND_SMOOTH_PERIMETER_KERNEL(FundRegion const& eRegion, std::vector<int> const& GraverBasis, int const& MinLenSegment, int const& SmoothLen)
{
  int nRows=eRegion.nRows;
  int nCols=eRegion.nCols;
  int eProd=nRows * nCols;
  //
  int sizReg=eRegion.ListPos.size();
  int sizBasis=GraverBasis.size()/2;
  std::vector<int> ListStatusConn(sizReg, -1);
  std::vector<int> MappingPoint(eProd,-1);
  for (int iPt=0; iPt<sizReg; iPt++) {
    int ePt=eRegion.ListPos[iPt];
    MappingPoint[ePt] = iPt;
  }
  //
  int StartPoint=0;
  int iRegion=0;
  struct InfoPoint {
    int ePoint;
    int ePreceding;
    int eAge;
  };
  struct ResultProcessConnected {
    std::vector<InfoPoint> ListInfoPoint;
    std::vector<int> MappingInfo;
    int Oldest;
  };
  std::vector<int> PointStatus(eProd,0);
  auto FuncInsertSegment=[&](std::vector<int> const& TheSegment) -> void {
    int len=TheSegment.size();
    for (int i=0; i<len; i++) {
      double eSumX=0;
      double eSumY=0;
      for (int iShift=-SmoothLen; iShift<=SmoothLen; iShift++) {
	int j=i + iShift;
	if (j < 0)
	  j += len;
	if (j >= len)
	  j -= len;
	int iPt=TheSegment[j];
	int ePt = eRegion.ListPos[iPt];
	int eY = ePt % nCols;
	int eX = (ePt - eY) / nCols;
	//
	eSumX += double(eX);
	eSumY += double(eY);
      }
      double posX_d=eSumX / double(1 + 2 * SmoothLen);
      double posY_d=eSumY / double(1 + 2 * SmoothLen);
      int posX_i = int(round(posX_d));
      int posY_i = int(round(posY_d));
      if (posX_i < 0) posX_i=0;
      if (posX_i >= nRows) posX_i = nRows-1;
      if (posY_i < 0) posY_i=0;
      if (posY_i >= nRows) posY_i = nCols-1;
      int NewPt = posX_i * nCols + posY_i;
      PointStatus[NewPt]=1;
    }
  };

  
  while(true) {
    int FindPoint=-1;
    while(true) {
      if (ListStatusConn[StartPoint] == -1) {
	FindPoint=StartPoint;
	break;
      }
      StartPoint++;
      if (StartPoint == sizReg)
	break;
    }
    if (FindPoint == -1)
      break;
    //
    // This function is the core.
    // It computes connected components and returns the fundamental info from it
    // to be used later on
    // The ListAllowedVector is the vector containing whether or not the vector is allowed.
    // The oldest point is the latest one inserted.
    //
    // Note: The ListStartPoint does not have to be in the allowed points.
    //
    auto GetInformationConnectedProcess=[&](std::vector<int> const& ListStartPoint, std::vector<int> const& ListAllowedVector) -> ResultProcessConnected {
      std::cerr << "Entering GetInformationConnectedProcess\n";
      int iterAge=0;
      std::vector<InfoPoint> ListInfoPoint;
      std::vector<int> LocalStatusConn(sizReg,1);
      std::vector<int> MappingInfo(sizReg,-1);
      int Oldest=-1;
      int sizListInfo=0;
      auto SetPoint=[&](int const& iPt, int const& iPrec) -> void {
	ListInfoPoint.push_back({iPt, iPrec, iterAge});
	MappingInfo[iPt] = sizListInfo;
	LocalStatusConn[iPt]=0;
	Oldest = iPt;
	sizListInfo++;
      };
      std::vector<int> ListPoint, ListPointNew;
      for (auto & eVal : ListStartPoint) {
	SetPoint(eVal, -1);
	ListPoint.push_back(eVal);
      }
      
      while(true) {
	iterAge++;
	//	std::cerr << "iterAge=" << iterAge << " |ListPoint|=" << ListPoint.size() << "\n";
	for (auto & iPt : ListPoint) {
	  int ePt = eRegion.ListPos[iPt];
	  int eY = ePt % nCols;
	  int eX = (ePt - eY) / nCols;
	  for (int u=0; u<sizBasis; u++) {
	    int eXnew = eX + GraverBasis[2*u];
	    int eYnew = eY + GraverBasis[2*u + 1];
	    if (eXnew >= 0 && eYnew >= 0 && eXnew < nRows && eYnew < nCols) {
	      int NewPt = eXnew * nCols + eYnew;
	      int eMap = MappingPoint[NewPt];
	      if (eMap != -1) {
		if (LocalStatusConn[eMap] == 1 && ListAllowedVector[eMap] == 1) {
		  SetPoint(eMap, iPt);
		  ListPointNew.push_back(eMap);
		}
	      }
	    }
	  }
	}
	//	std::cerr << "|ListPointNew|=" << ListPointNew.size() << "\n";
	if (ListPointNew.size() == 0)
	  break;
	ListPoint = ListPointNew;
	ListPointNew.clear();
      }
      std::cerr << "End GetInformationConnectedProcess iterAge=" << iterAge << " Oldest=" << Oldest << " |ListInfoPoint|=" << ListInfoPoint.size() << "\n";
      return {ListInfoPoint, MappingInfo, Oldest};
    };
    auto GetSequence=[&](ResultProcessConnected const& eRes) -> std::vector<int> {
      int ePt=eRes.Oldest;
      std::vector<int> ListPoint;
      while(true) {
	ListPoint.push_back(ePt);
	//
	int pos=eRes.MappingInfo[ePt];
	if (eRes.ListInfoPoint[pos].ePoint != ePt) {
	  std::cerr << "A clear bug by any mean\n";
	  throw TerminalException{1};
	}
	ePt=eRes.ListInfoPoint[pos].ePreceding;
	if (ePt == -1)
	  break;
      }
      return ListPoint;
    };
    //
    // First mark the connected component as done.
    //
    std::cerr << "\n\n";
    std::cerr << "iRegion=" << iRegion << "\n";
    std::vector<int> ListAllowedVector(sizReg,1);
    std::vector<int> ListStartPoint{FindPoint};
    ResultProcessConnected eRes = GetInformationConnectedProcess(ListStartPoint, ListAllowedVector);
    
    for (auto & eInfo : eRes.ListInfoPoint)
      ListStatusConn[eInfo.ePoint] = iRegion;
    std::cerr << "We have eRes, setting up ListStatusConn entries\n";
    //
    // Then do an iteration adding lines one by one.
    //
    std::vector<std::vector<int>> ListListSegment;
    while(true) {
      ResultProcessConnected eRes1 = GetInformationConnectedProcess(ListStartPoint, ListAllowedVector);
      std::cerr << "We have eRes1, setting up ListStatusConn entries\n";
      std::vector<int> eSeg1 = GetSequence(eRes1);
      std::cerr << "|eSeg1|=" << eSeg1.size() << "\n";
      if (int(eSeg1.size()) > MinLenSegment) {
	for (auto & ePt : eSeg1) {
	  ListAllowedVector[ePt] = 0;
	}
	int TheOldest = eRes1.Oldest;
	ListAllowedVector[TheOldest] = 1; // We reset this point as allowed.
	ResultProcessConnected eRes2 = GetInformationConnectedProcess(ListStartPoint, ListAllowedVector);
	std::cerr << "We have eRes2\n";
	if (eRes2.Oldest == TheOldest) {
	  // Joining the two paths together
	  int len1=eSeg1.size();
	  std::vector<int> NewSegment(len1);
	  for (int i1=0; i1<len1; i1++) {
	    int j1=len1-1-i1;
	    NewSegment[i1] = eSeg1[j1];
	  }
	  std::vector<int> eSeg2 = GetSequence(eRes2);
	  std::cerr << "|eSeg2|=" << eSeg2.size() << "\n";
	  for (auto & ePt : eSeg2)
	    NewSegment.push_back(ePt);
	  FuncInsertSegment(NewSegment);
	  for (auto & ePt : NewSegment) {
	    ListAllowedVector[ePt] = 0;
	  }
	}
	else {
	  FuncInsertSegment(eSeg1);
	}
	ListStartPoint.clear();
	for (int iPt=0; iPt<sizReg; iPt++)
	  if (ListAllowedVector[iPt] == 0)
	    ListStartPoint.push_back(iPt);
      }
      else {
	break;
      }
    }
    iRegion++;
  }

  std::vector<int> ListReturn;
  for (int iPt=0; iPt<eProd; iPt++)
    if (PointStatus[iPt] == 1)
      ListReturn.push_back(iPt);
  return {nRows, nCols, ListReturn};
}




FundRegion FIND_SMOOTH_PERIMETER(FundRegion const& eRegion, std::vector<int> const& GraverBasis)
{
  FundRegion eFund1=STEP_EXPANSION(eRegion, GraverBasis);
  FundRegion eFund2=STEP_REDUCTION(eFund1, GraverBasis);
  FundRegion eFund3=FIND_PERIMETER(eFund2, GraverBasis);
  int MinSizeConn=30;
  FundRegion eFund4=REMOVE_SMALL_CONN_COMPONENTS(eFund3, GraverBasis, MinSizeConn);
  int MinLenSegment=10;
  int SmoothLen=5;
  FundRegion eFund5=FIND_SMOOTH_PERIMETER_KERNEL(eFund4, GraverBasis, MinLenSegment, SmoothLen);
  FundRegion eFund6=REMOVE_SMALL_CONN_COMPONENTS(eFund5, GraverBasis, MinSizeConn);
  return eFund6;
}







cv::Mat APPLY_SEQUENCE_OPERATION(cv::Mat const& Mat, std::vector<int> const& GraverBasisRegion, std::vector<int> const& GraverBasisPerimeter, double const& MaxDist, std::vector<SingleOper> const& ListOper)
{
  std::vector<int> ListPoint;
  int optColor=1;
  for (auto & eOper : ListOper) {
    int eX = eOper.x;
    int eY = eOper.y;
    cv::Vec3b eRGB = Mat.at<cv::Vec3b>(eX, eY);
    std::cerr << "eOper.TheOper=" << eOper.TheOper << "\n";
    if (eOper.TheOper == 1) {
      FundRegion eFund1=FIND_REGION(Mat, eX, eY, GraverBasisRegion, eRGB, optColor, MaxDist);
      for (auto & ePt : eFund1.ListPos)
	ListPoint.push_back(ePt);
    }
    if (eOper.TheOper == 2) {
      FundRegion eFund1=FIND_REGION(Mat, eX, eY, GraverBasisRegion, eRGB, optColor, MaxDist);
      FundRegion eFund2=FIND_PERIMETER(eFund1, GraverBasisPerimeter);
      for (auto & ePt : eFund2.ListPos)
	ListPoint.push_back(ePt);
    }
    if (eOper.TheOper == 3) {
      FundRegion eFund1=FIND_REGION(Mat, eX, eY, GraverBasisRegion, eRGB, optColor, MaxDist);
      FundRegion eFund5=FIND_SMOOTH_PERIMETER(eFund1, GraverBasisRegion);
      for (auto & ePt : eFund5.ListPos)
	ListPoint.push_back(ePt);
    }
    if (eOper.TheOper == 4) {
      FundRegion eFund1=FIND_REGION(Mat, eX, eY, GraverBasisRegion, eRGB, optColor, MaxDist);
      FundRegion eFund2=STEP_EXPANSION(eFund1, GraverBasisRegion);
      for (auto & ePt : eFund2.ListPos)
	ListPoint.push_back(ePt);
    }
    if (eOper.TheOper == 5) {
      FundRegion eFund1=FIND_REGION(Mat, eX, eY, GraverBasisRegion, eRGB, optColor, MaxDist);
      FundRegion eFund2=STEP_EXPANSION(eFund1, GraverBasisRegion);
      FundRegion eFund3=FIND_PERIMETER(eFund2, GraverBasisPerimeter);
      for (auto & ePt : eFund3.ListPos)
	ListPoint.push_back(ePt);
    }
    if (eOper.TheOper == 6) {
      FundRegion eFund1=FIND_REGION(Mat, eX, eY, GraverBasisRegion, eRGB, optColor, MaxDist);
      FundRegion eFund2=STEP_EXPANSION(eFund1, GraverBasisRegion);
      FundRegion eFund3=FIND_PERIMETER(eFund2, GraverBasisPerimeter);
      FundRegion eFund4=REMOVE_SMALL_CONN_COMPONENTS(eFund3, GraverBasisRegion, 30);
      for (auto & ePt : eFund4.ListPos)
	ListPoint.push_back(ePt);
    }
  }
  //
  cv::Mat retMat = Mat;
  int nRows = Mat.rows;
  int nCols = Mat.cols;
  std::cerr << "APPPLY_SEQUENCE_OPERATION : nRows=" << nRows << " nCols=" << nCols << "\n";
  cv::Vec3b pBlack(0,0,0);
  cv::Vec3b pBlue(255,0,0);
  for (int iRow=0; iRow<nRows; iRow++)
    for (int iCol=0; iCol<nCols; iCol++)
      retMat.at<cv::Vec3b>(iRow,iCol) = pBlack;
  for (auto & ePt : ListPoint) {
    int eY = ePt % nCols;
    int eX = (ePt - eY) / nCols;
    retMat.at<cv::Vec3b>(eX, eY) = pBlue;
  }
  return retMat;
}


