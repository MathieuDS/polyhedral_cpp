#ifndef INCLUDE_COLOR_H
#define INCLUDE_COLOR_H

#include "FundamentalIncludes.h"

// This function implements the Euclidean method mentioned in
// https://en.wikipedia.org/wiki/Color_difference
// and
// https://www.compuphase.com/cmetric.htm
double ComputeEuclideanColorDistance(cv::Vec3b const& col1, cv::Vec3b const& col2)
{
  double DeltaR = double(col1[2] - col2[2]);
  double DeltaG = double(col1[1] - col2[1]);
  double DeltaB = double(col1[0] - col2[0]);
  double rMid = double(col1[2] + col2[2]) / 2;
  //
  double retVal = 2 * DeltaR * DeltaR + 4 * DeltaG * DeltaG + 3 * DeltaB * DeltaB + rMid * (DeltaR*DeltaR - DeltaB*DeltaB)/256;
  return sqrt(retVal);
}

#endif
