#include <string>
#include <iostream>
#include <utility>
#include <algorithm>
#include <vector>
#include <limits>
#include <unordered_map>


using namespace std;

int solution(vector<int> &A, int X, int Y, int Z) {
 // write your code in C++14 (g++ 6.2.0)
 int max_wait = 0;
 // fta: First time available
 int fta_X = 0;
 int fta_Y = 0;
 int fta_Z = 0;
 int miss_val = std::numeric_limits<int>::max();
 auto update=[&](int const& need) -> bool {
   int fta = miss_val;
   int choice = -1;
   if (need <= X) {
     if (fta_X < fta) {
       fta = fta_X;
       choice = 0;
     }
   }
   if (need <= Y) {
     if (fta_Y < fta) {
       fta = fta_Y;
       choice = 1;
     }
   }
   if (need <= Z) {
     if (fta_Z < fta) {
       fta = fta_Z;
       choice = 2;
     }
   }
   if (fta == miss_val)
     return false;
   if (choice == 0) {
     X -= need;
     max_wait = std::max(max_wait,fta);
     fta_X = max_wait + need;
   }
   if (choice == 1) {
     Y -= need;
     max_wait = std::max(max_wait,fta);
     fta_Y = max_wait + need;
   }
   if (choice == 2) {
     Z -= need;
     max_wait = std::max(max_wait,fta);
     fta_Z = max_wait + need;
   }
   return true;
 };
 for (auto & need : A) {
   bool test = update(need);
   if (!test)
     return -1;
 }
 return max_wait;
}



int main(int argc, char *argv[])
{
  if (argc <= 4) {
    std::cerr << "incomplete input\n";
    return 1;
  }
  int X, Y, Z;
  sscanf(argv[1], "%d", &X);
  sscanf(argv[2], "%d", &Y);
  sscanf(argv[3], "%d", &Z);
  vector<int> V(argc - 4);
  for (int i=4; i<argc; i++) {
    int val;
    sscanf(argv[i], "%d", &val);
    V[i - 4] = val;
  }
  std::cerr << "X=" << X << " Y=" << Y << " Z=" << Z << "\n";
  std::cerr << "V=";
  for (auto & val : V)
    std::cerr << " " << val;
  std::cerr << "\n";
  int max_wait = solution(V, X, Y, Z);
  std::cerr << "max_wait=" << max_wait << "\n";
}
