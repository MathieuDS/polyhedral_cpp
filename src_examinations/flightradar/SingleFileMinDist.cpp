#include "FlightRadar.h"



int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "SingleFileMinDist [File.csv]\n";
    return 0;
  }
  //
  std::string eFile = argv[1];
  std::vector<SinglePos> ListPos = ReadSingleFile(eFile);
  std::vector<TypeDist> ListDist = ComputeMinDist(ListPos);

  PrintingFlightMinDist(std::cout, ListPos, ListDist);
}
