FlightRadar code challenge
==========================

This is the proposed solution to the code challenge.
I had previous experience working on lon/lat coordinates
and on preprocessing file input. The link to relevant
open source code has been put (note that I am the author
of those codes).


Design
------

The implementation is straightforward and was not really optimized
for speed. In particular the algorithm for finding the nearest
flight is quadratic in its complexity which is fine for this
example.

But I put the XYZ coordinate in order to get somewhat better
performance. This is an optimization with erspect to my code.

Processing
----------

The entry 00000000 is I think something to be removed and so
was removed. As requested if some entry showed up again then it
is dropped.


Usage
-----

Use "make" to compile the programs.

Command for running it on a single file:
./SingleFileMinDist 20170901_080942.csv

Command for running on many files:
ls *.csv > ListFile
./MultipleFilesMinDist ListFile

Test
----

A single test.csv file has been put and a reference output is
there. It only tests the SingleFileMinDist code.