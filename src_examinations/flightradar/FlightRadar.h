#ifndef INCLUDE_FLIGHTRADAR_H
#define INCLUDE_FLIGHTRADAR_H

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <limits>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>

// Code for computing the distances
// From https://github.com/MathieuDutSik/ocean_works/blob/master/src_ocean/SphericalGeom.h

double GeodesicDistance(double const& LonDeg1, double const& LatDeg1, double const& LonDeg2, double const& LatDeg2)
{
  double pi=3.141592653589792;
  double lon1=pi*LonDeg1/double(180);
  double lat1=pi*LatDeg1/double(180);
  double x1=cos(lon1)*cos(lat1);
  double y1=sin(lon1)*cos(lat1);
  double z1=sin(lat1);

  double lon2=pi*LonDeg2/double(180);
  double lat2=pi*LatDeg2/double(180);
  double x2=cos(lon2)*cos(lat2);
  double y2=sin(lon2)*cos(lat2);
  double z2=sin(lat2);
  double scalprod=x1*x2+y1*y2+z1*z2;
  if (scalprod > 1)
    return 0;
  else
    return acos(scalprod);
}

double GeodesicDistanceKM(double const& LonDeg1, double const& LatDeg1, double const& LonDeg2, double const& LatDeg2)
{
  double EarthRadius=6370;
  return EarthRadius*GeodesicDistance(LonDeg1, LatDeg1, LonDeg2, LatDeg2);
}


struct XYZpos {
  double x;
  double y;
  double z;
};

XYZpos GetXYZpos(double const& LonDeg, double const& LatDeg)
{
  double pi=3.141592653589792;
  double lon=pi*LonDeg/double(180);
  double lat=pi*LatDeg/double(180);
  double x=cos(lon)*cos(lat);
  double y=sin(lon)*cos(lat);
  double z=sin(lat);
  return {x, y, z};
}




// String functions
// From https://github.com/MathieuDutSik/basic_common_cpp/blob/master/src_basic/Basic_string.h

std::vector<std::string> STRING_Split(std::string const& eStrA, std::string const& eStrB)
{
  size_t lenA=eStrA.length();
  size_t lenB=eStrB.length();
  std::vector<int> ListStatus(lenA,1);
  if (lenA >= lenB) {
    for (size_t iA=0; iA<lenA - lenB; iA++)
      if (ListStatus[iA] == 1) {
        bool IsMatch=true;
        for (size_t iB=0; iB<lenB; iB++) {
          std::string eCharA=eStrA.substr(iA+iB,1);
          std::string eCharB=eStrB.substr(iB,1);
          if (eCharA != eCharB)
            IsMatch=false;
        }
        if (IsMatch)
          for (size_t iB=0; iB<lenB; iB++)
            ListStatus[iA + iB]=0;
      }
  }
  std::vector<std::string> RetList;
  std::string eFound;
  for (size_t iA=0; iA<lenA; iA++) {
    std::string eChar=eStrA.substr(iA, 1);
    if (ListStatus[iA] == 1)
      eFound += eChar;
    if (ListStatus[iA] == 0) {
      size_t siz=eFound.length();
      if (siz > 0)
        RetList.push_back(eFound);
      eFound="";
    }
  }
  size_t siz=eFound.size();
  if (siz > 0)
    RetList.push_back(eFound);
  return RetList;
}






// Code for reading the files

struct SinglePos {
  std::string flightname;
  double lon;
  double lat;
};


std::vector<SinglePos> ReadSingleFile(std::string const& eFile)
{
  std::vector<SinglePos> ListPos;
  //
  std::ifstream is(eFile);
  std::string line;
  int iLine=0;
  std::string drop_entry = "00000000";
  std::unordered_set<std::string> eset;
  while (std::getline(is, line)) {
    if (iLine >= 1) {
      std::vector<std::string> LStr = STRING_Split(line, ",");
      std::string flightname = LStr[0];
      double lat = std::stod(LStr[1]);
      double lon = std::stod(LStr[2]);
      if (flightname != drop_entry && eset.count(flightname) == 0) {
        ListPos.push_back({flightname, lat, lon});
        eset.insert(flightname);
      }
    }
    iLine++;
  }
  //  std::cerr << "iLine=" << iLine << " |ListPos|=" << ListPos.size() << "\n";
  return ListPos;
}

std::vector<std::string> ReadListFile(std::string const& eFile)
{
  std::vector<std::string> ListFile;
  //
  std::ifstream is(eFile);
  std::string line;
  while (std::getline(is, line))
    ListFile.push_back(line);
  std::sort(ListFile.begin(), ListFile.end());
  return ListFile;
}



struct TypeDist {
  int i;
  double dist;
};

std::vector<TypeDist> ComputeMinDist(std::vector<SinglePos> const& ListPos)
{
  int n_pos = ListPos.size();
  std::vector<XYZpos> ListXYZ(n_pos);
  for (int i_pos=0; i_pos<n_pos; i_pos++) {
    ListXYZ[i_pos] = GetXYZpos(ListPos[i_pos].lon, ListPos[i_pos].lat);
  }
  double EarthRadius=6370;
  std::vector<TypeDist> ListDist(n_pos);
  for (int i_pos=0; i_pos<n_pos; i_pos++) {
    double x1 = ListXYZ[i_pos].x;
    double y1 = ListXYZ[i_pos].y;
    double z1 = ListXYZ[i_pos].z;
    //
    double max_scal = std::numeric_limits<double>::min();
    int idx_max = -1;
    //
    for (int j_pos=0; j_pos<n_pos; j_pos++) {
      double x2 = ListXYZ[j_pos].x;
      double y2 = ListXYZ[j_pos].y;
      double z2 = ListXYZ[j_pos].z;
      double scal = x1 * x2 + y1 * y2 + z1 * z2;
      if (i_pos != j_pos) {
        if (scal > max_scal) {
          idx_max = j_pos;
          max_scal = scal;
        }
      }
    }
    double min_dist = 0;
    if (max_scal <= 1)
      min_dist = EarthRadius * acos(max_scal);
    //
    ListDist[i_pos] = {idx_max, min_dist};
  }
  return ListDist;
}


void PrintingFlightMinDist(std::ostream & os, std::vector<SinglePos> const& ListPos, std::vector<TypeDist> const& ListDist)
{
  for (size_t i_pos=0; i_pos<ListPos.size(); i_pos++) {
    size_t j_pos = ListDist[i_pos].i;
    double dist = ListDist[i_pos].dist;
    os << ListPos[i_pos].flightname << " " << ListPos[j_pos].flightname << " " << dist << "\n";
  }
}


//
// Now the code for the computation of the trajectories
//







void PrintingConsecutivePositions(std::ostream & os, std::vector<std::string> const& ListFile)
{
  struct TypePosition {
    double lon;
    double lat;
  };

  // We need to use a map because the entries may change in time.
  std::unordered_map<std::string, TypePosition> CompleteMap;

  for (auto & eFile : ListFile) {
    os << "Processed CSV file `" << eFile << "`:\n";
    // Reading new position and updating
    // It could be new positions, or old ones being updated.
    std::vector<SinglePos> ListPos = ReadSingleFile(eFile);
    for (auto & e_ent : ListPos) {
      TypePosition & ePosition = CompleteMap[e_ent.flightname];
      ePosition.lon = e_ent.lon;
      ePosition.lat = e_ent.lat;
    }
    // Converting to std::vector for computing distance.
    // suboptimal, but ok for the task.

    std::unordered_map<std::string, TypePosition>::const_iterator iter=CompleteMap.begin();
    std::vector<SinglePos> CompleteListPos;
    while (iter != CompleteMap.end() ) {
      CompleteListPos.push_back({iter->first, iter->second.lon, iter->second.lat});
      iter++;
    }
    // Now recomputing the distance with the quadratic algorithm
    std::vector<TypeDist> CompleteListDist = ComputeMinDist(CompleteListPos);

    PrintingFlightMinDist(os, CompleteListPos, CompleteListDist);
  }
}



#endif
