#include "FlightRadar.h"



int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "MultipleFileMinDist [File.csv]\n";
    return 0;
  }
  //
  std::string ListFile_str = argv[1];
  std::vector<std::string> ListFile = ReadListFile(ListFile_str);
  PrintingConsecutivePositions(std::cout, ListFile);
}
