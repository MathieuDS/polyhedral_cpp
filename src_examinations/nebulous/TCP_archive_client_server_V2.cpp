#include <iostream>
#include <boost/asio.hpp>


#include <boost/archive/tmpdir.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/split_free.hpp>




struct TypeIndex {
  int idx1;
  int idx2;
  int idx3;
};

struct TypeSum {
  int idx12;
  int idx23;
};





namespace boost::serialization {

  // TypeCtypeExch
  template<class Archive>
  inline void serialize(Archive & ar, TypeIndex & eTypIdx, const unsigned int version)
  {
    ar & make_nvp("idx1", eTypIdx.idx1);
    ar & make_nvp("idx2", eTypIdx.idx2);
    ar & make_nvp("idx3", eTypIdx.idx3);
  }

  // TypeCtypeExch
  template<class Archive>
  inline void serialize(Archive & ar, TypeSum & eTypSum, const unsigned int version)
  {
    ar & make_nvp("idx12", eTypSum.idx12);
    ar & make_nvp("idx23", eTypSum.idx23);
  }

}


template<typename T>
T read_data(boost::asio::ip::tcp::endpoint & endpoint)
{
  std::cerr << "read_data, step 1\n";
  boost::asio::io_service io_service;
  std::cerr << "read_data, step 2\n";
  boost::asio::ip::tcp::acceptor acceptor(io_service, endpoint);
  std::cerr << "read_data, step 3\n";
  boost::asio::ip::tcp::socket socket(io_service);
  std::cerr << "read_data, step 4\n";
  acceptor.accept(socket);

  std::cerr << "read_data, step 5\n";
  boost::system::error_code error;
  std::cerr << "read_data, step 6\n";
  boost::asio::streambuf receive_buffer;
  std::cerr << "read_data, step 7\n";
  boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
  std::cerr << "read_data, step 8\n";
  if( error && error != boost::asio::error::eof ) {
    std::cout << "receive failed: " << error.message() << "\n";
    exit(1);
  } else {
    std::cerr << "read_data, step 9\n";
    T data;
    std::cerr << "read_data, step 10\n";
    const char* data1 = boost::asio::buffer_cast<const char*>(receive_buffer.data());
    std::cerr << "read_data, step 11\n";
    std::stringstream iss(data1);
    std::cerr << "read_data, step 12\n";
    boost::archive::text_iarchive ia(iss);
    std::cerr << "read_data, step 13\n";
    ia >> data;
    std::cerr << "read_data, step 14\n";
    return data;
  }
}




template<typename T>
void send_data(boost::asio::ip::tcp::endpoint & endpoint, const T& data)
{
  std::cerr << "send_data, step 1\n";
  boost::asio::io_service io_service;
  std::cerr << "send_data, step 2\n";
  boost::asio::ip::tcp::socket socket(io_service);
  std::cerr << "send_data, step 3\n";
  socket.connect(endpoint);

  std::ostringstream archive_stream;
  std::cerr << "send_data, step 4\n";
  boost::archive::text_oarchive archive(archive_stream);
  std::cerr << "send_data, step 5\n";
  archive << data;
  std::cerr << "send_data, step 6\n";
  std::string outbound_data = archive_stream.str();
  std::cerr << "send_data, step 7\n";
  boost::system::error_code error;
  std::cerr << "send_data, step 8\n";
  boost::asio::write(socket, boost::asio::buffer(outbound_data), error);
  std::cerr << "send_data, step 9\n";
  if (error) {
    std::cout << "send failed: " << error.message() << "\n";
    exit(1);
  }
  std::cerr << "send_data, step 10\n";
}




int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Program is run as\n";
    std::cerr << "TCP_archive_client_server [opt]\n";
    std::cerr << "With opt=0 (for server) and opt=1 (for client)\n";
    return 1;
  }
  int n;
  (void)sscanf(argv[1], "%d", &n);
  //
  boost::asio::ip::tcp::endpoint endpoint1(boost::asio::ip::tcp::v4(), 1234 );
  boost::asio::ip::tcp::endpoint endpoint2(boost::asio::ip::tcp::v4(), 1235 );
  if (n == 0) { // server
    TypeIndex eIndex = read_data<TypeIndex>(endpoint1);
    std::cerr << "Server, step 5\n";
    TypeSum eSum{eIndex.idx1 + eIndex.idx2, eIndex.idx2 + eIndex.idx3};
    std::cerr << "Server, step 6\n";
    send_data<TypeSum>(endpoint2, eSum);
    std::cerr << "Server, step 7\n";
  }
  if (n == 1) { // client
    TypeIndex eIndex{45, 54, 67};
    std::cerr << "Client, step 4\n";
    send_data<TypeIndex>(endpoint1, eIndex);
    std::cerr << "Client, step 5\n";
    TypeSum eSum = read_data<TypeSum>(endpoint2);
    std::cerr << "Client, step 6\n";
    std::cerr << "idx12=" << eSum.idx12 << " idx23=" << eSum.idx23 << "\n";
  }
}
