#include <iostream>
#include <boost/asio.hpp>

std::string read_server(boost::asio::ip::tcp::socket & socket) {
  boost::asio::streambuf buf;
  boost::asio::read_until( socket, buf, "\n" );
  std::string data = boost::asio::buffer_cast<const char*>(buf.data());
  return data;
}


void send_server(boost::asio::ip::tcp::socket & socket, const std::string& message) {
  const std::string msg = message + "\n";
  boost::asio::write( socket, boost::asio::buffer(message) );
}



int main() {
  boost::asio::io_service io_service;
  boost::asio::ip::tcp::acceptor acceptor_(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 1234 ));
  boost::asio::ip::tcp::socket socket_(io_service);
  acceptor_.accept(socket_);
  std::string name = read_server(socket_);
  std::cout << "That guy contacted us. name=" << name << "\n";
  send_server(socket_, "Hello From Server!");
  std::cout << "Servent sent Hello message to Client!\n";
  return 0;
}
