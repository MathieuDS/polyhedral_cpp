#include <iostream>
#include <boost/asio.hpp>

int main() {
  boost::asio::io_service io_service;
  boost::asio::ip::tcp::socket socket(io_service);
  socket.connect( boost::asio::ip::tcp::endpoint( boost::asio::ip::address::from_string("127.0.0.1"), 1234 ));
  const std::string msg = "Hubert Blaine Wolfeschlegelsteinhausenbergerdorff\n";
  boost::system::error_code error;
  boost::asio::write( socket, boost::asio::buffer(msg), error );
  if( !error ) {
    std::cout << "Client sent hello message!\n";
  } else {
    std::cout << "send failed: " << error.message() << "\n";
  }
  // getting response from server
  boost::asio::streambuf receive_buffer;
  boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
  if( error && error != boost::asio::error::eof ) {
    std::cout << "receive failed: " << error.message() << "\n";
  } else {
    const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
    std::cout << data << "\n";
  }
}
