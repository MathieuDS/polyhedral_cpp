#include <iostream>
#include <boost/asio.hpp>


std::string read_server(boost::asio::ip::tcp::socket & socket) {
  boost::asio::streambuf buf;
  boost::asio::read_until( socket, buf, "\n" );
  std::string data = boost::asio::buffer_cast<const char*>(buf.data());
  return data;
}


void send_server(boost::asio::ip::tcp::socket & socket, const std::string& message) {
  const std::string msg = message + "\n";
  boost::asio::write( socket, boost::asio::buffer(message) );
}




int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Program is run as\n";
    std::cerr << "TCP_archive_client_server [opt]\n";
    std::cerr << "With opt=0 (for server) and opt=1 (for client)\n";
    return 1;
  }
  int n;
  (void)sscanf(argv[1], "%d", &n);
  //
  if (n == 0) {
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::acceptor acceptor_(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 1234 ));
    boost::asio::ip::tcp::socket socket_(io_service);
    acceptor_.accept(socket_);
    std::string name = read_server(socket_);
    std::cout << "That guy contacted us. name=" << name << "\n";
    send_server(socket_, "Hello From Server!");
    std::cout << "Servent sent Hello message to Client!\n";
  }
  if (n == 1) {
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::socket socket(io_service);
    socket.connect( boost::asio::ip::tcp::endpoint( boost::asio::ip::address::from_string("127.0.0.1"), 1234 ));
    const std::string msg = "Hubert Blaine Wolfeschlegelsteinhausenbergerdorff\n";
    boost::system::error_code error;
    boost::asio::write( socket, boost::asio::buffer(msg), error );
    if( !error ) {
      std::cout << "Client sent hello message!\n";
    } else {
      std::cout << "send failed: " << error.message() << "\n";
    }
    // getting response from server
    boost::asio::streambuf receive_buffer;
    boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
    if( error && error != boost::asio::error::eof ) {
      std::cout << "receive failed: " << error.message() << "\n";
    } else {
      const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
      std::cout << data << "\n";
    }
  }
}
