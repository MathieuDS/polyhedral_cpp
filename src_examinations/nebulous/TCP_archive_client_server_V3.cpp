#include <iostream>
#include <boost/asio.hpp>


#include <boost/archive/tmpdir.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/split_free.hpp>




struct TypeIndex {
  int idx1;
  int idx2;
  int idx3;
};

struct TypeSum {
  int idx12;
  int idx23;
};





namespace boost::serialization {

  // TypeCtypeExch
  template<class Archive>
  inline void serialize(Archive & ar, TypeIndex & eTypIdx, const unsigned int version)
  {
    ar & make_nvp("idx1", eTypIdx.idx1);
    ar & make_nvp("idx2", eTypIdx.idx2);
    ar & make_nvp("idx3", eTypIdx.idx3);
  }

  // TypeCtypeExch
  template<class Archive>
  inline void serialize(Archive & ar, TypeSum & eTypSum, const unsigned int version)
  {
    ar & make_nvp("idx12", eTypSum.idx12);
    ar & make_nvp("idx23", eTypSum.idx23);
  }

}


void process_error(const boost::system::error_code& error)
{
  if( !error ) {
    std::cout << "Client sent data!\n";
  } else {
    std::cout << "send failed: " << error.message() << "\n";
    exit(1);
  }
}



template<typename T>
T read_data(boost::asio::ip::tcp::socket & socket)
{
  std::cerr << "read_data, step 1\n";
  boost::asio::streambuf buf;
  std::cerr << "read_data, step 2\n";
  boost::asio::read_until( socket, buf, "\n" );
  std::cerr << "read_data, step 3\n";
  std::string data = boost::asio::buffer_cast<const char*>(buf.data());
  std::cerr << "read_data, step 4\n";
  T data_o;
  std::cerr << "read_data, step 5\n";
  std::stringstream iss(data);
  std::cerr << "read_data, step 6\n";
  boost::archive::text_iarchive ia(iss);
  std::cerr << "read_data, step 7\n";
  ia >> data_o;
  std::cerr << "read_data, step 8\n";
  return data_o;
}




template<typename T>
void send_data(boost::asio::ip::tcp::socket & socket, const T& data) {
  std::cerr << "send_data, step 1\n";
  std::ostringstream archive_stream;
  std::cerr << "send_data, step 2\n";
  boost::archive::text_oarchive archive(archive_stream);
  std::cerr << "send_data, step 3\n";
  archive << data;
  std::cerr << "send_data, step 4\n";
  std::string outbound_data = archive_stream.str() + "\n";
  std::cerr << "send_data, step 5\n";
  boost::system::error_code error;
  std::cerr << "send_data, step 6\n";
  boost::asio::write(socket, boost::asio::buffer(outbound_data), error);
  std::cerr << "send_data, step 7\n";
  if (error) {
    std::cout << "send failed: " << error.message() << "\n";
    exit(1);
  }
  std::cerr << "send_data, step 8\n";
}




int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Program is run as\n";
    std::cerr << "TCP_archive_client_server [opt]\n";
    std::cerr << "With opt=0 (for server) and opt=1 (for client)\n";
    return 1;
  }
  int n;
  (void)sscanf(argv[1], "%d", &n);
  //
  boost::asio::io_service io_service;
  boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), 1234 );
  if (n == 0) { // server
    std::cerr << "Server, step 1\n";
    boost::asio::ip::tcp::acceptor acceptor(io_service, endpoint);
    std::cerr << "Server, step 2\n";
    boost::asio::ip::tcp::socket socket(io_service);
    std::cerr << "Server, step 3\n";
    acceptor.accept(socket);
    std::cerr << "Server, step 4\n";
    TypeIndex eIndex = read_data<TypeIndex>(socket);
    std::cerr << "Server, step 5\n";
    TypeSum eSum{eIndex.idx1 + eIndex.idx2, eIndex.idx2 + eIndex.idx3};
    std::cerr << "Server, step 6\n";
    send_data<TypeSum>(socket, eSum);
    std::cerr << "Server, step 7\n";
  }
  if (n == 1) { // client
    std::cerr << "Client, step 1\n";
    boost::asio::ip::tcp::socket socket(io_service);
    std::cerr << "Client, step 2\n";
    socket.connect(endpoint);
    std::cerr << "Client, step 3\n";
    TypeIndex eIndex{45, 54, 67};
    std::cerr << "Client, step 4\n";
    send_data<TypeIndex>(socket, eIndex);
    std::cerr << "Client, step 5\n";
    TypeSum eSum = read_data<TypeSum>(socket);
    std::cerr << "Client, step 6\n";
    std::cerr << "idx12=" << eSum.idx12 << " idx23=" << eSum.idx23 << "\n";
  }
}
