#include <iostream>
#include <vector>
#include <set>

int GetLengthSequence(std::vector<int> const& A, int const& M)
{
  int siz=A.size();
  int maxlen=0;
  int currlen=0;
  for (int i=0; i<siz; i++) {
    if (A[i] >= M)
      currlen++;
    else
      currlen=0;
    if (currlen > maxlen)
      maxlen=currlen;
  }
  return maxlen;
}


int GetMaximumAreaRectangle(std::vector<int> const& A)
{
  std::set<int> Aset(A.begin(), A.end());
  int MaxArea=0;
  for (auto & eM : Aset) {
    int len=GetLengthSequence(A, eM);
    std::cerr << "eM=" << eM << " len=" << len << "\n";
    int eArea = eM * GetLengthSequence(A, eM);
    if (eArea > MaxArea)
      MaxArea = eArea;
  }
  return MaxArea;
}

int main(int argc, char* argv[])
{
  std::cerr << "MaxArea=" << GetMaximumAreaRectangle({1,1,1,1,3}) << "\n";
  std::cerr << "MaxArea=" << GetMaximumAreaRectangle({1,1,1,1,4,4}) << "\n";
}
