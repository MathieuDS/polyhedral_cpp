#include <iostream>
#include <vector>

int MaximumPartialSum(std::vector<int> const& A)
{
  int n=A.size();
  int ans=0;
  int sum=0;
  for (int i=0; i<n; i++) {
    sum += A[i];
    ans = std::max(ans, sum);
    if (sum<0) sum=0;
  }
  return ans;
  
}



int main(int argc, char* argv[])
{
  std::cerr << "PartSum=" << MaximumPartialSum({3,1,-3,-6,5,3,-1,1}) << "\n";
}
