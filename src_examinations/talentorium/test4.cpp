#include <iostream>
#include <utility>

int main(int argc, char* argv[])
{
  int x=2;
  int y=3;
  std::cerr << "x=" << x << " y=" << y << "\n";
  std::swap(x,y);
  std::cerr << "x=" << x << " y=" << y << "\n";
}
