#include <vector>
#include<string>
#include <iostream>
#include <fstream>
int main(int argc, char** argv)
{
  std::ifstream is(argv[1]);
  std::vector<int> V(3);
  int val2, val1, val0;
  size_t pos = 0;
  while(true) {
    int val;
    is >> val;
    if (pos == 0) {
      val0 = val;
    }
    if (pos == 1) {
      val1 = val0;
      val0 = val;
    }
    if (pos > 1) {
      val2 = val1;
      val1 = val0;
      val0 = val;
      if (val2 > val1 && val1 < val0)
        std::cerr << "pos=" << pos << "\n";
    }
    pos++;
  }
}
