

Eigen::Matrix<uint8_t> Get_Grayscale(const Eigen::Tensor<uint8_t,3>& ColorImage)
{
  std::vector<size_t> Dims = ColorImage.dims();
  size_t n = Dims[0];
  size_t m = Dims[1];
  Eigen::Matrix<uint8_t> RetImage(n, m);
  size_t prod = n * m;

  auto process=[&](size_t i_proc, size_t n_proc) -> void {
    size_t siz = prod / n_proc;
    size_t k_end, k_begin = i_proc * siz;
    if (i_proc == n_proc-1)
      k_end = prod;
    else
      k_end = siz * (i_proc + 1);
    for (size_t k = k_begin; k<k_end; k++) {
      i = k / m;
      j = k % m;
      RetImage(i,j) = ColorImage(i,j,0);
    }
  };
  std::vector<std::thread> V;
  size_t n_proc = 4;
  for (size_t iProc=0; iProc<n_proc; iProc++)
    V.push_back(process, iProc, n_proc);
  
  return RetImage;
}




