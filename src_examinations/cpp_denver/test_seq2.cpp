#include <vector>
#include<string>
#include <iostream>
#include <fstream>
int main(int argc, char** argv)
{
  std::ifstream is(argv[1]);
  size_t n;
  is >> n;
  std::vector<int> V(n);
  for (size_t i=0; i<n; i++)
    is >> V[i];
  size_t low = 0;
  size_t high = n-1;
  while(true) {
    std::cerr << "low=" << low << " high=" << high << "\n";
    size_t mid = (low + high) / 2;
    if (V[mid-1] > V[mid] && V[mid] < V[mid+1]) {
      std::cerr << "pos=" << mid << "\n";
      break;
    }
    if (V[mid-1] > V[mid]) { // We are in decreasing
      low = mid;
    } else {
      if (V[mid] < V[mid+1]) { // We are in increasing part
        high = mid;
      }
    }
  }
}
