void qsort(int a[], int l, int r) {
    if (l >= r) return;
    int m = a[l];
    int i = l, j = r;
    while (i < j) {
        while (i < j && m <= a[j]) {
            j--;
        }
        a[i] = a[j];
        while (i < j && a[i] <= m) {
            i++;
        }
        a[j] = a[i];
    }
    a[i] = m;
    qsort(a, l, i-1);
    qsort(a, i+1, r);
}
