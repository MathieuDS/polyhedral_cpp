#include "Temp_common.h"


std::vector<std::vector<int>> EnumerateMatroidLinearColoring(int const& nbColor, int const& nPoint, std::vector<std::vector<std::vector<int>>> const& ListRelevantSubset)
{
  std::vector<std::vector<int>> ListSolution;
  int nbSelected=0;
  std::vector<int> CurrentSolution(nPoint,0);
  auto IsStateOK=[&](std::vector<int> const& eSol) -> bool {
    /*
    std::cerr << "IsStateOK eSol=";
    for (auto & eVal : eSol)
      std::cerr << " " << eVal;
    std::cerr << "\n";
    */
    for (int iPoint=0; iPoint<nbSelected; iPoint++) {
      for (auto & eFam : ListRelevantSubset[iPoint]) {
	int TheSum=0;
	for (auto & eVal : eFam)
	  TheSum += eSol[eVal];
	int TheResidual = TheSum % nbColor;
	if (TheResidual == 0) {
	  /*
	  std::cerr << "FAIL : TheSum=" << TheSum << " TheResidual=" << TheResidual << "\n";
	  std::cerr << "eFam=";
	  for (auto & eVal : eFam)
	    std::cerr << " " << eVal;
	  std::cerr << "\n";
	  */
	  return false;
	}
      }
    }
    //    std::cerr << "PASS\n";
    return true;
  };
  auto SimpleNext=[&]() -> bool {
    while(true) {
      if (CurrentSolution[nbSelected-1] < nbColor-1) {
	CurrentSolution[nbSelected-1]++;
	return true;
      }
      else {
	nbSelected--;
      }
      if (nbSelected == 0)
	break;
    }
    return false;
  };
  auto NextToFeasible=[&]() -> bool {
    while(true) {
      if (IsStateOK(CurrentSolution))
	return true;
      bool test=SimpleNext();
      if (!test)
	break;
    }
    return false;
  };
  auto NextInTree=[&]() -> bool {
    if (nbSelected == nPoint) {
      bool test1=SimpleNext();
      if (!test1)
	return false;
      return NextToFeasible();
    }
    CurrentSolution[nbSelected]=1;
    nbSelected++;
    return NextToFeasible();
  };

  
  int iter=0;
  while(true) {
    bool test=NextInTree();
    if (!test)
      break;
    /*
    std::cerr << "iter=" << iter << " vect=";
    for (auto & eVal : CurrentSolution)
      std::cerr << " " << eVal;
      std::cerr << "\n";*/
    if (nbSelected == nPoint)
      ListSolution.push_back(CurrentSolution);
    iter++;
  }
  std::cerr << "iter=" << iter << " |ListSolution|=" << ListSolution.size() << "\n";
  return ListSolution;
}
