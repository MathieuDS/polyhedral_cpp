#include "PACK_ContinuousCase.h"
int main(int argc, char *argv[])
{
  try {
    if (argc != 4) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "ENUM_ContinuousPacking [n] [d] [OutFile]\n";
      return -1;
    }
    //
    int n;
    (void)sscanf(argv[1], "%d", &n);
    //
    int d;
    (void)sscanf(argv[2], "%d", &d);
    //
    std::string OutFile(argv[3]);
    //
    std::cerr << "n=" << n << " d=" << d << " OutFile=" << OutFile << "\n";
    //
    std::vector<std::vector<CoordCube>> RetList = EnumerationNonExtensiblePacking(n,d);
    int nbPack=RetList.size();
    std::cerr << "|RetList|=" << nbPack << "\n";
    //
    std::ofstream os(OutFile);
    os << "return [\n";
    for (int iPack=0; iPack<nbPack; iPack++) {
      if (iPack > 0)
        os << ",\n";
      PrintSinglePacking(os, RetList[iPack]);
    }
    os << "];\n";
    std::cerr << "Normal termination of the program\n";
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
