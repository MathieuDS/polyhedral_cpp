#ifndef CANONIC_SET_DEFINE
#define CANONIC_SET_DEFINE

#include "MAT_Matrix.h"

struct ExhaustiveGroup {
  int n;
  std::vector<MyVector<int> > ListGroupElement;
};

ExhaustiveGroup CANONIC_ReadFullGroup(std::istream & is)
{
  int n, grpsize;
  int eVal;
  is >> n;
  is >> grpsize;
  std::vector<MyVector<int> > ListGroupElement(grpsize);
  for (int igrp=0; igrp<grpsize; igrp++) {
    MyVector<int> eElt(n);
    for (int i=0; i<n; i++) {
      is >> eVal;
      eElt[i]=eVal;
    }
    ListGroupElement[igrp]=eElt;
  }
  return {n, ListGroupElement};
}



MyVector<int> CANONIC_ActionVector(MyVector<int> const& eVectINP, MyVector<int> const& eElt)
{
  int n=eElt.size();
  MyVector<int> eVectImg=MyVector<int>(n);
  for (int iS=0; iS<n; iS++) {
    int iSnew=eElt[iS];
    eVectImg[iSnew]=eVectINP[iS];
  }
  return eVectImg;
}


/* test if eVect1 is strictly smaller than eVect2 */
bool CANONIC_IsStrictlySmaller(MyVector<int> const& eVect1, MyVector<int> const& eVect2)
{
  int n=eVect1.size();
  for (int i=0; i<n; i++) {
    if (eVect1[i] == 1 && eVect2[i] == 0)
      return true;
    if (eVect1[i] == 0 && eVect2[i] == 1)
      return false;
  }
  return false;
}

bool CANONIC_IsSmallestInOrbit(std::vector<int> const& eVectSet, ExhaustiveGroup const& ExGrp)
{
  int n=ExGrp.n;
  MyVector<int> eVectINP=MyVector<int>(n);
  for (int i=0; i<n; i++)
    eVectINP[i]=0;
  for (auto & eVal : eVectSet)
    eVectINP[eVal]=1;
  for (auto & eElt : ExGrp.ListGroupElement) {
    MyVector<int> eVectImg=CANONIC_ActionVector(eVectINP, eElt);
    bool test=CANONIC_IsStrictlySmaller(eVectImg, eVectINP);
    if (test)
      return false;
  }
  return true;
}

  
std::vector<std::vector<int> > CANONIC_FullEnumerationOrbit(ExhaustiveGroup const& ExGrp, int const& siz)
{
  std::vector<int> eVect(siz);
  std::vector<std::vector<int> > TotalListVect;
  if (siz == 0) {
    std::vector<int> eVectRet;
    TotalListVect.push_back(eVectRet);
    return TotalListVect;
  }
  int n=ExGrp.n;
  for (std::vector<int> & eSet : CANONIC_FullEnumerationOrbit(ExGrp, siz-1)) {
    int FirstNextVal;
    if (eSet.size() == 0) {
      FirstNextVal=0;
    }
    else {
      FirstNextVal=eSet[siz-2]+1;
    }
    for (int NewVal=FirstNextVal; NewVal<n; NewVal++) {
      std::vector<int> NewSet=eSet;
      NewSet.push_back(NewVal);
      bool test=CANONIC_IsSmallestInOrbit(NewSet, ExGrp);
      if (test)
	TotalListVect.push_back(NewSet);
    }
  }
  std::cerr << "Finish work at siz=" << siz << "\n";
  return TotalListVect;
}


#endif
