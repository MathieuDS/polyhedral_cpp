#include "COMB_CanonicSet.h"

int main(int argc, char *argv[])
{
  try {
    if (argc != 4) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "EnumerationKset [DATAGRP] [K] [FileOut]\n";
      std::cerr << "\n";
      std::cerr << "DATAGRP : The file containing all group elements\n";
      std::cerr << "K       : The value of k used for the enumeration\n";
      std::cerr << "FileOut : The file where data is saved\n";
      std::cerr << "It returns the lexicographically minimal k-sets\n";
      return -1;
    }
    //
    std::cerr << "Reading input\n";
    //
    std::ifstream GRPfs(argv[1]);
    ExhaustiveGroup ExGrp=CANONIC_ReadFullGroup(GRPfs);
    //
    int k;
    sscanf(argv[2], "%d", &k);
    //
    std::vector<std::vector<int> > TotalListSet=CANONIC_FullEnumerationOrbit(ExGrp, k);
    std::ofstream SETfs(argv[3]);
    std::vector<std::vector<int>> NewList;
    for (auto& eVect : TotalListSet) {
      std::vector<int> NewVect;
      for (auto & eVal : eVect)
	NewVect.push_back(eVal+1);
      NewList.push_back(NewVect);
    }
    SETfs << "return ";
    WriteStdVectorStdVectorGAP(SETfs, NewList);
    SETfs << ";\n";
    //
    std::cerr << "Completion of the program\n";
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
