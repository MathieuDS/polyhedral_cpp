#include "COLOR_EnumerationTech.h"

int main(int argc, char *argv[])
{
  try {
    if (argc != 4) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "COLOR_MatroidLinearColorings [nbColor] [FileIn] [FileOut]\n";
      std::cerr << "\n";
      std::cerr << "nbColor : The number of colors\n";
      std::cerr << "FileIn  : The input file\n";
      std::cerr << "FileOut : The output file\n";
      std::cerr << "It returns the lexicographically minimal k-sets\n";
      return -1;
    }
    //
    std::cerr << "Reading input\n";
    //
    int nbColor;
    sscanf(argv[1], "%d", &nbColor);
    //
    std::ifstream is(argv[2]);
    int nPoint;
    is >> nPoint;
    std::vector<std::vector<std::vector<int>>> ListRelevantBlock(nPoint);
    for (int u=0; u<nPoint; u++) {
      int nbRel;
      is >> nbRel;
      std::vector<std::vector<int>> eRelevantBlock(nbRel);
      for (int v=0; v<nbRel; v++) {
	int siz;
	is >> siz;
	std::vector<int> eRelevant(siz);
	for (int w=0; w<siz; w++) {
	  int eVal;
	  is >> eVal;
	  eRelevant[w]=eVal;
	}
	eRelevantBlock[v] = eRelevant;
      }
      ListRelevantBlock[u] = eRelevantBlock;
    }
    //
    std::vector<std::vector<int>> ListSolution = EnumerateMatroidLinearColoring(nbColor, nPoint, ListRelevantBlock);
    //
    std::ofstream os(argv[3]);
    os << "return [";
    bool IsFirst=true;
    for (auto & eSol : ListSolution) {
      if (!IsFirst)
	os << ",\n";
      IsFirst=false;
      os << "[";
      for (int i=0; i<nPoint; i++) {
	if (i>0)
	  os << ",";
	os << eSol[i];
      }
      os << "]";
    }
    os << "];\n";
    //
    std::cerr << "Completion of the program\n";
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
