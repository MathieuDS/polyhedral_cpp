#ifndef COMB_RANDOM_SIMULATION_PACKING
#define COMB_RANDOM_SIMULATION_PACKING

#include "Temp_common.h"
#include "MAT_Matrix.h"
#include "COMB_Combinatorics_elem.h"
#include "openscad.h"
#include "Namelist.h"

MyMatrix<int> GetUncoveredPoint(int const& valX, int const& valY, int const& d, MyMatrix<int> const& ListCornerCube)
{
  int nbRow=ListCornerCube.rows();
  int nbCol=ListCornerCube.cols();
  std::cerr << "nbRow=" << nbRow << " nbCol=" << nbCol << "\n";
  if (nbCol != d) {
    std::cerr << "Inconsistency\n";
    throw TerminalException{1};
  }
  MyMatrix<int> TotalSet = BuildSet(d, valX);
  int nbTotal=TotalSet.rows();
  std::cerr << "nbTotal=" << nbTotal << "\n";
  MyMatrix<int> eBlock = BuildSet(d, valY);
  int blockSiz = eBlock.rows();
  std::cerr << "blockSiz=" << blockSiz << "\n";
  std::vector<int> VectStatus(nbTotal, 1);
  for (int iRow=0; iRow<nbRow; iRow++) {
    for (int iBlock=0; iBlock<blockSiz; iBlock++) {
      MyVector<int> V(d);
      for (int i=0; i<d; i++) {
	int eX = ListCornerCube(iRow, i) + eBlock(iBlock, i);
	if (eX >= valX)
	  eX -= valX;
	V(i) = eX;
      }
      int pos = PositionBuildSet(d, valX, V);
      int sumErr=0;
      for (int i=0; i<d; i++) {
	int diff=TotalSet(pos, i) - V(i);
	sumErr += diff*diff;
      }
      if (sumErr > 0) {
	std::cerr << "Big problem here\n";
      }
      VectStatus[pos] = 0;
    }
  }
  int nbUncovered=0;
  for (int iTotal=0; iTotal<nbTotal; iTotal++) {
    if (VectStatus[iTotal] == 1) {
      nbUncovered++;
    }
  }
  std::cerr << "nbUncovered=" << nbUncovered << "\n";
  MyMatrix<int> RetMat(nbUncovered, d);
  int idx=0;
  for (int iTotal=0; iTotal<nbTotal; iTotal++) {
    if (VectStatus[iTotal] == 1) {
      for (int i=0; i<d; i++)
	RetMat(idx, i) = TotalSet(iTotal, i);
      idx++;
    }
  }
  return RetMat;
}


FullNamelist NAMELIST_GetPackingFunctionality()
{
  std::map<std::string, SingleBlock> ListBlock;
  //
  std::map<std::string, int> ListIntValues1;
  std::map<std::string, bool> ListBoolValues1;
  std::map<std::string, double> ListDoubleValues1;
  std::map<std::string, std::vector<double> > ListListDoubleValues1;
  std::map<std::string, std::vector<int> > ListListIntValues1;
  std::map<std::string, std::string> ListStringValues1;
  std::map<std::string, std::vector<std::string> > ListListStringValues1;
  ListStringValues1["XMLfile"] = "XMLunset";
  ListIntValues1["X"]=4;
  ListIntValues1["Y"]=2;
  ListIntValues1["d"]=3;
  ListIntValues1["nbTrans"]=3;
  ListStringValues1["FileRead"] = "unset";
  ListStringValues1["FileScad"] = "unset";
  ListDoubleValues1["RadiusSphere"] = 0.2;
  ListListIntValues1["ColorSphere"]={255,255,0};
  ListListIntValues1["ColorTile"]={255,0,0};
  ListBoolValues1["FillHoleSphere"]=false;
  ListBoolValues1["PutCubicTile"]=false;
  ListDoubleValues1["HalfCubeSize"]=-1;
  //
  SingleBlock BlockOPT;
  BlockOPT.ListIntValues=ListIntValues1;
  BlockOPT.ListBoolValues=ListBoolValues1;
  BlockOPT.ListDoubleValues=ListDoubleValues1;
  BlockOPT.ListListDoubleValues=ListListDoubleValues1;
  BlockOPT.ListListIntValues=ListListIntValues1;
  BlockOPT.ListStringValues=ListStringValues1;
  BlockOPT.ListListStringValues=ListListStringValues1;
  ListBlock["OPTION"]=BlockOPT;
  //
  return {ListBlock, "undefined"};
}


FullNamelist NAMELIST_GetRandomPackingSearchFunctionality()
{
  std::map<std::string, SingleBlock> ListBlock;
  //
  std::map<std::string, int> ListIntValues1;
  std::map<std::string, bool> ListBoolValues1;
  std::map<std::string, double> ListDoubleValues1;
  std::map<std::string, std::vector<double> > ListListDoubleValues1;
  std::map<std::string, std::vector<int> > ListListIntValues1;
  std::map<std::string, std::string> ListStringValues1;
  std::map<std::string, std::vector<std::string> > ListListStringValues1;
  ListStringValues1["XMLfile"] = "XMLunset";
  ListIntValues1["X"]=4;
  ListIntValues1["Y"]=2;
  ListIntValues1["d"]=3;
  ListBoolValues1["ShowNewLevel"]=false;
  ListBoolValues1["ShowSpecificLevel"]=false;
  ListBoolValues1["SpecificLaminationAnalysis"]=false;
  ListIntValues1["SpecificLevel"]=-1;
  //
  SingleBlock BlockOPT;
  BlockOPT.ListIntValues=ListIntValues1;
  BlockOPT.ListBoolValues=ListBoolValues1;
  BlockOPT.ListDoubleValues=ListDoubleValues1;
  BlockOPT.ListListDoubleValues=ListListDoubleValues1;
  BlockOPT.ListListIntValues=ListListIntValues1;
  BlockOPT.ListStringValues=ListStringValues1;
  BlockOPT.ListListStringValues=ListListStringValues1;
  ListBlock["OPTION"]=BlockOPT;
  //
  return {ListBlock, "undefined"};
}




struct OptionCubePackingSearch {
  bool ShowNewLevel;
  bool ShowSpecificValue;
  int SpecificLevel;
  bool SpecificLaminationAnalysis;
};




void RandomEnumerationCubes(int const& valX, int const& valY, int const& d, OptionCubePackingSearch const& RecOpt)
{
  MyMatrix<int> ListVect=BuildSet(d, valX);
  int nbSet=ListVect.rows();
  std::cerr << "nbSet=" << nbSet << "\n";
  std::vector<int> StatusTile(nbSet);
  int quot = valX / valY;
  int nbLevel=1;
  for (int u=0; u<d; u++)
    nbLevel *= quot;
  std::cerr << "nbLevel=" << nbLevel << "\n";
  std::vector<int> ListTile(nbLevel);
  std::vector<int> ListITileAllowed(nbSet);
  auto CheckAllowedness=[&](int const& iLevel, int const& iTile) -> bool {
    //    std::cerr << "iLevel=" << iLevel << " iTile=" << iTile << "\n";
    for (int jLevel=0; jLevel<iLevel; jLevel++) {
      int eTile=ListTile[jLevel];
      bool IsOK=false;
      for (int i=0; i<d; i++) {
	//	std::cerr << "iTile=" << iTile << " eTile=" << eTile << " i=" << i << "\n";
	//	std::cerr << "|ListVect|=" << ListVect.rows() << " / "<< ListVect.cols() << "\n";
	int diff = ListVect(iTile, i) - ListVect(eTile, i);
	//	std::cerr << "diff=" << diff << "\n";
	if (diff != 0) {
	  int diff_res = diff % valY;
	  if (diff_res == 0)
	    IsOK=true;
	}
      }
      if (!IsOK)
	return false;
    }
    return true;
  };
  std::set<int> ListNbTileMax;
  int nbIter=0;
  while(true) {
    for (int iSet=0; iSet<nbSet; iSet++)
      StatusTile[iSet] = 1;
    bool IsFinished=false;
    int LevelFound = - 1;
    for (int iLevel=0; iLevel<nbLevel; iLevel++) {
      if (!IsFinished) {
	int nbTileFeasible=0;
	for (int jSet=0; jSet<nbSet; jSet++) {
	  if (StatusTile[jSet] == 1) {
	    bool test = CheckAllowedness(iLevel, jSet);
	    if (test) {
	      ListITileAllowed[nbTileFeasible] = jSet;
	      nbTileFeasible++;
	    }
	    else {
	      StatusTile[jSet]=0;
	    }
	  }
	}
	if (nbTileFeasible > 0) {
	  int iTile = rand() % nbTileFeasible;
	  int eTile = ListITileAllowed[iTile];
	  ListTile[iLevel] = eTile;
	  LevelFound=iLevel + 1;
	}
	else {
	  IsFinished=true;
	}
      }
    }
    auto PrintStream=[&](std::ostream & os) -> void {
      os << "LevelFound=" << LevelFound << "\n";
      for (int iLevel=0; iLevel < LevelFound; iLevel++) {
	int eTile = ListTile[iLevel];
	for (int u=0; u<d; u++)
	  os << " " << ListVect(eTile, u);
	os << "\n";
      }
      os << std::endl;
    };
    auto FindIrreducibleDimension=[&]() -> int {
      MyMatrix<int> TheTiling(LevelFound, d);
      for (int iLevel=0; iLevel < LevelFound; iLevel++) {
	int eTile = ListTile[iLevel];
	for (int u=0; u<d; u++)
	  TheTiling(iLevel, u) = ListVect(eTile, u);
      }
      MyMatrix<int> ListHole=GetUncoveredPoint(valX, valY, d, TheTiling);
      int nbHole=ListHole.rows();
      int nbIrrDim=0;
      for (int i=0; i<d; i++) {
	std::vector<int> ListAttValues(valX, 0);
	for (int iHole=0; iHole<nbHole; iHole++) {
	  int eVal=ListHole(iHole, i);
	  ListAttValues[eVal]++;
	}
	int eNNZ=0;
	for (int iX=0; iX<valX; iX++) {
	  if (ListAttValues[iX] > 0)
	    eNNZ++;
	}
	if (eNNZ != valY)
	  nbIrrDim++;
      }
      return nbIrrDim;
    };
    if (RecOpt.ShowNewLevel && ListNbTileMax.find(LevelFound) == ListNbTileMax.end()) {
      ListNbTileMax.insert(LevelFound);
      PrintStream(std::cerr);
      PrintStream(std::cout);
    }
    if (RecOpt.ShowSpecificValue && RecOpt.SpecificLevel == LevelFound) {
      PrintStream(std::cerr);
      PrintStream(std::cout);
    }
    if (RecOpt.SpecificLaminationAnalysis && RecOpt.SpecificLevel == LevelFound) {
      int IrreducibleDimension=FindIrreducibleDimension();
      std::cerr << "IrrDim=" << IrreducibleDimension << "\n";
      std::cout << "IrrDim=" << IrreducibleDimension << "\n";
    }
    //
    nbIter++;
    int nbIterRed = nbIter % 10000;
    if (nbIterRed == 0)
      std::cerr << "nbIter=" << nbIter << "\n";
  }

}


std::vector<PlaneOpenscad> GenerateParallelepiped(MyVector<double> const& V, MyVector<double> const& R, std::vector<int> const& eColor)
{
  MyMatrix<int> MatSign(4,2);
  MatSign(0,0) = 1;
  MatSign(0,1) = 1;
  MatSign(1,0) = 1;
  MatSign(1,1) = -1;
  MatSign(2,0) = -1;
  MatSign(2,1) = -1;
  MatSign(3,0) = -1;
  MatSign(3,1) = 1;
  MyMatrix<int> MatPartition(3,2);
  MatPartition(0,0) = 0;
  MatPartition(1,0) = 1;
  MatPartition(2,0) = 2;
  MatPartition(0,1) = 0;
  MatPartition(1,1) = 2;
  MatPartition(2,1) = 3;
  std::vector<PlaneOpenscad> ListPlane;
  for (int i=0; i<3; i++) {
    for (int j=0; j<2; j++) {
      double eXYZ;
      if (j == 0)
	eXYZ = V(i) - R(i);
      else
	eXYZ = V(i) + R(i);
      std::vector<MyVector<double>> ListVert(4);
      for (int u=0; u<4; u++) {
	MyVector<double> eVert(3);
	int idxSign=0;
	for (int iCol=0; iCol<3; iCol++) {
	  double eVal;
	  if (iCol == i) {
	    eVal = eXYZ;
	  }
	  else {
	    int eSign = MatSign(u, idxSign);
	    eVal = V(iCol) + eSign * R(iCol);
	    idxSign++;
	  }
	  eVert(iCol) = eVal;
	}
	ListVert[u] = eVert;
	//	std::cerr << "u=" << u << " V=";
	//	WriteVector(std::cerr, eVert);
      }
      //      std::cerr << "\n";
      
      for (int u=0; u<2; u++) {
	std::vector<MyVector<double>> ListVertPart(3);
	for (int v=0; v<3; v++)
	  ListVertPart[v] = ListVert[MatPartition(v,u)];
	PlaneOpenscad ePlane{ListVertPart, eColor};
	ListPlane.push_back(ePlane);
      }
    }
  }
  return ListPlane;
}



#endif
