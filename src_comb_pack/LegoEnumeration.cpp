#include "LegoEnumeration.h"

int main(int argc, char *argv[])
{
  try {
    FullNamelist eFull=NAMELIST_GetStandardLegoEnumeration();
    if (argc != 2) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "LegoEnumeration [FileOption.nml]\n";
      std::cerr << "\n";
      std::cerr << "FileOption.nml : The filename containing the list of options:\n";
      NAMELIST_WriteNamelistFile(std::cerr, eFull);
      return -1;
    }
    //
    std::cerr << "Reading input\n";
    std::string eFileName=argv[1];
    NAMELIST_ReadNamelistFile(eFileName, eFull);
    //
    GeneralEnumerationLego(eFull);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
  std::cerr << "Completion of the program\n";
}
