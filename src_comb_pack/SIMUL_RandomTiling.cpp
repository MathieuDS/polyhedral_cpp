#include "COMB_RandomSimulationPacking.h"
int main(int argc, char *argv[])
{
  try {
    FullNamelist eFull=NAMELIST_GetRandomPackingSearchFunctionality();
    if (argc != 2) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "SIMUL_RandomTiling [FileOption.nml]\n";
      std::cerr << "\n";
      std::cerr << "FileOption.nml : The filename containing the list of options:\n";
      NAMELIST_WriteNamelistFile(std::cerr, eFull);
      return -1;
    }
    std::string eFileName=argv[1];
    NAMELIST_ReadNamelistFile(eFileName, eFull);
    //
    SingleBlock BlOption = eFull.ListBlock.at("OPTION");
    int valX = BlOption.ListIntValues.at("X");
    int valY = BlOption.ListIntValues.at("Y");
    int valD = BlOption.ListIntValues.at("d");
    bool ShowNewLevel = BlOption.ListBoolValues.at("ShowNewLevel");
    bool ShowSpecificLevel = BlOption.ListBoolValues.at("ShowSpecificLevel");
    int SpecificLevel = BlOption.ListIntValues.at("SpecificLevel");
    OptionCubePackingSearch RecOpt{ShowNewLevel, ShowSpecificLevel, SpecificLevel};
    //
    RandomEnumerationCubes(valX, valY, valD, RecOpt);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
