#ifndef INCLUDE_LEGO_ENUMERATION
#define INCLUDE_LEGO_ENUMERATION

#include "Temp_common.h"
#include "PlaneGraph.h"
#include "COMB_Stor.h"
#include "SATsolver.h"
#include "Namelist.h"


struct tripl {
  int next;
  int prev;
  int inv;
};


struct BlockTypeTot {
  int len;
  int lenBound;
  std::vector<int> ListInternalDeg;
  std::vector<tripl> ListVal;
};


BlockTypeTot ReadBlockTypeTot(std::istream & is)
{
  int len, lenBound;
  is >> len;
  is >> lenBound;
  //  std::cerr << "len=" << len << " lenBound=" << lenBound << "\n";
  std::vector<int> ListInternalDeg(lenBound);
  for (int i=0; i<lenBound; i++) {
    int eDeg;
    is >> eDeg;
    ListInternalDeg[i]=eDeg;
    std::cerr << "i=" << i << " / " << lenBound << " eDeg=" << eDeg << "\n";
  }
  std::vector<tripl> ListVal(len);
  for (int i=0; i<len; i++) {
    int next, prev, inv;
    is >> next;
    is >> prev;
    is >> inv;
    ListVal[i]={next,prev,inv};
    if (next != -1 && next >= len) {
      std::cerr << "Wrong value for next = " << next << "\n";
      throw TerminalException{1};
    }
    if (next != -1 && prev >= len) {
      std::cerr << "Wrong value for prev = " << prev << "\n";
      throw TerminalException{1};
    }
    if (inv < 0 || inv >= len) {
      std::cerr << "Wrong value for inv = " << inv << "\n";
      throw TerminalException{1};
    }
    //    std::cerr << "i=" << i << " next=" << next << " prev=" << prev << " inv=" << inv << "\n";
  }
  return {len,lenBound,ListInternalDeg,ListVal};
}


std::vector<BlockTypeTot> ReadListBlockTypeTot(std::string const& eFile)
{
  std::ifstream is(eFile);
  int nbBlockType;
  is >> nbBlockType;
  std::vector<BlockTypeTot> ListBlockType(nbBlockType);
  for (int iBlockType=0; iBlockType<nbBlockType; iBlockType++)
    ListBlockType[iBlockType]=ReadBlockTypeTot(is);
  return ListBlockType;
}

/*
  In a partial solution, we must store external cycles
  According to classical philosophy of such enumeration
  we should have full data at all times. This means:
  ---An array containing the next, prev and inv status of directed edge
  ---Value -1 if the value has not been assigned.
  ---List Of External Cycles
  ---List of degrees of directed edge on the external cycles.
  ---
Principles:
  ---If an edge is boundary, it has either next = -1 or prev = -1
  ---For a valid directed edge, inv should always be well defined
  ---For the list of blocks, we should allow multiple types of blocks.
     This is both for the case of lego and the case of bifaced graphs
  ---But we should deal with the fact that many types are identical, again
     both for the case of bifaced graphs and for lego constructions
  ---We should allow the multiplicities of faces to be specified or unbounded.


Encoding:
  ---For the blocks, we map the directed edge in normal order.
     That is the edges with ePrev=-1 are the ones defining the boundary.
  ---For the structure, we still use the directed edges with ePrev=-1
     but we orient in reversed order.
  ---That way, when we make the identification, it coincides. The Next
     on the structure coincides with the "Next" on the block.
  ---But for the matching of the directed edges, we need to take the inverse.
*/
std::vector<PlanGraphOriented> TotalEnumerationBlock(std::vector<BlockTypeTot> const& ListBlockType, std::vector<int> const& ListMultiplicityBlock, int const& nbBlock, int const& MaxGenus, int const& deg, int const& nbDE, int const& MAX_ITER, int const& MAX_NB_GRAPH, int const& MAX_NB_SECOND)
{
  int nbBlockType=ListBlockType.size();
  int MaxNbCycle=nbBlock-1;
  int MaxLenBoundType=0;
  int MaxLenType=0;
  if (MaxGenus != 0) {
    std::cerr << "Further additional programming is needed on that delicate matter\n";
    throw TerminalException{1};
  }
  for (int iBlockType=0; iBlockType<nbBlockType; iBlockType++) {
    int lenBound=ListBlockType[iBlockType].lenBound;
    if (lenBound > MaxLenBoundType)
      MaxLenBoundType=lenBound;
    int len=ListBlockType[iBlockType].len;
    if (len > MaxLenType)
      MaxLenType=len;
  }
  /*  struct CycleEdgeMap {
    int iMap;
    int iStatus;
    };*/
  struct SecProperty {
    int iCycle;
    int iDEcycle;
    int iDEblock;
    int lenSec;
  };
  struct ExtensionRecord {
    int iBlockType;
    int nbSec;
    //    std::vector<CycleEdgeMap> ListMapDEcycle;
    std::vector<SecProperty> ListSection;
  };
  std::vector<int> Prov_StatusAttained(MaxLenType);
  std::vector<int> Prov_MapVect(MaxLenType);
  int iBlockFirst;
  //  std::vector<CycleEdgeMap> ListMapDEcycle(nbDE);
  std::vector<SecProperty> ListSection(MaxLenBoundType);
  //  ExtensionRecord RecExtens{-1,0,ListMapDEcycle,ListSection};
  ExtensionRecord RecExtens{-1,0,ListSection};
  struct PairDEinternalDeg {
    int eDE;
    int InternalDeg;
  };
  struct PairPosLen {
    int pos;
    int len;
  };
  struct OneLevel {
    int nbUsedDE;
    std::vector<tripl> ListVal;
    std::vector<int> ListMult;
    int nbCycle;
    int LowerBoundGenus;
    std::vector<PairPosLen> ListPosLenCycle;
    std::vector<PairDEinternalDeg> ListCycleDEintDeg;
    ExtensionRecord RecExtens;
  };
  std::vector<tripl> ListVal(nbDE);
  std::vector<int> ListMult(nbBlockType);
  std::vector<PairPosLen> ListPosLenCycle(MaxNbCycle);
  std::vector<PairDEinternalDeg> ListCycleDEintDeg(nbDE);
  //
  OneLevel eLev{0, ListVal, ListMult, 0, 0, ListPosLenCycle, ListCycleDEintDeg, RecExtens};
  std::vector<OneLevel> ListLevel;
  for (int iBlock=0; iBlock<nbBlock; iBlock++)
    ListLevel.push_back(eLev);
  int TheLevel;
  std::vector<PlanGraphOriented> ListRet;
  int nbGraph=0;
  auto FuncInsert=[&](PlanGraphOriented const& PLori) -> void {
    for (auto & fPLori : ListRet)
      if (IsEquivalentPlanGraphOriented(PLori, fPLori))
	return;
    ListRet.push_back(PLori);
    nbGraph++;
    std::cerr << "Now |ListRet|=" << nbGraph << "\n";
  };
  IntegerSubsetStorage *Vlist;
  Vlist = new IntegerSubsetStorage;
  VSLT_InitializeStorage(Vlist, nbDE);
  auto ComputeConnectedComponents=[&](int const& lev) -> void {
    int nbUsedDE=ListLevel[lev].nbUsedDE;
    int nbBoundDE=0;
    for (int iDE=0; iDE<nbUsedDE; iDE++) {
      int eNext=ListLevel[lev].ListVal[iDE].next;
      int ePrev=ListLevel[lev].ListVal[iDE].prev;
      if (eNext == -1 && ePrev == -1) {
	std::cerr << "lev=" << lev << " iDE=" << iDE << " nbDE=" << nbDE << "\n";
	std::cerr << "eNext=" << eNext << " ePrev=" << ePrev << "\n";
	std::cerr << "eInv=" << ListLevel[lev].ListVal[iDE].inv << "\n";
	std::cerr << "Big error. Please correct\n";
	throw TerminalException{1};
      }
      if (ePrev == -1) {
	VSLT_StoreValue(Vlist, iDE);
	//	std::cerr << "Inserting iDE=" << iDE << "\n";
	nbBoundDE++;
      }
    }
    //    std::cerr << "lev=" << lev << " nbUsedDE=" << nbUsedDE << " nbBoundDE=" << nbBoundDE << "\n";
    /*
    for (int iDE=0; iDE<nbDE; iDE++) {
      int eNext=ListLevel[lev].ListVal[iDE].next;
      int ePrev=ListLevel[lev].ListVal[iDE].prev;
      int eInv =ListLevel[lev].ListVal[iDE].inv;
      std::cerr << "iDE=" << iDE << " next=" << eNext << " prev=" << ePrev << " inv=" << eInv << "\n";
      }*/
    int nbCycle=0;
    int posCycDE=0;
    ListLevel[lev].ListPosLenCycle[0].pos=0;
    while(true) {
      if (VSLT_IsEmpty(Vlist) == 1)
	break;
      int TheFirstDE=VSLT_TheFirstPosition(Vlist);
      int eDE=TheFirstDE;
      int lenCycle=0;
      int ThePos=0;
      while(true) {
	lenCycle++;
	VSLT_RemoveValue(Vlist,eDE);
	//
	int workDE=eDE;
	int eInternalDeg=0;
	while(true) {
	  int eNext=ListLevel[lev].ListVal[workDE].next;
	  if (eNext == -1)
	    break;
	  eInternalDeg++;
	  workDE=eNext;
	}
	//	std::cerr << "eDE=" << eDE << " eInternalDeg=" << eInternalDeg << "\n";
	ListLevel[lev].ListCycleDEintDeg[posCycDE]={eDE, eInternalDeg};
	posCycDE++;
	eDE=ListLevel[lev].ListVal[workDE].inv;
	if (eDE == TheFirstDE)
	  break;
      }
      //      std::cerr << "nbCycle=" << nbCycle << " ThePos=" << ThePos << " lenCycle=" << lenCycle << "\n";
      ListLevel[lev].ListPosLenCycle[nbCycle] = {ThePos, lenCycle};
      ThePos += lenCycle;
      nbCycle++;
    }
    //    std::cerr << "lev=" << lev << " nbCycle=" << nbCycle << "\n";
    ListLevel[lev].nbCycle=nbCycle;
  };
  auto Compute_additionalGenus=[&](int const& lev) -> int {
    int nbSec=ListLevel[lev].RecExtens.nbSec;
    int nbChange=0;
    for (int iSec=0; iSec<nbSec; iSec++) {
      int jSec=NextIdx(nbSec, iSec);
      int iCycle=ListLevel[lev].RecExtens.ListSection[iSec].iCycle;
      int jCycle=ListLevel[lev].RecExtens.ListSection[jSec].iCycle;
      if (iCycle != jCycle)
	nbChange++;
    }
    int res = nbChange % 2;
    int additionalGenus = (nbChange + res)/2;
    return additionalGenus;
  };
  auto TestEmbedding=[&](int const& lev, int const& iBlockType, int const& iCycle, int const& iDEcycle, int const& iDEblock) -> int {
    //    std::cerr << "----------------------- TestEmbedding ------------------\n";
    //    std::cerr << "lev=" << lev << " iBlockType=" << iBlockType << " iCycle=" << iCycle << " iDEcycle=" << iDEcycle << " iDEblock=" << iDEblock << "\n";
    int PosStart=ListLevel[lev].ListPosLenCycle[iCycle].pos;
    int lenCycle=ListLevel[lev].ListPosLenCycle[iCycle].len;
    int lenBound=ListBlockType[iBlockType].lenBound;
    //    std::cerr << "PosStart=" << PosStart << " lenCycle=" << lenCycle << " lenBound=" << lenBound << "\n";
    int jDEblock=iDEblock;
    int jDEcycle=iDEcycle;
    bool IsFirst=true;
    bool NeedClose=false;
    int lenSec=0;
    while(true) {
      //      int eDEblob=ListLevel[lev].ListCycleDEintDeg[PosStart + jDEcycle].eDE;
      int degree1=ListLevel[lev].ListCycleDEintDeg[PosStart + jDEcycle].InternalDeg;
      int jDEblockNext=NextIdx(lenBound, jDEblock);
      int degree2=ListBlockType[iBlockType].ListInternalDeg[jDEblockNext];
      int sumDeg=degree1 + degree2;
      //      std::cerr << "eDEblob=" << eDEblob << " jDEcycle=" << jDEcycle << "\n";
      //      std::cerr << "degree1=" << degree1 << " degree2=" << degree2 << " sumDeg=" << sumDeg << "\n";
      if (sumDeg > deg) {
	return -1;
      }
      if (IsFirst) {
	int jDEcyclePrev=PrevIdx(lenCycle,jDEcycle);
	int degree1prev=ListLevel[lev].ListCycleDEintDeg[PosStart + jDEcyclePrev].InternalDeg;
	int degree2prev=ListBlockType[iBlockType].ListInternalDeg[jDEblock];
	int sumDegPrev=degree1prev + degree2prev;
	if (sumDegPrev > deg)
	  return -1;
	if (sumDegPrev == deg)
	  NeedClose=true;
      }
      IsFirst=false;
      //      std::cerr << "NeedClose=" << NeedClose << "\n";
      lenSec++;
      if (sumDeg < deg)
	return lenSec;
      //      std::cerr << "Assignation ListMapDEcycle, jDEblock=" << jDEblock << " eDEblob=" << eDEblob << "\n";
      //      ListLevel[lev].RecExtens.ListMapDEcycle[eDEblob]={jDEblock,1};
      if (lenSec == lenBound) {
	if (!NeedClose)
	  return -1;
	else
	  return lenSec;
      }
      jDEcycle=NextIdx(lenCycle, jDEcycle);
      jDEblock=NextIdx(lenBound, jDEblock);
    }
  };
  auto InitialBlockComponent=[&](int const& lev, int const& iBlockType, int const& iCycle, int const& iDEcycle) -> int {
    int lenBound=ListBlockType[iBlockType].lenBound;
    for (int iDEblock=0; iDEblock<lenBound; iDEblock++) {
      int lenSec=TestEmbedding(lev, iBlockType, iCycle, iDEcycle, iDEblock);
      //      std::cerr << "iDEblock=" << iDEblock << " lenSec=" << lenSec << "\n";
      if (lenSec != -1) {
	ListLevel[lev].RecExtens.nbSec=1;
	ListLevel[lev].RecExtens.iBlockType=iBlockType;
	ListLevel[lev].RecExtens.ListSection[0]={iCycle, iDEcycle, iDEblock, lenSec};
	return 1;
      }
    }
    return 0;
  };
  auto iBlockType_increase=[&](int const& lev, int & iBlockType) -> int {
    while(true) {
      if (iBlockType < nbBlockType-1) {
	iBlockType++;
	int eMult=ListMultiplicityBlock[iBlockType];
	if (eMult == -1 || ListLevel[lev].ListMult[iBlockType] < eMult)
	  return 1;
      }
      else
	return 0;
    }
  };
  auto NextInTree_sma=[&](int const& lev) -> int {
    int iBlockType=ListLevel[lev].RecExtens.iBlockType;
    int iCycle=ListLevel[lev].RecExtens.ListSection[0].iCycle;
    int iDEcycle=ListLevel[lev].RecExtens.ListSection[0].iDEcycle;
    int iDEblock=ListLevel[lev].RecExtens.ListSection[0].iDEblock;
    while(true) {
      int lenBound=ListBlockType[iBlockType].lenBound;
      if (iDEblock < lenBound-1) {
	iDEblock++;
	int lenSec=TestEmbedding(lev, iBlockType, iCycle, iDEcycle, iDEblock);
	if (lenSec != -1) {
	  ListLevel[lev].RecExtens.ListSection[0]={iCycle, iDEcycle, iDEblock, lenSec};
	  return 1;
	}
      }
      else {
	iDEblock=0;
	int lenCycle=ListLevel[lev].ListPosLenCycle[iCycle].len;
	if (iDEcycle < lenCycle-1) {
	  iDEcycle++;
	  int lenSec=TestEmbedding(lev, iBlockType, iCycle, iDEcycle, iDEblock);
	  if (lenSec != -1) {
	    ListLevel[lev].RecExtens.ListSection[0]={iCycle, iDEcycle, iDEblock, lenSec};
	    return 1;
	  }
	}
	else {
	  iDEcycle=0;
	  int nbCycle=ListLevel[lev].nbCycle;
	  if (iCycle < nbCycle-1) {
	    iCycle++;
	    int lenSec=TestEmbedding(lev, iBlockType, iCycle, iDEcycle, iDEblock);
	    if (lenSec != -1) {
	      ListLevel[lev].RecExtens.ListSection[0]={iCycle, iDEcycle, iDEblock, lenSec};
	      return 1;
	    }
	  }
	  else {
	    iCycle=0;
	    int res=iBlockType_increase(lev, iBlockType);
	    if (res == 0)
	      return 0;
	    int lenSec=TestEmbedding(lev, iBlockType, iCycle, iDEcycle, iDEblock);
	    if (lenSec != -1) {
	      ListLevel[lev].RecExtens.ListSection[0]={iCycle, iDEcycle, iDEblock, lenSec};
	      return 1;
	    }
	  }
	}
      }
    }
    return 0;
  };
  auto InitialComponent=[&](int const& lev, int const& iCycle, int const& iDEcycle) -> int {
    ListLevel[lev].RecExtens.nbSec=1;
    for (int iBlockType=iBlockFirst; iBlockType<nbBlockType; iBlockType++) {
      int eMult=ListMultiplicityBlock[iBlockType];
      if (eMult == -1 || ListLevel[lev].ListMult[iBlockType] < eMult) {
	ListLevel[lev].RecExtens.iBlockType=iBlockType;
	int res=InitialBlockComponent(lev, iBlockType, iCycle, iDEcycle);
	if (res == 1)
	  return 1;
      }
    }
    return 0;
  };
  auto HasAllSideInclusion=[&](int const& lev) -> int {
    int nbCycle=ListLevel[lev].nbCycle;
    for (int iCycle=0; iCycle<nbCycle; iCycle++) {
      int lenCycle=ListLevel[lev].ListPosLenCycle[iCycle].len;
      for (int iDEcycle=0; iDEcycle<lenCycle; iDEcycle++) {
	int res=InitialComponent(lev, iCycle, iDEcycle);
	if (res == 0)
	  return 0;
      }
    }
    return 1;
  };
  auto IsAdmissible=[&](int const& lev) -> int {
    if (HasAllSideInclusion(lev) == 0)
      return 0;
    int LowerBoundNeeded=ListLevel[lev].nbCycle + lev + 1;
    if (LowerBoundNeeded > nbBlock)
      return 0;
    return 1;
  };
  auto FirstSetMapping=[&](int const& lev) -> int {
    int nbCycle=ListLevel[lev].nbCycle;
    for (int iCycle=0; iCycle<nbCycle; iCycle++) {
      int lenCycle=ListLevel[lev].ListPosLenCycle[iCycle].len;
      for (int iDEcycle=0; iDEcycle<lenCycle; iDEcycle++) {
	int res=InitialComponent(lev, iCycle, iDEcycle);
	//	std::cerr << "After InitialComponent iCycle=" << iCycle << " iDEcycle=" << iDEcycle << "\n";
	if (res == 1)
	  return 1;
      }
    }
    return 0;
  };
  auto AssignationFromIBlockType=[&](int const& iBlockType) -> int {
    for (int iDE=0; iDE<nbDE; iDE++)
      ListLevel[0].ListVal[iDE]={-1,-1,-1};
    int len=ListBlockType[iBlockType].len;
    for (int iDE=0; iDE<len; iDE++)
      ListLevel[0].ListVal[iDE]=ListBlockType[iBlockType].ListVal[iDE];
    for (int jBlockType=0; jBlockType<nbBlockType; jBlockType++)
      ListLevel[0].ListMult[jBlockType]=0;
    ListLevel[0].ListMult[iBlockType]=1;
    ListLevel[0].nbUsedDE=len;
    ListLevel[0].LowerBoundGenus=0;
    ComputeConnectedComponents(0);
    TheLevel=0;
    if (IsAdmissible(0) == 0)
      return 0;
    if (FirstSetMapping(0) == 0)
      return 0;
    return 1;
  };
  auto ShiftFromChoices=[&]() -> int {
    //    std::cerr << "Before integration process\n";
    int nbUsedDE=ListLevel[TheLevel].nbUsedDE;
    /*    for (int i=0; i<nbUsedDE; i++) {
      int iNext=ListLevel[TheLevel].ListVal[i].next;
      int iPrev=ListLevel[TheLevel].ListVal[i].prev;
      int iInv =ListLevel[TheLevel].ListVal[i].inv;
      std::cerr << "i=" << i << " iPrev=" << iPrev << " iNext=" << iNext << " iInv=" << iInv << "\n";
      }*/
    TheLevel++;
    //    std::cerr << "ShiftFromChoices, now TheLevel=" << TheLevel << "\n";
    ListLevel[TheLevel]=ListLevel[TheLevel-1];
    int additionalGenus=Compute_additionalGenus(TheLevel-1);
    //    std::cerr << "additionalGenus=" << additionalGenus << "\n";
    ListLevel[TheLevel].LowerBoundGenus = ListLevel[TheLevel-1].LowerBoundGenus + additionalGenus;
    int iBlockType=ListLevel[TheLevel-1].RecExtens.iBlockType;
    int len=ListBlockType[iBlockType].len;
    int lenBound=ListBlockType[iBlockType].lenBound;
    int lev=TheLevel-1;
    int nbSec=ListLevel[lev].RecExtens.nbSec;
    ListLevel[TheLevel].ListMult[iBlockType]++;
    for (int i=0; i<len; i++)
      Prov_StatusAttained[i]=0;
    //    std::cerr << "nbSec=" << nbSec << "\n";
    for (int iSec=0; iSec<nbSec; iSec++) {
      int iCycle  =ListLevel[lev].RecExtens.ListSection[iSec].iCycle;
      int iDEcycle=ListLevel[lev].RecExtens.ListSection[iSec].iDEcycle;
      int iDEblock=ListLevel[lev].RecExtens.ListSection[iSec].iDEblock;
      int lenSec  =ListLevel[lev].RecExtens.ListSection[iSec].lenSec;
      int PosStart=ListLevel[lev].ListPosLenCycle[iCycle].pos;
      int lenCycle=ListLevel[lev].ListPosLenCycle[iCycle].len;
      //      std::cerr << "lenCycle=" << lenCycle << " lenSec=" << lenSec << "\n";
      for (int i=0; i<lenSec; i++) {
	int eDE=ListLevel[lev].ListCycleDEintDeg[PosStart + iDEcycle].eDE;
	int eDEinv=ListLevel[lev].ListVal[eDE].inv;
	int iDEblockInv=ListBlockType[iBlockType].ListVal[iDEblock].inv;
	//	std::cerr << "eDE=" << eDE << " eDEinv=" << eDEinv << " iDEblock=" << iDEblock << " iDEblockInv=" << iDEblockInv << "\n";
	Prov_StatusAttained[iDEblock]=1;
	Prov_StatusAttained[iDEblockInv]=-1;
	Prov_MapVect[iDEblock]=eDEinv;
	Prov_MapVect[iDEblockInv]=eDE;
	iDEcycle=NextIdx(lenCycle, iDEcycle);
	iDEblock=NextIdx(lenBound, iDEblock);
      }
    }
    int nbNewDE=0;
    for (int i=0; i<len; i++)
      if (Prov_StatusAttained[i] == 0)
	nbNewDE++;
    int New_nbUsedDE=nbNewDE + nbUsedDE;
    //    std::cerr << "len=" << len << "\n";
    //    std::cerr << "nbUsedDE=" << nbUsedDE << " nbNewDE=" << nbNewDE << " New_nbUsedDE=" << New_nbUsedDE << " nbDE=" << nbDE << "\n";
    if (New_nbUsedDE > nbDE)
      return 0;
    ListLevel[TheLevel].nbUsedDE=New_nbUsedDE;
    for (int i=0; i<len; i++)
      if (Prov_StatusAttained[i] == 0) {
	Prov_MapVect[i]=nbUsedDE;
	nbUsedDE++;
      }
    for (int i=0; i<len; i++) {
      int iNext=ListBlockType[iBlockType].ListVal[i].next;
      int iPrev=ListBlockType[iBlockType].ListVal[i].prev;
      int iInv =ListBlockType[iBlockType].ListVal[i].inv;
      //      std::cerr << "iNext=" << iNext << " iPrev=" << iPrev << " iInv=" << iInv << "\n";
      int iMap   =Prov_MapVect[i];
      int iInvMap=Prov_MapVect[iInv];
      int iNextMap, iPrevMap;
      if (iPrev != -1)
	iPrevMap=Prov_MapVect[iPrev];
      else
	iPrevMap=-1;
      if (iNext != -1)
	iNextMap=Prov_MapVect[iNext];
      else
	iNextMap=-1;
      if (Prov_StatusAttained[i] == 1) {
	if (iPrevMap != -1) {
	  std::cerr << "Inconsistent value here\n";
	  throw TerminalException{1};
	}
	iPrevMap=ListLevel[TheLevel-1].ListVal[iMap].prev;
      }
      if (Prov_StatusAttained[i] == -1) {
	if (iNextMap != -1) {
	  std::cerr << "Inconsistent value here\n";
	  throw TerminalException{1};
	}
	iNextMap=ListLevel[TheLevel-1].ListVal[iMap].next;
      }
      ListLevel[TheLevel].ListVal[iMap]={iNextMap, iPrevMap, iInvMap};
    }
    /*    std::cerr << "Result of integration process\n";
    for (int i=0; i<New_nbUsedDE; i++) {
      int iNext=ListLevel[TheLevel].ListVal[i].next;
      int iPrev=ListLevel[TheLevel].ListVal[i].prev;
      int iInv =ListLevel[TheLevel].ListVal[i].inv;
      std::cerr << "i=" << i << " iPrev=" << iPrev << " iNext=" << iNext << " iInv=" << iInv << "\n";
      }*/
    
    //    std::cerr << "Before ComputeConnectedComponents\n";
    ComputeConnectedComponents(TheLevel);
    if (ListLevel[TheLevel].nbCycle == 0) {
      int nbUsedDE=ListLevel[TheLevel].nbUsedDE;
      std::vector<permlib::dom_int> eListNext(nbUsedDE), eListInv(nbUsedDE);
      for (int iDE=0; iDE<nbUsedDE; iDE++) {
	eListNext[iDE] = ListLevel[TheLevel].ListVal[iDE].next;
	eListInv [iDE] = ListLevel[TheLevel].ListVal[iDE].inv;
      }
      PlanGraphOriented PLori{nbUsedDE, permlib::Permutation(eListInv), permlib::Permutation(eListNext)};
      FuncInsert(PLori);
    }
    //    std::cerr << " After ComputeConnectedComponents\n";
    return 1;
  };
  auto GoUpNextInTree_big=[&]() -> int {
    while(true) {
      TheLevel--;
      int test=NextInTree_sma(TheLevel);
      if (test == 1)
	return 1;
      if (TheLevel == 0)
	return 0;
    }
  };
  auto NextInTree_big=[&]() -> int {
    //    std::cerr << "Before ShiftFromChoices\n";
    int val1=ShiftFromChoices();
    //    std::cerr << "ShiftFromChoices val1=" << val1 << "\n";
    if (val1 == 0)
      return GoUpNextInTree_big();
    int res2=IsAdmissible(TheLevel);
    //    std::cerr << "IsAdmissible res2=" << res2 << "\n";
    if (res2 == 0)
      return GoUpNextInTree_big();
    int res3=FirstSetMapping(TheLevel);
    //    std::cerr << "FirstSetMapping, res3=" << res3 << "\n";
    if (res3 == 0)
      return GoUpNextInTree_big();
    return 1;
  };
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  for (int iBlockType=0; iBlockType<nbBlockType; iBlockType++) {
    iBlockFirst=iBlockType;
    AssignationFromIBlockType(iBlockType);
    int iter=0;
    while(true) {
      //      std::cerr << "iter=" << iter << " TheLevel=" << TheLevel << " nbBlock=" << nbBlock << "\n";
      //      std::cerr << "Before NextInTree_big\n";
      int test=NextInTree_big();
      //      std::cerr << " After NextInTree_big\n";
      if (test == 0)
	break;
      iter++;
      if (MAX_ITER != -1)
	if (iter >= MAX_ITER)
	  break;
      if (MAX_NB_GRAPH != -1)
	if (nbGraph >= MAX_NB_GRAPH)
	  break;
      if (MAX_NB_SECOND != -1) {
	if (iter % 10000 == 0) {
	  end = std::chrono::system_clock::now();
	  int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
	  if (elapsed_seconds > MAX_NB_SECOND)
	    break;
	}
      }
    }
    std::cerr << "iter=" << iter << " nbGraph=" << nbGraph << "\n";
  }
  delete Vlist;
  return ListRet;
}


//
// The SAT approach to finding maps
//

struct PairIBlockIDE {
  int iBlock;
  int iDE;
};


struct SATdataStructLegoProblem {
  bool SelfAdj;
  int nbBlock;
  int nbPair;
  int MaxLen;
  int MaxLenBound;
  std::vector<PairIBlockIDE> ListAllBasicPair;
  std::vector<int> ListNextPair;
  std::vector<int> ListPrevPair;
  int nbVar;
  std::vector<std::pair<int,int>> ListVar;
};


SATdataStructLegoProblem ComputeInitialDataStructureLegoProblem(std::vector<BlockTypeTot> const& ListBlock, int const& deg, bool const& SelfAdj)
{
  std::vector<PairIBlockIDE> ListAllBasicPair;
  int nbBlock=ListBlock.size();
  int MaxLen=0;
  int MaxLenBound=0;
  for (int iBlock=0; iBlock<nbBlock; iBlock++) {
    int len=ListBlock[iBlock].len;
    if (len > MaxLen)
      MaxLen=len;
    int lenBound=ListBlock[iBlock].lenBound;
    if (lenBound > MaxLenBound)
      MaxLenBound=lenBound;
  }
  MyMatrix<int> MatPair(nbBlock,MaxLenBound);
  int iPair=0;
  for (int iBlock=0; iBlock<nbBlock; iBlock++) {
    int lenBound=ListBlock[iBlock].lenBound;
    for (int iDE=0; iDE<lenBound; iDE++) {
      MatPair(iBlock,iDE)=iPair;
      ListAllBasicPair.push_back({iBlock, iDE});
      iPair++;
    }
  }
  int nbPair=ListAllBasicPair.size();
  std::cerr << "nbPair = " << nbPair << "\n";
  std::vector<int> ListNextPair(nbPair), ListPrevPair(nbPair);
  for (int iPair=0; iPair<nbPair; iPair++) {
    int iBlock=ListAllBasicPair[iPair].iBlock;
    int iDE   =ListAllBasicPair[iPair].iDE;
    int lenBound=ListBlock[iBlock].lenBound;
    int iDEnext=NextIdx(lenBound,iDE);
    int iDEprev=PrevIdx(lenBound,iDE);
    int iPairNext=MatPair(iBlock,iDEnext);
    int iPairPrev=MatPair(iBlock,iDEprev);
    ListNextPair[iPair]=iPairNext;
    ListPrevPair[iPair]=iPairPrev;
    //    std::cerr << "iPair=" << iPair << " iPairNext=" << iPairNext << " iPairPrev=" << iPairPrev << "\n";
  }
  //
  // Form the variables themselves
  //
  std::vector<std::pair<int,int>> ListVar;
  for (int iPair=0; iPair<nbPair-1; iPair++)
    for (int jPair=iPair+1; jPair<nbPair; jPair++)
      if (SelfAdj || ListAllBasicPair[iPair].iBlock != ListAllBasicPair[jPair].iBlock) {
	ListVar.push_back({iPair,jPair});
      }
  int nbVar=ListVar.size();
  for (int iVar=0; iVar<nbVar; iVar++) {
    int iPair=ListVar[iVar].first;
    int jPair=ListVar[iVar].second;
    int iBlock=ListAllBasicPair[iPair].iBlock;
    int iDE   =ListAllBasicPair[iPair].iDE;
    int jBlock=ListAllBasicPair[jPair].iBlock;
    int jDE   =ListAllBasicPair[jPair].iDE;
    std::cerr << "iVar = " << iVar << " iPair=" << iPair << "(" << iBlock << "/" << iDE << ") jPair=" << jPair << "(" << jBlock << "/" << jDE << ")\n";
  }
  return {SelfAdj, nbBlock, nbPair, MaxLen, MaxLenBound, ListAllBasicPair, ListNextPair, ListPrevPair, nbVar, ListVar};
}


int FindIVar(SATdataStructLegoProblem const& eData, int const& iPair, int const& jPair)
{
  if (iPair > jPair)
    return FindIVar(eData, jPair, iPair);
  if (eData.SelfAdj) {
    int pos=0;
    for (int u=0; u<iPair; u++)
      pos += eData.nbPair-1-u;
    pos += jPair - iPair - 1;
    return pos;
  }
  else {
    for (int iVar=0; iVar<eData.nbVar; iVar++)
      if (eData.ListVar[iVar] == std::pair<int,int>({iPair,jPair}))
	return iVar;
  }
  return -1;
};






SATformulation GetSATformulationSeriesBlock(std::vector<BlockTypeTot> const& ListBlock, int const& deg, SATdataStructLegoProblem const& eData)
{
  int nbVar=eData.ListVar.size();
  std::cerr << "nbVar = " << nbVar << "\n";
  //
  // The list of conditions.
  //
  std::vector<std::vector<int>> ListConditions;
  //
  // Exclusions rules: If one iPair is selected then the other ones should not be
  //
  int nbPair=eData.nbPair;
  std::vector<std::vector<int>> ListGroupVar(nbPair);
  for (int iVar=0; iVar<nbVar; iVar++) {
    int iPair=eData.ListVar[iVar].first;
    int jPair=eData.ListVar[iVar].second;
    ListGroupVar[iPair].push_back(iVar);
    ListGroupVar[jPair].push_back(iVar);
  }
  for (int iPair=0; iPair<nbPair; iPair++) {
    int len=ListGroupVar[iPair].size();
    /*    std::cerr << "iPair=" << iPair << " ListGroupVar=";
    for (auto & eVal : ListGroupVar[iPair])
      std::cerr << " " << eVal;
      std::cerr << "\n";*/
    for (int i=0; i<len-1; i++)
      for (int j=i+1; j<len; j++) {
	int iVarP1=ListGroupVar[iPair][i]+1;
	int jVarP1=ListGroupVar[iPair][j]+1;
	ListConditions.push_back({-iVarP1,-jVarP1});
      }
  }
  std::cerr << "First part of conditions obtained |ListConditions|=" << ListConditions.size() << "\n";
  //
  // At least one selected
  //
  for (int iPair=0; iPair<nbPair; iPair++) {
    int len=ListGroupVar[iPair].size();
    std::vector<int> eList(len);
    for (int i=0; i<len; i++) {
      int iVarP1=ListGroupVar[iPair][i]+1;
      eList[i]=iVarP1;
    }
    ListConditions.push_back(eList);
  }
  std::cerr << "Second part of conditions obtained |ListConditions|=" << ListConditions.size() << "\n";
  //
  // Cycle enumeration: the big work is here
  //
  struct eInfo {
    std::vector<int> eListVar;
    std::vector<int> ListPairInvolved;
    int LastPair;
    int sumDeg;
  };
  auto InsertBadConf=[&](eInfo const& eConf) -> void {
    //    std::cerr << "InsertBadConf:";
    int siz=eConf.eListVar.size();
    std::vector<int> eIns(siz);
    //    std::vector<int> eSet=VectorAsSet(eConf.eListVar);
    //    for (auto & eVal : eSet)
    //      std::cerr << " " << eVal;
    for (int i=0; i<siz; i++) {
      int eVal=eConf.eListVar[i];
      //      std::cerr << " " << eVal;
      int iVarP1=eVal+1;
      eIns[i]=-iVarP1;
    }
    //    std::cerr << "  ListPairInvolved =";
    //    for (auto & eVal : eConf.ListPairInvolved) {
    //      int iBlock=eData.ListAllBasicPair[eVal].iBlock;
    //      int iDE=eData.ListAllBasicPair[eVal].iDE;
    //      std::cerr << " " << eVal << "(" << iBlock << "/" << iDE << ")";
    //    }
    //    std::cerr << " LastPair=" << eConf.LastPair << "\n";
    ListConditions.push_back(eIns);
  };
  for (int iPair=0; iPair<nbPair; iPair++) {
    std::cerr << "iPair=" << iPair << " / " << nbPair << " Now |ListConditions|=" << ListConditions.size() << " deg=" << deg << "\n";
    int jPair=eData.ListPrevPair[iPair];
    int iBlockFirst=eData.ListAllBasicPair[iPair].iBlock;
    int iDEfirst=eData.ListAllBasicPair[iPair].iDE;
    int degBasic=ListBlock[iBlockFirst].ListInternalDeg[iDEfirst];
    std::vector<eInfo> ListConf;
    ListConf.push_back({{},{},jPair,degBasic});
    for (int i=0; i<deg; i++) {
      std::vector<eInfo> NewListConf;
      for (auto & eConf : ListConf) {
	int LastPair=eConf.LastPair;
	for (auto & eVar : ListGroupVar[LastPair]) {
	  bool IsCorrect=true;
	  if (PositionVect(eConf.eListVar, eVar) != -1)
	    IsCorrect=false;
	  if (PositionVect(eConf.ListPairInvolved, eData.ListVar[eVar].first) != -1)
	    IsCorrect=false;
	  if (PositionVect(eConf.ListPairInvolved, eData.ListVar[eVar].second) != -1)
	    IsCorrect=false;
	  if (i>0) {
	    if (eVar <= eConf.eListVar[i-1])
	      IsCorrect=false;
	  }
	  if (IsCorrect) {
	    eInfo NewConf=eConf;
	    NewConf.eListVar.push_back(eVar);
	    int ePair;
	    if (eData.ListVar[eVar].first == LastPair)
	      ePair=eData.ListVar[eVar].second;
	    else
	      ePair=eData.ListVar[eVar].first;
	    NewConf.ListPairInvolved.push_back(LastPair);
	    NewConf.ListPairInvolved.push_back(ePair);
	    int iBlockFound=eData.ListAllBasicPair[ePair].iBlock;
	    int iDEfound=eData.ListAllBasicPair[ePair].iDE;
	    int TheDeg=ListBlock[iBlockFound].ListInternalDeg[iDEfound];
	    int NewLastPair=eData.ListPrevPair[ePair];
	    if (NewLastPair == jPair) {
	      if (NewConf.sumDeg < deg || NewConf.sumDeg > deg)
		InsertBadConf(NewConf);
	    }
	    else {
	      NewConf.sumDeg += TheDeg;
	      NewConf.LastPair=NewLastPair;
	      if (NewConf.sumDeg > deg)
		InsertBadConf(NewConf);
	      else
		NewListConf.push_back(NewConf);
	    }
	  }
	}
      }
      std::cerr << "|ListConf|=" << ListConf.size() << " |NewListConf|=" << NewListConf.size() << "\n";
      ListConf=NewListConf;
    }
  }
  std::cerr << "Third part of conditions obtained |ListConditions|=" << ListConditions.size() << "\n";
  return {eData.nbVar, ListConditions};
}


std::vector<PlanGraphOriented> SAT_EnumerationBlockGraphOriented(std::vector<BlockTypeTot> const& ListBlock, int const& deg, int const& Genus, bool const& SelfAdj, int const& MAX_ITER, int const& MAX_NB_GRAPH)
{
  int nbBlock=ListBlock.size();
  SATdataStructLegoProblem eData=ComputeInitialDataStructureLegoProblem(ListBlock, deg, SelfAdj);
  SATformulation TheFormulation=GetSATformulationSeriesBlock(ListBlock, deg, eData);

  
  int MaxLen=eData.MaxLen;
  int MaxLenBound=eData.MaxLenBound;
  int nbVar=TheFormulation.nbVar;
  std::vector<PlanGraphOriented> ListRet;
  auto FuncInsert=[&](PlanGraphOriented const& PLori) -> void {
    int Charac=EulerPoincareCharacteristic(PLori);
    if (Charac != 2 - 2*Genus)
      return;
    for (auto & fPLori : ListRet)
      if (IsEquivalentPlanGraphOriented(PLori, fPLori))
	return;
    ListRet.push_back(PLori);
    std::cerr << "|ListRet|=" << ListRet.size() << "\n";
  };
  int nbDE=0;
  for (int iBlock=0; iBlock<nbBlock; iBlock++) {
    int len=ListBlock[iBlock].len;
    int lenBound=ListBlock[iBlock].lenBound;
    nbDE += len - lenBound;
  }
  std::cerr << "nbDE=" << nbDE << "\n";
  MyMatrix<int> KillIBlockIDE(nbBlock, 2*MaxLenBound);
  std::vector<int> ListMatch(nbBlock);
  MyMatrix<int> MatrixDEdescription(nbDE,2);
  MyMatrix<int> MatrixDEdescriptionRev(nbBlock,MaxLen);
  MyMatrix<int> MapPairPair_Block(nbBlock,MaxLen);
  MyMatrix<int> MapPairPair_DE(nbBlock,MaxLen);
  std::function<int(Face const&)> f=[&](Face const& eFace) -> int {
    if (!IsFeasible(TheFormulation, eFace)) {
      std::cerr << "The solution is not feasible\n";
      exit(1);
    }
    for (int iBlock=0; iBlock<nbBlock; iBlock++)
      ListMatch[iBlock]=0;
    std::cerr << "nbBlock=" << nbBlock << "\n";
    int nbVarSelected=0;
    for (int iBlock=0; iBlock<nbBlock; iBlock++)
      for (int i=0; i<MaxLen; i++) {
	MapPairPair_Block(iBlock,i)=-1;
	MapPairPair_DE(iBlock,i)=-1;
	MatrixDEdescriptionRev(iBlock,i)=-1;
      }
    for (int iBlock=0; iBlock<nbBlock; iBlock++)
      for (int i=0; i<2*MaxLenBound; i++)
	KillIBlockIDE(iBlock, i)=-1;
    for (int iVar=0; iVar<nbVar; iVar++)
      if (eFace[iVar] == 1) {
	nbVarSelected++;
	//	std::cerr << "iVar=" << iVar << "\n";
	//	int iPair=eData.ListVar[iVar].first;
	//	int iBlock=eData.ListAllBasicPair[iPair].iBlock;
	//	int iDE1=eData.ListAllBasicPair[iPair].iDE;
	//	int iDE2=ListBlock[iBlock].ListVal[iDE1].inv;
	
	int jPair=eData.ListVar[iVar].second;
	int jBlock=eData.ListAllBasicPair[jPair].iBlock;
	int jDE1=eData.ListAllBasicPair[jPair].iDE;
	int jDE2=ListBlock[jBlock].ListVal[jDE1].inv;
	//	std::cerr << "Block(i/j)=" << iBlock << "/" << jBlock << "  iDE1/2=" << iDE1 << "/" << iDE2 << "  jDE1/2=" << jDE1 << "/" << jDE2 << "\n";
	int pos=ListMatch[jBlock];
	//	std::cerr << "iBlock=" << iBlock << "  pos=" << pos << " MaxLen=" << MaxLen << " MaxLenBound=" << MaxLenBound << "\n";
	KillIBlockIDE(jBlock,pos)=jDE1;
	KillIBlockIDE(jBlock,pos+1)=jDE2;
	ListMatch[jBlock]=pos+2;
      }
    std::cerr << "nbVarSelected=" << nbVarSelected << "\n";
    std::cerr << "After KillBlockIDE loop\n";
    auto IsPairKill=[&](int const& uBlock, int const& uDE) -> bool {
      int len=ListMatch[uBlock];
      for (int u=0; u<len; u++)
	if (KillIBlockIDE(uBlock,u) == uDE)
	  return true;
      return false;
    };
    int posDE=0;
    for (int iBlock=0; iBlock<nbBlock; iBlock++) {
      int len=ListBlock[iBlock].len;
      for (int iDE=0; iDE<len; iDE++)
	if (!IsPairKill(iBlock,iDE)) {
	  //	  std::cerr << "MatrixDEdescription iBlock=" << iBlock << " iDE=" << iDE << " posDE=" << posDE << "\n";
	  if (posDE >= nbDE) {
	    std::cerr << "We should not be in that situation\n";
	    exit(1);
	  }
	  MatrixDEdescription(posDE,0)=iBlock;
	  MatrixDEdescription(posDE,1)=iDE;
	  MatrixDEdescriptionRev(iBlock,iDE)=posDE;
	  //	  std::cerr << "After assignations\n";
	  posDE++;
	}
    }
    for (int iVar=0; iVar<nbVar; iVar++)
      if (eFace[iVar] == 1) {
	int iPair=eData.ListVar[iVar].first;
	int jPair=eData.ListVar[iVar].second;
	int iBlock=eData.ListAllBasicPair[iPair].iBlock;
	int jBlock=eData.ListAllBasicPair[jPair].iBlock;
	int iDE1=eData.ListAllBasicPair[iPair].iDE;
	int jDE1=eData.ListAllBasicPair[jPair].iDE;
	int iDE2=ListBlock[iBlock].ListVal[iDE1].inv;
	int jDE2=ListBlock[jBlock].ListVal[jDE1].inv;
	int posDE1=MatrixDEdescriptionRev(iBlock,iDE1);
	int posDE2=MatrixDEdescriptionRev(iBlock,iDE2);
	std::cerr << "jDE1=" << jDE1 << " jDE2=" << jDE2 << "\n";
	MatrixDEdescriptionRev(jBlock,jDE1)=posDE2;
	MatrixDEdescriptionRev(jBlock,jDE2)=posDE1;
	MapPairPair_Block(iBlock,iDE1)=jBlock;
	MapPairPair_Block(iBlock,iDE2)=jBlock;
	MapPairPair_DE(iBlock,iDE1)=jDE2;
	MapPairPair_DE(iBlock,iDE2)=jDE1;
	std::cerr << "After assignations\n";
      }
    std::vector<permlib::dom_int> eListInv(nbDE), eListNext(nbDE);
    for (int iDEfull=0; iDEfull<nbDE; iDEfull++) {
      int iBlock=MatrixDEdescription(iDEfull,0);
      int iDE=MatrixDEdescription(iDEfull,1);
      int iDEinv=ListBlock[iBlock].ListVal[iDE].inv;
      int invDEfull=MatrixDEdescriptionRev(iBlock,iDEinv);
      eListInv[iDEfull]=invDEfull;
      //
      int iDEnext=ListBlock[iBlock].ListVal[iDE].next;
      int nextDEfull;
      if (iDEnext != -1) {
	nextDEfull=MatrixDEdescriptionRev(iBlock,iDEnext);
      }
      else {
	int jBlock=MapPairPair_Block(iBlock,iDE);
	int jDE=MapPairPair_DE(iBlock,iDE);
	if (jDE == -1) {
	  std::cerr << "Major error. Value should not be -1\n";
	  std::cerr << "jBlock=" << jBlock << " jDE=" << jDE << "\n";
	  exit(1);
	}
	int jDEnext=ListBlock[jBlock].ListVal[jDE].next;
	nextDEfull=MatrixDEdescriptionRev(jBlock,jDEnext);
      }
      eListNext[iDEfull]=nextDEfull;
    }
    PlanGraphOriented PLori{nbDE, permlib::Permutation(eListInv), permlib::Permutation(eListNext)};
    FuncInsert(PLori);
    int siz=ListRet.size();
    if (MAX_NB_GRAPH != -1)
      if (siz >= MAX_NB_GRAPH)
	return 1;
    return 0;
  };
  SATenumeration(TheFormulation, MAX_ITER, f);
  return ListRet;
}



FullNamelist NAMELIST_GetStandardLegoEnumeration()
{
  std::map<std::string, SingleBlock> ListBlock;
  // PROC
  std::map<std::string, int> ListIntValues1;
  std::map<std::string, bool> ListBoolValues1;
  std::map<std::string, std::string> ListStringValues1;
  std::map<std::string, std::vector<int> > ListListIntValues1;

  ListStringValues1["FileListBlockType"]="unset";
  ListStringValues1["OutFile"]="unset";
  ListStringValues1["Approach"]="unset";
  ListListIntValues1["ListMult"]={};
  ListIntValues1["deg"]=-1;
  ListIntValues1["Genus"]=0;
  ListIntValues1["MAX_ITER"]=-1;
  ListIntValues1["MAX_NB_GRAPH"]=-1;
  ListIntValues1["MAX_NB_SECOND"]=-1;
  ListBoolValues1["SelfAdj"]=false;
  SingleBlock BlockENUM;
  BlockENUM.ListIntValues=ListIntValues1;
  BlockENUM.ListBoolValues=ListBoolValues1;
  BlockENUM.ListStringValues=ListStringValues1;
  BlockENUM.ListListIntValues=ListListIntValues1;
  ListBlock["ENUM"]=BlockENUM;
  return {ListBlock, "undefined"};
}


void GeneralEnumerationLego(FullNamelist const& eFull)
{
  SingleBlock BlENUM=eFull.ListBlock.at("ENUM");
  std::string FileListBlockType=BlENUM.ListStringValues.at("FileListBlockType");
  std::string OutFile=BlENUM.ListStringValues.at("OutFile");
  std::vector<int> ListMult=BlENUM.ListListIntValues.at("ListMult");
  int deg=BlENUM.ListIntValues.at("deg");
  int Genus=BlENUM.ListIntValues.at("Genus");
  bool SelfAdj=BlENUM.ListBoolValues.at("SelfAdj");
  int MAX_ITER=BlENUM.ListIntValues.at("MAX_ITER");
  int MAX_NB_GRAPH=BlENUM.ListIntValues.at("MAX_NB_GRAPH");
  int MAX_NB_SECOND=BlENUM.ListIntValues.at("MAX_NB_SECOND");
  std::string Approach=eFull.ListBlock.at("ENUM").ListStringValues.at("Approach");

  std::vector<BlockTypeTot> ListBlockType=ReadListBlockTypeTot(FileListBlockType);
  std::vector<BlockTypeTot> ListBlock;
  int nbBlockType=ListBlockType.size();
  int nbBlock=0;
  for (int iBlockType=0; iBlockType<nbBlockType; iBlockType++) {
    int eMult=ListMult[iBlockType];
    nbBlock += eMult;
    for (int i=0; i<eMult; i++)
      ListBlock.push_back(ListBlockType[iBlockType]);
  }
  std::vector<PlanGraphOriented> ListRet;
  bool WeAreDone=false;
  if (Approach == "SAT") {
    ListRet=SAT_EnumerationBlockGraphOriented(ListBlock, deg, Genus, SelfAdj, MAX_ITER, MAX_NB_GRAPH);
    WeAreDone=true;
  }
  if (Approach == "EXHAUST") {
    int nbDE=0;
    for (int iBlock=0; iBlock<nbBlock; iBlock++) {
      int len=ListBlock[iBlock].len;
      int lenBound=ListBlock[iBlock].lenBound;
      nbDE += len - lenBound;
    }
    ListRet=TotalEnumerationBlock(ListBlockType, ListMult, nbBlock, Genus, deg, nbDE, MAX_ITER, MAX_NB_GRAPH, MAX_NB_SECOND);
    WeAreDone=true;
  }
  if (!WeAreDone) {
    std::cerr << "Failed to find a matching entry\n";
    throw TerminalException{1};
  }
    
  int nbRet=ListRet.size();
  std::cerr << "nbRet=" << nbRet << " OutFile = " << OutFile << "\n";
  std::ofstream os(OutFile);
  os << "return [";
  for (int iRet=0; iRet<nbRet; iRet++) {
    if (iRet > 0)
      os << ",\n";
    WritePlanGraphOrientedGAP(os, ListRet[iRet]);
  }
  os << "];\n";
}

#endif
