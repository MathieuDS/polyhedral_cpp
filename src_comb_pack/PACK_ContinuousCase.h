#ifndef PACK_CONTINUOUS_CASE
#define PACK_CONTINUOUS_CASE

#include "GRAPH_bliss.h"
#include "GRAPH_BitsetType.h"
#include "hash_functions.h"

/*
  The cube coordinates are assigned one by one.
  Their position will be something like
  [t1 + alpha1 , t2 + alpha2 , ..... , tN + alphaN]
  ---
  This makes a std::vector a standard description of one cube.
  The alphaI has to be an integer between 0 and d-1.
  ---
  If unassigned then the PairV is set to {-1,0}.
  At the end of the process the unassigned are set to new
  parameters.

 */

struct PairPV {
  int indexParam;
  int value;
};

using CoordCube = std::vector<PairPV>;



std::vector<CoordCube> CombPack_FindExtensionCandidates(int const& n, int const& d, std::vector<CoordCube> const& PreviousList)
{
  int MaxParam=-1;
  for (auto & eCube : PreviousList)
    for (auto & eCoord : eCube)
      MaxParam = std::max(MaxParam, eCoord.indexParam);
  //  std::cerr << "MaxParam=" << MaxParam << "\n";
  //
  CoordCube NakedCand = std::vector<PairPV>(n, {-1,0});
  std::vector<CoordCube> ListCand{NakedCand};
  for (auto & eCube : PreviousList) {
    std::vector<CoordCube> NewListCand;
    for (auto & fCube : ListCand) {
      bool IsAdj=false;
      for (int i=0; i<n; i++) {
        if (fCube[i].indexParam != -1) {
          if (eCube[i].indexParam == fCube[i].indexParam) {
            if (eCube[i].value != fCube[i].value)
              IsAdj=true;
          }
        }
      }
      if (IsAdj) {
        NewListCand.push_back(fCube);
      }
      else {
        for (int i=0; i<n; i++) {
          if (fCube[i].indexParam == -1) {
            for (int eVal=0; eVal<d; eVal++) {
              if (eVal != eCube[i].value) {
                CoordCube nCube = fCube;
                nCube[i] = {eCube[i].indexParam, eVal};
                NewListCand.push_back(nCube);
              }
            }
          }
        }
      }
    }
    ListCand = NewListCand;
  }
  //
  std::vector<int> ListNumberEmpty;
  int MaxNumberEmpty=0;
  for (auto & eCube : ListCand) {
    int nbEmpty=0;
    for (int i=0; i<n; i++)
      if (eCube[i].indexParam == -1)
        nbEmpty++;
    ListNumberEmpty.push_back(nbEmpty);
    MaxNumberEmpty = std::max(MaxNumberEmpty, nbEmpty);
  }
  std::vector<CoordCube> RetList;
  for (int iCand=0; iCand<int(ListCand.size()); iCand++) {
    if (ListNumberEmpty[iCand] == MaxNumberEmpty) {
      CoordCube eCube = ListCand[iCand];
      int idx = MaxParam;
      for (int i=0; i<n; i++) {
        if (eCube[i].indexParam == -1) {
          idx++;
          eCube[i] = {idx, 0};
        }
      }
      RetList.push_back(eCube);
    }
  }
  return RetList;
}

template<typename Tgr>
Tgr CombPack_GetWeightMatrix(int const& n, int const& d, std::vector<CoordCube> const& TheList)
{
  int MaxParam=-1;
  for (auto & eCube : TheList)
    for (auto & eCoord : eCube)
      MaxParam = std::max(MaxParam, eCoord.indexParam);
  int nbParam = MaxParam + 1;
  int nbCell=TheList.size();
  int nbVert = n * ( nbCell + nbParam * d);
  Tgr eGR(nbVert);
  eGR.SetHasColor(true);
  // Form the structure
  for (int iCell=0; iCell<nbCell; iCell++)
    for (int i=0; i<n-1; i++)
      for (int j=i+1; j<n; j++) {
        int ePt1 = n*iCell + i;
        int ePt2 = n*iCell + j;
        eGR.AddAdjacent(ePt1, ePt2);
        eGR.AddAdjacent(ePt2, ePt1);
      }
  for (int iCell=0; iCell<nbCell-1; iCell++)
    for (int jCell=iCell+1; jCell<nbCell; jCell++)
      for (int i=0; i<n; i++) {
        int ePt1 = n*iCell + i;
        int ePt2 = n*jCell + i;
        eGR.AddAdjacent(ePt1, ePt2);
        eGR.AddAdjacent(ePt2, ePt1);
      }
  for (int iCell=0; iCell<nbCell; iCell++)
    for (int i=0; i<n; i++) {
      int ePt = n*iCell + i;
      eGR.SetColor(ePt, 0);
    }
  //
  // The cycle structure of parameters
  //
  for (int iParam=0; iParam<nbParam; iParam++) {
    for (int iVal=0; iVal<d; iVal++) {
      for (int jVal=0; jVal<d; jVal++) {
        int ePt1 = n*nbCell + d*iParam + iVal;
        int ePt2 = n*nbCell + d*iParam + jVal;
        eGR.AddAdjacent(ePt1, ePt2);
        eGR.AddAdjacent(ePt2, ePt1);
      }
    }
  }
  for (int iParam=0; iParam<nbParam; iParam++) {
    for (int iVal=0; iVal<d; iVal++) {
      int ePt = n*nbCell + d*iParam + iVal;
      eGR.SetColor(ePt, 1);
    }
  }
  //
  // Building the adjacencies of the packing itself.
  //
  for (int iCell=0; iCell<nbCell; iCell++) {
    for (int i=0; i<n; i++) {
      int iParam = TheList[iCell][i].indexParam;
      int iVal = TheList[iCell][i].value;
      int ePt1 = n*iCell + i;
      int ePt2 = n*nbCell + d*iParam + iVal;
      eGR.AddAdjacent(ePt1, ePt2);
      eGR.AddAdjacent(ePt2, ePt1);
    }
  }
  return eGR;
}


struct PackingComb {
  std::vector<CoordCube> ListCell;
  std::string inv;
};




std::vector<std::vector<CoordCube>> EnumerationNonExtensiblePacking(int const& n, int const& d)
{
  std::vector<PackingComb> ListMaximal;
  //
  auto GetGR=[&](PackingComb const& ePack) -> GraphBitset {
    return CombPack_GetWeightMatrix<GraphBitset>(n, d, ePack.ListCell);
  };
  auto FuncInsert=[&](std::vector<PackingComb> & TheList, PackingComb const& ePack) -> void {
    for (auto & fPack : TheList) {
      if (ePack.inv == fPack.inv) {
        if (IsIsomorphicGraph(GetGR(ePack), GetGR(fPack)))
          return;
      }
    }
    TheList.push_back(ePack);
  };
  auto GetPackingComb=[&](std::vector<CoordCube> const& tList) -> PackingComb {
    return {tList, MD5_hash_string(GetCanonicalForm_string(CombPack_GetWeightMatrix<GraphBitset>(n, d, tList)))};
  };
  //
  PackingComb EmptyPacking{ {}, ""};
  std::vector<PackingComb> WorkingList{EmptyPacking};
  int iter=0;
  while(true) {
    std::vector<PackingComb> NewWorkingList;
    for (auto & ePack : WorkingList) {
      std::vector<CoordCube> ListExtens = CombPack_FindExtensionCandidates(n, d, ePack.ListCell);
      //      std::cerr << "|ListExtens|=" << ListExtens.size() << "\n";
      if (ListExtens.size() == 0) {
        FuncInsert(ListMaximal, ePack);
      }
      else {
        for (auto & ePoss : ListExtens) {
          std::vector<CoordCube> NewCubePack = ePack.ListCell;
          NewCubePack.push_back(ePoss);
          PackingComb NewPackComb = GetPackingComb(NewCubePack);
          FuncInsert(NewWorkingList, NewPackComb);
        }
      }
    }
    std::cerr << "iter=" << iter << " |NewWorkingList|=" << NewWorkingList.size() << " |ListMaximal|=" << ListMaximal.size() << "\n";
    iter++;
    if (NewWorkingList.size() == 0)
      break;
    WorkingList = NewWorkingList;
  }
  //
  std::vector<std::vector<CoordCube>> RetList;
  for (auto & ePackingComb : ListMaximal)
    RetList.push_back(ePackingComb.ListCell);
  return RetList;
}


void PrintSinglePacking(std::ostream& os, std::vector<CoordCube> const& singlePack)
{
  int nbCube=singlePack.size();
  os << "[";
  for (int iCube=0; iCube<nbCube; iCube++) {
    if (iCube > 0)
      os << ",\n";
    CoordCube eCube = singlePack[iCube];
    os << "[";
    int n=eCube.size();
    for (int i=0; i<n; i++) {
      int iParam=eCube[i].indexParam+1;
      int value=eCube[i].value;
      if (i > 0)
        os << ",";
      if (value == 0)
        os << "t" << iParam;
      else
        os << "t" << iParam << "+" << value;
    }
    os << "]";
  }
  os << "]";
}




#endif
