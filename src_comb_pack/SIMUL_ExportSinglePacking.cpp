#include "COMB_RandomSimulationPacking.h"
int main(int argc, char *argv[])
{
  try {
    FullNamelist eFull=NAMELIST_GetPackingFunctionality();
    if (argc != 2) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used as\n";
      std::cerr << "SIMUL_ExportSinglePacking [FileOption.nml]\n";
      std::cerr << "\n";
      std::cerr << "FileOption.nml : The filename containing the list of options:\n";
      NAMELIST_WriteNamelistFile(std::cerr, eFull);
      return -1;
    }
    std::string eFileName=argv[1];
    NAMELIST_ReadNamelistFile(eFileName, eFull);
    //
    SingleBlock BlOption = eFull.ListBlock.at("OPTION");
    int valX = BlOption.ListIntValues.at("X");
    int valY = BlOption.ListIntValues.at("Y");
    int valD = BlOption.ListIntValues.at("d");
    std::string eFileRead = BlOption.ListStringValues.at("FileRead");
    std::string eFileScad = BlOption.ListStringValues.at("FileScad");
    //
    std::ifstream is(eFileRead);
    if (!IsExistingFile(eFileRead)) {
      std::cerr << "The file eFileRead=" << eFileRead << " does not exist\n";
      throw TerminalException{1};
    }
    MyMatrix<int> ListCornerCube=ReadMatrix<int>(is);
    int nbCorner=ListCornerCube.rows();
    std::cerr << "nbCorner=" << nbCorner << "\n";
    MyMatrix<int> ListUncoveredPoint = GetUncoveredPoint(valX, valY, valD, ListCornerCube);
    int nbUncov = ListUncoveredPoint.rows();
    std::cerr << "nbUncov=" << nbUncov << "\n";
    //
    if (eFileScad != "unset") {
      if (valD != 3) {
	std::cerr << "We should have dimension d=" << valD << " equal to 3\n";
	throw TerminalException{1};
      }
      int nbTrans=BlOption.ListIntValues.at("nbTrans");
      double RadiusSphere = BlOption.ListDoubleValues.at("RadiusSphere");
      std::vector<int> ColorSphere = BlOption.ListListIntValues.at("ColorSphere");
      std::vector<int> ColorTile = BlOption.ListListIntValues.at("ColorTile");
      bool FillHoleSphere = BlOption.ListBoolValues.at("FillHoleSphere");
      bool PutCubicTile = BlOption.ListBoolValues.at("PutCubicTile");
      double HalfCubeSize = BlOption.ListDoubleValues.at("HalfCubeSize");
      MyMatrix<int> ListTrans = BuildSet(valD, nbTrans);
      int nbTransTot = ListTrans.rows();
      //
      std::vector<LineOpenscad> ListLine;
      std::vector<PlaneOpenscad> ListPlane;
      std::vector<SphereOpenscad> ListSphere;
      std::vector<ArrowOpenscad> ListArrow;
      for (int iTransTot=0; iTransTot<nbTransTot; iTransTot++) {
	MyVector<int> eTransTot = GetMatrixRow(ListTrans, iTransTot);
	if (FillHoleSphere) {
	  for (int iUncov=0; iUncov<nbUncov; iUncov++) {
	    MyVector<int> eUncov = GetMatrixRow(ListUncoveredPoint, iUncov);
	    MyVector<double> eV(3);
	    for (int i=0; i<3; i++)
	      eV(i) = double(eUncov(i) + valX * eTransTot(i)) + 0.5;
	    SphereOpenscad eSph{eV, RadiusSphere, ColorSphere};
	    ListSphere.push_back(eSph);
	  }
	}
	if (PutCubicTile) {
	  for (int iCorner=0; iCorner<nbCorner; iCorner++) {
	    MyVector<int> eCorner = GetMatrixRow(ListCornerCube, iCorner);
	    MyVector<double> R = VectorWithIdenticalEntries(3, HalfCubeSize);
	    MyVector<double> V(3);
	    for (int i=0; i<3; i++) {
	      int eCorner_xyz = eCorner(i) + valX * eTransTot(i);
	      V(i) = double(eCorner_xyz) + double(valY) / double(2);
	    }
	    std::vector<PlaneOpenscad> LPlane = GenerateParallelepiped(V, R, ColorTile);
	    ListPlane.insert(ListPlane.end(), LPlane.begin(), LPlane.end());
	  }
	}
      }
      std::cerr << "|ListPlane|=" << ListPlane.size() << "\n";
      std::cerr << "|ListSphere|=" << ListSphere.size() << "\n";
      DescOpenscad eDesc{ListLine, ListPlane, ListSphere, ListArrow};
      WriteOpenSCADinfo(eFileScad, eDesc);
    }

  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
}
