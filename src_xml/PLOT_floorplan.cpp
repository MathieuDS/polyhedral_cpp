#include "floorplan.h"

int main(int argc, char *argv[])
{
  try {
    FullNamelist eFull = FloorplanPlotting();
    if (argc != 2) {
      std::cerr << "Number of argument is = " << argc << "\n";
      std::cerr << "This program is used to transpose XML file to openscad\n";
      std::cerr << "PLOT_floorplan [input.nml]\n";
      NAMELIST_WriteNamelistFile(std::cerr, eFull);
      return -1;
    }
    std::string eFileName=argv[1];
    NAMELIST_ReadNamelistFile(eFileName, eFull);
    //
    SingleBlock eBlPLOT=eFull.ListBlock.at("PLOT");
    std::string XMLfile=eBlPLOT.ListStringValues.at("XMLfile");
    std::string OpenScadFile=eBlPLOT.ListStringValues.at("OpenScadFile");
    double RadiusLine  = eBlPLOT.ListDoubleValues.at("RadiusLine");
    double RadiusSphere= eBlPLOT.ListDoubleValues.at("RadiusSphere");
    std::vector<int> ColorLine = eBlPLOT.ListListIntValues.at("ColorLine");
    std::vector<int> ColorPlane = eBlPLOT.ListListIntValues.at("ColorPlane");
    std::vector<int> ColorSphere = eBlPLOT.ListListIntValues.at("ColorSphere");
    //
    OptionFloorPlot recOpt;
    recOpt.RadiusLine = RadiusLine;
    recOpt.RadiusSphere = RadiusSphere;
    recOpt.ColorLine = ColorLine;
    recOpt.ColorPlane = ColorPlane;
    recOpt.ColorSphere = ColorSphere;
    //
    std::string content = ReadXmlFile(XMLfile);
    std::cerr << "|content| = " << content.size() << "\n";
    //
    std::pair<DescOpenscad,FloorplanInfo> ePairDescFloor = OpenscadDescriptionFloorplan(content, recOpt);
    //
    WriteOpenSCADinfo(OpenScadFile, ePairDescFloor.first);
  }
  catch (TerminalException const& e) {
    exit(e.eVal);
  }
  std::cerr << "Completion of the program\n";
}
