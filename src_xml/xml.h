#ifndef INCLUDE_XML
#define INCLUDE_XML

#include "Temp_common.h"
#include "Basic_file.h"

#include <rapidxml.hpp>
//#include <rapidxml_iterators.hpp>
#include <rapidxml_print.hpp>
#include <rapidxml_utils.hpp>



std::string ReadXmlFile(std::string const& eFile)
{
  if (!IsExistingFile(eFile)) {
    std::cerr << "The file eFile=" << eFile << " is missing\n";
    throw TerminalException{1};
  }
  std::cerr << "eFile=" << eFile << "\n";
  std::ifstream file(eFile);
  std::stringstream buffer;
  buffer << file.rdbuf();
  file.close();
  std::string content(buffer.str());
  std::cerr << "ReadXmlFile : |content| = " << content.size() << "\n";
  return content;
}




#endif
