#ifndef INCLUDE_FLOORPLAN
#define INCLUDE_FLOORPLAN

#include "xml.h"
#include "openscad.h"

#include "Temp_common.h"
#include "Namelist.h"


#include "GRAPH_GraphicalFunctions.h"



FullNamelist FloorplanPlotting()
{
  std::map<std::string, SingleBlock> ListBlock;
  //
  std::map<std::string, int> ListIntValues1;
  std::map<std::string, bool> ListBoolValues1;
  std::map<std::string, double> ListDoubleValues1;
  std::map<std::string, std::vector<double> > ListListDoubleValues1;
  std::map<std::string, std::vector<int> > ListListIntValues1;
  std::map<std::string, std::string> ListStringValues1;
  std::map<std::string, std::vector<std::string> > ListListStringValues1;
  ListStringValues1["XMLfile"] = "XMLunset";
  ListStringValues1["OpenScadFile"] = "unset.scad";
  ListDoubleValues1["RadiusLine"] = 0.1;
  ListDoubleValues1["RadiusSphere"] = 0.2;
  ListListIntValues1["ColorLine"]={255,255,0};
  ListListIntValues1["ColorPlane"]={255,255,0};
  ListListIntValues1["ColorSphere"]={255,255,0};
  //
  SingleBlock BlockDESC;
  BlockDESC.ListIntValues=ListIntValues1;
  BlockDESC.ListBoolValues=ListBoolValues1;
  BlockDESC.ListDoubleValues=ListDoubleValues1;
  BlockDESC.ListListDoubleValues=ListListDoubleValues1;
  BlockDESC.ListListIntValues=ListListIntValues1;
  BlockDESC.ListStringValues=ListStringValues1;
  BlockDESC.ListListStringValues=ListListStringValues1;
  ListBlock["PLOT"]=BlockDESC;
  //
  return {ListBlock, "undefined"};
}



struct OptionFloorPlot {
  double RadiusLine;
  double RadiusSphere;
  std::vector<int> ColorLine;
  std::vector<int> ColorPlane;
  std::vector<int> ColorSphere;
};











struct SingleFloorplan {
  int level;
  double elevation;
  std::vector<MyVector<double>> ListPt;
  std::vector<std::pair<int,int>> ListEdge;
  std::vector<int> eCycle;
};



struct FloorplanInfo {
  std::vector<SingleFloorplan> ListFloors;
};




std::pair<DescOpenscad,FloorplanInfo> OpenscadDescriptionFloorplan(std::string const& content, OptionFloorPlot const& recOpt)
{
  std::vector<PlaneOpenscad> ListPlane;
  std::vector<LineOpenscad> ListLine;
  std::vector<SphereOpenscad> ListSphere;
  std::vector<SingleFloorplan> ListFloors;
  //
  rapidxml::xml_document<> doc;
  std::string contentCP = content;
  doc.parse<0>(&contentCP[0]);
  //
  rapidxml::xml_node<> *pRoot = doc.first_node();  
  int nbStair=0;
  for (rapidxml::xml_node<> *pNode=pRoot->first_node("stair"); pNode; pNode=pNode->next_sibling()) {
    std::string strLevel=pNode->first_attribute("level")->value();
    int level=atoi(strLevel.c_str());
    std::string strElev=pNode->first_attribute("elevation")->value();
    double elev = atof(strElev.c_str());
    std::cerr << "level=" << strLevel << " level=" << strLevel << " elevation=" << strElev << "\n";
    rapidxml::xml_node<> *pWalls=pNode->first_node("walls");
    std::vector<MyVector<double>> ListPoint;
    std::vector<std::pair<int,int>> ListEdge;
    double epsilon=0.00001;
    auto FuncInsert=[&](MyVector<double> const& eV) -> int {
      int nbPt=ListPoint.size();
      std::cerr << " eV=" << eV(0) << " " << eV(1) << " " << eV(2) << "\n";
      for (int iPt=0; iPt<nbPt; iPt++) {
	MyVector<double> dV = ListPoint[iPt] - eV;
	double err = fabs(dV(0)) + fabs(dV(1)) + fabs(dV(2));
	if (err < epsilon) {
	  std::cerr << "       find iPt=" << iPt << "\n";
	  return iPt;
	}
      }
      SphereOpenscad eSph{eV, recOpt.RadiusSphere, recOpt.ColorSphere};
      ListSphere.push_back(eSph);
      ListPoint.push_back(eV);
      std::cerr << "       New  nbPt=" << nbPt << "\n";
      return nbPt;
    };
    
    int iWall=0;
    for (rapidxml::xml_node<> *pWall=pWalls->first_node("wall"); pWall; pWall=pWall->next_sibling()) {
      std::cerr << "iWall=" << iWall << "\n";
      std::string strPoints=pWall->first_attribute("points")->value();
      std::vector<std::string> LStr=STRING_Split(strPoints, ",");
      std::vector<double> LVal;
      for (auto & eStr : LStr)
	LVal.push_back(atof(eStr.c_str()));
      MyVector<double> V1(3);
      V1(0)=LVal[0];
      V1(1)=LVal[1];
      V1(2)=elev;
      int pos1=FuncInsert(V1);
      MyVector<double> V2(3);
      V2(0)=LVal[2];
      V2(1)=LVal[3];
      V2(2)=elev;
      int pos2=FuncInsert(V2);
      ListEdge.push_back({pos1, pos2});
      //
      MyVector<double> pt0=ListPoint[pos1];
      MyVector<double> pt1=ListPoint[pos2];
      LineOpenscad eLine{pt0, pt1, recOpt.RadiusLine, recOpt.ColorLine};
      ListLine.push_back(eLine);
      iWall++;
    }
    //
    // First creating the graph
    //
    int nbVert = ListPoint.size();
    GraphBitset GRred(nbVert);
    for (auto & eEdge : ListEdge) {
      int eVert = eEdge.first;
      int fVert = eEdge.second;
      GRred.AddAdjacent(eVert, fVert);
      GRred.AddAdjacent(fVert, eVert);
    }
    std::cerr << "Before call to GRAPH_FindAllCycles\n";
    std::vector<std::vector<int>> ListCycle = GRAPH_FindAllCycles(GRred);
    std::cerr << "After call to GRAPH_FindAllCycles\n";
    int nbCycle=ListCycle.size();
    if (nbCycle > 0) {
      std::vector<MyVector<double>> ListVert;
      std::vector<int> eCycle = ListCycle[0];
      for (auto & eVert : eCycle)
	ListVert.push_back(ListPoint[eVert]);
      PlaneOpenscad ePlane{ListVert, recOpt.ColorPlane};
      ListPlane.push_back(ePlane);
      //
      SingleFloorplan eFloor{level, elev, ListPoint, ListEdge, eCycle};
      ListFloors.push_back(eFloor);
    }
    std::cerr << "  nbVert=" << nbVert << "  nbStair=" << nbStair << "  nbCycle=" << nbCycle << "\n";
    nbStair++;
  }
  std::cerr << "nbStair=" << nbStair << "\n";
  DescOpenscad eDesc{ListLine, ListPlane, ListSphere};
  FloorplanInfo eFl{ListFloors};
  return {eDesc, eFl};
}









#endif
