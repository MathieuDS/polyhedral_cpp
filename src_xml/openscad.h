#ifndef OPENSCAD_INCLUDE
#define OPENSCAD_INCLUDE

#include "MAT_Matrix.h"



void WriteColorOpenscad(std::ostream & os, std::vector<int> const& color)
{
  auto val=[&](int const& i) -> double {
    return double(color[i]) / double(255);
  };
  os << "color( c = [" << val(0) << "," << val(1) << "," << val(2) << "], alpha = 1.0)";
}


struct PlaneOpenscad {
  std::vector<MyVector<double>> ListVert;
  std::vector<int> color;
};


struct LineOpenscad {
  MyVector<double> pt0;
  MyVector<double> pt1;
  double r;
  std::vector<int> color;
};


struct SphereOpenscad {
  MyVector<double> pt;
  double r;
  std::vector<int> color;
};


struct ArrowOpenscad {
  MyVector<double> pt;
  MyVector<double> direction;
  double r;
  double fn_vector;
  std::vector<int> color;
};





struct DescOpenscad {
  std::vector<LineOpenscad> ListLine;
  std::vector<PlaneOpenscad> ListPlane;
  std::vector<SphereOpenscad> ListSphere;
  std::vector<ArrowOpenscad> ListArrow;
};






void WriteOpenSCADinfo(std::string const& file, DescOpenscad const& eDesc)
{
  std::ofstream os(file);
  std::vector<LineOpenscad> ListLine;
  std::vector<PlaneOpenscad> ListPlane;
  std::vector<SphereOpenscad> ListSphere;
  std::vector<ArrowOpenscad> ListArrow;
  auto WritePoint=[&](MyVector<double> const& ePt) -> void {
    os << "[" << ePt(0) << "," << ePt(1) << "," << ePt(2) << "]";
  };
  //
  // Our functions to write down
  //
  os << "module locate(p1, p2) {\n";
  os << "   assign(p = p2 - p1)\n";
  os << "   assign(distance = norm(p)) {\n";
  os << "      translate(p1)\n";
  os << "      rotate([0, 0, atan2(p[1], p[0])])\n";
  os << "      rotate([0, atan2(sqrt(pow(p[0], 2)+pow(p[1], 2)),p[2]), 0])\n";
  os << "      children();\n";
  os << "   }\n";
  os << "}\n";
  os << "\n\n";
  os << "module make_edge(pt0, pt1, r) {\n";
  os << "    assign(p0 = pt0, p1 = pt1)\n";
  os << "     locate(p0,p1)\n";
  os << "     cylinder(r=r, h=norm(p1-p0));\n";
  os << "}\n";
  os << "\n\n";
  os << "module arrow(v, r, fn_vector) {\n";
  os << "    l=norm(v);\n";
  os << "    rotate(atan2(v[1],v[0]))\n";
  os << "        rotate([0,-atan2(v[2],norm([v[0],v[1]])),0])\n";
  os << "            rotate([0,90,0]){\n";
  os << "                cylinder(h=l*.8,r1=r,r2=r,$fn=fn_vector);\n";
  os << "                translate([0,0,l*.8])cylinder(h=l*.2,r1=3*r,r2=0,$fn=fn_vector);\n";
  os << "    }\n";
  os << "}\n";
  os << "\n\n";
  //
  // ListLine
  //
  for (auto & eLine : eDesc.ListLine) {
    WriteColorOpenscad(os, eLine.color);
    os << " {\n";
    os << "make_edge(";
    WritePoint(eLine.pt0);
    os << ",";
    WritePoint(eLine.pt1);
    os << "," << eLine.r << ");\n";
    os << "}\n";
  }
  //
  // Plane to print
  //
  for (auto & ePlane : eDesc.ListPlane) {
    WriteColorOpenscad(os, ePlane.color);
    os << " {\n";
    os << "  polyhedron(points=[";
    bool IsFirst=true;
    for (auto & ePt : ePlane.ListVert) {
      if (IsFirst == false)
	os << ",";
      IsFirst=false;
      WritePoint(ePt);
    }
    os << "], faces = [ [";
    int len=ePlane.ListVert.size();
    for (int i=0; i<len; i++) {
      if (i>0)
	os << ",";
      os << i;
    }
    os << "] ] );\n";
    os << "}\n";
  }
  //
  // ListSphere
  //
  for (auto & eSph : eDesc.ListSphere) {
    WriteColorOpenscad(os, eSph.color);
    os << " {\n";
    os << "  translate(";
    WritePoint(eSph.pt);
    os << ") sphere(" << eSph.r << ");\n";
    os << "}\n";
  }
  //
  // ListArrow
  //
  for (auto & eArrow : eDesc.ListArrow) {
    WriteColorOpenscad(os, eArrow.color);
    os << " {\n";
    os << "  translate(";
    WritePoint(eArrow.pt);
    os << ") arrow(";
    WritePoint(eArrow.direction);
    os << ", " << eArrow.r << ", " << eArrow.fn_vector << ");\n";
    os << "}\n";
  }
}



#endif
