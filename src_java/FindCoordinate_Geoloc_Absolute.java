import java.util.ArrayList;
import java.lang.Float;


public class FindCoordinate_Geoloc_Absolute {
    float rRad;
    Vector3 CentCirc;
    float RawRadius;
    Vector3 BestFit;
    private native void AbsoluteSingle(double[] vect1, double[] vect2, double[] vect3, double[] vect4, double[] vect5);
    public FindCoordinate_Geoloc_Absolute (ArrayList<Vector3> CoordMat, ArrayList<Float> ListDist, ArrayList<Float> ListDistError, Float Zmeas, Float Zerror)
    {
	int siz=CoordMat.size();
	double[] CoordInput=new double[3*siz];
	for (int i=0; i<siz; ++i) {
	    Vector3 eVect=CoordMat.get(i);
	    double x=eVect.x;
	    double y=eVect.y;
	    double z=eVect.z;
	    CoordInput[3*i  ]=x;
	    CoordInput[3*i+1]=y;
	    CoordInput[3*i+2]=z;
	}
	double[] ListDist_A=new double[siz];
	double[] ListDistError_A=new double[siz];
	for (int i=0; i<siz; ++i) {
	    ListDist_A[i]=ListDist.get(i);
	    ListDistError_A[i]=ListDistError.get(i);
	}
	//
	double [] alt_and_err=new double[2];
	alt_and_err[0]=Zmeas;
	alt_and_err[1]=Zerror;
	//
	double[] reply = new double[8];
	AbsoluteSingle(CoordInput, ListDist_A, ListDistError_A, alt_and_err, reply);
	System.out.print("JAVA: After call to AbsoluteSingle\n");
	System.out.print("JAVA: reply[0]=" + reply[0] + "\n");
	System.out.print("JAVA: reply[1]=" + reply[1] + "\n");
	System.out.print("JAVA: reply[2]=" + reply[2] + "\n");
	double rRad_d=reply[0];
	rRad=(float) rRad_d;
	//
	double CentX=reply[1];
	double CentY=reply[2];
	double CentZ=reply[3];
	float CentX_f=(float) CentX;
	float CentY_f=(float) CentY;
	float CentZ_f=(float) CentZ;
	CentCirc=new Vector3(CentX_f, CentY_f, CentZ_f);
	//
	double RawRadius_d=reply[4];
	RawRadius=(float) RawRadius_d;
	//
	double fitX=reply[5];
	double fitY=reply[6];
	double fitZ=reply[7];
	System.out.print("fitX=" + fitX + " fitY=" + fitY + " fitZ=" + fitZ + "\n");
	float FitX_f=(float) fitX;
	float FitY_f=(float) fitY;
	float FitZ_f=(float) fitZ;
	BestFit=new Vector3(FitX_f, FitY_f, FitZ_f);
	//
	System.out.print("JAVA: After FoundCoord assignation\n");
    }
    public float Get_rRad() {
	return rRad;
    }
    public Vector3 Get_CentCirc() {
	return CentCirc;
    }
    public float Get_RawRadius() {
	return RawRadius;
    }
    public Vector3 Get_BestFit() {
	return BestFit;
    }
    static {
	System.loadLibrary("metric");
    }
}
