import java.util.ArrayList;
import java.lang.Float;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.io.PrintWriter;

public class StandaloneGeoloc_Absolute {

    public static void main(String[] args) {
	try {
	    int argv=args.length;
	    if (argv != 2) {
		System.out.print("Number of arguments should be 2");
		throw new IOException("Number of arguments should be 2");
	    }
	    String FileI=args[0];
	    String FileO=args[1];
	    System.out.print("FileI = " + FileI + "\n");
	    System.out.print("FileO = " + FileO + "\n");
	    ArrayList<String> ListLines = new ArrayList<String>();
	    for (String line : Files.readAllLines(Paths.get(FileI))) {
		ListLines.add(line);
	    }
	    int nbPoint=Integer.parseInt(ListLines.get(0));
	    System.out.print("nbPoint=" + nbPoint + "\n");
	    int nbLine=ListLines.size();
	    System.out.print("nbLine=" + nbLine + "\n");
	    ArrayList<Vector3> CoordMat = new ArrayList<Vector3>();
	    ArrayList<Float> ListDist = new ArrayList<Float>();
	    ArrayList<Float> ListDistError = new ArrayList<Float>();
	    for (int iPt=0; iPt<nbPoint; iPt++) {
		System.out.print("  iPt=" + iPt + "\n");
		String line=ListLines.get(iPt+1);
		System.out.print("    line=" + line + "\n");
		String[] parts=line.split(" ");
		//
		String xStr=parts[0];
		System.out.print("    xStr=" + xStr + "\n");
		Float x=Float.parseFloat(xStr);
		System.out.print("      x=" + x + "\n");
		//
		String yStr=parts[1];
		System.out.print("    yStr=" + yStr + "\n");
		Float y=Float.parseFloat(yStr);
		System.out.print("      y=" + y + "\n");
		//
		String zStr=parts[2];
		System.out.print("    zStr=" + zStr + "\n");
		Float z=Float.parseFloat(zStr);
		System.out.print("      z=" + z + "\n");
		//
		String dStr=parts[3];
		System.out.print("    dStr=" + dStr + "\n");
		Float d=Float.parseFloat(dStr);
		System.out.print("      d=" + d + "\n");
		//
		String dErrStr=parts[4];
		System.out.print("    dErrStr=" + dErrStr + "\n");
		Float dErr=Float.parseFloat(dErrStr);
		System.out.print("      dErr=" + dErr + "\n");
		//
		Vector3 eVect = new Vector3(x,y,z);
		System.out.print("Before assignation\n");
		CoordMat.add(eVect);
		System.out.print("After assignation\n");
		ListDist.add(d);
		ListDistError.add(dErr);
	    }
	    //
	    String lineB=ListLines.get(nbPoint+1);
	    System.out.print("lineB=" + lineB + "\n");
	    String[] partsB=lineB.split(" ");
	    Float Zmeas=Float.parseFloat(partsB[0]);
	    Float Zerror=Float.parseFloat(partsB[1]);
	    System.out.print("Before FindCoordinate_AbsoluteSingle\n");
	    FindCoordinate_Geoloc_Absolute eFind=new FindCoordinate_Geoloc_Absolute(CoordMat, ListDist, ListDistError, Zmeas, Zerror);
	    System.out.print(" After FindCoordinate_AbsoluteSingle\n");
	    PrintWriter writer = new PrintWriter(FileO);
	    //
	    Float rRad=eFind.Get_rRad();
	    System.out.print("rRad=" + rRad + "\n");
	    writer.println(" " + rRad);
	    //
	    Vector3 CentCirc=eFind.Get_CentCirc();
	    Float xC=CentCirc.x;
	    Float yC=CentCirc.y;
	    Float zC=CentCirc.z;
	    System.out.print("Centcirc x=" + xC + " y=" + yC + " z=" + zC + "\n");
	    writer.println(" " + xC + " " + yC + " " + zC);
	    //
	    Float RawRadius=eFind.Get_RawRadius();
	    System.out.print("RawRadius=" + RawRadius + "\n");
	    writer.println(" " + RawRadius);
	    //
	    Vector3 BestFit=eFind.Get_BestFit();
	    Float xB=BestFit.x;
	    Float yB=BestFit.y;
	    Float zB=BestFit.z;
	    System.out.print("BestFit x=" + xB + " y=" + yB + " z=" + zB + "\n");
	    writer.println("BestFit x=" + xB + " y=" + yB + " z=" + zB);
	    writer.close();
	}
	catch(IOException e) {
	    System.out.println("Error during IO operations");
	}
    }

}
