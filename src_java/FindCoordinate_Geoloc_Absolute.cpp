#include <jni.h>
#include "FindCoordinate_Geoloc_Absolute.h"
#include "Metric.h"

JNIEXPORT void JNICALL Java_FindCoordinate_1Geoloc_1Absolute_AbsoluteSingle(JNIEnv* env, jobject, jdoubleArray vectCoord, jdoubleArray vectDist, jdoubleArray vectDistError, jdoubleArray alt_and_error, jdoubleArray fullresult)
{
#ifdef DEBUG
  std::cerr << set::precision(9);
#endif  
  jsize nbPoint = env->GetArrayLength(vectDist);
  double *A;
  A = new double[3*nbPoint];
  env->GetDoubleArrayRegion(vectCoord, 0, 3*nbPoint, A);
  MyMatrix<double> CoordMat(nbPoint, 3);
  for (int i=0; i<nbPoint; i++) {
    double x=A[3*i];
    double y=A[3*i+1];
    double z=A[3*i+2];
    //    std::cerr << "i=" << i << " pt=" << x << " " << y << " " << z << "\n";
    CoordMat(i,0) = x;
    CoordMat(i,1) = y;
    CoordMat(i,2) = z;
  }
  delete [] A;
  //
  A = new double[nbPoint];
  env->GetDoubleArrayRegion(vectDist, 0, nbPoint,A);
  MyVector<double> ListDist(nbPoint);
  for (int i=0; i<nbPoint; i++)
    ListDist(i) = A[i];
  delete [] A;
  //
  A = new double[nbPoint];
  env->GetDoubleArrayRegion(vectDistError, 0, nbPoint,A);
  MyVector<double> ListDistError(nbPoint);
  for (int i=0; i<nbPoint; i++)
    ListDistError(i) = A[i];
  delete [] A;
  //
  A = new double[2];
  env->GetDoubleArrayRegion(alt_and_error, 0, 2, A);
  double Zmeas=A[0];
  double ZerrorInput=A[1];
#ifdef DEBUG
  std::cerr << "Inside FindCoordinate_Geoloc_Absolute\n";
  std::cerr << "Zmeas=" << Zmeas << " ZerrorInput=" << ZerrorInput << "\n";
#endif  
  delete [] A;
  //
#ifdef DEBUG
  std::cerr << "Before call to ComputeGeneralTrilateration\n";
#endif  
  ResultGeoloc_absolute resGeoloc=Geoloc_AbsoluteComputation(CoordMat, ListDist, ListDistError, Zmeas, ZerrorInput);
#ifdef DEBUG
  std::cerr << " After call to ComputeGeneralTrilateration\n";
#endif  
  //
  A = new double[8];
  A[0]=resGeoloc.sphCoord.r;
  for (int i=0; i<3; i++)
    A[i+1] = resGeoloc.sphCoord.x(i);
  A[4]=resGeoloc.RawRadius;
  for (int i=0; i<3; i++)
    A[i+5] = resGeoloc.BestFit(i);
  env->SetDoubleArrayRegion(fullresult, 0, 8, A);
  delete [] A;
}
