


let lwt_first (x: int) : unit =
  for i = 0 to 100 do
    Printf.printf "x=%i i=%i\n" x i
  done


let _ =
  Lwt_main.run (Lwt.return (lwt_first 2));
  Printf.printf "End of the program\n"


