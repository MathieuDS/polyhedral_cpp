

let w_fct : int -> unit Lwt.t =
  fun x ->
  let (timelen : float) = (float_of_int 1) +. (float_of_int (Random.int x)) in
  Printf.printf "timelen=%f\n" timelen;
  Lwt_unix.sleep timelen


let lwt_first (x : int) : unit Lwt.t =
  let (i : int ref) = ref 0 in
  let rec fct () : unit Lwt.t =
    i := !i + 1;
    if (!i == 3) then
      Lwt.return_unit
    else
      Lwt.bind (w_fct x) fct
  in fct()


let _ =
  Lwt_main.run (lwt_first 2);
  Printf.printf "End of the program\n";
