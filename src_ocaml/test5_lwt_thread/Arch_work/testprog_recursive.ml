

(*    let (timelen : float) = 1.0 + (float_of_int (Random.int x)) in *)


let lwt_first _ : unit Lwt.t =
  let (i : int ref) = ref 0 in
  let rec fct () : unit Lwt.t =
    let (timelen : float) = 1.0 in
    Printf.printf "i=%i  timelen=%f\n" !i timelen;
    i := !i + 1;
    if (!i == 100) then
      Lwt.return_unit
    else
      fct()
  in fct()


let _ =
  Lwt_main.run (lwt_first 2);
  Printf.printf "End of the program\n"


