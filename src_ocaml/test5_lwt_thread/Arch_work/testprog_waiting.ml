



let lwt_first (x : int) : unit Lwt.t =
  let (i : int ref) = ref 0 in
  let fct () : unit Lwt.t =
    let (timelen : float) = (float_of_int 1) +. (float_of_int (Random.int x)) in 
    Printf.printf "i=%i  timelen=%f\n" !i timelen;
    i := !i + 1;
    if (!i == 100) then
      Lwt.return_unit
    else
      Lwt_unix.sleep timelen
  in fct()


let _ =
  Lwt_main.run (lwt_first 2);
  Printf.printf "End of the program\n"


