
(* val productIB : int -> int -> int *)

let productIB x y = x*y

let sum_first_val d =
  let sum = ref 0 in
  for i = 0 to d do
    sum := !sum + i
  done;
  !sum


let _ =
  let eSum = sum_first_val 10 in 
  Printf.printf "%i\n" eSum


(*
let _ =
  fc_show 10
*)
