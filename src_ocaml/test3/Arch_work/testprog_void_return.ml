let productIB x y = x*y

let sum_first_val d =
  let sum = ref 0 in
  for i = 0 to d do
    sum := !sum + i
  done;
  !sum


let void_return =
  let sum = ref 0 in
  for i = 0 to 10 do
    sum := !sum + i
  done


let _ =
  let eSum = sum_first_val 10 in 
  Printf.printf "%i\n" eSum
