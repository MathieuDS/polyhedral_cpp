

let get_random_integer () = Random.int 10
(*  Printf.printf "val=%i\n" val *)


let _ =
  let ranval = ref 0 in
  for _ = 0 to 100 do
     ranval := get_random_integer();
     Printf.printf "val=%i\n" !ranval
  done;
  Printf.printf "End of the program\n"

