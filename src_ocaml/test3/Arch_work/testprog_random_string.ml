(* open Random *)

let print_random_integer () =
  Printf.printf "val=%i\n" 10

let random_compchoice () = match Random.int 3 with
    | 0 -> "1"
    | 1 -> "2"
    | 2 -> "3"
    | _ -> "Error"

let _ =
  for _ = 0 to 10 do
    let str = random_compchoice() in
    Printf.printf "The option is %s\n" str
  done

(*
let _ =
  let iterN = 100 in
  for i = 0 to iterN do
    print_random_integer
  done;
  Printf.printf "End of the program\n"
*)
