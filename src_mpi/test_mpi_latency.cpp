#include <mpi.h>
#include <vector>
#include <chrono>


int main()
{
  
  MPI_Init(NULL, NULL);
  using T = int64_t;
  MPI_Datatype mpi_typ = MPI_LONG_LONG_INT;
  int n_pes, myrank;
  MPI_Comm_size(MPI_COMM_WORLD, &n_pes);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  if (n_pes != 2) {
    MPI_Finalize();
    return 0;
  }
  std::chrono::time_point<std::chrono::system_clock> time0;
  int n_iter=20;
  std::vector<T> LTime(n_iter, 0);
  for (int iter=0; iter<n_iter; iter++) {
    if (myrank == 0) {
      std::chrono::time_point<std::chrono::system_clock> time1 = std::chrono::system_clock::now();
      T duration_send = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
      T Asend[1];
      Asend[0] = duration_send;
      MPI_Send(Asend, 1, mpi_typ, 1, 37, MPI_COMM_WORLD);
    } else {
      T Arecv[1];
      MPI_Recv(Arecv, 1, mpi_typ, 0, 37, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      T duration_send = Arecv[0];
      std::chrono::time_point<std::chrono::system_clock> time1 = std::chrono::system_clock::now();
      T duration_recv = std::chrono::duration_cast<std::chrono::nanoseconds>(time1 - time0).count();
      T delta_time = duration_recv - duration_send;
      LTime[iter] = delta_time;
    }
  }
  if (myrank == 1) {
    for (int iter=0; iter<n_iter; iter++)
      std::cerr << "iter=" << iter << " delta_time=" << LTime[iter] << "\n";
  }
  MPI_Finalize();
}
