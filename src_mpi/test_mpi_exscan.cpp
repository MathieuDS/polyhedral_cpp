#include <mpi.h>
#include <vector>


int main()
{
  MPI_Init(NULL, NULL);
  using T = int64_t;
  MPI_Datatype mpi_typ = MPI_LONG_LONG_INT;
  T Asend[1], Arecv[1];
  int n_pes, myrank;
  MPI_Comm_size(MPI_COMM_WORLD, &n_pes);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  Asend[0] = 1 + myrank;
  int err = MPI_Exscan(Asend, Arecv, 1, mpi_typ, MPI_SUM, MPI_COMM_WORLD);
  std::cout << "myrank=" << myrank << " send=" << Asend[0] << " recv=" << Arecv[0] << "\n";
  MPI_Finalize();
}
