#include <mpi.h>
#include <vector>


int main()
{
  MPI_Init(NULL, NULL);
  using T = int64_t;
  MPI_Datatype mpi_typ = MPI_LONG_LONG_INT;
  T Asend[3];
  int n_pes, myrank;
  MPI_Comm_size(MPI_COMM_WORLD, &n_pes);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  Asend[0] = 3 + 100*myrank;
  Asend[1] = 2 + 100*myrank;
  Asend[2] = 42 + 100*myrank;
  int siz_recv = 0;
  if (myrank == 0) {
    siz_recv = 3 * n_pes;
  }
  std::vector<T> Arecv(siz_recv);
  MPI_Gather(Asend, 3, mpi_typ,
             Arecv.data(), 3, mpi_typ,
             0, MPI_COMM_WORLD);
  if (myrank == 0) {
    for (int i=0; i<siz_recv; i++)
      std::cout << "i=" << i << " Arecv=" << Arecv[i] << "\n";
  }


  MPI_Finalize();
}
