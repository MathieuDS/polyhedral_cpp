/*
  This program ends after 2230 iterations.
*/

//import msleep from 'sleep';

async function ForeverFunction ()
{
    let num = await TheInfinitePromise();
    console.log("That line will NEVER get printed");
};

function TheInfinitePromise()
{
    return new Promise((resolve, reject) => {
//        sleep.msleep(300);
        console.log("TheInfinitePromise");
        resolve(TheInfinitePromise());
    })
}

ForeverFunction();
