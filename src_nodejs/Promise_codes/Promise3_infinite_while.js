/*
  This program ends after 2230 iterations.
*/


async function ForeverFunction ()
{
    let num = await TheInfinitePromise();
    console.log("That line will NEVER get printed");
};

const promise1 = (idx) => new Promise(resolve => {
    console.log('Beginning of promise1 idx=' + idx);
    setTimeout(function() {
        resolve('foo');
    }, 90);
});



async function TheInfinitePromise()
{
    let iter = 0;
    while(1)
    {
        let num = await promise1(iter);
        iter = iter+1
        console.log("iter=" + iter + " num=" + num);
    }
    resolve('Should NEVER be returned.');
}

ForeverFunction();
