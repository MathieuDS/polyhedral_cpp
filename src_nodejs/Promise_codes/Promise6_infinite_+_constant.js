
const TimePromise = (idx, delay) => new Promise(resolve => {
    console.log('Beginning of promise1 idx=' + idx, ' delay=', delay);
    setTimeout(function() {
        resolve('foo');
    }, delay);
});



async function TheInfinitePromise()
{
    let iter = 0;
    while(1)
    {
        let num = await TimePromise(iter, 90);
        iter = iter+1
        console.log("iter=" + iter + " num=" + num);
    }
    resolve('Should NEVER be returned.');
}


const SingleOperation = () =>
      Promise.race([TheInfinitePromise(), TimePromise(47, 1000)]);


SingleOperation();
