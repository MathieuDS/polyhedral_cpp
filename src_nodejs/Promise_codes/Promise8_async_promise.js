
const promise1 = (idx) => new Promise(resolve => {
    console.log('Beginning of promise1 idx=' + idx);
    setTimeout(function() {
        resolve('foo');
    }, 90);
});



async function TheInfinitePromise()
{
    let iter = 0;
    while(1)
    {
        let num = await promise1(iter);
        iter = iter+1
        console.log("iter=" + iter + " num=" + num);
    }
    resolve('Should NEVER be returned.');
}

const RunScript = () =>
      new Promise(resolve => {
          TheInfinitePromise();
          resolve('asnwer');
      });


RunScript();
