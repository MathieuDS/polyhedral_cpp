/*
  This program ends after 2230 iterations.
*/



const promise1 = (idx, delay) => new Promise((resolve, reject) => {
    console.log('Beginning of promise1 idx=' + idx);
    setTimeout(function() {
        resolve('foo');
    }, delay);
});



async function TheInfinitePromise(delay)
{
    let iter = 0;
    while(1)
    {
        let num = await promise1(iter, delay);
        iter = iter+1
        console.log("iter=" + iter + " num=" + num + " delay=" + delay);
    }
    resolve('Should NEVER be returned.');
}

async function TwoFunctions ()
{
//    const promiseRace = () => new Promise.race([TheInfinitePromise(300), TheInfinitePromise(500)]);
//    let num = await promiseRace();
    let num = await Promise.race([TheInfinitePromise(300), TheInfinitePromise(500)]);
};

TwoFunctions();
