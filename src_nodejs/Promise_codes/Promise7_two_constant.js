
const TimePromise = (delay) => new Promise(resolve => {
    console.log('Beginning of TimePromise delay=', delay);
    setTimeout(function() {
        resolve('foo');
    }, delay);
});



const SingleOperation = () =>
      Promise.all([TimePromise(90), TimePromise(3000)]);


async function TheOper() {
    let result = await SingleOperation();
    console.log('TheOper result=', result);
}


TheOper();
