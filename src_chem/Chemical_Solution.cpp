#include "Temp_Chemical_Solution.h"

int main(int argc, char *argv[])
{
  std::ofstream OUTfs;
  if (argc != 3) {
    std::cerr << "Number of argument is = " << argc << "\n";
    std::cerr << "This program is used as\n";
    std::cerr << "Chemical_Solution [DATAREACT] [DATACONC]\n";
    std::cerr << "\n";
    std::cerr << "DATAREACT: The input data of the chemical reactions\n";
    std::cerr << "DATACONC : The input data of the initial chemical concentrations\n";
    return -1;
  }
  //
  try {
    std::cerr << "Reading input reaction file\n";
    std::ifstream STreact;
    STreact.open(argv[1]);
    SoluDesc<double> eDesc=ReadChemicalSolutionFile<double>(STreact);
    STreact.close();
    //
    std::cerr << "Reading input initial concentration\n";
    std::ifstream STconc;
    STconc.open(argv[2]);
    std::vector<double> ListInitConc=ReadConcentrationFile<double>(eDesc, STconc);
    STconc.close();
    //
    double TheTol=1e-30;
    std::vector<double> ListEquiConc=SolveSolutionSystem<double>(eDesc, ListInitConc, TheTol);
    //
    std::cerr << "Completion of the program\n";
  }
  catch (std::vector<std::string> & e) {
    PrintMultipleErrorMessageAndDie(e);
  }
}
