#ifndef TEMP_CHEMISTRY_SURFACE_TENSION
#define TEMP_CHEMISTRY_SURFACE_TENSION

#include "MAT_Matrix.h"
struct MagicConstants {
  double Rconstant; // ideal gas law constant
  double TheTol;
  double TerminationNormDiff;
  // Now variable arrays
  double GammaInf;
  double k; // limiting adsorption coefficient
  double h; // infinite dilution surface partial molar heat
};

struct Solution {
  double Cnominal; // nomial concentration that has been inserted
  double AoverV; // quotient of surface area over volume
  double T; // Temperature in Kelvin
  double gamma0; // surface tension of pure water
  // Computed fields
  double Cbulk; // Bulk concentration
  double Gamma; // surface concentration
  double gamma; // surface tension
  // Now the differentials:
  double dgamma_dGammaInf;
  double dgamma_dk;
  double dgamma_dh;
};

struct GlobalSolution {
  double TheObjective;
  double dObj_dGammaInf;
  double dObj_dk;
  double dObj_dh;
};


void CreateAMPL_initFile(MyMatrix<double> const& DATA_MEAS, 
			 MagicConstants *eCst)
{
  std::string OUTfile;
  OUTfile="surftens.mod";
  std::ofstream os;
  os.open(OUTfile);
  int nbMeas=DATA_MEAS.rows();
  os << "param n;\n";
  os << "var GammaInf;\n";
  os << "var h;\n";
  os << "var k;\n";
  os << "param R=" << eCst->Rconstant << ";\n";
  os << "var Cb{i in 1..n};\n";
  os << "var f;\n";
  os << "param Cnominal{i in 1..n};\n";
  os << "param gammaMeas{i in 1..n};\n";
  os << "param TempMeas{i in 1..n};\n";
  os << "param AoverV{i in 1..n};\n";
  os << "param gamma0{i in 1..n};\n";
  os << "var gamma{i in 1..n};\n";
  os << "var Gamma{i in 1..n};\n";
  os << "eqn1: f= sum{i in 1..n} (gamma[i] - gammaMeas[i])^2;\n";
  os << "eqn2 {i in 1..n}: Cb[i] = Cnominal[i] - AoverV[i]*Gamma[i];\n";
  os << "eqn3 {i in 1..n}: k*Cb[i] = Gamma[i]/(GammaInf - Gamma[i])*exp(-2*(h/(R*TempMeas[i]))*(Gamma[i]/GammaInf));\n";
  os << "eqn4 {i in 1..n}: gamma[i] = gamma0[i] + GammaInf*R*TempMeas[i]*( log(1 - Gamma[i]/GammaInf) + h/(R*TempMeas[i])*(Gamma[i]/GammaInf)^2);\n";
  os << "minimize ssq: f;\n";
  os << "subject to UpperBound {i in 1..n}: Gamma[i] <= GammaInf;\n";
  os << "subject to LowerBound {i in 1..n}: Gamma[i] >= 0;\n";
  os << "subject to R1: k >= 0;\n";
  os << "subject to R2: h >= 0;\n";
  os << "subject to R3: GammaInf >= 0;\n";
  os << "subject to LowerCb {i in 1..n}: Cb[i] >= 0;\n";
  os.close();
  //
  OUTfile="surftens.dat";
  os.open(OUTfile);
  os << "param n:=" << nbMeas << ";\n";
  os << "param Cnominal :=";
  for (int iMeas=0; iMeas<nbMeas; iMeas++) {
    double Cnominal=DATA_MEAS(iMeas, 0);
    os << " " << iMeas+1 << " " << Cnominal;
  }
  os << ";\n";
  //
  os << "param gammaMeas :=";
  for (int iMeas=0; iMeas<nbMeas; iMeas++) {
    double gammaMeas=DATA_MEAS(iMeas, 1);
    os << " " << iMeas+1 << " " << gammaMeas;
  }
  os << ";\n";
  //
  os << "param TempMeas :=";
  for (int iMeas=0; iMeas<nbMeas; iMeas++) {
    double TempMeas=DATA_MEAS(iMeas, 2);
    os << " " << iMeas+1 << " " << TempMeas;
  }
  os << ";\n";
  //
  os << "param AoverV :=";
  for (int iMeas=0; iMeas<nbMeas; iMeas++) {
    double AoverV=DATA_MEAS(iMeas, 3);
    os << " " << iMeas+1 << " " << AoverV;
  }
  os << ";\n";
  //
  os << "param gamma0 :=";
  for (int iMeas=0; iMeas<nbMeas; iMeas++) {
    double gamma0=DATA_MEAS(iMeas, 4);
    os << " " << iMeas+1 << " " << gamma0;
  }
  os << ";\n";
}



void ComputeSolution(MagicConstants *eCst,
		     Solution* eSol)
{
  double GammaInf=eCst->GammaInf;
  double Cnominal=eSol->Cnominal;
  double k=eCst->k;
  double h=eCst->h;
  double AoverV=eSol->AoverV;
  double Rcst=eCst->Rconstant;
  double eTemp=eSol->T;
  double RT=Rcst*eTemp;
  double C=-2*h/RT;
  // The pair of solutions
  double GammaLow=0;
  double uLow=GammaLow/GammaInf;
  double uUpp, GammaUpp, ErrorUpp;
  //  std::cerr << "Cnominal=" << Cnominal << "\n";
  //  std::cerr << "AoverV=" << AoverV << "\n";
  //  std::cerr << "GammaInf=" << GammaInf << "\n";
  //  std::cerr << "GammaLow=" << GammaLow << "\n";
  //  std::cerr << "uLow=" << uLow << "\n";
  //  std::cerr << "k=" << k << "\n";
  //  std::cerr << "h=" << h << "\n";
  //  std::cerr << "Rcst=" << Rcst << "\n";
  //  std::cerr << "eTemp=" << eTemp << "\n";
  //  std::cerr << "C=" << C << "\n";
  double ErrorLow=(Cnominal - AoverV*GammaLow) - (uLow/(1-uLow))*(exp(C*uLow)/k);
  double delta=0.1;
  while(true) {
    GammaUpp=GammaInf - delta*GammaInf;
    uUpp=GammaUpp/GammaInf;
    ErrorUpp=(Cnominal - AoverV*GammaUpp) - (uUpp/(1-uUpp))*(exp(C*uUpp)/k);
    if (ErrorUpp < 0)
      break;
    delta=delta/2;
  }
  while(true) {
    //    std::cerr << "Gamma(Low/Upp) = " << GammaLow << " / " << GammaUpp << "\n";
    double GammaMid=(GammaLow + GammaUpp)/2;
    double uMid=GammaMid/GammaInf;
    double ErrorMid=(Cnominal - AoverV*GammaMid) - (uMid/(1-uMid))*(exp(C*uMid)/k);
    if (ErrorLow * ErrorMid > 0) {
      GammaLow=GammaMid;
      ErrorLow=ErrorMid;
    }
    else {
      GammaUpp=GammaMid;
      ErrorUpp=ErrorMid;
    }
    double eDiff=GammaUpp - GammaLow;
    //    std::cerr << "eDiff=" << eDiff << "\n";
    if (eDiff < eCst->TheTol) {
      //      std::cerr << "Now leaving you\n";
      //      std::cerr << "TheTol=" << eCst->TheTol << "\n";
      break;
    }
  }
  // The key values
  double Gamma=(GammaLow + GammaUpp)/2;
  double u=Gamma/GammaInf;
  double Cbulk=Cnominal - AoverV*Gamma;
  double eFact=log(1-u) + (h/RT)*u*u;
  double gamma=eSol->gamma0 + GammaInf*RT*eFact;
  // The differentials
  //  double PsiFunc=(u/(1-u))*exp(C*u);
  //  double Psi    =(u/(1-u))*exp(C*u)
  double PsiDiff=(1 + (1-u)*u*C)*exp(C*u) /((1-u)*(1-u));
  double Jacobian=k*AoverV + PsiDiff/GammaInf;
  double dGamma_dk=Cbulk/Jacobian;
  double dGamma_dGammaInf = (Gamma/(GammaInf*GammaInf))*PsiDiff/Jacobian;
  double dGamma_dh=(u*u/(1-u))*(2/RT)*exp(C*u)/Jacobian;
  // gamma is expressed as gamma(GammaInf, h, Gamma)
  double dgamma_dx=RT*(log(1-u) + u/(1-u)) - h*(u*u);
  double dgamma_dy=Gamma*u;
  double dgamma_dz=-RT/(1-u) + 2*h*u;
  // The final differentiations
  double dgamma_dGammaInf=dgamma_dx + dgamma_dz * dGamma_dGammaInf;
  double dgamma_dh=dgamma_dy + dgamma_dz * dGamma_dh;
  double dgamma_dk=dgamma_dz * dGamma_dk;
  // The assignations
  eSol->Gamma=Gamma;
  eSol->Cbulk=Cbulk;
  eSol->gamma=gamma;
  eSol->dgamma_dGammaInf=dgamma_dGammaInf;
  eSol->dgamma_dk=dgamma_dk;
  eSol->dgamma_dh=dgamma_dh;
}

void ComputeGlobalSolution(MyMatrix<double> const & DATA_MEAS, 
			   MagicConstants *eCst,
			   GlobalSolution* eSolGlob)
{
  int nbMeas=DATA_MEAS.rows();
  double TheObjective=0;
  double dObj_dGammaInf=0;
  double dObj_dk=0;
  double dObj_dh=0;
  Solution* eSol;
  eSol=new Solution;
  for (int iMeas=0; iMeas<nbMeas; iMeas++) {
    double Cnominal=DATA_MEAS(iMeas, 0);
    double gammaMeas=DATA_MEAS(iMeas, 1);
    double TempMeas=DATA_MEAS(iMeas, 2);
    double AoverV=DATA_MEAS(iMeas, 3);
    double gamma0=DATA_MEAS(iMeas, 4);
    //    std::cerr << "iMeas=" << iMeas << "\n";
    //    std::cerr << "Cnominal=" << Cnominal << "\n";
    eSol->Cnominal=Cnominal;
    eSol->AoverV=AoverV;
    eSol->gamma0=gamma0;
    eSol->T=TempMeas;
    ComputeSolution(eCst, eSol);
    double eDelta=eSol->gamma - gammaMeas;
    TheObjective=TheObjective + pow(eDelta, 2);
    dObj_dGammaInf=dObj_dGammaInf + 2*eDelta*eSol->dgamma_dGammaInf;
    dObj_dk=dObj_dk + 2*eDelta*eSol->dgamma_dk;
    dObj_dh=dObj_dh + 2*eDelta*eSol->dgamma_dh;
  }
  delete eSol;
  eSolGlob->TheObjective=TheObjective;
  eSolGlob->dObj_dGammaInf=dObj_dGammaInf;
  eSolGlob->dObj_dk=dObj_dk;
  eSolGlob->dObj_dh=dObj_dh;
}


void PrintSolution(std::ostream & os, 
		   MyMatrix<double> const & DATA_MEAS, 
		   MagicConstants *eCst)
{
  int nbMeas=DATA_MEAS.rows();
  Solution* eSol;
  eSol=new Solution;
  double GammaInf=eCst->GammaInf;
  double k=eCst->k;
  double h=eCst->h;
  os << "GammaInf = " << GammaInf << "\n";
  os << "k = " << k << "\n";
  os << "h = " << h << "\n";
  for (int iMeas=0; iMeas<nbMeas; iMeas++) {
    double Cnominal=DATA_MEAS(iMeas, 0);
    double gammaMeas=DATA_MEAS(iMeas, 1);
    double TempMeas=DATA_MEAS(iMeas, 2);
    double AoverV=DATA_MEAS(iMeas, 3);
    double gamma0=DATA_MEAS(iMeas, 4);
    eSol->Cnominal=Cnominal;
    eSol->AoverV=AoverV;
    eSol->gamma0=gamma0;
    eSol->T=TempMeas;
    ComputeSolution(eCst, eSol);
    double Gamma=eSol->Gamma;
    double u=Gamma/GammaInf;
    double C=h/(eCst->Rconstant *TempMeas);
    double eFact1=log(1-u);
    double eFact2=C*u*u;
    double Cbulk=eSol->Cbulk;
    double TotCont1=AoverV*Gamma;
    double TotCont2=Cbulk;
    double TotCont=TotCont1 + TotCont2;
    os << "gamma(Meas/model) = " << gammaMeas << " / " << eSol->gamma << "\n";
    os << "  Gamma , Cnominal / Cbulk = " << eSol->Gamma << " , " << Cnominal << " / " << eSol->Cbulk << "\n";
    //    os << "  u=" << u << " C=" << C << "\n";
    //    os << "  eFact1/2=" << eFact1 << " / " << eFact2 << "\n";
    //    os << "  TotCont12=" << TotCont1 << " / " << TotCont2 << "\n";
  }
  delete eSol;
}






void SteepestDescent(MyMatrix<double> const & DATA_MEAS, 
		     MagicConstants *eCst, 
		     GlobalSolution *eSolGlob)
{
  ComputeGlobalSolution(DATA_MEAS, eCst, eSolGlob);
  double TheObjective=eSolGlob->TheObjective;
  double DirGammaInf=-eSolGlob->dObj_dGammaInf;
  double DirK=-eSolGlob->dObj_dk;
  double DirH=-eSolGlob->dObj_dh;
  std::cerr << "TheObjective=" << TheObjective << "\n";
  PrintSolution(std::cerr, DATA_MEAS, eCst);
  std::cerr << "DirGammaInf=" << DirGammaInf << "\n";
  std::cerr << "DirK=" << DirK << "\n";
  std::cerr << "DirH=" << DirH << "\n";
  // pointers
  
  GlobalSolution *NewSolGlob;
  MagicConstants *NewCst;
  NewSolGlob=new GlobalSolution;
  NewCst=new MagicConstants;
  //
  double GammaInf=eCst->GammaInf;
  double k=eCst->k;
  double h=eCst->h;
  //
  double TheMult=1;
  if (DirGammaInf < 0) {
    double TheMult_gamma=-GammaInf/DirGammaInf;
    if (TheMult_gamma < TheMult)
      TheMult=TheMult_gamma/2;
  }
  if (DirK < 0) {
    double TheMult_k = - k/DirK;
    if (TheMult_k < TheMult)
      TheMult=TheMult_k/2;
  }
  if (DirH < 0) {
    double TheMult_h= - h/DirH;
    if (TheMult_h < TheMult)
      TheMult=TheMult_h/2;
  }
  std::cerr << "Initially TheMult=" << TheMult << "\n";
  NewCst->TheTol=eCst->TheTol;
  NewCst->Rconstant=eCst->Rconstant;
  int nbIter=0;
  while(true) {
    nbIter++;
    double New_GammaInf=eCst->GammaInf + TheMult*DirGammaInf;
    double New_k=eCst->k + TheMult*DirK;
    double New_h=eCst->h + TheMult*DirH;
    NewCst->GammaInf=New_GammaInf;
    NewCst->k=New_k;
    NewCst->h=New_h;
    ComputeGlobalSolution(DATA_MEAS, NewCst, NewSolGlob);
    std::cerr << "Loop 1 TheMult=" << TheMult << " object=" << NewSolGlob->TheObjective << "\n";
    std::cerr << "   GammaInf/k/h=" << New_GammaInf << " / " << New_k << " / " << New_h << "\n";
    if (NewSolGlob->TheObjective < TheObjective)
      break;
    TheMult=TheMult/2;
    if (nbIter > 1000) {
      std::cerr << "Numerical errors, DIE!\n";
      throw TerminalException{1};
      //      exit(1);
    }
      
  }
  /*
  while(true) {
    double New_GammaInf=eCst->GammaInf + TheMult*DirGammaInf;
    double New_k=eCst->k + TheMult*DirK;
    double New_h=eCst->h + TheMult*DirH;
    NewCst->GammaInf=New_GammaInf;
    NewCst->k=New_k;
    NewCst->h=New_h;
    ComputeGlobalSolution(DATA_MEAS, NewCst, NewSolGlob);
    std::cerr << "Loop 2 TheMult=" << TheMult << "\n";
    if (NewSolGlob->TheObjective < TheObjective)
      break;
    TheMult=TheMult*2;
  }
  */
  eCst->GammaInf=NewCst->GammaInf;
  eCst->k=NewCst->k;
  eCst->h=NewCst->h;
  delete NewSolGlob;
  delete NewCst;
}




void IterationToReduce(MyMatrix<double> const & DATA_MEAS, 
		       MagicConstants *eCst)
{
  GlobalSolution* eSolGlob;
  eSolGlob=new GlobalSolution;
  double TerminationNormDiff=eCst->TerminationNormDiff;
  while(true) {
    SteepestDescent(DATA_MEAS, eCst, eSolGlob);
    std::cerr << "After SteepestDescent\n";
    std::cerr << "Now GammaInf=" << eCst->GammaInf << "\n";
    std::cerr << "Now k=" << eCst->k << "\n";
    std::cerr << "Now h=" << eCst->h << "\n";
    double Err1=T_abs(eSolGlob->dObj_dGammaInf);
    double Err2=T_abs(eSolGlob->dObj_dk);
    double Err3=T_abs(eSolGlob->dObj_dh);
    double eNormDiff=Err1 + Err2 + Err3;
    if (eNormDiff < TerminationNormDiff)
      break;
  }
  delete eSolGlob;
}




#endif
