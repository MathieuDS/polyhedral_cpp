#ifndef TEMP_CHEMICAL_SOLUTION
#define TEMP_CHEMICAL_SOLUTION

#ifdef USE_MPREAL
#include "mpreal.h"
#endif

#include "MAT_Matrix.h"
#include "Basic_string.h"
#include "Basic_file.h"
#include "MAT_MatrixInt.h"
#include "ChemicalTypes.h"

#include <Eigen/Dense>
#include <Eigen/LU>
#include <cassert>

// code for IPOPT

//#include "RegisteredTNLP.hpp"
#include "IpIpoptApplication.hpp"
#include "IpTNLP.hpp"





template<typename T>
std::vector<std::vector<T> > ReadCSV_ConcentrationFile(SoluDesc<T> const& eDesc, std::istream & is)
{
  int nbChem=eDesc.ListName.size();
  std::vector<T> ListConc(nbChem);
  //  int nbLine=FILE_GetNumberLine(eFile);
  // determining number of commas
  //  std::ifstream IN;
  //  IN.open(eFile.c_str() );
  std::set<int> ListNbEnt;
  std::vector<int> ListIChem;
  std::vector<std::vector<T> > ListListConc;
  std::string eLine;
  int nbLine=0;
  while(std::getline(is, eLine)) {
    nbLine++;
    std::vector<std::string> eList1=STRING_Split(eLine, ",");
    int nbEnt1=eList1.size();
    std::vector<std::string> eList2=STRING_Split(eLine, ";");
    int nbEnt2=eList2.size();
    int nbEnt=0;
    std::vector<std::string> eList;
    if (nbEnt1 > 1) {
      nbEnt=nbEnt1;
      eList=eList1;
    }
    if (nbEnt2 > 1) {
      nbEnt=nbEnt2;
      eList=eList2;
    }
    if (nbEnt == 0) {
      std::vector<std::string> eErr={"Wrong number of entries"};
      throw(eErr);
    }
    ListNbEnt.insert(nbEnt);
    //
    std::string eName=eList[0];
    int iChemSearch=-1;
    for (int iChem=0; iChem<nbChem; iChem++)
      if (eDesc.ListName[iChem] == eName)
	iChemSearch=iChem;
    if (iChemSearch == -1) {
      std::vector<std::string> LStr;
      LStr.push_back("We did not find the name in the list\n");
      LStr.push_back("eName=" + eName + "\n");
      LStr.push_back("List of known chemical constituents:\n");
      for (int iChem=0; iChem<nbChem; iChem++)
	LStr.push_back("iChem=" + IntToString(iChem) + " eName=" + eDesc.ListName[iChem] + "\n");
      throw LStr;
    }
    ListIChem.push_back(iChemSearch);
    //
    std::vector<double> LConc(nbEnt-1);
    for (int iEnt=0; iEnt<nbEnt-1; iEnt++) {
      std::string eEnt=eList[iEnt+1];
      double eVal=atof(eEnt.c_str());
      LConc[iEnt]=eVal;
    }
    ListListConc.push_back(LConc);
  }
  int siz=ListNbEnt.size();
  if (siz > 1) {
    std::vector<std::string> eErr={"Variable number of entries between lines"};
    throw(eErr);
  }
  int nbCase=siz-1;
  std::vector<std::vector<T> > ListListConcRet(nbCase);
  for (int iCase=0; iCase<nbCase; iCase++) {
    std::vector<T> ListConc(nbChem);
    for (int iChem=0; iChem<nbChem; iChem++)
      ListConc[iChem]=0;
    for (int iLine=0; iLine<nbLine; iLine++) {
      int iChem=ListIChem[iLine];
      ListConc[iChem]=ListListConc[iLine][iCase];
    }
    ListListConcRet[iCase]=ListConc;
  }
  return ListListConcRet;
}



template<typename T>
std::vector<T> ReadConcentrationFile(SoluDesc<T> const& eDesc, std::istream &is)
{
  int nbChem=eDesc.ListName.size();
  std::vector<T> ListConc(nbChem);
  for (int iChem=0; iChem<nbChem; iChem++)
    ListConc[iChem]=0;
  //
  int nbInitChem;
  is >> nbInitChem;
  for (int iInitChem=0; iInitChem<nbInitChem; iInitChem++) {
    //    std::cerr << "iInitChem=" << iInitChem << "\n";
    std::string eName;
    is >> eName;
    double eConc_d;
    is >> eConc_d;
    //    std::cerr << "  eConc_d=" << eConc_d << "\n";
    T eConc=eConc_d;
    int iChemSearch=-1;
    for (int iChem=0; iChem<nbChem; iChem++)
      if (eDesc.ListName[iChem] == eName)
	iChemSearch=iChem;
    if (iChemSearch == -1) {
      std::vector<std::string> LStr;
      LStr.push_back("We did not find the name in the list\n");
      LStr.push_back("eName=" + eName + "\n");
      LStr.push_back("List of known chemical constituents:\n");
      for (int iChem=0; iChem<nbChem; iChem++)
	LStr.push_back("iChem=" + IntToString(iChem) + " eName=" + eDesc.ListName[iChem] + "\n");
      throw LStr;
    }
    ListConc[iChemSearch]=eConc;
  }
  return ListConc;
}




template<typename T>
SoluDesc<T> ReadChemicalSolutionFile(std::istream &is)
{
  int nbChem, nbReact, nbComp, eVal;
  std::string eName, eNature, eSep;
  std::map<std::string, int> MappingPos;
  SoluDesc<T> eDesc;
  is >> nbChem;
  for (int iChem=0; iChem<nbChem; iChem++) {
    //    std::cerr << "iChem=" << iChem << "\n";
    is >> eName;
    //    std::cerr << "  eName=" << eName << "\n";
    is >> eNature;
    //    std::cerr << "  eNature=" << eNature << "\n";
    MappingPos[eName]=iChem;
    if (eNature != "dissolved" && eNature != "solvent" && eNature != "substrate") {
      std::vector<std::string> LStr;
      LStr.push_back("Error in input of data. Allowed: dissolved, solvent, substrate\n");
      LStr.push_back("eNature=" + eNature + "\n");
      throw LStr;
    }
    int eElectrical;
    is >> eElectrical;
    //    std::cerr << "  eElectrical=" << eElectrical << "\n";
    eDesc.ListName.push_back(eName);
    eDesc.ListNature.push_back(eNature);
    eDesc.ListElectricalCharge.push_back(eElectrical);
  }
  is >> nbReact;
  //  std::cerr << "nbReact=" << nbReact << "\n";
  std::vector<T> ListWeight(nbReact);
  for (int iReact=0; iReact<nbReact; iReact++) {
    //    std::cerr << "iReact=" << iReact << "\n";
    std::vector<int> ListVal(nbChem,0);
    is >> nbComp;
    //    std::cerr << "  nbComp=" << nbComp << "\n";
    is >> eSep;
    //    std::cerr << "  eSep=" << eSep << "\n";
    if (eSep != ":") {
      std::vector<std::string> LStr{"Separator should be :\n"};
      throw LStr;
    }
    for (int iComp=0; iComp<nbComp; iComp++) {
      //      std::cerr << "  iComp=" << iComp << "\n";
      is >> eVal;
      //      std::cerr << "    eVal=" << eVal << "\n";
      is >> eName;
      //      std::cerr << "    eName=" << eName << "\n";
      auto search=MappingPos.find(eName);
      int iChem=-1;
      if (search != MappingPos.end()) {
	iChem=search->second;
      }
      else {
	std::vector<std::string> LStr{"Entry " + eName + " has not been defined\n"};
	throw LStr;
      }
      //      int iChem=MappingPos[eName];
      //      std::cerr << "    iChem=" << iChem << "\n";
      ListVal[iChem]=eVal;
    }
    is >> eSep;
    if (eSep != "K" && eSep != "pK") {
      std::vector<std::string> LStr{"Separator should be K or pK\n"};
      throw LStr;
    }
    T eVal, eK;
    is >> eVal;
    if (eSep == "K") {
      eK=eVal;
    }
    else {
      eK=exp(-eVal*log(10));
    }
    //    std::cerr << "log(10)=" << log(10) << "\n";
    //    std::cerr << "  K=" << eK << "\n";
    ChemicalReact<T> eReact;
    eReact.K=eK;
    eReact.ListVal=ListVal;
    eDesc.ListChemicalReact.push_back(eReact);
    //    ListWeight[iReact]=1/(eK*eK);
    //    ListWeight[iReact]=1;
    //    ListWeight[iReact]=1/sqrt(eK);
    //    ListWeight[iReact]=1/eK;
    ListWeight[iReact]=pow(eK, -0.3333);
  }
  MyMatrix<int> SpannedSpace(nbReact, nbChem);
  for (int iReact=0; iReact<nbReact; iReact++)
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eVal=eDesc.ListChemicalReact[iReact].ListVal[iChem];
      SpannedSpace(iReact, iChem)=eVal;
    }
  MyMatrix<int> NSP=NullspaceIntTrMat<int>(SpannedSpace);
  int nbEqua=NSP.rows();
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    std::vector<int> eEqua(nbChem,0);
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eVal=NSP(iEqua, iChem);
      eEqua[iChem]=eVal;
    }
    eDesc.ListEquation.push_back(eEqua);
  }
  eDesc.ListWeight=ListWeight;
  return eDesc;
}

template<typename T>
void PrintSingleEquation(SoluDesc<T> const& eDesc, int const & iEqua, std::ostream& os)
{
  int nbChem=eDesc.ListName.size();
  std::vector<int> LVal=eDesc.ListEquation[iEqua];
  int IsFirst=1;
  for (int iChem=0; iChem<nbChem; iChem++) {
    int eVal=LVal[iChem];
    if (eVal != 0) {
      if (IsFirst == 1) {
	IsFirst=0;
	if (eVal == -1) {
	  os << " - ";
	}
	else {
	  if (eVal != 1) {
	    os << " " << eVal << " ";
	  }
	  else {
	    os << " ";
	  }
	}
      }
      else {
	if (eVal == -1) {
	  os << " - ";
	}
	else {
	  if (eVal != 1) {
	    if (eVal > 0) {
	      os << " + " << eVal << " ";
	    }
	    else {
	      os << eVal << " ";
	    }
	  }
	  else {
	    os << " + ";
	  }
	}
      }
      os << "[" << eDesc.ListName[iChem] << "]";
    }
  }
}



template<typename T>
void PrintInitialState(SoluDesc<T> const& eDesc, std::ostream& os)
{
  int nbChem=eDesc.ListName.size();
  os << "Number Chemical quantity = " << nbChem << "\n";
  for (int iChem=0; iChem<nbChem; iChem++) {
    os << "iChem=" << iChem << " : " << eDesc.ListName[iChem];
    os << " nature=" << eDesc.ListNature[iChem];
    os << "\n";
  }
  int nbReact=eDesc.ListChemicalReact.size();
  os << "Number Chemical reactions = " << nbReact << "\n";
  for (int iReact=0; iReact<nbReact; iReact++) {
    ChemicalReact<T> eReact=eDesc.ListChemicalReact[iReact];
    std::string eName;
    int jReact=iReact+1;
    os << "iReact=" << jReact << "\n";
    os << "  ";
    int IsFirst=1;
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eVal=eReact.ListVal[iChem];
      if (eVal>0) {
	if (IsFirst == 0)
	  os << " +";
	IsFirst=0;
	os << " ";
	if (eVal > 1)
	  os << eVal;
	eName=eDesc.ListName[iChem];
	os << eName;
      }
    }
    os << " <---->";
    IsFirst=1;
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eVal=-eReact.ListVal[iChem];
      if (eVal>0) {
	if (IsFirst == 0)
	  os << " +";
	IsFirst=0;
	os << " ";
	if (eVal > 1)
	  os << eVal;
	eName=eDesc.ListName[iChem];
	os << eName;
      }
    }
    os << "\n";
    //
    os << "   K=" << eReact.K << "\n";
    //
    os << "  ";
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eVal=eReact.ListVal[iChem];
      if (eVal>0 && eDesc.ListNature[iChem] == "dissolved") {
	os << " ";
	eName=eDesc.ListName[iChem];
	os << "[" << eName << "]";
	if (eVal > 1)
	  os << "^" << eVal;
      }
    }
    os << " = K";
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eVal=-eReact.ListVal[iChem];
      if (eVal>0 && eDesc.ListNature[iChem] == "dissolved") {
	os << " ";
	eName=eDesc.ListName[iChem];
	os << "[" << eName << "]";
	if (eVal > 1)
	  os << "^" << eVal;
      }
    }
    os << "\n";
  }
  int nbEqua=eDesc.ListEquation.size();
  os << "Number found relations = " << nbEqua << "\n";
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    int jEqua=iEqua+1;
    os << "iEqua=" << jEqua << " :";
    PrintSingleEquation(eDesc, iEqua, os);
    os << " = const\n";
  }
}

template<typename T>
T ThePow(T const& eVal, int nb)
{
  T retVal;
  if (nb == 0) {
    retVal=1;
    return retVal;
  }
  int absnb=abs(nb);
  retVal=1;
  for (int i=0; i<absnb; i++) {
    retVal=retVal*eVal;
  }
  if (nb < 0) {
    retVal=1/retVal;
  }
  return retVal;
}



template<typename T>
void PrintFinalState(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, std::vector<T> const& TheSol, std::ostream& os)
{
  int nbChem=eDesc.ListName.size();
  std::vector<int> ListStringLen;
  os << "Obtained solution:\n";
  for (int iChem=0; iChem<nbChem; iChem++) {
    os << "iChem=" << iChem << " : ";
    os << "[" << eDesc.ListName[iChem] << "] = ";
    os << TheSol[iChem] << "\n";
  }
  int nbEqua=eDesc.ListEquation.size();
  os << "Number of conservation equations = " << nbEqua << "\n";
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    int jEqua=iEqua+1;
    os << "iEqua=" << jEqua << " :";
    PrintSingleEquation(eDesc, iEqua, os);
    os << "\n";
    T eSumStart=0;
    T eSumFinal=0;
    std::vector<int> LVal=eDesc.ListEquation[iEqua];
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eVal=LVal[iChem];
      eSumStart=eSumStart + eVal*ListInitConc[iChem];
      eSumFinal=eSumFinal + eVal*TheSol[iChem];
    }
    T eDiffStartFinal=eSumStart - eSumFinal;
    os << "start/final/diff = " << eSumStart << " " << eSumFinal << " " << eDiffStartFinal << "\n";
  }
  int nbReact=eDesc.ListChemicalReact.size();
  os << "Number of chemical reactions = " << nbReact << "\n";
  for (int iReact=0; iReact<nbReact; iReact++) {
    ChemicalReact<T> eReact=eDesc.ListChemicalReact[iReact];
    os << "iReact=" << iReact << "\n";
    T TheProduct1=1;
    T TheProduct2=eReact.K;
    std::stringstream eStr1;
    std::stringstream eStr2;
    eStr2 << "K";
    for (int iChem=0; iChem<nbChem; iChem++)
      if (eDesc.ListNature[iChem] == "dissolved") {
	int eCoeff=eReact.ListVal[iChem];
	int absCoeff=abs(eCoeff);
	T eConc=TheSol[iChem];
	T ePow=ThePow(eConc, absCoeff);
	if (eCoeff > 0) {
	  TheProduct1=TheProduct1*ePow;
	  eStr1 << " [" << eDesc.ListName[iChem] << "]";
	  if (eCoeff != 1) {
	    eStr1 << "^" << eCoeff;
	  }
	}
	if (eCoeff < 0) {
	  TheProduct2=TheProduct2*ePow;
	  eStr2 << " [" << eDesc.ListName[iChem] << "]";
	  if (absCoeff != 1) {
	    eStr2 << "^" << absCoeff;
	  }
	}
      }
    os << "TheProd1=" << TheProduct1 << " : " << eStr1.str() << "\n";
    os << "TheProd2=" << TheProduct2 << " : " << eStr2.str() << "\n";
    T eDiff=TheProduct1 - TheProduct2;
    os << "eDiff=" << eDiff << "\n";
  }

}


template<typename T>
std::vector<T> GetListReactionProducts(SoluDesc<T> const& eDesc, std::vector<T> const& ListConc)
{
  int nbReact=eDesc.ListChemicalReact.size();
  int nbChem=eDesc.ListName.size();
  std::vector<T> ListListProduct;
  for (int iReact=0; iReact<nbReact; iReact++) {
    ChemicalReact<T> eReact=eDesc.ListChemicalReact[iReact];
    //    std::cerr << "Before deadly computation\n";
    T TheProduct1=1;
    T TheProduct2=eReact.K;
    for (int iChem=0; iChem<nbChem; iChem++)
      if (eDesc.ListNature[iChem] == "dissolved") {
	int eCoeff=eReact.ListVal[iChem];
	int absCoeff=abs(eCoeff);
	T eConc=ListConc[iChem];
	T ePow=ThePow(eConc, absCoeff);
	//	std::cerr << "absCoeff=" << absCoeff << "\n";
	//	std::cerr << "eConc=" << eConc << "\n";
	//	std::cerr << "ePow=" << ePow << "\n";
	if (eCoeff > 0) {
	  TheProduct1=TheProduct1*ePow;
	}
	if (eCoeff < 0) {
	  TheProduct2=TheProduct2*ePow;
	}
      }
    ListListProduct.push_back(TheProduct1);
    ListListProduct.push_back(TheProduct2);
  }
  return ListListProduct;
}


template<typename T>
std::vector<T> GetListReactionDefect(SoluDesc<T> const& eDesc, std::vector<T> const& ListConc)
{
  int nbReact=eDesc.ListChemicalReact.size();
  std::vector<T> ListListProduct=GetListReactionProducts(eDesc, ListConc);
  std::vector<T> ListDefect;
  for (int iReact=0; iReact<nbReact; iReact++) {
    T TheProduct1=ListListProduct[2*iReact];
    T TheProduct2=ListListProduct[2*iReact+1];
    T eDiff=TheProduct1 - TheProduct2;
    ListDefect.push_back(eDiff);
  }
  return ListDefect;
}

template<typename T>
void AssignListReactionWeights(SoluDesc<T> & eDesc, std::vector<T> const& ListConc)
{
  int nbReact=eDesc.ListChemicalReact.size();
  std::vector<T> ListListProduct=GetListReactionProducts(eDesc, ListConc);
  std::vector<T> ListWeight;
  std::vector<T> ListSum;
  for (int iReact=0; iReact<nbReact; iReact++) {
    T TheProduct1=ListListProduct[2*iReact];
    T TheProduct2=ListListProduct[2*iReact+1];
    T eSum=TheProduct1 + TheProduct2;
    ListSum.push_back(eSum);
    T eW=1/eSum;
    ListWeight.push_back(eW);
    std::cerr << "iReact=" << iReact << " eSum=" << eSum << "\n";
  }
  T eMin=*std::min_element(ListSum);
  std::cerr << "eMin=" << eMin << "\n";
  T eProduct=1;
  int eNb=0;
  int IsFirst=1;
  for (int iReact=0; iReact<nbReact; iReact++) {
    if (ListSum[iReact] > 10*eMin) {
      eProduct=eProduct*ListSum[iReact];
      eNb++;
    }
  }
  eDesc.ListWeight=ListWeight;
}









template<typename T>
T PenaltyToSolution(SoluDesc<T> const& eDesc, std::vector<T> const& ListConc)
{
  int nbReact=eDesc.ListChemicalReact.size();
  std::vector<T> ListDefect=GetListReactionDefect(eDesc, ListConc);
  T eNorm=0;
  for (int iReact=0; iReact<nbReact; iReact++) {
    //    std::cerr << "iReact=" << iReact << "\n";
    T eDiff=ListDefect[iReact];
    //    std::cerr << "  eDiff=" << eDiff << "\n";
    T eWei=eDesc.ListWeight[iReact];;
    eNorm=eNorm + eDiff*eDiff*eWei;
  }
  return eNorm;
}



template<typename T>
std::vector<T> DecreaseSpecificDirection_Direct(SoluDesc<T> const& eDesc, std::vector<T> const& ListConc, std::vector<T> const& DeltaMovement)
{
  std::vector<T> NewListConc;
  int nbDecreasingConc=0;
  T minAllowedDelta=0;
  int nbChem=eDesc.ListName.size();
  //  int nbReact=eDesc.ListChemicalReact.size();
  for (int iChem=0; iChem<nbChem; iChem++) {
    if (DeltaMovement[iChem] < 0) {
      T AllowedDelta=-ListConc[iChem]/DeltaMovement[iChem];
      if (nbDecreasingConc == 0) {
	minAllowedDelta=AllowedDelta;
      }
      else {
	if (AllowedDelta < minAllowedDelta)
	  minAllowedDelta=AllowedDelta;
      }
      nbDecreasingConc++;
    }
  }
  if (nbDecreasingConc == 0) {
    std::vector<std::string> LStr;
    LStr.push_back("Movement concentrations\n");
    for (int iChem=0; iChem<nbChem; iChem++) {
      LStr.push_back("iChem=" + IntToString(iChem) + " mov=" + DoubleToString(DeltaMovement[iChem]) + "\n");
    }
    LStr.push_back("Well we need to work some more\n");
    throw LStr;
  }
  std::cerr << "minAllowedDelta=" << minAllowedDelta << "\n";
  int NbSubdi=10;
  int NbIter=10;
  T minDelta=0;
  T maxDelta=minAllowedDelta;
  for (int iter=0; iter<NbIter; iter++) {
    std::vector<T> ListValDelta;
    std::vector<T> ListPenalty;
    T eNsubdi=NbSubdi-1;
    T DDelta=maxDelta - minDelta;
    //    std::cerr << "minDelta=" << minDelta << "\n";
    //    std::cerr << "maxDelta=" << maxDelta << "\n";
    //    std::cerr << "DDelta=" << DDelta << "\n";
    for (int iSubdi=0; iSubdi<NbSubdi; iSubdi++) {
      T eSubdi=iSubdi;
      T eDelta=minDelta + DDelta*(eSubdi/eNsubdi);
      ListValDelta.push_back(eDelta);
      //      std::cerr << "eDelta=" << eDelta << "\n";
      // 
      NewListConc.clear();
      for (int iChem=0; iChem<nbChem; iChem++) {
	//	std::cerr << "iChem=" << iChem << "\n";
	//	std::cerr << "ListConc[iChem]=" << ListConc[iChem] << "\n";
	//	std::cerr << "eDelta=" << eDelta << "\n";
	//	std::cerr << "DeltaMovement[iChem]=" << DeltaMovement[iChem] << "\n";
	//	T eProd=eDelta*DeltaMovement[iChem];
	//	std::cerr << "eDelta*DeltaMovement[iChem]=" << eProd << "\n";
	T eNewConc=ListConc[iChem] + eDelta*DeltaMovement[iChem];
	//	std::cerr << "iChem=" << iChem << " eNewConc=" << eNewConc << "\n";
	NewListConc.push_back(eNewConc);
      }
      T NewPenalty=PenaltyToSolution(eDesc, NewListConc);
      //      std::vector<T> NewListDefect=GetListReactionDefect(eDesc, NewListConc);
      //      for (int iReact=0; iReact<nbReact; iReact++) {
      //	std::cerr << "iReact=" << iReact << " eDefect=" << NewListDefect[iReact] << "\n";
      //      }
      //      for (int iChem=0; iChem<nbChem; iChem++) {
      //	std::cerr << "iChem=" << iChem << " c=" << NewListConc[iChem] << "\n";
      //      }
      //      std::cerr << "Defect2=" << NewListDefect[2] << "\n";
      //      std::cerr << "NewPenalty=" << NewPenalty << "\n";
      //      std::cerr << "iSubdi=" << iSubdi << "\n";
      ListPenalty.push_back(NewPenalty);
    }
    int idxFound=0;
    T minValPenalty=ListPenalty[0];
    for (int iSubdi=1; iSubdi<NbSubdi; iSubdi++) {
      //      std::cerr << "iSubdi=" << iSubdi << "\n";
      //      std::cerr << "minValPenalty=" << minValPenalty << "\n";
      //      std::cerr << "ListPenalty[iSubdi]=" << ListPenalty[iSubdi] << "\n";
      if (ListPenalty[iSubdi] < minValPenalty) {
	idxFound=iSubdi;
	minValPenalty=ListPenalty[iSubdi];
      }
    }
    //    std::cerr << "idxFound=" << idxFound << "\n";
    int idx1=idxFound-1;
    if (idx1 < 0)
      idx1=0;
    int idx2=idxFound+1;
    if (idx2 > NbSubdi-1)
      idx2=NbSubdi-1;
    //    std::cerr << "NbSubdi=" << NbSubdi << "\n";
    //    std::cerr << "idx1=" << idx1 << "\n";
    //    std::cerr << "idx2=" << idx2 << "\n";
    minDelta=ListValDelta[idx1];
    maxDelta=ListValDelta[idx2];
    //    std::cerr << "new values for min/maxDelta\n";
    //    std::cerr << "minDelta=" << minDelta << "\n";
    //    std::cerr << "maxDelta=" << maxDelta << "\n";
  }
  T eDelta=(minDelta + maxDelta)/2;
  std::cerr << "eDelta=" << eDelta << "\n";
  NewListConc.clear();
  for (int iChem=0; iChem<nbChem; iChem++) {
    T eNewConc=ListConc[iChem] + eDelta*DeltaMovement[iChem];
    NewListConc.push_back(eNewConc);
  }
  return NewListConc;
}




template<typename T>
std::vector<T> DecreaseSpecificDirection_DichotomyDerivative(SoluDesc<T> const& eDesc, std::vector<T> const& ListConc, std::vector<T> const& DeltaMovement)
{
  std::vector<T> NewListConc;
  int nbDecreasingConc=0;
  T minAllowedDelta=0;
  int nbChem=eDesc.ListName.size();
  for (int iChem=0; iChem<nbChem; iChem++) {
    if (DeltaMovement[iChem] < 0 && eDesc.ListNature[iChem] == "dissolved") {
      T AllowedDelta=-ListConc[iChem]/DeltaMovement[iChem];
      if (nbDecreasingConc == 0) {
	minAllowedDelta=AllowedDelta;
      }
      else {
	if (AllowedDelta < minAllowedDelta)
	  minAllowedDelta=AllowedDelta;
      }
      nbDecreasingConc++;
    }
  }
  if (nbDecreasingConc == 0) {
    std::vector<std::string> LStr{"Well we need to work some more\n"};
    throw LStr;
  }
  std::cerr << "minAllowedDelta=" << minAllowedDelta << "\n";
  int NbSubdi=10;
  int NbIter=10;
  T minDelta=0;
  T maxDelta=minAllowedDelta;
  for (int iter=0; iter<NbIter; iter++) {
    std::vector<T> ListValDelta;
    std::vector<T> ListPenalty;
    for (int iSubdi=0; iSubdi<NbSubdi; iSubdi++) {
      T eSubdi=iSubdi;
      T eNsubdi=NbSubdi-1;
      T DDelta=maxDelta - minDelta;
      T eDelta=minDelta + DDelta*(eSubdi/eNsubdi);
      ListValDelta.push_back(eDelta);
      NewListConc.clear();
      for (int iChem=0; iChem<nbChem; iChem++) {
	T eNewConc=ListConc[iChem] + eDelta*DeltaMovement[iChem];
	NewListConc.push_back(eNewConc);
      }
      T NewPenalty=PenaltyToSolution(eDesc, NewListConc);
      ListPenalty.push_back(NewPenalty);
    }
    int idxFound=0;
    T minValPenalty=ListPenalty[0];
    for (int iSubdi=1; iSubdi<NbSubdi; iSubdi++) {
      if (ListPenalty[iSubdi] < minValPenalty) {
	idxFound=iSubdi;
	minValPenalty=ListPenalty[iSubdi];
      }
    }
    int idx1=idxFound-1;
    if (idx1 < 0)
      idx1=0;
    int idx2=idxFound+1;
    if (idx2 > NbSubdi-1)
      idx2=NbSubdi-1;
    minDelta=ListValDelta[idx1];
    maxDelta=ListValDelta[idx2];
  }
  T eDelta=(minDelta + maxDelta)/2;
  NewListConc.clear();
  for (int iChem=0; iChem<nbChem; iChem++) {
    T eNewConc=ListConc[iChem] + eDelta*DeltaMovement[iChem];
    NewListConc.push_back(eNewConc);
  }
  return NewListConc;
}




template<typename T>
std::vector<T> SolutionMethod_ArtificialKinetic(SoluDesc<T> const& eDesc, std::vector<T> const& ListConc, int const& OptChoice)
{
  int nbReact=eDesc.ListChemicalReact.size();
  int nbChem=eDesc.ListName.size();
  std::vector<T> ListDefect=GetListReactionDefect(eDesc, ListConc);
  T eZer;
  eZer=0;
  std::vector<T> DeltaMovement(nbChem, eZer);
  if (OptChoice == 1) {
    for (int iReact=0; iReact<nbReact; iReact++) {
      T eDefect=ListDefect[iReact];
      std::vector<int> LCoeff=eDesc.ListChemicalReact[iReact].ListVal;
      for (int iChem=0; iChem<nbChem; iChem++) {
	int eCoeff=LCoeff[iChem];
	T eCoeff_T=eCoeff;
	DeltaMovement[iChem]=DeltaMovement[iChem] - eCoeff_T*eDefect;
      }
    }
  }
  if (OptChoice == 2) {
    T maxDefect=0;
    int iReactFound=-1;
    for (int iReact=0; iReact<nbReact; iReact++) {
      T eDefect=ListDefect[iReact];
      T absDefect=T_abs(eDefect);
      if (absDefect > maxDefect) {
	maxDefect=absDefect;
	iReactFound=iReact;
      }
    }
    std::cerr << "iReactFound=" << iReactFound << "\n";
    T eDefect=ListDefect[iReactFound];
    std::vector<int> LCoeff=eDesc.ListChemicalReact[iReactFound].ListVal;
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eCoeff=LCoeff[iChem];
      T eCoeff_T=eCoeff;
      DeltaMovement[iChem]= - eCoeff_T*eDefect;
      std::cerr << "iChem=" << iChem << " mov=" << DeltaMovement[iChem] << "\n";
    }
  }
  return DecreaseSpecificDirection_Direct(eDesc, ListConc, DeltaMovement);
}

template<typename T>
std::vector<T> SolutionMethod_TryAllReact(SoluDesc<T> const& eDesc, std::vector<T> const& ListConc)
{
  int nbReact=eDesc.ListChemicalReact.size();
  int nbChem=eDesc.ListName.size();
  std::vector<T> ListDefect=GetListReactionDefect(eDesc, ListConc);
  std::vector<T> RetListConc;
  T RetPenalty;
  int IsFirst=1;
  for (int iReact=0; iReact<nbReact; iReact++) {
    T eZer=0;
    std::vector<T> DeltaMovement(nbChem, eZer);
    T eDefect=ListDefect[iReact];
    std::vector<int> LCoeff=eDesc.ListChemicalReact[iReact].ListVal;
    //    std::cerr << " eDefect=" << eDefect << "\n";
    if (eDefect > 0) {
      for (int iChem=0; iChem<nbChem; iChem++) {
	int eCoeff=LCoeff[iChem];
	T eCoeff_T=eCoeff;
	DeltaMovement[iChem]= - eCoeff_T*eDefect;
	//	std::cerr << "iChem=" << iChem << " eCoeff_T=" << eCoeff_T << "\n";
      }
      std::vector<T> CandListConc=DecreaseSpecificDirection_Direct(eDesc, ListConc, DeltaMovement);
      T CandPenalty=PenaltyToSolution(eDesc, CandListConc);
      if (IsFirst == 1) {
	IsFirst=0;
	RetListConc=CandListConc;
	RetPenalty=CandPenalty;
      }
      else {
	if (CandPenalty < RetPenalty) {
	  RetListConc=CandListConc;
	  RetPenalty=CandPenalty;
	}
      }
    }
  }
  return RetListConc;
}





template<typename T>
void Compute_Differential(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, std::vector<T> const& ListConc, MyMatrix<T> & DifferentialMat, MyVector<T> & ListB, std::vector<T> &ListDefect)
{
  int nbChem=eDesc.ListName.size();
  int nbEqua=eDesc.ListEquation.size();
  int nbReact=eDesc.ListChemicalReact.size();
  int iLine=0;
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    std::vector<int> eEqua=eDesc.ListEquation[iEqua];
    T eSum=0;
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eValI=eEqua[iChem];
      T eValT=eValI;
      DifferentialMat(iLine, iChem)=eValT;
      T eDiff=ListInitConc[iChem] - ListConc[iChem];
      eSum=eSum + eValT*eDiff;
    }
    ListB(iLine)=eSum;
    iLine++;
  }
  ListDefect=GetListReactionDefect(eDesc, ListConc);
  for (int iReact=0; iReact<nbReact; iReact++) {
    ChemicalReact<T> eReact=eDesc.ListChemicalReact[iReact];
    T eSum=-ListDefect[iReact];
    for (int iChem=0; iChem<nbChem; iChem++) {
      T eMatEntry=0;
      if (eDesc.ListNature[iChem] == "dissolved") {
	int eVal=eReact.ListVal[iChem];
	if (eVal > 0) {
	  eMatEntry=eVal;
	  for (int jChem=0; jChem<nbChem; jChem++)
	    if (eDesc.ListNature[jChem] == "dissolved" && eReact.ListVal[jChem] > 0) {
	      T eConc=ListConc[jChem];
	      int fVal=eReact.ListVal[jChem];
	      if (iChem == jChem)
		fVal=fVal-1;
	      T ePow=ThePow(eConc, fVal);
	      eMatEntry=eMatEntry*ePow;
	    }
	}
	if (eVal < 0) {
	  int kVal=-eVal;
	  eMatEntry=-kVal*eReact.K;
	  for (int jChem=0; jChem<nbChem; jChem++)
	    if (eDesc.ListNature[jChem] == "dissolved" && eReact.ListVal[jChem] < 0) {
	      T eConc=ListConc[jChem];
	      int fVal=-eReact.ListVal[jChem];
	      if (iChem == jChem)
		fVal=fVal-1;
	      T ePow=ThePow(eConc, fVal);
	      eMatEntry=eMatEntry*ePow;
	    }
	}
      }
      DifferentialMat(iLine, iChem)=eMatEntry;
    }
    ListB(iLine)=eSum;
    iLine++;
  }
}

template<typename T>
std::vector<T> GetGradientDirection(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, std::vector<T> const& ListConc)
{
  int nbChem=eDesc.ListName.size();
  int nbUnknown=nbChem;
  int nbEqua=eDesc.ListEquation.size();
  int nbReact=eDesc.ListChemicalReact.size();
  MyMatrix<T> DifferentialMat(nbUnknown, nbUnknown);
  MyVector<T> ListB(nbUnknown);
  std::vector<T> ListDefect;
  Compute_Differential(eDesc, ListInitConc, ListConc, DifferentialMat, ListB, ListDefect);
  std::vector<T> TheGradientReact(nbReact, 0);
  for (int iReact=0; iReact<nbReact; iReact++) {
    ChemicalReact<T> eReact=eDesc.ListChemicalReact[iReact];
    std::vector<int> LVal=eReact.ListVal;
    T eWei=eDesc.ListWeight[iReact];
    T eSum=0;
    for (int jReact=0; jReact<nbReact; jReact++) {
      T eDefect=ListDefect[jReact];
      for (int iChem=0; iChem<nbChem; iChem++) {
	T eVal=DifferentialMat(nbEqua+jReact,iChem);
	int eValI=LVal[iChem];
	T eValT=eValI;
	eSum = eSum + eDefect*eValT*eVal;
      }
    }
    TheGradientReact[iReact]=eSum*eWei;
  }
  std::vector<T> TheGradient(nbUnknown,0);
  for (int iReact=0; iReact<nbReact; iReact++) {
    T eGrad=TheGradientReact[iReact];
    std::vector<int> LVal=eDesc.ListChemicalReact[iReact].ListVal;
    for (int iChem=0; iChem<nbChem; iChem++) {
      T eValT=LVal[iChem];
      TheGradient[iChem]=TheGradient[iChem] - eGrad*eValT;
    }
  }
  return TheGradient;
}

template<typename T>
std::vector<T> SteepestGradientDecrease(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, std::vector<T> const& ListConc)
{
  std::vector<T> TheGradient=GetGradientDirection(eDesc, ListInitConc, ListConc);
  return DecreaseSpecificDirection_Direct(eDesc, ListConc, TheGradient);
}



template<typename T>
std::vector<T> SolutionMethod_NewtonIteration(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, std::vector<T> const& ListConc)
{
  int nbChem=eDesc.ListName.size();
  int nbUnknown=eDesc.ListName.size();
  MyMatrix<T> DifferentialMat(nbUnknown, nbUnknown);
  MyVector<T> ListB(nbUnknown);
  std::vector<T> ListDefect;
  Compute_Differential(eDesc, ListInitConc, ListConc, DifferentialMat, ListB, ListDefect);
  MyMatrix<T> eInvMat=Inverse(DifferentialMat);
  MyMatrix<T> Cg=TransposedMat(eInvMat);
  MyVector<T> eProd=VectorMatrix(ListB, Cg);
  std::vector<T> retListCons;
  for (int iChem=0; iChem<nbChem; iChem++)
    {
      T eVal=eProd(iChem);
      T NewVal=ListConc[iChem] - eVal;
      retListCons.push_back(NewVal);
    }
  return retListCons;
}



#ifdef USE_MPREAL
template<typename T>
std::vector<T> SolutionMethod_NewtonIteration_EIGEN(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, std::vector<T> const& ListConc)
{
  using mpfr::mpreal;
  using namespace Eigen;
  int nbChem=eDesc.ListName.size();
  int dim=eDesc.ListName.size();
  MyMatrix<T> DifferentialMat(dim, dim);
  MyVector<T> ListB(dim);
  std::vector<T> ListDefect;
  Compute_Differential(eDesc, ListInitConc, ListConc, DifferentialMat, ListB, ListDefect);
  const int digits = 150;
  mpreal::set_default_prec(mpfr::digits2bits(digits));
  Matrix<mpreal,Dynamic,Dynamic> m(dim,dim);
  for (int i=0; i<dim; i++)
    for (int j=0; j<dim; j++) {
      T eVal=DifferentialMat(i, j);
      m(i,j)=eVal;
    }
  Matrix<mpreal,Dynamic,Dynamic> mInv=m.inverse();
  MyVector<T> eProd(dim);
  for (int iChem=0; iChem<nbChem; iChem++) {
    T eSum=0;
    for (int jChem=0; jChem<nbChem; jChem++) {
      T eVal=ListB(jChem);
      mpreal eVal_mp=mInv(iChem,jChem);
      T eVal_T=eVal_mp;
      eSum += eVal_T*eVal;
    }
    eProd(iChem)=eSum;
  }
  std::vector<T> retListCons;
  for (int iChem=0; iChem<nbChem; iChem++) {
    T eVal=eProd(iChem);
    T NewVal=ListConc[iChem] + eVal;
    retListCons.push_back(NewVal);
  }
  return retListCons;
}
#else
template<typename T>
std::vector<T> SolutionMethod_NewtonIteration_EIGEN(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, std::vector<T> const& ListConc)
{
  using namespace Eigen;
  int nbChem=eDesc.ListName.size();
  int dim=eDesc.ListName.size();
  MyMatrix<T> DifferentialMat(dim, dim);
  MyVector<T> ListB(dim);
  std::vector<T> ListDefect;
  Compute_Differential(eDesc, ListInitConc, ListConc, DifferentialMat, ListB, ListDefect);
  Matrix<double,Dynamic,Dynamic> m(dim,dim);
  for (int i=0; i<dim; i++)
    for (int j=0; j<dim; j++) {
      T eVal=DifferentialMat(i, j);
      m(i,j)=eVal;
    }
  Matrix<double,Dynamic,Dynamic> mInv=m.inverse();
  MyVector<T> eProd(dim);
  for (int iChem=0; iChem<nbChem; iChem++) {
    T eSum=0;
    for (int jChem=0; jChem<nbChem; jChem++) {
      T eVal=ListB(jChem);
      double eVal_mp=mInv(iChem,jChem);
      T eVal_T=eVal_mp;
      eSum=eSum + eVal_T*eVal;
    }
    eProd(iChem)=eSum;
  }
  std::vector<T> retListCons;
  for (int iChem=0; iChem<nbChem; iChem++) {
    T eVal=eProd(iChem);
    T NewVal=ListConc[iChem] + eVal;
    retListCons.push_back(NewVal);
  }
  return retListCons;
}
#endif



class IPOPT_tnlp_chem : public Ipopt::TNLP
{
public:
  IPOPT_tnlp_chem(const IPOPT_tnlp_chem&) = delete;
  IPOPT_tnlp_chem& operator=(const IPOPT_tnlp_chem &) = delete;
  IPOPT_tnlp_chem(SoluDesc<double> const& inpDesc, std::vector<double> const& ListInitConc, std::vector<double> const& StartConc);
  //  IPOPT_tnlp_chem();
  virtual ~IPOPT_tnlp_chem();
  virtual bool get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g,
			    Ipopt::Index& nnz_h_lag, IndexStyleEnum& index_style);
  virtual bool get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u,
			       Ipopt::Index m, Ipopt::Number* g_l, Ipopt::Number* g_u);
  virtual bool get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x,
				  bool init_z, Ipopt::Number* z_L, Ipopt::Number* z_U,
				  Ipopt::Index m, bool init_lambda,
				  Ipopt::Number* lambda);
  virtual bool eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number& obj_value);
  virtual bool eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number* grad_f);
  virtual bool eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Number* g);
  virtual bool eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
			  Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index* iRow, Ipopt::Index *jCol,
			  Ipopt::Number* values);
  // This is the Hessian. We are not concerned here.
  // since function is zero.
  virtual bool eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
		      Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number* lambda,
		      bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow,
		      Ipopt::Index* jCol, Ipopt::Number* values);
  virtual bool get_scaling_parameters(Ipopt::Number& obj_scaling,
				      bool& use_x_scaling, Ipopt::Index n,
				      Ipopt::Number* x_scaling,
				      bool& use_g_scaling, Ipopt::Index m,
				      Ipopt::Number* g_scaling);
  virtual void finalize_solution(Ipopt::SolverReturn status,
				 Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L, const Ipopt::Number* z_U,
				 Ipopt::Index m, const Ipopt::Number* g, const Ipopt::Number* lambda,
				 Ipopt::Number obj_valu,
				 const Ipopt::IpoptData* ip_data,
				 Ipopt::IpoptCalculatedQuantities* ip_cq);
  virtual std::vector<double> GetFinal() const;
private:
  SoluDesc<double>  eDesc;
  std::vector<double> ListInitConc;
  std::vector<double> StartConc;
  std::vector<double> FinalSolution;
};

//IPOPT_tnlp_chem::IPOPT_tnlp_chem() {
//}

IPOPT_tnlp_chem::IPOPT_tnlp_chem(SoluDesc<double> const& inpDesc, std::vector<double> const& TheInitConc, std::vector<double> const& TheStartConc) {
  eDesc=inpDesc;
  ListInitConc=TheInitConc;
  StartConc=TheStartConc;
}

IPOPT_tnlp_chem::~IPOPT_tnlp_chem() {
}

bool IPOPT_tnlp_chem::get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g,
				   Ipopt::Index& nnz_h_lag, IndexStyleEnum& index_style) {
  int nbChem=eDesc.ListName.size();
  n=nbChem;
  m=nbChem;
  // number of zeros in the constraints
  nnz_jac_g=0;
  int nbEqua=eDesc.ListEquation.size();
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eValI=eDesc.ListEquation[iEqua][iChem];
      if (eValI != 0) {
	nnz_jac_g++;
      }
    }
  }
  int nbReact=eDesc.ListChemicalReact.size();
  for (int iReact=0; iReact<nbReact; iReact++) {
    ChemicalReact<double> eReact=eDesc.ListChemicalReact[iReact];
    std::vector<int> LVal=eReact.ListVal;
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eValI=LVal[iChem];
      if (eValI != 0) {
	nnz_jac_g++;
      }
    }
  }
  // Jacobian of constraint. zero since function is zero
  nnz_h_lag=0;
  // Ipopt::Indexing C-style : 0-based
  index_style = Ipopt::TNLP::C_STYLE;
  return true;
}
bool IPOPT_tnlp_chem::get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u,
				      Ipopt::Index m, Ipopt::Number* g_l, Ipopt::Number* g_u) {
  int nbChem=eDesc.ListName.size();
  for (int iChem=0; iChem<nbChem; iChem++) {
    x_l[iChem]=0;
    x_u[iChem]=1e19;
  }
  int nbEqua=eDesc.ListEquation.size();
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    double eVal=0;
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eValI=eDesc.ListEquation[iEqua][iChem];
      double eValD=eValI;
      eVal=eVal + eValD*ListInitConc[iChem];
    }
    g_l[iEqua]=eVal;
    g_u[iEqua]=eVal;
  }
  int nbReact=eDesc.ListChemicalReact.size();
  for (int iReact=0; iReact<nbReact; iReact++) {
    g_l[nbEqua+iReact]=0;
    g_u[nbEqua+iReact]=0;
  }
  return true;
}

bool IPOPT_tnlp_chem::get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x,
					 bool init_z, Ipopt::Number* z_L, Ipopt::Number* z_U,
					 Ipopt::Index m, bool init_lambda,
					 Ipopt::Number* lambda) {
  int nbChem=eDesc.ListName.size();
  for (int iChem=0; iChem<nbChem; iChem++) {
    x[iChem]=StartConc[iChem];
  }
  return true;
}

bool IPOPT_tnlp_chem::eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number& obj_value) {
  obj_value=0;
  return true;
}

bool IPOPT_tnlp_chem::eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number* grad_f) {
  int nbChem=eDesc.ListName.size();
  for (int iChem=0; iChem<nbChem; iChem++) {
    grad_f[iChem]=0;
  }
  return true;
}

bool IPOPT_tnlp_chem::eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Number* g) {
  int nbChem=eDesc.ListName.size();
  int nbEqua=eDesc.ListEquation.size();
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    double eVal=0;
    for (int iChem=0; iChem<nbChem; iChem++) {
      int eValI=eDesc.ListEquation[iEqua][iChem];
      double eValD=eValI;
      eVal=eVal + eValD*x[iChem];
    }
    g[iEqua]=eVal;
  }
  std::vector<double> ListConc(nbChem);
  for (int iChem=0; iChem<nbChem; iChem++) {
    ListConc[iChem]=x[iChem];
  }
  std::vector<double> ListDefect=GetListReactionDefect(eDesc, ListConc);
  int nbReact=eDesc.ListChemicalReact.size();
  for (int iReact=0; iReact<nbReact; iReact++) {
    g[nbEqua+iReact]=ListDefect[iReact];
  }
  return true;    
}

bool IPOPT_tnlp_chem::eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
				 Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index* iRow, Ipopt::Index *jCol,
				 Ipopt::Number* values) {
  int nbChem=eDesc.ListName.size();
  int nbEqua=eDesc.ListEquation.size();
  int nbReact=eDesc.ListChemicalReact.size();
  int nnz=0;
  if (values == NULL) {
    for (int iEqua=0; iEqua<nbEqua; iEqua++) {
      for (int iChem=0; iChem<nbChem; iChem++) {
	int eValI=eDesc.ListEquation[iEqua][iChem];
	if (eValI != 0) {
	  iRow[nnz]=iEqua;
	  jCol[nnz]=iChem;
	  nnz++;
	}
      }
    }
    for (int iReact=0; iReact<nbReact; iReact++) {
      ChemicalReact<double> eReact=eDesc.ListChemicalReact[iReact];
      std::vector<int> LVal=eReact.ListVal;
      for (int iChem=0; iChem<nbChem; iChem++) {
	int eValI=LVal[iChem];
	if (eValI != 0 && eDesc.ListNature[iChem] == "dissolved") {
	  iRow[nnz]=iReact + nbEqua;
	  jCol[nnz]=iChem;
	  nnz++;
	}
      }
    }
  }
  else {
    for (int iEqua=0; iEqua<nbEqua; iEqua++) {
      for (int iChem=0; iChem<nbChem; iChem++) {
	int eValI=eDesc.ListEquation[iEqua][iChem];
	if (eValI != 0) {
	  double eValD=eValI;
	  values[nnz]=eValD;
	  nnz++;
	}
      }
    }
    for (int iReact=0; iReact<nbReact; iReact++) {
      ChemicalReact<double> eReact=eDesc.ListChemicalReact[iReact];
      for (int iChem=0; iChem<nbChem; iChem++) {
	double eMatEntry=0;
	int IsNonZero=0;
	if (eDesc.ListNature[iChem] == "dissolved") {
	  int eVal=eReact.ListVal[iChem];
	  if (eVal > 0) {
	    eMatEntry=eVal;
	    IsNonZero=1;
	    for (int jChem=0; jChem<nbChem; jChem++) {
	      int fVal=eReact.ListVal[jChem];
	      if (eDesc.ListNature[jChem] == "dissolved" && fVal > 0) {
		double eConc=x[jChem];
		if (iChem == jChem)
		  fVal=fVal-1;
		double ePow=ThePow(eConc, fVal);
		eMatEntry=eMatEntry*ePow;
	      }
	    }
	  }
	  if (eVal < 0) {
	    int kVal=-eVal;
	    eMatEntry=-kVal*eReact.K;
	    IsNonZero=1;
	    for (int jChem=0; jChem<nbChem; jChem++) {
	      int fVal=-eReact.ListVal[jChem];
	      if (eDesc.ListNature[jChem] == "dissolved" && fVal > 0) {
		double eConc=x[jChem];
		if (iChem == jChem)
		  fVal=fVal-1;
		double ePow=ThePow(eConc, fVal);
		eMatEntry=eMatEntry*ePow;
	      }
	    }
	  }
	}
	if (IsNonZero == 1) {
	  values[nnz]=eMatEntry;
	  nnz++;
	}
      }
    }
  }
  return true;
}

// This is the Hessian. We are not concerned here.
// since function is zero.
bool IPOPT_tnlp_chem::eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
			     Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number* lambda,
			     bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow,
			     Ipopt::Index* jCol, Ipopt::Number* values) {
  return true;
}

bool IPOPT_tnlp_chem::get_scaling_parameters(Ipopt::Number& obj_scaling,
					     bool& use_x_scaling, Ipopt::Index n,
					     Ipopt::Number* x_scaling,
					     bool& use_g_scaling, Ipopt::Index m,
					     Ipopt::Number* g_scaling) {
  int nbChem=eDesc.ListName.size();
  int nbEqua=eDesc.ListEquation.size();
  int nbReact=eDesc.ListChemicalReact.size();
  //
  use_x_scaling=true;
  double MinConc=1e-7;
  for (int iChem=0; iChem<nbChem; iChem++) {
    double eRelConc=StartConc[iChem];
    if (eRelConc < MinConc) {
      eRelConc=MinConc;
    }
    x_scaling[iChem]=1/eRelConc;
  }
  //
  use_g_scaling=true;
  bool UseSpecialScaling=false;
  for (int iEqua=0; iEqua<nbEqua; iEqua++) {
    g_scaling[iEqua]=1;
  }
  std::vector<double> ListListProduct=GetListReactionProducts(eDesc, StartConc);
  for (int iReact=0; iReact<nbReact; iReact++) {
    double eScal;
    if (UseSpecialScaling) {
      double TheProduct1=ListListProduct[2*iReact];
      double TheProduct2=ListListProduct[2*iReact+1];
      double eRelProduct=TheProduct1 + TheProduct2;
      eScal=1/eRelProduct;
    }
    else {
      eScal=1;
    }
    g_scaling[iReact + nbEqua]=eScal;
  }
  return true;
}

void IPOPT_tnlp_chem::finalize_solution(Ipopt::SolverReturn status,
					Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L, const Ipopt::Number* z_U,
					Ipopt::Index m, const Ipopt::Number* g, const Ipopt::Number* lambda,
					Ipopt::Number obj_valu,
					const Ipopt::IpoptData* ip_data,
					Ipopt::IpoptCalculatedQuantities* ip_cq) {
  std::cerr << "finalize_solution, step 1\n";
  int nbChem=eDesc.ListName.size();
  std::cerr << "finalize_solution, step 2\n";
  for (int iChem=0; iChem<nbChem; iChem++) {
    FinalSolution.push_back(x[iChem]);
  }
  std::cerr << "finalize_solution, step 3\n";
}

std::vector<double> IPOPT_tnlp_chem::GetFinal() const
{
  return FinalSolution;
}


std::vector<double> SolutionMethod_IPOPT(SoluDesc<double> const& eDesc, double const& TheTol, std::vector<double> const& ListInitConc, std::vector<double> const& StartConc)
{
  //  std::cerr << "SolutionMethod_IPOPT, step 1\n";
  std::vector<double> TheRet;
  //  std::cerr << "SolutionMethod_IPOPT, step 1\n";
  Ipopt::SmartPtr<IPOPT_tnlp_chem> mynlp = new IPOPT_tnlp_chem(eDesc, ListInitConc, StartConc);
  //  std::cerr << "SolutionMethod_IPOPT, step 2\n";
  Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();
  //  std::cerr << "SolutionMethod_IPOPT, step 3\n";
  app->RethrowNonIpoptException(true);
  //  std::cerr << "SolutionMethod_IPOPT, step 4\n";
  app->Options()->SetNumericValue("tol", 1e-15);
  //  std::cerr << "SolutionMethod_IPOPT, step 5\n";
  app->Options()->SetStringValue("mu_strategy", "adaptive");
  //  std::cerr << "SolutionMethod_IPOPT, step 6\n";
  app->Options()->SetStringValue("output_file", "ipopt.out");
  //  std::cerr << "SolutionMethod_IPOPT, step 7\n";
  app->Options()->SetStringValue("derivative_test", "first-order");
  //  std::cerr << "SolutionMethod_IPOPT, step 8\n";
  Ipopt::ApplicationReturnStatus status;
  //  std::cerr << "SolutionMethod_IPOPT, step 9\n";
  status = app->Initialize();
  //  std::cerr << "SolutionMethod_IPOPT, step 10\n";
  if (status != Ipopt::Solve_Succeeded) {
    std::vector<std::string> LStr{"Error during IPOPT initialization\n"};
    throw LStr;
  }
  //  std::cerr << "SolutionMethod_IPOPT, step 11\n";
  status = app->OptimizeTNLP(mynlp);
  //  std::cerr << "SolutionMethod_IPOPT, step 12\n";
  //  std::cerr << "After OptimizeTNLP\n";
  if (status != Ipopt::Solve_Succeeded) {
    std::vector<std::string> LStr{"Error during IPOPT solutioning\n"};
    throw LStr;
  }
  //  std::cerr << "After Status check\n";
  TheRet=mynlp->GetFinal();
  //  std::cerr << "After TheRET\n";
  return TheRet;
}

void PrintMultipleErrorMessageAndDie(std::vector<std::string> const& LVect)
{
  for (auto& eStr : LVect)
    std::cerr << eStr;
  throw TerminalException{1};
  //  exit(1);
}


template<typename T>
std::vector<T> SolveSolutionSystem(SoluDesc<T> const& eDesc, std::vector<T> const& ListInitConc, T const& TheTol)
{
  std::vector<T> StartConc=ListInitConc;
  T StartPenalty=PenaltyToSolution(eDesc, StartConc);
  PrintInitialState(eDesc, std::cerr);
  std::vector<T> NewConc;
  int nbIter=0;
  int IntervalPrint=1000;
  while(true) {
    nbIter++;
    std::cerr << "-------------------------------------------\n";
    std::cerr << "nbIter=" << nbIter << " TheTol=" << TheTol << " StartPenalty=" << StartPenalty << "\n";
    std::cout << "-------------------------------------------\n";
    std::cout << "nbIter=" << nbIter << " TheTol=" << TheTol << " StartPenalty=" << StartPenalty << "\n";
    // First strategy, move in all directions
    int OptChoice=1;
    NewConc=SolutionMethod_ArtificialKinetic(eDesc, StartConc, OptChoice);
    T NewPenalty=PenaltyToSolution(eDesc, NewConc);
    std::cerr << "NewPenaltyArtKin(AllDir)=" << NewPenalty << "\n";
    std::cout << "NewPenaltyArtKin(AllDir)=" << NewPenalty << "\n";
    // Second strategy, move in one direction
    OptChoice=2;
    std::vector<T> NewConcOD=SolutionMethod_ArtificialKinetic(eDesc, StartConc, OptChoice);
    T NewPenaltyOD=PenaltyToSolution(eDesc, NewConcOD);
    std::cerr << "NewPenaltyArtKin(OneDir)=" << NewPenaltyOD << "\n";
    std::cout << "NewPenaltyArtKin(OneDir)=" << NewPenaltyOD << "\n";
    if (NewPenaltyOD < NewPenalty) {
      NewConc=NewConcOD;
      NewPenalty=NewPenaltyOD;
      std::cerr << "CHOOSING ArtKin(OneDir)\n";
    }
    // Try the IPOPT
    if (nbIter > 1000) {
      std::vector<double> NewConcIP=SolutionMethod_IPOPT(eDesc, TheTol, ListInitConc, StartConc);
      T NewPenaltyIP=PenaltyToSolution(eDesc, NewConcIP);
      std::cerr << "NewPenaltyIPOPT=" << NewPenaltyIP << "\n";
      std::cout << "NewPenaltyIPOPT=" << NewPenaltyIP << "\n";
      if (NewPenaltyIP < NewPenalty) {
	NewConc=NewConcIP;
	NewPenalty=NewPenaltyIP;
	std::cerr << "CHOOSING IPOPT solution\n";
      }
    }
    // Try all possible reactions
    //    std::vector<T> NewConcTryAll=SolutionMethod_TryAllReact(eDesc, StartConc);
    //    T NewPenaltyTryAll=PenaltyToSolution(eDesc, NewConcTryAll);
    //    if (NewPenaltyOD < NewPenalty) {
    //      NewConc=NewConcTryAll;
    //      NewPenalty=NewPenaltyTryAll;
    //      std::cerr << "CHOOSING ArtKin(TryAll)\n";
    //    }
    // Steepest gradient attempt
    std::vector<T> NewConcSteepest=SteepestGradientDecrease(eDesc, ListInitConc, StartConc);
    T NewPenaltySteepest=PenaltyToSolution(eDesc, NewConcSteepest);
    std::cerr << "NewPenaltySteepest=" << NewPenaltySteepest << "\n";
    std::cout << "NewPenaltySteepest=" << NewPenaltySteepest << "\n";
    if (NewPenaltySteepest < NewPenalty) {
      NewConc=NewConcSteepest;
      NewPenalty=NewPenaltySteepest;
      std::cerr << "CHOOSING Steepest gradient\n";
    }
    // Newton attempt
    std::vector<T> ConcNewton;
    int IsCorrect=1;
    try {
      //      ConcNewton=SolutionMethod_NewtonIteration(eDesc, StartConc);
      ConcNewton=SolutionMethod_NewtonIteration(eDesc, ListInitConc, StartConc);
    }
    catch (Inverse_exception<T> & e) {
      std::cerr << e.errmsg << "\n";
      IsCorrect=0;
    }
    if (IsCorrect) {
      std::cerr << "Newton iteration run successfully\n";
      T NewPenaltyNewton=PenaltyToSolution(eDesc, ConcNewton);
      std::cerr << "NewPenaltyNewton=" << NewPenaltyNewton << "\n";
      if (NewPenaltyNewton < NewPenalty) {
	NewConc=ConcNewton;
	NewPenalty=NewPenaltyNewton;
	std::cerr << "CHOOSING Newton\n";
      }
    }
    // Newton using EIGEN
    std::vector<T> ConcNewtonEIGEN=SolutionMethod_NewtonIteration_EIGEN(eDesc, ListInitConc, StartConc);
    T NewPenaltyNewtonEIGEN=PenaltyToSolution(eDesc, ConcNewtonEIGEN);
    std::cerr << "NewPenaltyNewtonEIGEN=" << NewPenaltyNewtonEIGEN << "\n";
    std::cout << "NewPenaltyNewtonEIGEN=" << NewPenaltyNewtonEIGEN << "\n";
    if (NewPenaltyNewtonEIGEN < NewPenalty) {
      NewConc=ConcNewtonEIGEN;
      NewPenalty=NewPenaltyNewtonEIGEN;
      std::cerr << "CHOOSING Newton EIGEN\n";
    }


    // Wrapping it up
    std::cerr << "Chosen NewPENALTY=" << NewPenalty << "\n";
    if (NewPenalty > StartPenalty) {
      std::cerr << "End of the game for us. No further improvements\n";
      NewConc=StartConc;
      break;
    }
    if (NewPenalty < TheTol) {
      std::cerr << "Within tolerance\n";
      break;
    }
    int res=nbIter % IntervalPrint;
    if (res == 0) {
      PrintFinalState(eDesc, ListInitConc, NewConc, std::cerr);
    }
    StartConc=NewConc;
    StartPenalty=NewPenalty;
    //    AssignListReactionWeights(eDesc, StartConc);
  }
  PrintFinalState(eDesc, ListInitConc, NewConc, std::cerr);
  return NewConc;
}



#endif
