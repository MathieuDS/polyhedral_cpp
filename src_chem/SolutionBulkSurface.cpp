#include "Temp_SurfaceTension.h"
int main(int argc, char *argv[])
{
  if (argc != 4) {
    std::cerr << "Number of argument is = " << argc << "\n";
    std::cerr << "This program is used as\n";
    std::cerr << "SolutionBulkSurface [DATAMEAS] [INIT] [OUTPUT]\n";
    std::cerr << "DATAMEAS: The input data of the measurement\n";
    std::cerr << "    1st col Cnominal\n";
    std::cerr << "    2nd col Surface tension\n";
    std::cerr << "    3rd col Temperature\n";
    std::cerr << "    4th col AoverV\n";
    std::cerr << "    5th col gamma0\n";
    std::cerr << "       the surface)\n";
    std::cerr << "INIT: The initial values of GammaInf, k and h\n";
    std::cerr << "OUTPUT: The output of computed values\n";
    std::cerr << "    1st line GammaInfinity  \n";
    std::cerr << "    2nd line k (limiting adsorption coefficient)  \n";
    std::cerr << "    3rd line h (infinite dilution surface partial  \n";
    std::cerr << "       molar heat of mixing of the surfactant at\n";
    std::cerr << "       the surface)\n";
    std::cerr << "Then the full list of values\n";
    std::cerr << "n\n";
    return -1;
  }
  //
  std::cerr << "Reading input\n";
  std::ifstream MATR_MEAS;
  MATR_MEAS.open(argv[1]);
  MyMatrix<double> DATA_MEAS=ReadMatrix<double>(MATR_MEAS);
  MATR_MEAS.close();
  //
  double GammaInf_init, k_init, h_init;
  std::ifstream INIT;
  INIT.open(argv[2]);
  INIT >> GammaInf_init;
  INIT >> k_init;
  INIT >> h_init;
  INIT.close();
  std::cerr << "GammaInf_init=" << GammaInf_init << "\n";
  std::cerr << "k_init=" << k_init << "\n";
  std::cerr << "h_init=" << h_init << "\n";
  //
  MagicConstants *eCst;
  eCst=new MagicConstants;
  eCst->Rconstant=8.3144621;
  eCst->TheTol=1e-15;
  eCst->TerminationNormDiff=1e-10;
  eCst->GammaInf=GammaInf_init;
  eCst->k=k_init;
  eCst->h=h_init;
  //
  CreateAMPL_initFile(DATA_MEAS, eCst);
  //
  IterationToReduce(DATA_MEAS, eCst);
  //
  std::ofstream OUTPUT;
  OUTPUT.open(argv[1]);
  PrintSolution(OUTPUT, DATA_MEAS, eCst);
  OUTPUT.close();
  //
  delete eCst;
}
