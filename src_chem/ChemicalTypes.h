#ifndef CHEMICAL_TYPES_DEFINE
#define CHEMICAL_TYPES_DEFINE

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

// status can be "equality" or "solubility"
template<typename T>
struct ChemicalReact {
  std::vector<int> ListVal;
  T K;
  std::string status;
};

template<typename T>
struct SoluDesc {
  std::vector<std::string> ListName;
  std::vector<std::string> ListNature;
  std::vector<int> ListElectricalCharge;
  std::vector<ChemicalReact<T> > ListChemicalReact;
  std::vector<std::vector<int> > ListEquation;
  std::vector<T> ListWeight;
};

#endif
