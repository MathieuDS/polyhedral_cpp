extern crate syn;
extern crate quote;
extern crate proc_macro;
use std::str::FromStr;



fn add_vec(item_group: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let mut pos = 0;
    let mut list_double_dot = Vec::new();
    let l_item : Vec<proc_macro::TokenTree> = item_group.clone().into_iter().map(
        |x| {
            println!("pos={} x1={:?} x2={}", pos, x, x);
            match x.clone() {
                proc_macro::TokenTree::Punct(punct) => {
                    if punct.as_char() == ':' {
                        list_double_dot.push(pos);
                    }
                },
                _ => (),
            }
            pos += 1;
            x
        }).collect();
    let mut new_item_group = proc_macro::TokenStream::new();
    let n_sep = list_double_dot.len();
    for i_sep in 0..n_sep {
        let pos_double_dot = list_double_dot[i_sep];
        println!("pos_double_dot={:?}", pos_double_dot);
        let pos_first = pos_double_dot + 1;
        let pos_last = if i_sep == n_sep - 1 {
            l_item.len()
        } else {
            list_double_dot[i_sep+1] - 1
        } - 1;
        let title : proc_macro::TokenStream = l_item[pos_double_dot-1..pos_double_dot].to_vec().into_iter().collect();
        let expr : proc_macro::TokenStream = l_item[pos_first..pos_last].to_vec().into_iter().collect();
        let new_code = format!("{}: Vec<{}>,", title, expr);
        println!("new_code={}", new_code);
        let entry = proc_macro::TokenStream::from_str(&new_code).expect("Failure in construction of the Vec entries");
        new_item_group.extend(entry);
    }
    new_item_group
}




#[proc_macro_attribute]
pub fn derive_vec(_attr: proc_macro::TokenStream, item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    println!("derive_vec item={:?}", item);
    let mut idx = 0;
    let item_ret = item.into_iter().map(
        |x| {
            println!("x={:?} idx={}", x, idx);
            idx += 1;
            match x {
                proc_macro::TokenTree::Group(group) => {
                    let new_group = proc_macro::Group::new(group.delimiter(), add_vec(group.stream()));
                    return proc_macro::TokenTree::Group(new_group);
                },
                v => {
                    return v;
                },
            };
        }).collect();
    item_ret
}


