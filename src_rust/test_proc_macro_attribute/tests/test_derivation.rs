extern crate test_proc_macro_attribute;
use std::collections::BTreeMap;
use test_proc_macro_attribute::derive_vec;

#[derive(Default)]
#[derive_vec]
struct TestState {
    data1: u32,
    data2: Vec<u64>,
    data3: Vec<(u32,String)>,
    data4: BTreeMap<u32,String>,
}

#[test]
fn test_types() {
    let mut typ1 = TestState::default();
    typ1.data1.push(4);
    assert_eq!(typ1.data2.len(), 0);
    assert_eq!(typ1.data3.len(), 0);
    assert_eq!(typ1.data4.len(), 0);
}
