use std::collections::BTreeMap;
use std::ops::Bound::{Included, Unbounded};


fn main() {
    let mut map = BTreeMap::<usize, usize>::new();

    for i in 0..10 {
        map.insert(3 * i, i);
    }
    let mut after = map.range((Included(2), Unbounded));
    println!("val = {:?}", after.next());
}
