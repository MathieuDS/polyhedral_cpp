use std::collections::HashMap;
use serde::Deserialize;
use serde::Serialize;
//use tokio_tungstenite::connect_async;
//use futures_util::StreamExt;
//use futures_util::SinkExt;
//use std::error::Error;
use warp;
use warp::Filter;


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SingleFlight {
    pub departure: String,
    pub arrival: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CompleteFlight {
    pub list_flight: Vec<SingleFlight>,
}

#[derive(Debug)]
struct EndpointError {
}

impl EndpointError {
    fn new() -> EndpointError {
        EndpointError{}
    }
}



fn get_endpoints(input: CompleteFlight) -> Result<SingleFlight, EndpointError> {
    let mut map_departure_arrival = HashMap::new();
    let mut map_arrival_departure = HashMap::new();
    for flight in &input.list_flight {
        if map_departure_arrival.contains_key(&flight.departure) {
            return Err(EndpointError::new());
        }
        if map_arrival_departure.contains_key(&flight.arrival) {
            return Err(EndpointError::new());
        }
        map_departure_arrival.insert(flight.departure.clone(), flight.arrival.clone());
        map_arrival_departure.insert(flight.arrival.clone(), flight.departure.clone());
    }
    let mut n_flight : usize = 0;
    let mut final_airport = |airport: &String, the_map: HashMap<String,String>| -> String {
        let mut final_airport = airport.clone();
        while let Some(arr) = the_map.get(&final_airport) {
            final_airport = arr.to_string();
            n_flight += 1
        }
        final_airport
    };
    match input.list_flight.first() {
        None => Err(EndpointError::new()),
        Some(flight) => {
            let airport = &flight.departure;
            let airport_departure = final_airport(&airport, map_arrival_departure);
            let airport_arrival   = final_airport(&airport, map_departure_arrival);
            if input.list_flight.len() != n_flight {
                return Err(EndpointError::new());
            }
            let sing_flight = SingleFlight {departure: airport_departure, arrival: airport_arrival};
            Ok(sing_flight)
        }
    }
}




pub async fn endpoints_handler(body: CompleteFlight) -> Result<impl warp::Reply, warp::Rejection> {
//    let user_id = body.user_id;

    let res = get_endpoints(body);
    match res {
        Err(_) => Err(warp::reject()),
        Ok(res) => Ok(warp::reply::json(&res))
    }
}








#[tokio::main]
async fn main() {
    let endpoints = warp::path("endpoints");
    let endpoints_route = endpoints
        .and(warp::body::json())
        .and_then(endpoints_handler);

    let routes = endpoints_route;
    warp::serve(routes).run(([127, 0, 0, 1], 4741)).await;
}
