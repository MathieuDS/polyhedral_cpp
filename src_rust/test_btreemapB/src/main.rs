use std::collections::BTreeMap;
use rand::Rng;

fn get_rand_vec() -> Vec<u8> {
    let mut rng = rand::thread_rng();
    let len = rng.gen_range(0..5);
    let mut v = Vec::new();
    for _ in 0..len {
        let val = rng.gen_range(0..5) as u8;
        v.push(val);
    }
    v
}


fn main() {
    let mut map = BTreeMap::<Vec<u8>,u8>::new();
    for i in 0..100 {
        let v1 = get_rand_vec();
        map.insert(v1, i);
    }
    for (k,v) in &map {
        println!("k={:?} v={}", k, v);
    }
}
