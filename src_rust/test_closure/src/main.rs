

fn two_times<F>(f: F, x: i32) -> i32 where F: Fn(i32) -> i32 {
    f(f(x))
}


fn main() {
    let x = 4;
    let oper_a = move |y : i32| -> i32 {
        x + y
    };
    let oper_b = move |y : i32| -> i32 {
        42 + y
    };
    println!("oper_a(3)={}", oper_a(3));
    println!("two_time(oper_a, 3)={}", two_times(oper_a, 3));
    println!("two_time(oper_b, 3)={}", two_times(oper_b, 3));
}
