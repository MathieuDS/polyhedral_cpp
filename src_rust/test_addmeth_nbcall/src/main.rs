extern crate jsonrpc_core;
use std::sync::{Arc, Mutex};
use jsonrpc_core::*;
fn main() {
    let lk = Arc::new(Mutex::<i32>::new(0));
    let increment_nb_call = move || {
        let mut w : std::sync::MutexGuard<i32> = lk.lock().unwrap();
        *w += 1;
    };
    let increment_nb_call_1 = increment_nb_call.clone();
    let increment_nb_call_2 = increment_nb_call.clone();

    let mut io = IoHandler::new();
    io.add_method("method1", move |_: Params| {
        increment_nb_call_1();
        Ok(Value::String("method 1".into()))
    });
    io.add_method("method2", move |_: Params| {
        increment_nb_call_2();
        Ok(Value::String("method 2".into()))
    });
}
