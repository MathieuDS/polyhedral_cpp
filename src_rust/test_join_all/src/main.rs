extern crate tokio;

use futures::future::join_all;
//use tokio::fs;

#[tokio::main]
async fn main() {
    let mut handles = Vec::new();
    for i in 0..3 {
        let i_cp = i.clone();
        handles.push(async move {
            return i_cp;
        });
    }
    let result = join_all(handles).await;
    println!("result={:?}", result);
}
