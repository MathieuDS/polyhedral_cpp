#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
//extern crate serde_derive;



//use serde::{Deserialize, Serialize};
//use serde;
//use serde_derive;
use serde_json::Result;

#[derive(Debug, Serialize)]
struct Address {
    street: String,
    city: String,
}

fn print_an_address() -> Result<()> {
    // Some data structure.
    let address = Address {
        street: "10 Downing Street".to_owned(),
        city: "London".to_owned(),
    };

    // Serialize it to a JSON string.
    let j = serde_json::to_string(&address)?;

    // Print, write to a file, or send to an HTTP server.
    println!("{}", j);

    Ok(())
}

fn main() {
    print_an_address().unwrap();
}
