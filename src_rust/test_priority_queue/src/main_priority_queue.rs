use std::collections::HashMap;

pub trait PriorityQueue<Element> {
    /// create a new priority queue.
    fn new() -> Self;
    /// check whether the queue has no elements.
    fn is_empty(&self) -> bool;
    /// returns the highest-priority element but does not modify the queue.
    fn peek(&self) -> Option<Element>;
    /// add an element to the queue with an associated priority.
    fn insert(&mut self, element: Element, priority: u64);
    /// remove the element from the queue that has the highest priority, and return it.
    fn pop(&mut self) -> Option<Element>;
}

pub struct PriorityQueueImpl(HashMap<Vec<u8>, Vec<u8>>);

// Do not modify anything above ^^^

pub struct ShiftXorHasher {
    state: u64,
    level: bool,
}

impl std::hash::Hasher for ShiftXorHasher {
    fn write(&mut self, bytes: &[u8]) {
        if self.level == false {
            eprintln!("First part of the hash computation. Processing the length");
            self.level = true
        } else {
            eprintln!("Begin of hashes");
            let mut prod = 1;
            let len = bytes.len();
            eprintln!("    len={}", len);
            let mut pos = 0;
            for &byte in bytes {
                eprintln!("    byte={}", byte);
                self.state += prod * u64::from(byte);
                pos += 1;
                if pos < 8 {
                    prod *= 256
                }
            }
            eprintln!("  End of hashes");
        }
    }

    fn finish(&self) -> u64 {
        eprintln!("hasher cal  led: {:#010x} {}", self.state, self.state);
        self.state
    }
}

pub struct BuildShiftXorHasher;
impl std::hash::BuildHasher for BuildShiftXorHasher {
    type Hasher = ShiftXorHasher;
    fn build_hasher(&self) -> ShiftXorHasher {
        ShiftXorHasher { state: 0, level: false }
    }
}


//use std::convert::TryFrom;


fn u64_to_vec_u8(x: &u64) -> Vec<u8> {
    // transmute was considered but consideredd unsafe
    let mut x_work = x.clone();
//    x_work = &(x_work * x_work + 4);
    let mut vec = Vec::<u8>::new();
    for _i in 0..8 {
        let res : u8 = (x_work % 256) as u8;
        x_work /= 256;
        vec.push(res);
    }
    vec
}

fn vec_u8_to_u64(x: &Vec<u8>) -> u64 {
    let mut prod = 1;
    let mut val = 0;
    for i in 0..8 {
        val += prod * u64::from(x[i]);
        if i < 7 {
            prod *= 256
        }
    }
    val
}


use std::collections::hash_map::RandomState;

impl PriorityQueue<Vec<u8>> for PriorityQueueImpl {
    // TODO: finish the implementation

    fn new() -> Self {
        PriorityQueueImpl(std::collections::HashMap::<Vec<u8>,Vec<u8>,RandomState>::new())
    }

    fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    fn peek(&self) -> Option<Vec<u8>> {
        let mut is_first = true;
        let mut highest_priority = 0;
        let mut vec_ret = None;
        for (k, v) in &self.0 {
            let priority = vec_u8_to_u64(&k);
            if is_first {
                highest_priority = priority;
                vec_ret = Some(v.clone());
                is_first = false
            } else {
                if priority > highest_priority {
                    highest_priority = priority;
                    vec_ret = Some(v.clone());
                }
            }
        }
        vec_ret
    }

    fn insert(&mut self, element: Vec<u8>, priority: u64) {
        let vec_prio = u64_to_vec_u8(&priority);
        self.0.insert(vec_prio, element);
    }

    fn pop(&mut self) -> Option<Vec<u8>> {
        let mut highest_priority = 0;
        let mut best_pair = None;
        for (k, v) in &self.0 {
            let priority = vec_u8_to_u64(&k);
            match best_pair {
                None => {
                    highest_priority = priority;
                    best_pair = Some((k.clone(), v.clone()));
                },
                Some(_) => {
                    if priority > highest_priority {
                        highest_priority = priority;
                        best_pair = Some((k.clone(), v.clone()));
                    }
                },
            }
        }
        match best_pair {
            None => None,
            Some((k, v)) => {
                self.0.remove(&k);
                Some(v)
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut queue = PriorityQueueImpl::new();
        assert!(queue.is_empty());

        queue.insert(vec![0], 5);
        assert!(!queue.is_empty());
        assert_eq!(queue.peek(), Some(vec![0]));

        queue.insert(vec![1], 10);
        queue.insert(vec![2], 3);
        queue.insert(vec![3], 4);
        queue.insert(vec![4], 6);

        assert_eq!(queue.pop(), Some(vec![1]));
        assert_eq!(queue.pop(), Some(vec![4]));
        assert_eq!(queue.pop(), Some(vec![0]));
        assert_eq!(queue.pop(), Some(vec![3]));
        assert_eq!(queue.pop(), Some(vec![2]));

        assert!(queue.is_empty());
    }
}



fn main() {
}
