use serde::Deserialize;
use serde::Serialize;
use tokio_tungstenite::connect_async;
use futures_util::StreamExt;
use futures_util::SinkExt;
//use tokio_tungstenite::stream::Stream;
//use jsonrpc_core::{Error as JsonRpcError};
//use jsonrpc_core::*;
//use std::sync::{Arc, Mutex};
//use futures_util::stream::stream::StreamExt;
//use std::net::{SocketAddr};
//use jsonrpc_http_server::{ServerBuilder};


#[allow(non_snake_case)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ResultStruct {
    difficulty: String,
    extraData: String,
    gasLimit: String,
    gasUsed: String,
    logsBloom: String,
    miner: String,
    nonce: String,
    number: String,
    parentHash: String,
    receiptRoot: String,
    sha3Uncles: String,
    stateRoot: String,
    timestamp: String,
    transactionsRoot: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PairResultSubscription {
    pub result: ResultStruct,
    pub subscription: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ResultTrans {
    pub jsonrpc: String,
    pub method: String,
    pub params: PairResultSubscription,
}








#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NewHeads {
    pub jsonrpc: String,
    pub id: u64,
    pub method: String,
    pub params: Vec<String>,
}



#[tokio::main]
async fn main() {
    let url_name = "wss://eth-mainnet.alchemyapi.io/v2/XQDZ-hFcsUFlbHpFsb-NaFJzugCQK8Og";
    let (ws_stream, _) = connect_async(url_name).await.unwrap();
    let (mut write, mut read) = ws_stream.split();
    let new_head = NewHeads{jsonrpc:"2.0".to_string(), id: 1, method: "eth_subscribe".to_string(), params: vec!["newHeads".to_string()] };
    println!( "We have new_head");
    let handle = tokio::spawn(async move {
        println!( "Start message");
        let mut pos = 0;
        loop {
            println!("Passing pos={}", pos);
            pos +=1;
            let e_ent : Option<Result<tokio_tungstenite::tungstenite::Message, tokio_tungstenite::tungstenite::Error>> = read.next().await;
            println!("We have e_ent");
            match e_ent
            {
                None => {
                    println!("None case");
                },
                Some(msg) => {
                    println!( "Have some");
                    match msg
                    {
                        Err(e) =>
                        {
                            println!( "Error on receiving msg: {:?}", e );
                        }
                        Ok(m) => {
                            Arc<
                                HashMap::trans_id , data
                                vec!
                            Each thread handle a specific subset of hash.
                                query: from the hash, gets actually the NFT from the specified thread.
                                RocksDB
                                Each process its own database file (avoid contention).
                                If you change nr of process, restructure the data (want rarely).
                                Specify interval of data stored (last block).
                                When recovering, do the eth_subscribe, not from newHeads but from the last one stored.
                            map_thread[pos % n_proc]
                            println!( "Ok message");
                            let str = m.to_text().unwrap();
                            println!( "str = {}", str);
                        },
                    }
                },
            }
        }
    });
    let str : String = serde_json::to_string(&new_head).unwrap();
    println!("str={}", str);
    write.send(tokio_tungstenite::tungstenite::Message::Text(str)).await.unwrap_or_else(
        |e| { panic!("Error at write.send with e={}", e); }
    );
    handle.await.unwrap_or_else(
        |e| { panic!("Error at handle.await with e={}", e); }
    );

}
