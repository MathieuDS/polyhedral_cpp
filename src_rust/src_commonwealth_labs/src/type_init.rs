use std::fs::File;
use std::io::{self, prelude::*, BufReader};


pub fn read_entries(str_file: &String) -> Vec<String> {
    let file = File::open(str_file).expect("Failed to open file");
    let reader = BufReader::new(file);
    let mut list_line = Vec::<String>::new();
    for line in reader.lines() {
        match line {
            Ok(x) => {
                list_line.push(x);
            },
            Err(_) => {
            }
        }
    }
    list_line
}
