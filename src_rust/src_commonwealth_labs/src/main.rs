//extern crate jsonrpc_core;
use std::process;
//use std::error::Error;

mod type_init;


fn main() {
    println!("Beginning Merkle tree");

    let arguments: Vec<String> = std::env::args().collect();
    let nb_arg = arguments.len();
    if nb_arg != 3 {
        println!("nb_arg={} should be 3", nb_arg);
        println!("Exiting program. It is run as");
        println!("test_merkle   initial_list_of_entries   list_test");
        process::exit(1);
    }
    let str_file_initial_list = &arguments[1];
    let str_file_test_list = &arguments[2];
    println!("file_initial_list = {}     file_test_list = {}", str_file_initial_list, str_file_test_list);
    //
    let list_initial = type_init::read_entries(str_file_initial_list);
    let list_test    = type_init::read_entries(str_file_test_list);
    //
    

    
    println!("|list_initial|={} |list_test|={}", list_initial.len(), list_test.len());
}
