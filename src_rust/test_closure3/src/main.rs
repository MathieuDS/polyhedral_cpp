

fn two_times<F>(mut f: F, x: i32) -> () where F: FnMut(i32) -> () {
    // f(f(x)) does not compile
    f(x)
}


fn main() {
    let x = 4;
    let mut vec_return = Vec::<i32>::new();
    let oper_b = move |y : i32| -> () {
        let val = x + y;
        vec_return.push(val);
    };
    two_times(oper_b, 3);
    println!("two_time(oper_b, 3). vec_return={:?}", vec_return);
}
