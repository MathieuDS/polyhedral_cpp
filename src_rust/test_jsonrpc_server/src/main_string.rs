extern crate jsonrpc_core;
extern crate jsonrpc_http_server;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use jsonrpc_core::*;
use jsonrpc_http_server::{ServerBuilder};

#[derive(Serialize, Deserialize)]
struct TypeEch {
    eval: String,
}

fn main() {
    let fizz_buzz = |eval: String| -> String{
        let eval_i : i32 = eval.parse().unwrap();
        if eval_i == 3 { return "fizz".to_string();}
        if eval_i == 4 { return "4".to_string(); }
        if eval_i == 5 { return "buzz".to_string(); }
        return "unset".to_string();
    };
    //
    let mut io = IoHandler::new();
    io.add_method("fizz_buzz", move |params: Params| {
        match params.parse::<TypeEch>() {
            Ok(val) => Ok(Value::String(fizz_buzz(val.eval))),
            Err(e) => {
                println!("Error during parsing {:?}", e);
                Ok(Value::String("Something went wrong".into()))
            },
        }
    });
    //
    let my_hostname = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
    let socket = SocketAddr::new(my_hostname, 8906);
    //
    let server = ServerBuilder::new(io)
        .start_http(&socket)
        .expect("Server must start with no issues");
    server.wait()
}
