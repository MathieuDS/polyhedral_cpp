extern crate jsonrpc_core;
extern crate jsonrpc_http_server;

use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use jsonrpc_core::*;
use jsonrpc_http_server::{ServerBuilder};

fn main() {
    let fizz_buzz = |eval: u64| -> String{
        if eval == 3 { return "fizz".to_string();}
        if eval == 4 { return "4".to_string(); }
        if eval == 5 { return "buzz".to_string(); }
        return "unset".to_string();
    };
    //
    let mut io = IoHandler::new();
    io.add_method("fizz_buzz", move |params: Params| {
        match params.parse::<u64>() {
            Ok(val) => Ok(Value::String(fizz_buzz(val))),
            Err(e) => {
                println!("Error during parsing {:?}", e);
                Ok(Value::String("Something went wrong".into()))
            },
        }
    });
    //
    let my_hostname = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
    let socket = SocketAddr::new(my_hostname, 8906);
    //
    let server = ServerBuilder::new(io)
        .start_http(&socket)
        .expect("Server must start with no issues");
    server.wait()
}
