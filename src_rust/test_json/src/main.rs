use std::collections::HashMap;
use std::process;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

struct StatInfo {
    n_objects : usize,
    n_bytes : usize,
}



impl StatInfo {
    fn new() -> StatInfo {
        StatInfo { n_objects: 0, n_bytes: 0}
    }
    fn increase(&mut self, len: usize) -> () {
        self.n_objects += 1;
        self.n_bytes += len;
    }
}


fn process_line(line: String, map: &mut HashMap<String,StatInfo>) -> () {
    const BRACE_UP : char = '{';
    const BRACE_DOWN : char = '}';
    const BRACKET_UP : char = '[';
    const BRACKET_DOWN : char = ']';
    //
    const NEXT : char = '\\';
    const SEPARATOR : char = ':';
    const COMMA : char = ',';
    const QUOTATION : char = '\"';
    //
    let mut chars = line.chars();
    let mut insert_token = |str : String, len : usize| -> () {
        let stat = map.entry(str).or_insert(StatInfo::new());
        stat.increase(len);
    };
    struct State {
        pass : bool,
        is_in_string : bool,
        first_insert: bool,
        lev_brace : i32,
        lev_bracket : i32,
        has_token : bool,
        len : usize,
        token: String,
    }
    impl State {
        fn new() -> State {
            State { pass: false, is_in_string: false, first_insert: false, lev_brace: 0, lev_bracket: 0, has_token: false, len: 0, token: String::new() }
        }
    }
    let mut state = State::new();
    let string_state_update = |ch : char, _state: &mut State| -> () {
        if (*_state).is_in_string {
            (*_state).first_insert = true;
        }
        match ch {
            NEXT => {
                if !(*_state).pass {
                    (*_state).pass = true;
                }
            },
            QUOTATION => {
                if !(*_state).pass {
                    (*_state).is_in_string = !(*_state).is_in_string;
                }
                (*_state).pass = false;
            },
            _ => {
                (*_state).pass = false;
            }
        }
    };
    let nested_state_update = |ch : char, _state: &mut State| -> () {
        if !(*_state).is_in_string {
            match ch {
                BRACE_UP => (*_state).lev_brace += 1,
                BRACE_DOWN => (*_state).lev_brace -= 1,
                BRACKET_UP => (*_state).lev_bracket += 1,
                BRACKET_DOWN => (*_state).lev_bracket -= 1,
                _ => {}
            }
        }
    };
    let mut token_state_update = |ch: char, _state: &mut State| -> () {
        if (*_state).is_in_string && !(*_state).has_token && (*_state).first_insert && (*_state).lev_brace == 1 && (*_state).lev_bracket == 0 {
            (*_state).token.push(ch);
            ();
        }
        match ch {
            COMMA => {
                if (*_state).has_token && !(*_state).is_in_string && (*_state).lev_brace == 1 && (*_state).lev_bracket == 0 {
                    insert_token((*_state).token.clone(), (*_state).len);
                    (*_state).token.clear();
                    (*_state).len = 0;
                    (*_state).has_token = false;
                    (*_state).first_insert = false
                }
            },
            SEPARATOR => {
                if !(*_state).is_in_string && (*_state).lev_brace == 1 && (*_state).lev_bracket == 0 {
                    (*_state).has_token = true;
                }
            },
            x => {
                if (*_state).has_token {
                    (*_state).len += x.len_utf8()
                }
            }
        }

    };
    while let Some(ch) = chars.next() {
        string_state_update(ch, &mut state);
        nested_state_update(ch, &mut state);
        token_state_update(ch, &mut state);
    }
    insert_token(state.token, state.len);
    if state.lev_brace != 0 || state.lev_bracket != 0 {
        panic!("The json file has some inconsistencies in number of brackets and braces");
    }
}




fn print_statistics(map: &HashMap<String,StatInfo>) -> () {
    let mut max_len_token = 0;
    let mut max_len_n_objects = 0;
    let head_token = "token";
    let head_n_objects = "number objects";
    let head_n_bytes = "total byte size";
    for (token, token_feature) in map {
        let len_token = token.chars().count();
        if len_token > max_len_token {
            max_len_token = len_token;
        }
        let len_n_objects = token_feature.n_objects.to_string().chars().count();
        if len_n_objects > max_len_n_objects {
            max_len_n_objects = len_n_objects;
        }
    }
    fn prt_head(head: &str, max_len: usize) -> () {
        let len_head = head.chars().count();
        let n_sp = if len_head <= max_len { 1 + max_len - len_head } else {1};
        print!("{}{}", head, " ".repeat(n_sp));
    }
    prt_head(head_token, max_len_token);
    prt_head(head_n_objects, max_len_n_objects);
    println!("{}", head_n_bytes);
    fn prt_space(str_head: &str, str_ent: &str, max_len: usize) -> () {
        let len_head = str_head.chars().count();
        let len_ent = str_ent.chars().count();
        let n_sp = if len_head > max_len { 1 + len_head - len_ent } else { 1 + max_len - len_ent };
        print!("{}{}", str_ent, " ".repeat(n_sp));
    }
    for (token, token_feature) in map {
        prt_space(head_token, token, max_len_token);
        prt_space(head_n_objects, &token_feature.n_objects.to_string(), max_len_n_objects);
        println!("{}", token_feature.n_bytes);
    }
}




fn compute_statistic(file_name: &String) -> () {
    let mut map = HashMap::new();

    let file = File::open(file_name).unwrap_or_else(
        |error| { panic!("Error while opening file={} error={}", file_name, error); }
    );
    let reader = BufReader::new(file);

    for (index,line) in reader.lines().enumerate() {
        process_line(line.unwrap_or_else(
            |e| { panic!("Error while processing line number {} error={}", index, e);}
        ), &mut map);
    }
    print_statistics(&map);
}


fn main() {
    let arguments : Vec<String> = std::env::args().collect();
    if arguments.len() != 2 {
        println!("json_type_stat file.json");
        process::exit(1);
    }
    let file_name = &arguments[1];
    compute_statistic(file_name);
}
