use tokio;

#[derive(Clone)]
struct Cont {
    delta: usize,
}

impl Cont {
    fn get(&mut self) -> usize {
        self.delta
    }
}

#[tokio::main]
async fn main() {
    let delta = 3;
    let cont = Cont { delta };
    let mut handles = Vec::new();
    for i in 0..10 {
        let mut cont = cont.clone();
        handles.push(tokio::spawn(async move {
            cont.get() + i.clone()
        }));
    }
    let _response = futures::future::join_all(handles).await;
}
