#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use std::process;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

use serde_json::Result;

#[derive(Debug, Serialize, Deserialize)]
struct Address {
    pub street: String,
    pub ip_address: Vec<u8>,
    pub city: String,
}

fn main() {
    println!("begin of test_serialize2");
    let arguments: Vec<String> = std::env::args().collect();
    let nb_arg = arguments.len();
    println!("nb_arg={}", nb_arg);
    if nb_arg != 2 {
	println!("Exiting program. Need just one argument");
	process::exit(1);
    }
    let filename = &arguments[1];
    println!("filename = {}", filename);
    let file = File::open(filename).unwrap();
    println!("We have file");
    
    let reader = BufReader::new(file);
    println!("We have reader");
    
    let u : Address = serde_json::from_reader(reader).unwrap();
    println!("We have u");
    println!("street = {}    city = {}", u.street, u.city);
}

