use tokio;
use futures::stream::{self, StreamExt};

#[derive(Clone)]
struct Cont {
    delta: usize,
}

impl Cont {
    fn get(&mut self) -> usize {
        self.delta
    }
}

#[tokio::main]
async fn main() {
    let delta = 3;
    let cont = Cont { delta };
    let stream = stream::iter(0..10).then(|i| async move {
        cont.clone().get() + i.clone()
    });
    let _result : Vec<usize> = stream.collect().await;
}
