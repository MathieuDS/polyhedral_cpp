extern crate structopt;
extern crate html5ever;
extern crate url;


mod types;
mod parse;
mod fetch;
mod crawler;

use types::{Opt, print_list_link};
use structopt::StructOpt;
use crawler::extraction_links;



fn main() {
    let opt = Opt::from_args();
    println!("{:#?}", opt);
    //
    let start_url_string = opt.input.clone();
    let result_crawl = extraction_links(&start_url_string, &opt);
    println!("|correct links|={}", result_crawl.llink_corr.nbr);
    print_list_link(result_crawl.llink_corr);
    if opt.show_failures {
        println!("|bad links|={}", result_crawl.llink_bad.nbr);
        print_list_link(result_crawl.llink_bad);
        println!("|access failed links|={}", result_crawl.llink_acc.nbr);
        print_list_link(result_crawl.llink_acc);
        println!("|connection failed|={}", result_crawl.llink_connfail.nbr);
        print_list_link(result_crawl.llink_connfail);
        println!("|timed out links|={}", result_crawl.llink_timeout.nbr);
        print_list_link(result_crawl.llink_timeout);
        println!("|malformed links|={}", result_crawl.llink_malformed.nbr);
        print_list_link(result_crawl.llink_malformed);
    }
}


