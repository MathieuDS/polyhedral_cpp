use structopt::StructOpt;

#[derive(StructOpt, Debug, Clone)]
#[structopt(name = "web-crawler")]
pub struct Opt {
    // A flag, true if used in the command line. Note doc comment will
    // be used for the help message of the flag. The name of the
    // argument will be, by default, based on the name of the field.
    /// Activate debug mode
    #[structopt(short, long)]
    pub debug: bool,

    /// Print found links
    #[structopt(short, long)]
    pub print_links: bool,

    /// Show failed links for one reason or another
    #[structopt(short, long)]
    pub show_failures: bool,

    /// Maximum number of links to searh. 0 means no limit
    #[structopt(short, long, default_value = "0")]
    pub max_number_links: i32,

    /// Number of threads used
    #[structopt(short, long, default_value = "4")]
    pub nbthread: i32,

    /// timeout chosen
    #[structopt(short, long, default_value = "10")]
    pub timeout: u64,

    #[structopt(help = "Input link. Example http://en.wikipedia.org")]
    pub input: String,
}


#[derive(Default)]
pub struct ListLink {
    pub nbr: i32,
    pub llink: Vec<String>,
    pub keep_link: bool,
}

pub fn insert_link(llink: &mut ListLink, str: String) -> () {
    if llink.keep_link {
        llink.llink.push(str);
    }
    llink.nbr += 1;
    ()
}

pub fn constructor(keep_link: bool) -> ListLink {
    ListLink {nbr: 0, llink: Vec::<String>::new(), keep_link: keep_link}
}


pub fn print_list_link(llink: ListLink) -> () {
    if llink.keep_link {
        for elink in llink.llink {
            println!("  {}", elink);
        }
    }
}


#[derive(Default)]
pub struct ResultCrawl {
    pub llink_corr: ListLink,
    pub llink_bad: ListLink,
    pub llink_acc: ListLink,
    pub llink_connfail: ListLink,
    pub llink_timeout: ListLink,
    pub llink_malformed: ListLink,
}
