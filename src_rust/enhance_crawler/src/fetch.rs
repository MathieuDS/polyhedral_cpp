extern crate hyper;
extern crate url;

use std::io::Read;
use std::thread;
use std::time::Duration;
use std::sync::mpsc::channel;
use std::fmt;

use self::hyper::Client;
use self::hyper::status::StatusCode;
use self::url::{ParseResult, Url, UrlParser};
use types::Opt;
use parse;

#[derive(Debug, Clone)]
pub enum UrlState {
    Accessible(Url),
    BadStatus(Url, StatusCode),
    AccessFailed(Url),
    ConnectionFailed(Url),
    TimedOut(Url),
    Malformed(String),
}



impl fmt::Display for UrlState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            UrlState::Accessible(ref url) => format!("!! {}", url).fmt(f),
            UrlState::BadStatus(ref url, ref status) => format!("x {} ({})", url, status).fmt(f),
            UrlState::AccessFailed(ref url) => format!("!! {}", url).fmt(f),
            UrlState::ConnectionFailed(ref url) => format!("x {} (connection failed)", url).fmt(f),
            UrlState::TimedOut(ref url) => format!("x {} (timed out)", url).fmt(f),
            UrlState::Malformed(ref url) => format!("x {} (malformed)", url).fmt(f),
        }
    }
}

fn build_url(domain: &str, path: &str) -> ParseResult<Url> {
    let base_url_string = format!("http://{}", domain);
    let base_url = Url::parse(&base_url_string).unwrap();

    let mut raw_url_parser = UrlParser::new();
    let url_parser = raw_url_parser.base_url(&base_url);

    url_parser.parse(path)
}


fn extract_urls(domain: &str, res: &mut hyper::client::Response, url: Url, opt: &Opt) -> (UrlState, Vec<String>) {
    let mut body = String::new();
    match res.read_to_string(&mut body) {
        Ok(_) => {let dom = parse::parse_html(&body);
                  if url.domain() == Some(&domain) {
                      (UrlState::Accessible(url.clone()), parse::get_urls(&url, dom.document, opt))
                  }
                  else {
                      (UrlState::Accessible(url.clone()), vec![])
                  }
        },
        Err(_) => (UrlState::AccessFailed(url), vec![]),
    }
}


pub fn inspect_url(domain: &str, path: &str, opt: &Opt) -> (UrlState, Vec<String>) {
    match build_url(domain, path) {
        Ok(url) => {
            let (tx, rx) = channel();
            let req_tx = tx.clone();
            let u = url.clone();

            let domain = domain.to_owned();
            let timeout = opt.timeout.clone();
            let opt = opt.to_owned();
            thread::spawn(move || {
                let client = Client::new();
                let url_string = url.serialize();
                let resp = client.get(&url_string).send();
                let _ = req_tx.send(match resp {
                    Ok(mut r) => if let StatusCode::Ok = r.status {
                        extract_urls(&domain, &mut r, url, &opt)
                    } else {
                        (UrlState::BadStatus(url, r.status), vec![])
                    },
                    Err(_) => (UrlState::ConnectionFailed(url),vec![]),
                });
            });

            thread::spawn(move || {
                thread::sleep(Duration::from_secs(timeout));
                let _ = tx.send((UrlState::TimedOut(u), vec![]));
            });

            rx.recv().unwrap()
        }
        Err(_) => (UrlState::Malformed(path.to_owned()), vec![]),
    }
}



#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_download() {
        let opt = Opt{debug:false, print_links:false, show_failures:false, max_number_links:0, nbthread:1, timeout:10, input: "http://mathieudutour.altervista.org/Contact.html".to_string()};
        let domain = "mathieudutour.altervista.org";
        let path = "Contact.html";
        let (_, list_link) = inspect_url(domain, path, &opt);
        assert_eq!(list_link.len(), 4);
    }

}

