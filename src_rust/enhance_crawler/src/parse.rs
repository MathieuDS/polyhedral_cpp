extern crate url;

use std::string::String;

use html5ever::tendril::TendrilSink;
use html5ever::parse_document;
use html5ever::rcdom::{Handle, NodeData, RcDom};
use html5ever::interface::Attribute;
use types::Opt;
use self::url::{Url};


pub fn parse_html(source: &str) -> RcDom {
    parse_document(RcDom::default(), Default::default())
        .from_utf8()
        .read_from(&mut source.as_bytes())
        .unwrap()
}

fn get_full_url(str_url: &String, str_href: String) -> String {
//    println!("str_url={} str_href={}", str_url, str_href);
    if str_href.len() >= 7 {
        if str_href[0..7] == "http://".to_string() {
            return str_href;
        }
    }
    if str_href.len() >= 8 {
        if str_href[0..8] == "https://".to_string() {
            return str_href;
        }
    }
    let pos = str_url.rfind('/').unwrap();
    let str_pos : String = str_url[0..pos].to_string();
    let str_concat = str_pos + "/" + &str_href;
    str_concat
}

pub fn get_urls(url: &Url, handle: Handle, opt: &Opt) -> Vec<String> {
    let mut urls = vec![];
    let mut anchor_tags = vec![];
    let str_url : String = url.to_string();
//    println!("url = {}", url);
//    println!("str_full = {} pos = {}", str_full, pos);
    get_elements_by_name(handle, "a", &mut anchor_tags);

    for node in anchor_tags {
        if let NodeData::Element { ref attrs, .. } = node {
            for attr in attrs.borrow().iter() {
                let Attribute {
                    ref name,
                    ref value,
                } = *attr;
                if &*(name.local) == "href" {
                    let str_ins = get_full_url(&str_url, value.to_string());
                    if opt.debug {
                        println!("new url = {} str_ins = {}", value.to_string(), str_ins);
                    }
                    urls.push(str_ins);
                }
            }
        }
    }

    urls
}

fn get_elements_by_name(handle: Handle, element_name: &str, out: &mut Vec<NodeData>) {
    let node = handle;

    if let NodeData::Element {
        ref name,
        ref attrs,
        ref template_contents,
        ..
    } = node.data
    {
        if &*(name.local) == element_name {
            out.push(NodeData::Element {
                name: name.clone(),
                attrs: attrs.clone(),
                template_contents: template_contents.clone(),
                mathml_annotation_xml_integration_point: false,
            });
        }
    }

    for n in node.children.borrow().iter() {
        get_elements_by_name(n.clone(), element_name, out);
    }
}
