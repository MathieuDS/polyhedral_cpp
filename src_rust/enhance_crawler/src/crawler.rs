use std::collections::HashSet;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, Sender};
use std::thread;
use url::Url;
use fetch::{inspect_url, UrlState};

use types::{Opt, ResultCrawl, insert_link, constructor};

fn crawl_worker_thread(
    i_thread: &i32,
    domain: &str,
    to_visit: Arc<Mutex<Vec<String>>>,
    visited: Arc<Mutex<HashSet<String>>>,
    active_count: Arc<Mutex<i32>>,
    total_count: Arc<Mutex<i32>>,
    url_states: Sender<UrlState>,
    opt: &Opt,
) {
    loop {
        {
            let total_count_val = total_count.lock().unwrap();
            if opt.max_number_links > 0 && *total_count_val > opt.max_number_links {
                break;
            }
        }
        let current;
        {
            let mut to_visit_val = to_visit.lock().unwrap();
            let mut active_count_val = active_count.lock().unwrap();
            if to_visit_val.is_empty() {
                if *active_count_val > 0 {
                    continue;
                } else {
                    break;
                }
            };
            current = to_visit_val.pop().unwrap();
            *active_count_val += 1;
            assert!(*active_count_val <= opt.nbthread);
        }

        {
            let mut visited_val = visited.lock().unwrap();
            if visited_val.contains(&current) {
                let mut active_count_val = active_count.lock().unwrap();
                *active_count_val -= 1;
                continue;
            } else {
                visited_val.insert(current.to_owned());
            }
        }

        {
            let mut total_count_val = total_count.lock().unwrap();
            *total_count_val += 1;
        }
        if opt.debug {
            println!("i_thread={} current={}", i_thread, current);
        }
        let opt = opt.clone();
        let (url_state, list_link) = inspect_url(&domain, &current, &opt);
        {
            let mut to_visit_val = to_visit.lock().unwrap();
            for new_url in list_link {
                if true {
                    to_visit_val.push(new_url);
                }
            }
        }
        url_states.send(url_state).unwrap();

        {
            let mut active_count_val = active_count.lock().unwrap();
            *active_count_val -= 1;
            assert!(*active_count_val >= 0);
        }

    }
}

pub fn crawl<F>(start_url_string: &String, opt: &Opt, res: &mut ResultCrawl, mut f: F) -> () where F: FnMut(&mut ResultCrawl, &UrlState) -> () {
    let start_url = Url::parse(&start_url_string).unwrap();
    let domain = start_url
        .domain()
        .expect("I can't find a domain in your URL");
    //
    let to_visit = Arc::new(Mutex::new(vec![start_url.serialize()]));
    let active_count = Arc::new(Mutex::new(0));
    let total_count = Arc::new(Mutex::new(0));
    let visited = Arc::new(Mutex::new(HashSet::new()));

    let (tx, rx) = channel();

    for i_thread in 0..opt.nbthread {
        let domain = domain.to_owned();
        let to_visit = to_visit.clone();
        let visited = visited.clone();
        let active_count = active_count.clone();
        let total_count = total_count.clone();
        let opt = opt.clone();
        let tx = tx.clone();
        thread::spawn(move || {
            crawl_worker_thread(&i_thread, &domain, to_visit, visited, active_count, total_count, tx, &opt);
        });
    }

    let is_finished = move || -> bool {
        {
            let total_count_val = total_count.lock().unwrap();
            if opt.max_number_links > 0 && *total_count_val > opt.max_number_links {
                return true;
            }
        }
        let to_visit_val = to_visit.lock().unwrap();
        let active_count_val = active_count.lock().unwrap();
        return to_visit_val.is_empty() && *active_count_val == 0;
    };

    loop {
        if is_finished() {
            break;
        }
        match rx.recv() {
            Ok(x) => { f(res, &x); },
            _ => {break;},
        }
    }
    ()
}



pub fn extraction_links(start_url_string: &String, opt: &Opt) -> ResultCrawl {
    let insert_entry = move |res: &mut ResultCrawl, url_state: &UrlState| -> () {
	match url_state {
	    UrlState::Accessible(url) => insert_link(&mut res.llink_corr, url.to_string()),
	    UrlState::BadStatus(_, _) => insert_link(&mut res.llink_bad, url_state.to_string()),
	    UrlState::AccessFailed(url) => insert_link(&mut res.llink_acc, url.to_string()),
            UrlState::ConnectionFailed(url) => insert_link(&mut res.llink_connfail, url.to_string()),
            UrlState::TimedOut(url) => insert_link(&mut res.llink_timeout, url.to_string()),
            UrlState::Malformed(url) => insert_link(&mut res.llink_malformed, url.to_string()),
        }
        ()
    };
    let mut result_crawl = ResultCrawl{llink_corr: constructor(opt.print_links),
                                       llink_bad: constructor(opt.print_links && opt.show_failures),
                                       llink_acc: constructor(opt.print_links && opt.show_failures),
                                       llink_connfail: constructor(opt.print_links && opt.show_failures),
                                       llink_timeout: constructor(opt.print_links && opt.show_failures),
                                       llink_malformed: constructor(opt.print_links && opt.show_failures)};
    crawl(&start_url_string, &opt.clone(), &mut result_crawl, insert_entry);
    result_crawl
}



#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    fn get_nb_listlink(nbthread: i32) -> i32 {
        let opt = Opt{debug:false, print_links:false, show_failures:false, max_number_links:0, nbthread:nbthread, timeout:10, input: "http://mathieudutour.altervista.org/Publications/index.html".to_string()};
        let result_crawl  = extraction_links(&opt.input.clone(), &opt);
        result_crawl.llink_corr.nbr
    }
    
    #[test]
    fn test_thread_independence() {
        assert_eq!(get_nb_listlink(1), get_nb_listlink(4));
    }

}
