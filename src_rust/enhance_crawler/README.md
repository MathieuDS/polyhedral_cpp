# web-crawler

This is a multithreaded crawler designed for finding list of links in
a single domain.


DESIGN
------
 * We put a relatively fancy CLI interface, which allow some parameters
   to be changed. Use
       web-crawler --help
   to get the full interface.
 * Errors are fully processed and classified according to what happened.
 * By default errors are not shown but they can be presented if the user
   is interested.
 * A functionality of maximum number of links is present so as to prematurely
   terminate the enumeration if some problem happens on the way.


PARALLEL DESIGN
---------------
 * The tokio based framework hyper is used. It is based on a system of async/await
   for doing the operations. This means there is no active wait when retrieving
   data from server.
 * The number of threads is by default 4 but can be changed via command line
   option.
 * The number of threads is fixed at startup and wait for the list of available
   jobs to fill up.
   An alternative way would have been to use only async/await operations. In such
   a system when new url are found, some new promise are created.
 * We use a multiple producer, single consumer framework. Everytime a new
   link is found it is fed to a lambda function that processes it.
 * Timeouts are handled with two threads with the first ending winning.


TESTING
-------
 * Unit test are presented for the key functionality of downloading a page
   and extracting the links.
 * The integration test of downloading the links and checking how the number
   of threads influence the result is presented.


DOCKER
------
The docker of the application can be created via running:
docker build -f docker/Dockerfile .
As it is just provide the software to be used. There is no microservice
installed for the downloading


TODO / POSSIBLE IMPROVEMENTS
----------------------------
 * We could use the set_read_timeout for hyper in order to get neater code.
 * We could use async/await as parallel infrastructure.
 * Use atomics for counting. But mutex based solution works and runtime is
   limited by internet access.
 * Implement microservice access to the docker if the need arise.
