extern crate test_proc_macro_derive_core_code;
//extern crate test_proc_macro_derive_macro_code;
use test_proc_macro_derive_core_code::TestTrait;

#[derive(Default)]
struct TestState {
    data1: u32,
    data2: Vec<u64>,
    data3: Vec<(u32,String)>,
}

impl TestTrait for TestState {
    fn print(&self) {
        println!("specific implementation");
    }
}


#[test]
fn test_types() {
    let typ1 = TestState::default();
    typ1.print();
    println!("typ1 : data1={} |data2|={} |data3={}", typ1.data1, typ1.data2.len(), typ1.data3.len());
}
