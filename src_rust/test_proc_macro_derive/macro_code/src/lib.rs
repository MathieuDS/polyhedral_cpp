extern crate syn;
extern crate quote;
extern crate proc_macro;
use std::str::FromStr;


fn get_struct_name(input: proc_macro::TokenStream) -> Option<String> {
    let l_item : Vec<proc_macro::TokenTree> = input.into_iter().collect();
    match l_item.get(0) {
        None => return None,
        Some(item0) => {
            let item0 = format!("{}", item0);
            if item0 != "struct".to_string() {
                return None;
            }
        },
    }
    match l_item.get(1) {
        None => return None,
        Some(item1) => {
            let item1 = format!("{}", item1);
            return Some(item1);
        }
    }
}



#[proc_macro_derive(TestTrait)]
pub fn derive_testtrait(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    println!("derive_vec input={:?}", input);
    let mut idx = 0;
    for x in input.clone() {
        println!("x={:?} idx={}", x, idx);
        idx += 1;
    }
    let name = get_struct_name(input).expect("get_struct_name failed to get");
    let mut gen_code = format!("impl TestTrait for {}", name) + " {";
    gen_code += "  fn print(&self) {\n";
    gen_code += "    println!(\"Derive implementation of print used\");\n";
    gen_code += "  }\n";
    gen_code += "}\n";
    proc_macro::TokenStream::from_str(&gen_code).expect("Failure in constructing the Testtrait code")
}


