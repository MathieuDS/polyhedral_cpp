extern crate jsonrpc_core;

use std::sync::{Arc, Mutex};
use jsonrpc_core::*;

//use futures::prelude::*;
use futures::future;
//use futures::future::Map;


fn main() {
    let lk = Arc::new(Mutex::<i32>::new(0));
    let increment_nb_call = move || {
        let mut w : std::sync::MutexGuard<i32> = lk.lock().unwrap();
        *w += 1;
//        return Ok(Value::String("Correct running of the operation".into()));
        return Value::String("Correct running of the operation".into());
    };


    let mut io = IoHandler::new();
    io.add_method("method1", move |_: Params| {
        return future::ok(increment_nb_call());
    });
    /*
    io.add_method("method2", move |_: Params| {
        return future::ok(increment_nb_call());
    });
*/
}
