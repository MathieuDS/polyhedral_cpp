use std::collections::BTreeSet;



struct LowerBound<'a, T: 'static> {
    prec1: Option<T>,
    prec2: Option<T>,
    iter: std::collections::btree_set::Iter<'a, T>,
}

impl<'a, T> LowerBound<'a, T>
where
    T: PartialOrd + Clone,
{
    pub fn new(set: &'a BTreeSet<T>) -> Self {
        let mut iter = set.iter();
        let prec1 = None;
        let prec2 = iter.next().clone();
        Self { prec1, prec2: prec2.cloned(), iter }
    }

    pub fn get_lower_bound(&mut self, val: T) -> Option<T> {
        loop {
            match &self.prec2 {
                None => {
                    return self.prec1.as_ref().cloned();
                },
                Some(x) => {
                    if x.clone() > val {
                        return self.prec1.as_ref().cloned();
                    }
                }
            }
            self.prec1 = self.prec2.clone();
            self.prec2 = self.iter.next().cloned();
        }
    }
}


fn main() {
    let mut set = BTreeSet::<u8>::new();
    set.insert(4);
    set.insert(7);
    set.insert(8);
    set.insert(10);
    set.insert(24);
    set.insert(40);



    let mut lower_bound = LowerBound::<u8>::new(&set);
    let val = 15;
    println!("val={} lower_bound={:?}", val, lower_bound.get_lower_bound(val));
    let val = 17;
    println!("val={} lower_bound={:?}", val, lower_bound.get_lower_bound(val));
    let val = 25;
    println!("val={} lower_bound={:?}", val, lower_bound.get_lower_bound(val));
    let val = 27;
    println!("val={} lower_bound={:?}", val, lower_bound.get_lower_bound(val));
    let val = 42;
    println!("val={} lower_bound={:?}", val, lower_bound.get_lower_bound(val));
}
