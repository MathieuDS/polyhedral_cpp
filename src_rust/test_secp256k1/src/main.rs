extern crate secp256k1;

use secp256k1::{Secp256k1, Message};
use secp256k1::rand::OsRng;

fn main()
{
    println!("test_secp256k1, step 1");
    let secp = Secp256k1::new();
    println!("test_secp256k1, step 2");
    let mut rng = OsRng::new().expect("OsRng");
    println!("test_secp256k1, step 3");
    let (secret_key, public_key) = secp.generate_keypair(&mut rng);
    println!("test_secp256k1, step 4");
    let _secret_key_copy : secp256k1::key::SecretKey = secret_key.clone();
    println!("test_secp256k1, step 5");
    let _public_key_copy : secp256k1::key::PublicKey = public_key.clone();
    println!("test_secp256k1, step 6");
//    let estr = "abcdefghijklmnopkrstuvwxyz".to_string();
    let estr = "abcdefghijklmnopkrstuvwxyz123456".to_string();
    let str_u8 : &[u8] = estr.as_bytes();
    let len_u8 = str_u8.len();
    println!("len_u8={}", len_u8);
    let message = Message::from_slice(&str_u8).expect("Error in creation of message");
//    let message = Message::from_slice(&[0xab; 32]).expect("32 bytes");
    println!("test_secp256k1, step 7");
    //
    let sig : secp256k1::Signature = secp.sign(&message, &secret_key);
    println!("test_secp256k1, step 8");
    assert!(secp.verify(&message, &sig, &public_key).is_ok());
    println!("test_secp256k1, step 9");
}
