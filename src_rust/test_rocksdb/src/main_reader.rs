use std::process;


extern crate rocksdb;
use rocksdb::DB;

extern crate log;
use log::{info};
//use serde::{Deserialize, Serialize};
//use serde;
//use serde_derive;




fn get_database () -> DB {
    let file_database = "./database_rocksdb";
    let db_res = DB::open_default(file_database);
    match db_res {
        Ok(v) => v,
        Err(_) => {
            info!("get_database");
            process::exit(1);
        },
    }
}


fn main() {
    let db = get_database();

    let mut iter = db.raw_iterator();

    // Forwards iteration
    iter.seek_to_first();
    while iter.valid() {
        println!("Saw {:?} {:?}", iter.key(), iter.value());
        iter.next();
    }
    
}
