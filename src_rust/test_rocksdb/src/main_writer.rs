use std::process;


extern crate rocksdb;
use rocksdb::DB;

extern crate log;
use log::{info};
//use serde::{Deserialize, Serialize};
//use serde;
//use serde_derive;




fn get_database () -> DB {
    let file_database = "./database_rocksdb";
    let db_res = DB::open_default(file_database);
    match db_res {
        Ok(v) => v,
        Err(_) => {
            info!("get_database");
            process::exit(1);
        },
    }
}


fn main() {
    let db = get_database();
    //    db.put(b"my key", b"my value");
    let key_u8 : &[u8] = "my_key".as_bytes();
    let value_u8 : &[u8] = "my_value".as_bytes();
    db.put(key_u8, value_u8).unwrap();



    let n = 10;
    for x in 0..n {
        
        let x_str : &str= &x.to_string();
        let key_str : String = "my_key".to_owned();
        let value_str : String = "my value".to_owned();
        //
//        let key_strB = concat!(key_str, x_str); We have to use literals
//        let value_strB = concant!(value_str, x_str);
        let key_str_b = key_str + x_str;
        let value_str_b = value_str + x_str;
        let key_u8_b = key_str_b.as_bytes();
        let value_u8_b = value_str_b.as_bytes();
        db.put(key_u8_b, value_u8_b).unwrap();
    }

    
}
