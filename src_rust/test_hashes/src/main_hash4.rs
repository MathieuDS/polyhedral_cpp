pub struct ShiftXorHasher {
    state: u64,
    level: bool,
}

impl std::hash::Hasher for ShiftXorHasher {
    fn write(&mut self, bytes: &[u8]) {
        if self.level == false {
            eprintln!("First part of the hash computation. Processing the length");
            self.level = true
        } else {
            eprintln!("Begin of hashes");
            let mut prod = 1;
            let len = bytes.len();
            eprintln!("    len={}", len);
            let mut pos = 0;
            for &byte in bytes {
                eprintln!("    byte={}", byte);
                self.state += prod * u64::from(byte);
                pos += 1;
                if pos < 8 {
                    prod *= 256
                }
            }
            eprintln!("  End of hashes");
        }
    }

    fn finish(&self) -> u64 {
        eprintln!("hasher cal  led: {:#010x} {}", self.state, self.state);
        self.state
    }
}

pub struct BuildShiftXorHasher;





impl std::hash::BuildHasher for BuildShiftXorHasher {
    type Hasher = ShiftXorHasher;
    fn build_hasher(&self) -> ShiftXorHasher {
        ShiftXorHasher { state: 0, level: false }
    }
}


//use std::hash::BuildHasherDefault;
use std::collections::HashMap;
//use twox_hash::XxHash;
use std::collections::hash_map::RandomState;

fn main() {
//    let mut hm_1: HashMap<_, _, BuildHasherDefault<XxHash>> = Default::default();
    let mut hm = std::collections::HashMap::with_hasher(BuildShiftXorHasher);
    let v1 : Vec<u8> = vec![8,13];
    let v2 : Vec<u8> = vec![8,4];
    let v3 : Vec<u8> = vec![8,4,5];
    let v4 : Vec<u8> = vec![8,4,12,23,2,4,5,6];
    eprintln!("main , step 0");
    eprintln!("      |v|={}", v1.len());
    hm.insert(v1, 4);
    //
    eprintln!("main , step 1");
    eprintln!("      |v|={}", v2.len());
    hm.insert(v2, 4);
    //
    eprintln!("main , step 2");
    eprintln!("      |v|={}", v3.len());
    hm.insert(v3, 6);
    //
    eprintln!("main , step 3");
    eprintln!("      |v|={}", v4.len());
    hm.insert(v4.clone(), 8);
    hm.remove(&v4);
    //
    for (k, v) in hm {
        eprintln!("k={:?} v={:?}", k, v);
    }
    eprintln!("main , step 4");
}
