use std::collections::BTreeSet;
use rand::Rng;

fn get_rand_vec() -> Vec<u8> {
    let mut rng = rand::thread_rng();
    let mut v = Vec::new();
    for _ in 0..10 {
        let val = rng.gen_range(0..255) as u8;
        v.push(val);
    }
    v
}


fn main() {
    let mut map1 = BTreeSet::<Vec<u8>>::new();
    let len1 = 10;
    let len2 = 10;
    for _ in 0..len1 {
        let v = get_rand_vec();
        map1.insert(v);
    }
    let mut map2 = BTreeSet::<Vec<u8>>::new();
    for _ in 0..len2 {
        let v = get_rand_vec();
        map2.insert(v);
    }

    let mut v = Vec::<Vec<u8>>::new();
    let mut iter1 = map1.iter();
    let mut iter2 = map2.iter();
    let mut opt1 = iter1.next().cloned();
    let mut opt2 = iter2.next().cloned();
    loop {
        match &opt1 {
            Some(key1) => {
                match &opt2 {
                    Some(key2) => {
                        if key1 < key2 {
                            v.push(key1.clone());
                            opt1 = iter1.next().cloned();
                        } else {
                            v.push(key2.clone());
                            opt2 = iter2.next().cloned();
                        }
                    },
                    None => {
                        v.push(key1.clone());
                        opt1 = iter1.next().cloned();
                    },
                }
            },
            None => {
                loop {
                    match opt2 {
                        Some(key2) => {
                            v.push(key2.clone());
                            opt2 = iter2.next().cloned();
                        },
                        None => {
                            break;
                        },
                    }
                }
                break;
            },
        }
    }
    for i in 0..v.len() {
        let v = v.get(i).unwrap();
        println!("i={} v={:?}", i, v);
    }
    for i in 1..v.len() {
        let v1 = v.get(i-1).unwrap();
        let v2 = v.get(i).unwrap();
        assert!(v1 < v2);
    }
}
