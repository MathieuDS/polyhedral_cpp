extern crate curv;
extern crate multi_party_schnorr;
//extern crate protocols;

use curv::cryptographic_primitives::hashing::merkle_tree::MT256;
use curv::elliptic::curves::traits::ECScalar;
use curv::FE;
use multi_party_schnorr::protocols::multisig::{partial_sign, verify, EphKey, Keys, Signature};


fn main() {

//    let message: [u8; 12] = [79, 77, 69, 82, 79, 77, 69, 82, 79, 77, 69, 82];
    let message: [u8; 4] = [79, 77, 69, 82];
    println!("main, step 1");

    // party1 key gen:
    let mut keys_1 = Keys::create();
    println!("main, step 2");
    
    keys_1.I.update_key_pair(FE::zero());
    println!("main, step 3");
    
    let broadcast1 = Keys::broadcast(keys_1.clone());
    println!("main, step 4");
    // party2 key gen:
    let keys_2 = Keys::create();
    println!("main, step 5");
    let broadcast2 = Keys::broadcast(keys_2.clone());
    println!("main, step 6");
    let ix_vec = vec![broadcast1, broadcast2];
    println!("main, step 7");
    let e = Keys::collect_and_compute_challenge(&ix_vec);
    println!("main, step 8");
    
    let y1 = partial_sign(&keys_1, e.clone());
    println!("main, step 9");
    let y2 = partial_sign(&keys_2, e.clone());
    println!("main, step 10");
    let sig1 = Signature::set_signature(&keys_1.X.public_key, &y1);
    println!("main, step 11");
    let sig2 = Signature::set_signature(&keys_2.X.public_key, &y2);
    println!("main, step 12");
    // partial verify
    assert!(verify(&keys_1.I.public_key, &sig1, &e).is_ok());
    println!("main, step 13");
    assert!(verify(&keys_2.I.public_key, &sig2, &e).is_ok());
    println!("main, step 14");
    
    // merkle tree (in case needed)
    
    let ge_vec = vec![(keys_1.I.public_key).clone(), (keys_2.I.public_key).clone()];
    println!("main, step 15");
    let mt256 = MT256::create_tree(&ge_vec);
    println!("main, step 16");
    let proof1 = mt256.gen_proof_for_ge(&keys_1.I.public_key);
    println!("main, step 17");
    let proof2 = mt256.gen_proof_for_ge(&keys_2.I.public_key);
    println!("main, step 18");
    let root = mt256.get_root();
    println!("main, step 19");
    
    //TODO: reduce number of clones.
    // signing
    let party1_com = EphKey::gen_commit();
    println!("main, step 20");
    
    let party2_com = EphKey::gen_commit();
    println!("main, step 21");
    
    let eph_pub_key_vec = vec![
        party1_com.eph_key_pair.public_key.clone(),
        party2_com.eph_key_pair.public_key.clone(),
    ];
    println!("main, step 22");
    let pub_key_vec = vec![keys_1.I.public_key.clone(), keys_2.I.public_key.clone()];
    println!("main, step 23");
    
    let (it, xt, es) = EphKey::compute_joint_comm_e(pub_key_vec, eph_pub_key_vec, &message);
    println!("main, step 24");
    
    let y1 = party1_com.partial_sign(&keys_1.I, es.clone());
    println!("main, step 25");
    let y2 = party2_com.partial_sign(&keys_2.I, es.clone());
    println!("main, step 26");
    let y = EphKey::add_signature_parts(vec![y1, y2]);
    println!("main, step 27");
    let sig = Signature::set_signature(&xt, &y);
    println!("main, step 28");
    assert!(verify(&it, &sig, &es).is_ok());
    println!("main, step 29");
    
    assert!(MT256::validate_proof(&proof1, root).is_ok());
    println!("main, step 30");
    assert!(MT256::validate_proof(&proof2, root).is_ok());
    println!("main, step 31");
}
