use std::ops::{Add, Sub};


pub struct DataCont {
    a: u64,
    b: u64,
}

pub struct DataIE<E: Add + Sub + Copy> {
    a: E,
    b: E,
}


impl<E: Add + Sub + Sub<Output = E> + Copy> DataIE<E> {
    fn diff(&self) -> E {
        self.a - self.b
    }
}


#[macro_export]
macro_rules! impl_functA {
    ($a:ty) => {
        impl $a {
            fn plus(&self) -> u64 {
                self.a + self.b
            }
        }
    }
}

#[macro_export]
macro_rules! impl_functB {
    ($a:ident) => {
        impl<E: Add + Add<Output = E> + Copy + Sub> $a<E> {
            fn plus(&self) -> E {
                self.a + self.b
            }
        }
    }
}

impl_functA!{DataCont}
impl_functB!{DataIE}


fn main() {
    let d = DataCont { a: 4, b: 3 };
    let _val = d.plus();
}
