trait Printing<C> {
//    type TagPrint;
    fn write_batch(c: &C, l_ent: Vec<String>);
}

trait Prefix {
    fn get_prefix(&self) -> String;
}


pub struct PrefixPrint<A>(std::marker::PhantomData<A>);

pub struct DirectPrint<A>(std::marker::PhantomData<A>);

struct DataA {
    prefixA: String,
}

impl Prefix for DataA {
    fn get_prefix(&self) -> String {
        self.prefixA.clone()
    }
}




impl<C> Printing<C> for PrefixPrint<C>
where
    C: Prefix
{
    fn write_batch(c: &C, l_ent: Vec<String>) {
        let prefix = c.get_prefix();
        for e_ent in l_ent {
            println!("value={}{}", prefix, e_ent);
        }
    }
}




fn main() {

}
